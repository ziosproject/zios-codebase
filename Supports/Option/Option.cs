﻿using System.Collections.Generic;
using System.Linq;
namespace Zios.Supports.Option{
	using Zios.Extensions;
	using Zios.Supports.Hierarchy;
	public class Option : Option<string>{}
	public class Option<KeyType>{
		public static implicit operator Option<KeyType>(KeyType value){return new Option<KeyType>().Set(value);}
		public static implicit operator Option<KeyType>(int value){return new Option<KeyType>().Set(value);}
		public static Hierarchy<string,KeyType,int> sets = new Hierarchy<string,KeyType,int>();
		public Dictionary<KeyType,int> lookup = new Dictionary<KeyType,int>();
		public int value;
		//================
		// Setup
		//================
		public void Options(params KeyType[] keys){
			this.Build(null,keys);
		}
		public void Options(Dictionary<KeyType,int> data){
			var keys = data.Keys.ToArray();
			var values = data.Values.ToArray();
			this.Build(null,keys,values);
		}
		public void Build(string name=null,KeyType[] keys=null,int[] values=null){
			name = name ?? this.GetType().Name;
			var existing = Option<KeyType>.sets.TryGet(name);
			if(existing.IsNull()){
				keys = keys ?? new KeyType[0];
				existing =  Option<KeyType>.sets.AddNew(name);
				values = Enumerable.Range(0,keys.Length).Select(x=>1<<x).ToArray();
				for(var index=0;index<keys.Length;++index){
					var key = keys[index];
					var value = values[index];
					existing[key] = value;
				}
			}
			this.lookup = existing;
		}
		//================
		// Instance
		//================
		public KeyType GetTerm(int value){
			return this.lookup.First(x=>x.Value==value).Key;
		}
		public int GetValue(KeyType value){
			return this.lookup.TryGet(value);
		}
		public bool HasAny(params KeyType[] values){
			foreach(var term in values){
				var exists = (this.value & this.GetValue(term)) != 0;
				if(exists){return true;}
			}
			return false;
		}
		public bool Has(params KeyType[] values){
			foreach(var term in values){
				var exists = (this.value & this.GetValue(term)) != 0;
				if(!exists){return false;}
			}
			return true;
		}
		public Option<KeyType> Set(params KeyType[] values){
			foreach(var term in values){
				this.value |= this.GetValue(term);
			}
			return this;
		}
		public Option<KeyType> Unset(params KeyType[] values){
			foreach(var term in values){
				this.value &= ~this.GetValue(term);
			}
			return this;
		}
		public Option<KeyType> Set(params int[] values){
			var terms = values.Select(x=>this.GetTerm(x)).ToArray();
			return this.Set(terms);
		}
		public Option<KeyType> Unset(params int[] values){
			var terms = values.Select(x=>this.GetTerm(x)).ToArray();
			return this.Set(terms);
		}
		public Option<KeyType> Add(params KeyType[] values){return this.Set(values);}
		public Option<KeyType> Remove(params KeyType[] values){return this.Unset(values);}
		public Option<KeyType> Add(params int[] values){return this.Set(values);}
		public Option<KeyType> Remove(params int[] values){return this.Unset(values);}
	}
}
