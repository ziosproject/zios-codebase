using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
namespace Zios.Supports.Invoker{
	public delegate object Invoker(object target,object[] arguments);
	public static class InvokerManager{
		public static Delegate GetDelegate(this MethodInfo methodInfo,object scope){
			Func<Type[],Type> getType;
			var isAction = methodInfo.ReturnType.Equals((typeof(void)));
			var parameterTypes = methodInfo.GetParameters().Select(x=>x.ParameterType);
			if(isAction){
				getType = Expression.GetActionType;
			}
			else{
				getType = Expression.GetFuncType;
				parameterTypes = parameterTypes.Concat(new[]{methodInfo.ReturnType});
			}
			if(methodInfo.IsStatic){
				return Delegate.CreateDelegate(getType(parameterTypes.ToArray()),methodInfo);
			}
			return Delegate.CreateDelegate(getType(parameterTypes.ToArray()),scope,methodInfo.Name);
		}
		public static Invoker GetExpressionInvoker(this Delegate current){
			return current.Method.GetExpressionInvoker();
		}
		public static Invoker GetExpressionInvoker(this MethodInfo info){
			var instance = Expression.Parameter(typeof(object),"target");
			var arguments = Expression.Parameter(typeof(object[]),"arguments");
			var argumentExpressions = info.GetArgumentExpressions(arguments);
			var target = Expression.Convert(instance,info.DeclaringType);
			var call = info.IsStatic ? Expression.Call(info,argumentExpressions) : Expression.Call(target,info,argumentExpressions);
			if(info.ReturnType == typeof(void)){
				var action = Expression.Lambda<Action<object,object[]>>(call,instance,arguments).Compile();
				return (x,y)=>{
					action(x,y);
					return null;
				};
			}
			var expression = Expression.Lambda<Invoker>(Expression.Convert(call,typeof(object)),instance,arguments);
			return expression.Compile();
		}
		private static Expression[] GetArgumentExpressions(this MethodInfo method,Expression arguments){
			return method.GetParameters().Select((parameter,index)=>Expression.Convert(Expression.ArrayIndex(arguments,Expression.Constant(index)),parameter.ParameterType)).ToArray();
		}
		public static Invoker GetInvoker(this Delegate current){
			if(current.Target==null){return current.Method.GetExpressionInvoker();}
			return current.Method.GetInvoker();
		}
		public static Invoker GetInvoker(this MethodInfo info){
			var dynamicMethod = new DynamicMethod(string.Empty,typeof(object),new Type[]{typeof(object),typeof(object[])},info.DeclaringType.Module,true);
			var generator = dynamicMethod.GetILGenerator();
			var parameterTypes = info.GetParameters().Select(x=>x.ParameterType).ToArray();
			var locals = new LocalBuilder[parameterTypes.Length];
			for(var index=0;index<parameterTypes.Length;index++){
				locals[index] = generator.DeclareLocal(parameterTypes[index]);
			}
			for(var index=0;index<parameterTypes.Length;index++){
				generator.Emit(OpCodes.Ldarg_1);
				InvokerManager.EmitInt(generator,index);
				generator.Emit(OpCodes.Ldelem_Ref);
				InvokerManager.EmitReference(generator,parameterTypes[index]);
				generator.Emit(OpCodes.Stloc,locals[index]);
			}
			generator.Emit(OpCodes.Ldarg_0);
			for(var index=0;index<parameterTypes.Length;index++){
				generator.Emit(OpCodes.Ldloc,locals[index]);
			}
			generator.EmitCall(OpCodes.Call,info,null);
			if(info.ReturnType == typeof(void)){
				generator.Emit(OpCodes.Ldnull);
			}
			else{
				InvokerManager.EmitBox(generator,info.ReturnType);
			}
			generator.Emit(OpCodes.Ret);
			return (Invoker)dynamicMethod.CreateDelegate(typeof(Invoker));
		}
		public static Invoker GetAdvancedInvoker(this MethodInfo info){
			var dynamicMethod = new DynamicMethod(string.Empty,typeof(object),new Type[]{typeof(object),typeof(object[])},info.DeclaringType.Module,true);
			var generator = dynamicMethod.GetILGenerator();
			var parameters = info.GetParameters();
			var parameterTypes = parameters.Select(x=>x.ParameterType).Select(x=>x.IsByRef?x.GetElementType():x).ToArray();
			var locals = new LocalBuilder[parameterTypes.Length];
			for(var index=0;index<parameterTypes.Length;index++){
				locals[index] = generator.DeclareLocal(parameterTypes[index],true);
			}
			for(var index=0;index<parameterTypes.Length;index++){
				generator.Emit(OpCodes.Ldarg_1);
				InvokerManager.EmitInt(generator,index);
				generator.Emit(OpCodes.Ldelem_Ref);
				InvokerManager.EmitReference(generator,parameterTypes[index]);
				generator.Emit(OpCodes.Stloc,locals[index]);
			}
			generator.Emit(OpCodes.Ldarg_0);
			for(var index=0;index<parameterTypes.Length;index++){
				if(parameters[index].ParameterType.IsByRef)
					generator.Emit(OpCodes.Ldloca_S,locals[index]);
				else{
					generator.Emit(OpCodes.Ldloc,locals[index]);
				}
			}
			generator.EmitCall(OpCodes.Callvirt,info,null);
			if(info.ReturnType == typeof(void)){
				generator.Emit(OpCodes.Ldnull);
			}
			else{
				InvokerManager.EmitBox(generator,info.ReturnType);
			}
			for(var index=0;index<parameterTypes.Length;index++){
				if(parameters[index].ParameterType.IsByRef){
					generator.Emit(OpCodes.Ldarg_1);
					InvokerManager.EmitInt(generator,index);
					generator.Emit(OpCodes.Ldloc,locals[index]);
					if(locals[index].LocalType.IsValueType){
						generator.Emit(OpCodes.Box,locals[index].LocalType);
					}
					generator.Emit(OpCodes.Stelem_Ref);
				}
			}
			generator.Emit(OpCodes.Ret);
			return (Invoker)dynamicMethod.CreateDelegate(typeof(Invoker));
		}
		public static void EmitInt(ILGenerator generator,int value){
			if(value == -1){generator.Emit(OpCodes.Ldc_I4_M1);}
			else if(value == 0){generator.Emit(OpCodes.Ldc_I4_0);}
			else if(value == 1){generator.Emit(OpCodes.Ldc_I4_1);}
			else if(value == 2){generator.Emit(OpCodes.Ldc_I4_2);}
			else if(value == 3){generator.Emit(OpCodes.Ldc_I4_3);}
			else if(value == 4){generator.Emit(OpCodes.Ldc_I4_4);}
			else if(value == 5){generator.Emit(OpCodes.Ldc_I4_5);}
			else if(value == 6){generator.Emit(OpCodes.Ldc_I4_6);}
			else if(value == 7){generator.Emit(OpCodes.Ldc_I4_7);}
			else if(value == 8){generator.Emit(OpCodes.Ldc_I4_8);}
			if(value <= 8){return;}
			if(value > -129 && value < 128){
				generator.Emit(OpCodes.Ldc_I4_S,(SByte)value);
				return;
			}
			generator.Emit(OpCodes.Ldc_I4,value);
		}
		public static void EmitBox(ILGenerator generator,Type type){
			if(type.IsValueType){
				generator.Emit(OpCodes.Box,type);
			}
		}
		public static void EmitReference(ILGenerator generator,Type type){
			if(type.IsValueType){
				generator.Emit(OpCodes.Unbox_Any,type);
			}
			else{
				generator.Emit(OpCodes.Castclass,type);
			}
		}
	}
}