﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
namespace Zios.Supports.Hook{
	using Zios.Extensions;
	using Zios.Log;
	using Zios.Supports.Hierarchy;
	using Zios.Supports.Invoker;
	public class Hook{
		//=======================
		// STATIC
		//=======================
		public static Hierarchy<object,string,Hook> all = new Hierarchy<object,string,Hook>();
		public static void Remove(object target){Hook.all.Remove(target);}
		public static Hook Create(string name){return Hook.Create("Global",name);}
		public static Hook Create(object target,string name){
			var hook = Hook.all.AddNew(target).TryGet(name);
			if(hook.IsNull()){
				hook = Hook.all[target][name] = new Hook();
				hook.name = name;
			}
			return hook;
		}
		public static Hook Get(string name){return Hook.Create("Global",name);}
		public static Hook Get(object target,string name){return Hook.Create(target,name);}
		public static void Call(string name,params object[] terms){Hook.CallTarget("Global",name,terms);}
		public static void CallTarget(object target,string name,params object[] terms){Hook.Get(target,name).Call(terms);}
		public static ReturnType Call<ReturnType>(string name,params object[] terms){return Hook.CallTarget<ReturnType>("Global",name,terms);}
		public static ReturnType CallTarget<ReturnType>(object target,string name,params object[] terms){return Hook.Get(target,name).Call<ReturnType>(terms);}
		//=======================
		// INSTANCE
		//=======================
		public bool disabled;
		public string name;
		public List<HookMethod> methods = new List<HookMethod>();
		public List<HookMethod> cleanup = new List<HookMethod>();
		public HookMethod Build(object scope,Invoker method){
			var data = this.methods.Find(x=>x.invoker==method);
			if(data.IsNull()){
				data = this.methods.AddNew();
				data.hook = this;
				data.invoker = method;
				data.scope = scope;
			}
			return data;
		}
		public HookMethod Add<X>(Func<X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,X>(Func<A,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,X>(Func<A,B,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,X>(Func<A,B,C,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,X>(Func<A,B,C,D,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,X>(Func<A,B,C,D,E,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,F,X>(Func<A,B,C,D,E,F,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,F,G,X>(Func<A,B,C,D,E,F,G,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,F,G,H,X>(Func<A,B,C,D,E,F,G,H,X> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add(Action method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A>(Action<A> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B>(Action<A,B> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C>(Action<A,B,C> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D>(Action<A,B,C,D> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E>(Action<A,B,C,D,E> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,F>(Action<A,B,C,D,E,F> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,F,G>(Action<A,B,C,D,E,F,G> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add<A,B,C,D,E,F,G,H>(Action<A,B,C,D,E,F,G,H> method){return this.Build(method.Target,method.GetInvoker());}
		public HookMethod Add(object scope,Invoker invoker){return this.Build(scope,invoker);}
		public HookMethod Add(object scope,MethodInfo method){return this.Build(scope,method.GetInvoker());}
		public Invoker GetMethod(LambdaExpression expression){
			return ((MethodCallExpression)expression.Body).Method.GetInvoker();
		}
		public ReturnType Call<ReturnType>(params object[] terms){
			if(this.disabled){return default(ReturnType);}
			this.Clean();
			foreach(var data in this.methods){
				if(data.disabled){continue;}
				return (ReturnType)data.Call(terms);
			}
			return default(ReturnType);
		}
		public void Call(params object[] terms){
			if(this.disabled){return;}
			this.Clean();
			foreach(var data in this.methods){
				if(data.disabled){continue;}
				data.Call(terms);
			}
		}
		public List<ReturnType> CallAll<ReturnType>(params object[] terms){
			if(this.disabled){return new List<ReturnType>();}
			var output = new List<ReturnType>();
			this.Clean();
			foreach(var data in this.methods){
				if(data.disabled){continue;}
				output.Add((ReturnType)data.Call(terms));
			}
			return output;
		}
		public void Disable(){this.disabled = true;}
		public void Enable(){this.disabled = false;}
		private void Clean(){
			if(this.cleanup.Count>0){
				this.cleanup.ForEach(x=>this.methods.Remove(x));
				this.cleanup.Clear();
			}
		}
	}
	public delegate void Action<A,B,C,D,E>(A a,B b,C c,D d,E e);
	public delegate void Action<A,B,C,D,E,F>(A a,B b,C c,D d,E e,F f);
	public delegate void Action<A,B,C,D,E,F,G>(A a,B b,C c,D d,E e,F f,G g);
	public delegate void Action<A,B,C,D,E,F,G,H>(A a,B b,C c,D d,E e,F f,G g,H h);
	public delegate X Func<A,B,C,D,E,X>(A a,B b,C c,D d,E e);
	public delegate X Func<A,B,C,D,E,F,X>(A a,B b,C c,D d,E e,F f);
	public delegate X Func<A,B,C,D,E,F,G,X>(A a,B b,C c,D d,E e,F f,G g);
	public delegate X Func<A,B,C,D,E,F,G,H,X>(A a,B b,C c,D d,E e,F f,G g,H h);
	public class HookMethod{
		public Hook hook;
		public Invoker invoker;
		public object scope;
		public int limit = -1;
		public bool disabled;
		public object Call(params object[] terms){
			if(this.limit != -1){
				this.limit -= 1;
				if(this.limit < 1){this.Remove();}
			}
			try{return this.invoker(this.scope,terms);}
			catch(Exception exception){
				Log.Warning("[Hook] Method Call failed -- "+hook.name+". Possible method signature mismatch.");
				Log.Error(exception);
				return null;
			}
		}
		public void Remove(){
			this.disabled = true;
			this.hook.cleanup.Add(this);
		}
		public HookMethod Limit(int limit){
			this.limit = limit;
			return this;
		}
	}
	public static class HookExtensions{
		public static void CallHook(this object current,string name,params object[] terms){
			Hook.CallTarget(current,name,terms);
		}
		public static Hook GetHook(this object current,string name){return Hook.Create(current,name);}
		public static Hook CreateHook(this object current,string name){return Hook.Create(current,name);}
	}
}
