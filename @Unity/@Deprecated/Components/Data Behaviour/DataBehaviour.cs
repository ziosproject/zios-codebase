using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Zios.Unity.Components.DataBehaviour{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Shortcuts;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Call;
	using Zios.Unity.Extensions;
	using Zios.Unity.HookExtensions;
	using Zios.Unity.Locate;
	using Zios.Unity.Proxy;
	//asm Zios.Unity.Shortcuts;
	[ExecuteInEditMode][AddComponentMenu("")]
	public class DataBehaviour : MonoBehaviour{
		[Internal] public string parentPath;
		[Internal] public string path;
		public string alias;
		private string lastAlias;
		[NonSerialized] public List<DataDependency> dependents = new List<DataDependency>();
		[NonSerialized] public List<string> warnings = new List<string>();
		protected bool autoRename = true;
		protected bool setup;
		public virtual void Awake(){
			var name = this.GetType().Name.ToTitleCase();
			this.parentPath = this.gameObject.GetPath();
			this.path = this.GetPath();
			this.lastAlias = this.alias = this.alias.SetDefault(name);
			if(this.autoRename){
				while(Locate.GetObjectComponents<DataBehaviour>(this.gameObject).Exists(x=>x != this && x.alias == this.alias)){
					this.lastAlias = this.alias = this.alias.ToLetterSequence();
				}
			}
			if(!Application.isPlaying){
				this.CreateHook("Unity/Destroy");
				this.CreateHook("Unity/Validate");
				Hook.Get(this,"Unity/Validate").Add(this.CheckDependents);
				Hook.Get(this,"Unity/Validate").Add(this.CheckAlias);
			}
		}
		public virtual void Start(){
			this.setup = true;
			if(Application.isEditor){
				this.CheckDependents();
			}
		}
		//===============
		// Editor
		//===============
		public virtual void Reset(){
			if(Proxy.IsBusy()){return;}
			if(this.setup){
				this.CallHook("Unity/Reset");
				return;
			}
			this.CallHook("DataBehaviour/OnAttach");
		}
		public virtual void OnDisable(){
			if(!this.setup || Proxy.IsBusy()){return;}
			if(!this.gameObject.activeInHierarchy || !this.enabled){
				Hook.CallTarget(this.gameObject,"Unity/Disable");
				Hook.CallTarget(this.gameObject,"DataBehaviour/ComponentsChanged");
				this.gameObject.DelayCallHook(this.parentPath,"Unity/Disable");
				this.gameObject.DelayCallHook(this.parentPath,"DataBehaviour/ComponentsChanged");
			}
		}
		public virtual void OnEnable(){
			if(!this.setup || Proxy.IsBusy()){return;}
			if(!this.lastAlias.IsEmpty() && this.gameObject.activeInHierarchy && this.enabled){
				this.gameObject.DelayCallHook(this.parentPath,"Unity/Enable");
				this.gameObject.DelayCallHook(this.parentPath,"DataBehaviour/ComponentsChanged");
			}
		}
		public virtual void OnValidate(){
			this.DelayCallHook(this.path,"DataBehaviour/ValidateRaw",1);
			if(!this.CanValidate() || !this.setup){return;}
			this.DelayCallHook(this.path,"Unity/Validate",1);
			this.gameObject.DelayCallHook(this.parentPath,"DataBehaviour/ComponentsChanged");
		}
		public virtual void OnDestroy(){
			if(Application.isPlaying || Proxy.IsBusy()){return;}
			this.CallHook("Unity/Destroy");
			Hook.Remove(this);
		}
		public virtual void CheckAlias(){
			if(this.lastAlias != this.alias || this.alias.IsEmpty()){
				Hook.Call("Attributes/Refresh");
				this.lastAlias = this.alias;
				this.Awake();
			}
		}
		//===============
		// Editor - Dependents
		//===============
		public virtual void CheckDependents(){
			if(!this.setup){return;}
			foreach(var dependent in this.dependents){
				var currentDependent = dependent;
				dependent.processing = false;
				dependent.exists = false;
				if(dependent.target.IsNull() && dependent.dynamicTarget.IsNull()){continue;}
				if(dependent.target.IsNull() && !dependent.dynamicTarget.Call<bool>("HasData")){continue;}
				var target = dependent.target.IsNull() ? dependent.dynamicTarget.Call<GameObject>("Get") : dependent.target;
				dependent.method = ()=>{};
				if(!target.IsNull()){
					var types = dependent.types;
					foreach(var type in types){
						var currentType = type;
						dependent.exists = !target.GetComponent(currentType).IsNull();
						if(dependent.exists){break;}
						dependent.method = ()=>{
							var component = target.AddComponent(currentType);
							currentDependent.processing = component != null;
						};
					}
				}
			}
		}
		public virtual void RemoveDependent<Type>() where Type : Component{
			this.RemoveDependent<Type>(this.gameObject);
		}
		public virtual void RemoveDependent<Type>(object target) where Type : Component{
			this.RemoveDependent(target,typeof(Type));
		}
		public virtual void RemoveDependent(object target,params Type[] types){
			this.dependents.RemoveAll(x => Enumerable.SequenceEqual(x.types,types));
		}
		public virtual void AddDependent<Type>() where Type : Component{
			this.AddDependent<Type>(this.gameObject,true);
		}
		public virtual void AddDependent<Type>(object target,bool isScript=false) where Type : Component{
			this.AddDependent(target,isScript,typeof(Type));
		}
		public virtual void AddDependent(object target,bool isScript=false,params Type[] types){
			if(!Application.isEditor){return;}
			if(this.dependents.Exists(x=>Enumerable.SequenceEqual(x.types,types))){return;}
			Action delayAdd = ()=>this.DelayAddDependent(target,isScript,types);
			Hook.Get("Attributes/Ready").Add(delayAdd).Limit(1);
		}
		public virtual void DelayAddDependent(object target,bool isScript=false,params Type[] types){
			if(this.dependents.Exists(x=>Enumerable.SequenceEqual(x.types,types))){return;}
			if(target.IsNull()){return;}
			var dependent = new DataDependency();
			dependent.dynamicTarget = target.GetType().Name.Contains("Attribute") ? target : null;
			dependent.target = !dependent.dynamicTarget.IsNull() ? null : target.As<GameObject>();
			dependent.types = types;
			dependent.scriptName = isScript ? this.GetType().Name : "";
			dependent.message = "[target] is missing required component : [type]. Click here to add.";
			this.dependents.AddNew(dependent);
			Call.Delay(this.CheckDependents);
		}
	}
	public class DataDependency{
		public bool exists;
		public bool processing;
		public string scriptName;
		public object dynamicTarget;
		public GameObject target;
		public Type[] types;
		public string message;
		public Method method = ()=>{};
	}
}