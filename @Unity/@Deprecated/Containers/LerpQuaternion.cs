using System;
using UnityEngine;
namespace Zios.Unity.Deprecated{
	using Zios.Attributes.Deprecated;
	using Zios.Supports.Hook;
	using Zios.Unity.Time;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[Serializable]
	public class LerpQuaternion : LerpTransition{
		public float endProximity;
		private Quaternion? lastStart;
		private Quaternion? lastEnd;
		public override void Setup(string path,Component parent){
			base.Setup(path,parent);
			this.parent.gameObject.CreateHook(this.path+"/Transition/On End");
			this.parent.gameObject.CreateHook(this.path+"/Transition/On Start");
		}
		public virtual Quaternion Step(Quaternion current){
			return this.Step(current,current);
		}
		public virtual Quaternion Step(Quaternion start,Quaternion end){
			var distance = (Quaternion.Inverse(start)*end).eulerAngles.magnitude;
			if(distance <= this.endProximity){
				if(this.active){
					this.parent.gameObject.CallHook(this.path+"/Transition/On End");
					this.active = false;
				}
				return start;
			}
			if(this.isResetOnChange){
				if(this.lastEnd != end){
					this.Reset();
					this.active = false;
				}
			}
			if(!this.active){
				this.transition.Reset();
				this.parent.gameObject.CallHook(this.path+"/Transition/On Start");
				this.lastStart = start;
				this.lastEnd = end;
				this.active = true;
			}
			var percent = this.transition.Tick();
			var current = start;
			if(this.speed != 0){
				var speed = this.speed * percent;
				speed *= this.fixedTime ? Time.GetFixedDelta() : Time.GetDelta();
				current = Quaternion.RotateTowards(start,end,speed);
			}
			else{
				current = Quaternion.Slerp((Quaternion)this.lastStart,end,percent);
			}
			return current;
		}
	}
}