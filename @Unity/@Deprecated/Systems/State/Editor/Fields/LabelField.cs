using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.State{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.State;
	using Zios.Unity.Colors;
	using Zios.Unity.Editor.Drawers.Table;
	using Zios.Unity.Editor.Extensions;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.Pref;
	using Zios.Unity.Extensions.Convert;
	using Zios.Unity.Proxy;
	//asm Zios.Attributes.Supports;
	//asm Zios.Unity.Components.ManagedBehaviour;
	public class LabelField : TableField{
		public bool delayedContext;
		public LabelField(object target=null,TableRow row=null) : base(target,row){}
		public override void Draw(){
			var window = StateWindow.Get();
			this.DrawStyle();
			if(this.delayedContext){
				this.Clicked(1);
				this.delayedContext = false;
			}
			this.CheckHovered(window.scroll.x);
			this.CheckClicked(window.scroll.x);
		}
		public virtual void DrawStyle(){
			var window = StateWindow.Get();
			var stateRow = (StateRow)this.row.target;
			var row = this.row.target.As<StateRow>();
			var script = row.target;
			var darkSkin = EditorGUIUtility.isProSkin || EditorPref.Get<bool>("Zios.Theme.Dark",false);
			var name = this.target is string ? (string)this.target : this.target.As<StateRow>().name;
			var background = darkSkin ? "BoxBlackA30" : "BoxWhiteBWarm";
			var textColor = darkSkin ? Colors.Get("Silver") : Colors.Get("Black");
			var prefixColor = Colors.Get("DarkOrange").ToHex();
			var style = new GUIStyle(GUI.skin.label);
			style.margin.left = 5;
			var fieldHovered = window.row == this.row.order || this.hovered;
			if(fieldHovered){
				prefixColor = Colors.Get("ZestyOrange").ToHex();
				textColor = darkSkin ? Colors.Get("ZestyBlue") : Colors.Get("White");
				background = darkSkin ? "BoxBlackHighlightBlueAWarm" : "BoxBlackHighlightBlueDWarm";
			}
			if(this.row.selected){
				prefixColor = Colors.Get("ZestyOrange").ToHex();
				textColor = darkSkin ? Colors.Get("White") : Colors.Get("White");
				background = darkSkin ? "BoxBlackHighlightCyanA" : "BoxBlackHighlightCyanCWarm";
			}
			if(Proxy.IsPlaying()){
				textColor = Colors.Get("Gray");
				background = darkSkin ? "BoxBlackAWarm30" : "BoxWhiteBWarm50";
				bool usable = row.target is StateTable && row.target != window.target ? row.target.As<StateTable>().external : script.usable;
				if(usable){
					textColor = darkSkin ? Colors.Get("Silver") : Colors.Get("Black");
					background = darkSkin ? "BoxBlackA30" : "BoxWhiteBWarm";
				}
				if(script.used){
					textColor = darkSkin ? Colors.Get("White") : Colors.Get("White");
					background = darkSkin ? "BoxBlackHighlightYellowAWarm" : "BoxBlackHighlightYellowDWarm";
				}
				if(script.active){
					textColor = darkSkin ? Colors.Get("White") : Colors.Get("White");
					background = darkSkin ? "BoxBlackHighlightPurpleAWarm" : "BoxBlackHighlightPurpleDWarm";
				}
			}
			if(!row.section.IsEmpty()){
				style.margin.left = 33;
				style.fixedWidth -= 28;
			}
			style.normal.textColor = textColor;
			style.normal.background = UnityFile.GetAsset<Texture2D>(background);
			if(this.row.selected){style.hover = style.normal;}
			var currentRow = window.rowIndex[stateRow]+1;
			var totalRows = stateRow.requirements.Length;
			var prefix = stateRow.requirements.Length > 1 ? "<color="+prefixColor+"><i>["+currentRow+"/"+totalRows+"]</i></color>  " : "";
			var content = new GUIContent(prefix+name);
			StateWindow.Clip(content,style,-1,window.headerSize);
		}
		public override void Clicked(int button){
			var window = StateWindow.Get();
			var stateRow = (StateRow)this.row.target;
			var rowIndex = window.rowIndex[stateRow];
			var selected = this.row.table.rows.Where(x=>x.selected).ToArray();
			if(Event.current.alt && stateRow.requirements.Length > 1){
				var length = stateRow.requirements.Length;
				rowIndex += button == 1 ? -1 : 1;
				if(rowIndex < 0){rowIndex = length-1;}
				if(rowIndex >= length){rowIndex = 0;}
				window.rowIndex[stateRow] = rowIndex;
				window.BuildTable();
				return;
			}
			if(!this.row.selected && button == 1){this.delayedContext = true;}
			if(button == 0 || !this.row.selected){
				if(Event.current.shift){
					var allRows = this.row.table.rows;
					var firstIndex = selected.Length < 1 ? allRows.Count-1 : allRows.FindIndex(x=>x==selected.First());
					var lastIndex = selected.Length < 1 ? 0 : allRows.FindIndex(x=>x==selected.Last());
					var current = allRows.FindIndex(x=>x==this.row);
					var closest = current.Closest(firstIndex,lastIndex);
					foreach(var row in allRows.Skip(current.Min(closest)).Take(current.Distance(closest)+1)){
						row.selected = !row.disabled;
					}
				}
				else{
					var state = !this.row.selected;
					if(!Event.current.control){window.DeselectAll();}
					this.row.selected = state;
				}
			}
			else if(button == 1){
				var menu = new GenericMenu();
				var term = selected.Any(x=>x.target is StateRow && !x.target.As<StateRow>().section.IsEmpty()) ? "Regroup" : "Group";
				menu.AddItem(term+" Selected",false,window.GroupSelected);
				if(selected.Count(x=>!x.target.As<StateRow>().section.IsEmpty()) > 0){
					menu.AddItem("Ungroup Selected",false,window.UngroupSelected);
				}
				menu.AddItem("Selection/Invert",false,window.InvertSelection);
				menu.AddItem("Selection/Deselect All",false,window.DeselectAll);
				if(selected.Length == 1){
					menu.AddItem("Add Alternate Row",false,this.AddAlternativeRow,stateRow);
					if(rowIndex != 0){
						menu.AddItem("Remove Alternative Row",false,this.RemoveAlternativeRow,stateRow);
					}
				}
				menu.ShowAsContext();
			}
			window.Repaint();
		}
		public void AddAlternativeRow(object target){
			var window = StateWindow.Get();
			var row = (StateRow)target;
			var data = new List<StateRowData>(row.requirements);
			data.Add(new StateRowData());
			row.requirements = data.ToArray();
			window.target.Refresh();
			window.rowIndex[row] = row.requirements.Length-1;
			window.BuildTable();
		}
		public void RemoveAlternativeRow(object target){
			var window = StateWindow.Get();
			var row = (StateRow)target;
			var rowIndex = window.rowIndex[row];
			var data = new List<StateRowData>(row.requirements);
			data.RemoveAt(rowIndex);
			row.requirements = data.ToArray();
			window.rowIndex[row] = rowIndex-1;
			window.BuildTable();
		}
	}
}