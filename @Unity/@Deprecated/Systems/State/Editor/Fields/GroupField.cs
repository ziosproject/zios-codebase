using System.Linq;
namespace Zios.Unity.Editor.State{
	using Zios.Extensions.Convert;
	using Zios.State;
	using Zios.Unity.Editor.Drawers.Table;
	public class GroupField : StateField{
		public StateField[] columnFields = new StateField[0];
		public GroupField(object target=null,TableRow row=null) : base(target,row){}
		public int GetState(StateRequirement requirement){
			if(requirement.requireOn){return 1;}
			if(requirement.requireOff){return 2;}
			return 0;
		}
		public override void Draw(){
			if(columnFields.Length < 1){return;}
			var baseState = this.GetState(this.columnFields[0].target.As<StateRequirement>());
			var mismatched = this.columnFields.Count(x=>this.GetState(x.target.As<StateRequirement>())!=baseState) > 0;
			this.DrawStyle(mismatched ? -1 : baseState);
			this.CheckClicked();
		}
		public override void Clicked(int button){
			var baseState = this.GetState(this.columnFields[0].target.As<StateRequirement>());
			var mismatched = this.columnFields.Count(x=>this.GetState(x.target.As<StateRequirement>())!=baseState);
			if(mismatched == 0){
				foreach(var field in this.columnFields){
					field.Clicked(button);
				}
			}
		}
	}
}