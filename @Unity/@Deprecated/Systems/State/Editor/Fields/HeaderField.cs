using System.Linq;
using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.State{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.State;
	using Zios.Unity.Colors;
	using Zios.Unity.Editor.Drawers.Table;
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.Pref;
	using Zios.Unity.Extensions;
	using Zios.Unity.Proxy;

	//asm Zios.Attributes.Supports;
	//asm Zios.Unity.Components.DataBehaviour;
	//asm Zios.Unity.Components.ManagedBehaviour;
	public class HeaderField : TableField{
		public HeaderField(object target=null,TableRow row=null) : base(target,row){}
		public override void Draw(){
			var window = StateWindow.Get();
			var hidden = !this.target.Equals("") && !window.target.manual && this.target.As<StateRequirement>().name == "@External";
			if(hidden){return;}
			var target = this.target is StateRequirement ? this.target.As<StateRequirement>() : null;
			var script = !target.IsNull() ? target.target.As<StateBehaviour>() : null;
			var scroll = window.scroll;
			var label = this.target is string ? new GUIContent("") : new GUIContent(this.target.GetVariable<string>("name"));
			var style = new GUIStyle(GUI.skin.label);
			var mode = (HeaderMode)EditorPref.Get<int>("StateWindow-Mode",2);
			var darkSkin = EditorGUIUtility.isProSkin || EditorPref.Get<bool>("Zios.Theme.Dark",false);
			var textColor = Colors.Get("Gray");
			var background = darkSkin ? "BoxBlackAWarm30" : "BoxWhiteBWarm50";
			if(window.target.external){
				textColor = darkSkin ? Colors.Get("Silver") : Colors.Get("Black");
				background = darkSkin ? "BoxBlackA30" : "BoxWhiteBWarm";
			}
			if(label.text == ""){
				this.disabled = this.row.fields.Skip(1).Count(x=>!x.disabled) < 1;
				window.headerSize = 64;
				style.margin.left = 5;
				style.hover = style.normal;
				style.normal.background = UnityFile.GetAsset<Texture2D>(background);
				if(mode == HeaderMode.Vertical){
					window.headerSize = 35;
					style.fixedHeight = style.fixedWidth;
					StateWindow.Clip(label,style,0,window.headerSize);
				}
				if(mode != HeaderMode.Vertical){StateWindow.Clip(label,style,-1,-1);}
				return;
			}
			var fieldHovered = window.column == this.order;
			if(fieldHovered){
				background = darkSkin ? "BoxBlackHighlightBlueAWarm" : "BoxBlackHighlightBlueDWarm";
				textColor = darkSkin ? Colors.Get("ZestyBlue") : Colors.Get("White");
			}
			if(Proxy.IsPlaying() && !script.IsNull()){
				textColor = Colors.Get("Gray");
				background = darkSkin ? "BoxBlackAWarm30" : "BoxWhiteBWarm50";
				bool usable = target.name == "@External" ? window.target.external : script.usable;
				bool active = target.name == "@External" ? window.target.external : script.active;
				if(usable){
					textColor = darkSkin ? Colors.Get("Silver") : Colors.Get("Black");
					background = darkSkin ? "BoxBlackA30" : "BoxWhiteBWarm";
				}
				if(script.used){
					textColor = darkSkin ? Colors.Get("White") : Colors.Get("White");
					background = darkSkin ? "BoxBlackHighlightYellowAWarm" : "BoxBlackHighlightYellowDWarm";
				}
				if(active){
					textColor = darkSkin ? Colors.Get("White") : Colors.Get("White");
					background = darkSkin ? "BoxBlackHighlightPurpleAWarm" : "BoxBlackHighlightPurpleDWarm";
				}
			}
			style.normal.textColor = textColor;
			style.normal.background = UnityFile.GetAsset<Texture2D>(background);
			if(mode == HeaderMode.Vertical){
				window.cellSize = style.fixedHeight;
				var halfWidth = style.fixedWidth / 2;
				var halfHeight = style.fixedHeight / 2;
				var rotated = new GUIStyle(style).Rotate90();
				var last = GUILayoutUtility.GetRect(new GUIContent(""),rotated);
				GUIUtility.RotateAroundPivot(90,last.center);
				var position = new Rect(last.x,last.y,0,0);
				position.x +=  halfHeight-halfWidth;
				position.y += -halfHeight+halfWidth;
				style.overflow.left = (int)-scroll.y;
				label.text = style.overflow.left >= -(position.width/4)-9 ? label.text : "";
				label.ToLabel().DrawLabel(position,style);
				GUI.matrix = Matrix4x4.identity;
			}
			else{
				style.fixedWidth -= 36;
				window.cellSize = style.fixedWidth;
				if(mode == HeaderMode.HorizontalFit){
					var visible = this.row.fields.Skip(1).Where(x=>!x.disabled).ToList();
					var area = window.cellSize = (Screen.width-style.fixedWidth-56)/visible.Count;
					area = window.cellSize = Mathf.Floor(area-2);
					var lastEnabled = visible.Last() == this;
					style.margin.right = lastEnabled ? 18 : 0;
					style.fixedWidth = lastEnabled ? 0 : area;
				}
				style.alignment = TextAnchor.MiddleCenter;
				StateWindow.Clip(label,style,GUI.skin.label.fixedWidth+7,-1);
			}
			this.CheckClicked(0,scroll.y);
		}
		public override void Clicked(int button){
			if(button == 0){
				var mode = (EditorPref.Get<int>("StateWindow-Mode",2)+1)%3;
				EditorPref.Set<int>("StateWindow-Mode",mode);
				this.row.table.ShowAll();
				StateWindow.Get().Repaint();
				return;
			}
		}
	}
}