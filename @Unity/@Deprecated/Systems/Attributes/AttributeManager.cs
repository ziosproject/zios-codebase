using UnityEngine;
namespace Zios.Attributes{
	using Zios.Attributes.Supports;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Serializer.Attributes;
	using Zios.SystemAttributes;
	using Zios.Unity.Call;
	using Zios.Unity.Components.DataBehaviour;
	using Zios.Unity.Events;
	using Zios.Unity.Locate;
	using Zios.Unity.Log;
	using Zios.Unity.Proxy;
	using Zios.Unity.Time;
	//asm Zios.Shortcuts;
	//asm Zios.Supports.Hierarchy;
	//asm Zios.Unity.Shortcuts;
	[AutoInitialize(15)]
	public static class AttributeHook{
		static AttributeHook(){
			Events.Add("UnityEditor/SceneStart",AttributeManager.Get().OnEnable);
		}
	}
	[Singleton]
	public class AttributeManager{
		private static AttributeManager singleton;
		public static float nextRefresh = 0;
		public static float percentLoaded = 0;
		public bool disabled = false;
		public int editorRefreshPasses = -1;
		public bool editorIncludeDisabled = true;
		public bool refreshOnComponentsChanged = true;
		public bool safeMode = true;
		private float start;
		private float block;
		private DataBehaviour[] data = new DataBehaviour[0];
		private int nextIndex;
		private int stage;
		public static AttributeManager Get(){return AttributeManager.singleton;}
		//==============================
		// Editor
		//==============================
		[ContextMenu("Refresh")]
		public void ContextRefresh(){
			AttributeManager.PerformRefresh();
		}
		public static void PerformRefresh(){AttributeManager.Refresh();}
		public static void Refresh(int delay = 0){
			if(Proxy.IsPlaying() || AttributeManager.Get().disabled){return;}
			Events.Call("Attributes/Refresh");
			AttributeManager.nextRefresh = Time.Get() + delay;
		}
		//==============================
		// Unity
		//==============================
		public AttributeManager(){
			AttributeManager.singleton = this;
		}
		public void OnEnable(){
			this.Setup();
			AttributeManager.Refresh();
			this.EditorUpdate();
		}
		public void EditorUpdate(){
			if(AttributeManager.Get().disabled){return;}
			if(AttributeManager.nextRefresh > 0 && Time.Get() > AttributeManager.nextRefresh){
				if(Attribute.debug.HasAny("ProcessRefresh")){LogUnity.Editor("[AttributeManager] Refreshing...");}
				this.Setup();
			}
			if(this.editorRefreshPasses < 1){
				if(!Attribute.ready){
					this.Setup();
					this.SceneRefresh();
					if(Attribute.debug.HasAny("ProcessStage")){LogUnity.Editor("[AttributeManager] Stage 1 (Awake) start...");}
					Events.Call("Attributes/Setup");
					this.block = Time.Get();
					while(this.stage != 0){this.Process();}
				}
			}
			else if(this.stage != 0){
				for(var index=1;index<=this.editorRefreshPasses;++index){
					Call.Delay(this.Process);
				}
			}
		}
		public void OnDisable(){Events.RemoveAll(this);}
		//==============================
		// Main
		//==============================
		public void Setup(){
			this.stage = 1;
			this.nextIndex = 0;
			Attribute.ready = false;
			AttributeManager.nextRefresh = 0;
			this.SetupEvents();
		}
		public void SetupEvents(){
			if(!Proxy.IsPlaying()){
				Events.Register("Attributes/Setup");
				Events.Register("Attributes/Ready");
				Events.Register("Attributes/Refresh");
				Events.Remove("DataBehaviour/ComponentsChanged",AttributeManager.PerformRefresh);
				Events.Add("Events/Reset",AttributeManager.PerformRefresh);
				//if(this.refreshOnHierarchyChanged){Event.Add("UnityEditor/HierarchyChanged",AttributeManager.PerformRefresh);}
				if(this.refreshOnComponentsChanged){Events.Add("DataBehaviour/ComponentsChanged",AttributeManager.PerformRefresh);}
			}
			Events.Add("Unity/FirstUpdate",this.OnEnable);
			Events.Add("UnityEditor/Update",this.EditorUpdate);
			Events.Add("Unity/Validate",this.SetupEvents,this);
			Events.Add("Attributes/Refresh",AttributeManager.PerformRefresh);
		}
		public void Process(){
			if(this.stage > 0){
				if(this.stage == 1){this.StepAwake();}
				if(this.stage == 2){this.StepBuildLookup();}
				if(this.stage == 3){this.StepBuildData();}
				AttributeManager.percentLoaded = (((float)this.nextIndex / this.data.Length) / 4.0f) + ((this.stage-1)*0.25f);
			}
		}
		public void SceneRefresh(){
			var fullSweep = !Proxy.IsPlaying();
			if(fullSweep){
				Attribute.all.Clear();
				Attribute.lookup.Clear();
			}
			var includeEnabled = Attribute.ready || !Proxy.IsPlaying();
			var includeDisabled = !Attribute.ready || this.editorIncludeDisabled;
			if(Attribute.debug.HasAny("ProcessRefresh")){LogUnity.Editor("[AttributeManager] Scene Refreshing...");}
			this.data = Locate.GetSceneComponents<DataBehaviour>(includeEnabled,includeDisabled);
			if(Attribute.debug.HasAny("ProcessRefresh")){LogUnity.Editor("[AttributeManager] DataBehaviour Count : " + this.data.Length);}
			this.start = Time.Get();
			this.nextIndex = 0;
		}
		public void DisplayStageTime(string message){
			var duration = (Time.Get() - this.block) + " seconds.";
			LogUnity.Editor(message + " " + duration);
			this.block = Time.Get();
		}
		public void StepAwake(){
			if(this.nextIndex > this.data.Length-1){
				this.stage = 2;
				this.nextIndex = 0;
				if(Attribute.debug.HasAny("ProcessTime")){this.DisplayStageTime("[AttributeManager] Stage 1 (Awake)");}
				if(Attribute.debug.HasAny("ProcessStage")){LogUnity.Editor("[AttributeManager] Stage 2 (Build Lookup) start...");}
				return;
			}
			if(!this.data[this.nextIndex].IsNull()){
				this.data[this.nextIndex].Awake();
			}
			else if(Attribute.debug.HasAny("Issue")){LogUnity.Editor("[AttributeManager] Stage 1 (Awake) index " + this.nextIndex + " was null.");}
			this.nextIndex += 1;
		}
		public void StepBuildLookup(){
			if(this.nextIndex > Attribute.all.Count-1){
				this.stage = 3;
				this.nextIndex = 0;
				if(Attribute.debug.HasAny("ProcessTime")){this.DisplayStageTime("[AttributeManager] Stage 2 (Build Lookup)");}
				if(Attribute.debug.HasAny("ProcessStage")){LogUnity.Editor("[AttributeManager] Stage 3 (Build Data) start...");}
				return;
			}
			var attribute = Attribute.all[this.nextIndex];
			if(attribute.IsNull() || attribute.info.parent.IsNull()){
				if(Attribute.debug.HasAny("Issue")){LogUnity.Editor("[AttributeManager] Null attribute found.  Removing index " + this.nextIndex + ".");}
				Attribute.all.Remove(attribute);
				return;
			}
			attribute.BuildLookup();
			this.nextIndex += 1;
		}
		public void StepBuildData(){
			if(this.nextIndex > Attribute.all.Count-1){
				if(!Attribute.ready){
					if(Attribute.debug.HasAny("ProcessTime")){
						this.DisplayStageTime("[AttributeManager] Stage 3 (Build Data)");
						LogUnity.Editor("[AttributeManager] Refresh Complete : " + (Time.Get() - this.start) + " seconds.");
					}
				}
				Attribute.ready = true;
				AttributeManager.percentLoaded = 1;
				Events.Call("UnityEditor/RepaintInspectors");
				Events.Call("Attributes/Ready");
				Events.Rest("Attributes/Refresh",1);
				this.stage = 0;
				this.nextIndex = 0;
				return;
			}
			var attribute = Attribute.all[this.nextIndex];
			attribute.BuildData(attribute.info.data);
			attribute.BuildData(attribute.info.dataB);
			attribute.BuildData(attribute.info.dataC);
			this.nextIndex += 1;
		}
	}
}