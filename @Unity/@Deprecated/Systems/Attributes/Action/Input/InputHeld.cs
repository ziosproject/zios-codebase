using System;
using UnityEngine;
namespace Zios.Attributes.Actions{
	using Zios.Attributes.Supports;
	using Zios.Extensions;
	using Zios.State;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Inputs;
	using Zios.Unity.SystemAttributes;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Components.DataBehaviour;
	//asm Zios.Unity.Components.ManagedBehaviour;
	//asm Zios.Unity.Shortcuts;
	[AddComponentMenu("Zios/Deprecated/Action/Input/Input Held")]
	public class InputHeld : StateBehaviour{
		[Advanced] public InputRange requirement;
		public AttributeGameObject target;
		[InputName] public AttributeString inputName = "";
		[Advanced] public AttributeBool heldDuringIntensity = true;
		[Internal] public AttributeFloat intensity = 0;
		[NonSerialized] public InputInstance instance;
		public override void Awake(){
			base.Awake();
			this.inputName.Setup("Input Name",this);
			this.target.Setup("Input Target",this);
			this.intensity.Setup("Input Intensity",this);
			this.heldDuringIntensity.Setup("Input Held During Intensity",this);
			this.AddDependent<InputInstance>(target);
			this.SetInstance();
			this.GetHook("Unity/Validate").Add(this.SetInstance);
		}
		public void SetInstance(){
			this.instance = this.target.Get() ? this.target.Get().GetComponent<InputInstance>() : null;
		}
		public override void Use(){
			var inputSuccess = this.CheckInput();
			if(inputSuccess){
				base.Use();
			}
			else if(this.active){
				base.End();
			}
		}
		public virtual bool CheckInput(){
			if(InputState.disabled || this.instance.IsNull()){
				this.intensity.Set(0);
				return false;
			}
			var intensity = this.instance.GetIntensity(this.inputName);
			var valid = (this.heldDuringIntensity && intensity != 0) || (!this.heldDuringIntensity && this.instance.GetHeld(this.inputName));
			this.intensity.Set(intensity);
			return valid && InputState.CheckRequirement(this.requirement,this.intensity);
		}
	}
}