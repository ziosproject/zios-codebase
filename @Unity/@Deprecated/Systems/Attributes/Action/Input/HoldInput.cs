using System;
using UnityEngine;
namespace Zios.Attributes.Actions{
	using Zios.Attributes.Supports;
	using Zios.Extensions;
	using Zios.State;
	using Zios.Supports.Hook;
	using Zios.Unity.Inputs;
	using Zios.Unity.SystemAttributes;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Components.DataBehaviour;
	//asm Zios.Unity.Components.ManagedBehaviour;
	//asm Zios.Unity.Shortcuts;
	[AddComponentMenu("Zios/Deprecated/Action/Input/Hold Input")]
	public class HoldInput : StateBehaviour{
		public AttributeGameObject target;
		[InputName] public AttributeString inputName = "";
		[NonSerialized] public InputInstance instance;
		public override void Awake(){
			base.Awake();
			this.inputName.Setup("Input Name",this);
			this.target.Setup("Input Target",this);
			this.AddDependent<InputInstance>(target);
			this.SetInstance();
			this.GetHook("Unity/Validate").Add(this.SetInstance);
		}
		public void SetInstance(){
			this.instance = this.target.Get() ? this.target.Get().GetComponent<InputInstance>() : null;
		}
		public override void Use(){
			if(this.instance.IsNull()){return;}
			this.instance.CallHook("Hold Input",this.inputName.Get());
		}
		public override void End(){
			if(this.instance.IsNull()){return;}
			this.instance.CallHook("Release Input",this.inputName.Get());
		}
	}
}