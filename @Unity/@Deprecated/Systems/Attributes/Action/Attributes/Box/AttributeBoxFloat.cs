using UnityEngine;
namespace Zios.Attributes.Actions{
	using Zios.Attributes.Supports;
	using Zios.Unity.Pref;
	[AddComponentMenu("Zios/Deprecated/Attribute/Box/Box Float")]
	public class AttributeBoxFloat : AttributeBox<AttributeFloat>{
		public override void Store(){
			PlayerPref.Set<float>(this.value.info.fullPath,this.value);
		}
		public override void Load(){
			var value = PlayerPref.Get<float>(this.value.info.fullPath);
			this.value.Set(value);
		}
	}
}