using UnityEngine;
namespace Zios.Attributes.Actions{
	using Zios.Attributes.Supports;
	using Zios.State;
	using Zios.SystemAttributes;
	//asm Zios.Unity.Components.DataBehaviour;
	//asm Zios.Unity.Components.ManagedBehaviour;
	[AddComponentMenu("Zios/Deprecated/Action/Cast/Raycast")]
	public class RayCast : StateBehaviour{
		public AttributeVector3 source = new AttributeVector3();
		public AttributeVector3 direction = -Vector3.up;
		public AttributeFloat distance = 1;
		public LayerMask layers = -1;
		[Advanced] public Color debugColor = Color.blue;
		[Internal] public AttributeVector3 hitPoint = Vector3.zero;
		[Internal] public AttributeVector3 hitNormal = Vector3.zero;
		[Internal] public AttributeFloat hitDistance = 0;
		[Internal] public AttributeGameObject hit = new AttributeGameObject();
		[Internal] public RaycastHit castHit = new RaycastHit();
		public override void Awake(){
			base.Awake();
			this.source.Setup("Source",this);
			this.direction.Setup("Direction",this);
			this.distance.Setup("Distance",this);
			this.hit.Setup("Hit",this);
			this.hitPoint.Setup("Hit Point",this);
			this.hitNormal.Setup("Hit Normal",this);
			this.hitDistance.Setup("Hit Distance",this);
		}
		public override void Use(){
			var distance = this.distance == -1 ? Mathf.Infinity : this.distance.Get();
			var state = Physics.Raycast(this.source,this.direction,out this.castHit,distance,this.layers.value);
			if(state){
				this.hit.Set(this.castHit.collider.gameObject);
				this.hitPoint.Set(this.castHit.point);
				this.hitNormal.Set(this.castHit.normal);
				this.hitDistance.Set(this.castHit.distance);
			}
			this.Toggle(state);
		}
		public void OnDrawGizmosSelected(){
			if(!Attribute.ready){return;}
			Gizmos.color = this.debugColor;
			Vector3 start = this.source;
			var end = start + (direction * this.distance);
			Gizmos.DrawLine(start,end);
		}
	}
}