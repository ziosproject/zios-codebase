using UnityEngine;
namespace Zios.Attributes.Actions{
	using Zios.State;
	using Zios.Supports.Hook;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Components.DataBehaviour;
	//asm Zios.Unity.Components.ManagedBehaviour;
	//asm Zios.Unity.Shortcuts;
	[AddComponentMenu("Zios/Deprecated/Action/Event/Event Call")]
	public class EventCall : StateBehaviour{
		public EventTarget target = new EventTarget();
		public override void Awake(){
			base.Awake();
			this.target.Setup("Event",this);
			this.target.mode = EventMode.Listeners;
			this.GetHook("Unity/Validate").Add(this.Register);
		}
		public void Register(){
			Debug.Log(this.target.name + "!");
			this.CreateHook(this.target.name);
		}
		public override void Use(){
			this.target.Call();
			base.Use();
		}
	}
}