using System;
using UnityEngine;
namespace Zios.Attributes.Actions{
	using Zios.Attributes.Supports;
	using Zios.Extensions;
	using Zios.Supports.Hook;
	//asm Zios.Unity.Shortcuts;
	public enum EventMode{Listeners,Callers}
	[Serializable]
	public class EventTarget{
		public AttributeString name = "";
		public Target target = new Target();
		public EventMode mode = EventMode.Listeners;
		public void Setup(string name,Component component){
			this.name.Setup(name+"/Name",component);
			this.target.Setup(name+"/Target",component);
		}
		public void Listen(Action method){
			var target = this.target.Get();
			if(!this.name.IsEmpty() && !target.IsNull()){
				target.GetHook(this.name).Add(method);
			}
		}
		public void Call(){
			var target = this.target.Get();
			target.CallHook(this.name);
		}
	}
}