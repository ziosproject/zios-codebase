using UnityEngine;
namespace Zios.Attributes.Supports{
	using Zios.Extensions;
	using Zios.Unity.Extensions;
	using Zios.Unity.Extensions.Convert;
	[AddComponentMenu("")]
	public class AttributeVector3Data : AttributeData<Vector3,AttributeVector3,AttributeVector3Data>{
		public override Vector3 HandleSpecial(){
			var value = this.value;
			var special = AttributeVector3.specialList[this.special];
			if(this.attribute.mode == AttributeMode.Linked){return value;}
			else if(special == "Flip"){return value * -1;}
			else if(special == "Abs"){return value.Abs();}
			else if(special == "Sign"){return value.Sign();}
			else if(special == "Floor"){
				var x = Mathf.Floor(value.x);
				var y = Mathf.Floor(value.y);
				var z = Mathf.Floor(value.z);
				return new Vector3(x,y,z);
			}
			else if(special == "Ceil"){
				var x = Mathf.Ceil(value.x);
				var y = Mathf.Ceil(value.y);
				var z = Mathf.Ceil(value.z);
				return new Vector3(x,y,z);
			}
			else if(special == "Normalized"){return value.normalized;}
			else if(special == "Magnitude"){
				var magnitude = value.magnitude;
				return new Vector3(magnitude,magnitude,magnitude);
			}
			else if(special == "SqrMagnitude"){
				var sqrMagnitude = value.sqrMagnitude;
				return new Vector3(sqrMagnitude,sqrMagnitude,sqrMagnitude);
			}
			return value;
		}
		public override void Serialize(){
			if(!this.value.IsNull()){
				this.rawValue = this.value.ToText();
			}
		}
	}
}