using UnityEngine;
namespace Zios.Attributes.Supports{
	[AddComponentMenu("")]
	public class AttributeBoolData : AttributeData<bool,AttributeBool,AttributeBoolData>{
		public override bool HandleSpecial(){
			var value = this.value;
			var special = AttributeBool.specialList[this.special];
			if(this.attribute.mode == AttributeMode.Linked){return value;}
			else if(special == "Flip"){return !value;}
			return value;
		}
	}
}