using System;
using System.Collections.Generic;
namespace Zios.Attributes.Supports{
	[Serializable]
	public class AttributeBool : Attribute<bool,AttributeBool,AttributeBoolData>{
		public static string[] specialList = new string[]{"Copy","Flip"};
		public static Dictionary<Type,string[]> operators = new Dictionary<Type,string[]>(){
			{typeof(AttributeBoolData),new string[]{"And","Or"}},
			{typeof(AttributeIntData),new string[]{"And","Or"}},
			{typeof(AttributeFloatData),new string[]{"And","Or"}},
			//{typeof(AttributeVector3Data),new string[]{"And","Or"}}
		};
		public static Dictionary<string,string[]> comparers = new Dictionary<string,string[]>(){
			{"BoolBool",new string[]{"==","!="}},
			{"NumberNumber",new string[]{"<",">","<=",">=","==","!="}},
			//{"Vector3Vector3",new string[]{"<",">","<=",">=","==","!="}},
		};
		public AttributeBool() : this(false){}
		public AttributeBool(bool value){this.delayedValue = value;}
		public static implicit operator AttributeBool(bool current){return new AttributeBool(current);}
		public static implicit operator bool(AttributeBool current){return current.Get();}
		public override bool GetFormulaValue(){
			var value = true;
			for(var index=0;index<this.data.Length;++index){
				var current = false;
				var compare = this.info.data[index];
				var against = this.info.dataB[index];
				var compareIsNumber = compare is AttributeIntData || compare is AttributeFloatData;
				var againstIsNumber = against is AttributeIntData || against is AttributeFloatData;
				var operation = compare.operation == 0 ? "And" : "Or";
				if(operation == "Or" && value){break;}
				if(compare is AttributeBoolData && against is AttributeBoolData){
					var comparer = AttributeBool.comparers["BoolBool"][against.operation];
					var compareValue = ((AttributeBoolData)compare).Get();
					var againstValue = ((AttributeBoolData)against).Get();
					if(comparer == "=="){current = compareValue == againstValue;}
					else if(comparer == "!="){current = compareValue != againstValue;}
				}
				else if(compareIsNumber && againstIsNumber){
					var comparer = AttributeBool.comparers["NumberNumber"][against.operation];
					var compareValue = compare is AttributeIntData ? ((AttributeIntData)compare).Get() : ((AttributeFloatData)compare).Get();
					var againstValue = against is AttributeIntData ? ((AttributeIntData)against).Get() : ((AttributeFloatData)against).Get();
					if(comparer == "<"){current = compareValue < againstValue;}
					else if(comparer == ">"){current = compareValue > againstValue;}
					else if(comparer == "<="){current = compareValue <= againstValue;}
					else if(comparer == ">="){current = compareValue >= againstValue;}
					else if(comparer == "=="){current = compareValue == againstValue;}
					else if(comparer == "!+"){current = compareValue != againstValue;}
				}
				/*else if(compare is AttributeVector3Data && against is AttributeVector3Data){
					string comparer = AttributeBool.compareAgainst["Vector3Vector3"][against.operation];
					Vector3 compareValue = ((AttributeVector3Data)compare).Get();
					Vector3 againstValue = ((AttributeVector3Data)against).Get();
					if(comparer == "<"){current = compareValue < againstValue;}
					else if(comparer == ">"){current = compareValue > againstValue;}
					else if(comparer == "<="){current = compareValue >= againstValue;}
					else if(comparer == ">="){current = compareValue <= againstValue;}
					else if(comparer == "=="){current = compareValue == againstValue;}
					else if(comparer == "!+"){current = compareValue != againstValue;}
				}*/
				if(operation == "And"){value = value && current;}
				else if(operation == "Or"){value = value || current;}
			}
			return value;
		}
		public override Type[] GetFormulaTypes(){
			return new Type[]{typeof(AttributeBoolData),typeof(AttributeIntData),typeof(AttributeFloatData)};
		}
	}
}