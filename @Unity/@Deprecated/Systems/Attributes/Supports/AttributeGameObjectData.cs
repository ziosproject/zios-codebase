using UnityEngine;
namespace Zios.Attributes.Supports{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Unity.Extensions;
	[AddComponentMenu("")]
	public class AttributeGameObjectData : AttributeData<GameObject,AttributeGameObject,AttributeGameObjectData>{
		public override GameObject HandleSpecial(){
			var value = this.value;
			var special = AttributeGameObject.specialList[this.special];
			if(special == "Parent"){return value.GetParent();}
			return value;
		}
		public override GameObject Get(){
			var attribute = this.attribute;
			if(!Attribute.ready && Application.isPlaying){
				if(Attribute.debug.HasAny("Issue")){Debug.LogWarning("[AttributeData] Get attempt before attribute data built : " + attribute.fullPath,attribute.parent);}
				return default(GameObject);
			}
			else if(this.reference.IsNull()){
				var target = this.target.Get();
				if(target.IsNull() && !Attribute.getWarning.ContainsKey(this)){
					var source = "("+attribute.fullPath+")";
					var goal = (target.GetPath() + this.referencePath).Trim("/");
					if(Attribute.debug.HasAny("Issue")){Debug.LogWarning("[AttributeData] Get : No reference found for " + source + " to " + goal,attribute.parent);}
					Attribute.getWarning[this] = true;
				}
				return target;
			}
			this.value = ((AttributeGameObject)this.reference).Get();
			if(attribute.mode == AttributeMode.Linked){return this.value;}
			return this.HandleSpecial();
		}
	}
}