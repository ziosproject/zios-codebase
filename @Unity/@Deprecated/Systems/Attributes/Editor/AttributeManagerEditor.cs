using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Attributes{
	using Zios.Attributes;
	using Zios.Log;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.MonoBehaviourEditor;
	using Zios.Unity.Editor.ProxyEditor;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	//asm Zios.Unity.Editor.Inspectors;
	[AutoInitialize(14)][CustomEditor(typeof(AttributeManager))]
	public class AttributeManagerEditor : MonoBehaviourEditor{
		static AttributeManagerEditor(){
			Hook.Get("Attributes/Ready").Add(ProxyEditor.RepaintInspectors);
		}
		public override void OnInspectorGUI(){
			this.title = "Attributes";
			this.header = this.header ?? UnityFile.GetAsset<Texture2D>("AttributeManagerIcon.png");
			base.OnInspectorGUI();
		}
		[MenuItem("Zios/Attribute/Full Refresh %&R")]
		public static void FullRefresh(){
			Log.Show("[AttributeManager] Manual Refresh.");
			AttributeManager.Refresh();
		}
	}
}