using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Attributes.Supports.Target{
	using Zios.Attributes.Supports;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Unity.Colors;
	using Zios.Unity.Components.DataBehaviour;
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.Extensions;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.Pref;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Extensions;
	using Zios.Unity.HookExtensions;
	using Zios.Unity.Locate;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[CustomPropertyDrawer(typeof(Target),true)]
	public class TargetDrawer : PropertyDrawer{
		public bool setup;
		public static GUISkin skin;
		public override void OnGUI(Rect area,SerializedProperty property,GUIContent label){
			EditorUI.Reset();
			property.serializedObject.Update();
			var target = property.GetObject<Target>();
			TargetDrawer.Draw(area,target,label);
		}
		public static void Draw(Rect area,Target target,GUIContent label){
			if(target.parent.IsNull()){return;}
			if(TargetDrawer.skin.IsNull()){
				var skin = EditorGUIUtility.isProSkin || EditorPref.Get<bool>("Zios.Theme.Dark",false) ? "Dark" : "Light";
				TargetDrawer.skin = UnityFile.GetAsset<GUISkin>("Gentleface-" + skin + ".guiskin");
			}
			var toggleRect = new Rect(area);
			var propertyRect = new Rect(area);
			var labelWidth = label.text.IsEmpty() ? 0 : EditorGUIUtility.labelWidth;
			propertyRect.x += labelWidth + 18;
			propertyRect.width -= labelWidth + 18;
			toggleRect.x += labelWidth;
			toggleRect.width = 18;
			var previousMode = target.mode == TargetMode.Direct;
			var currentMode = previousMode.Draw(toggleRect,"",TargetDrawer.skin.GetStyle("TargetToggle"));
			if(previousMode != currentMode){
				target.mode = target.mode == TargetMode.Direct ? TargetMode.Search : TargetMode.Direct;
			}
			label.ToLabel().DrawLabel(area,null,true);
			ProxyEditor.RecordObject(target.parent,"Target Changes");
			if(target.mode == TargetMode.Direct){
				target.directObject = target.directObject.Draw<GameObject>(propertyRect,"",true);
			}
			else{
				target.Verify();
				var faded = GUI.skin.textField.Background("").TextColor(GUI.skin.textField.normal.textColor.SetAlpha(0.75f)).ContentOffset(-3,0).UseState("normal");
				var textRect = propertyRect;
				var result = !target.searchObject.IsNull() ? target.searchObject.GetPath().Trim("/") : "Not Found.";
				var textSize = TargetDrawer.skin.textField.CalcSize(new GUIContent(target.search));
				var subtleSize = faded.CalcSize(new GUIContent(result));
				var subtleX = propertyRect.x+propertyRect.width-subtleSize.x;
				var subtleWidth = subtleSize.x;
				var minimumX = propertyRect.x+textSize.x+3;
				if(subtleX < minimumX){
					subtleWidth -= (minimumX-subtleX);
					subtleX = minimumX;
				}
				propertyRect = propertyRect.SetX(subtleX).SetWidth(subtleWidth);
				EditorGUIUtility.AddCursorRect(propertyRect,MouseCursor.Zoom);
				if(!target.searchObject.IsNull() && propertyRect.Clicked(0)){
					Selection.activeGameObject = target.searchObject;
					Event.current.Use();
				}
				target.search = target.search.Draw(textRect);
				result.ToLabel().DrawLabel(propertyRect,faded);
			}
			if(GUI.changed && !target.IsNull()){
				target.Search();
				if(target.parent is DataBehaviour){
					var parent = target.parent.As<DataBehaviour>();
					parent.DelayCallHook(parent.path,"Unity/Validate");
					ProxyEditor.SetDirty(parent);
				}
			}
		}
	}
}