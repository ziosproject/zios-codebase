using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityObject = UnityEngine.Object;
namespace Zios.Attributes.Supports{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Log;
	using Zios.Supports.Hook;
	using Zios.Unity.Extensions;
	using Zios.Unity.Locate;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	public enum TargetMode{Search,Direct};
	[Serializable]
	public class Target{
		public static string defaultSearch = "[Self]";
		private List<GameObject> special = new List<GameObject>();
		private List<string> specialNames = new List<string>();
		public bool disabled;
		public string search = "";
		public GameObject directObject;
		public GameObject searchObject;
		public UnityObject parent;
		public TargetMode mode = TargetMode.Search;
		public string path;
		private bool verified;
		private string fallbackSearch = "";
		public static implicit operator Transform(Target value){return value.Get().transform;}
		public static implicit operator GameObject(Target value){return value.Get();}
		public static implicit operator UnityObject(Target value){return value.Get();}
		public GameObject Get(){
			this.Verify();
			var result = this.mode == TargetMode.Search ? this.searchObject : this.directObject;
			if(result.IsNull() && Application.isPlaying){
				Log.AddLimit(this,"[Target] No target found for : " + this.path).Type("Warning").Target(this.parent).Show();
			}
			return result;
		}
		public void Verify(){
			if(!this.verified && this.mode == TargetMode.Search && this.searchObject.IsNull()){
				this.Search();
				this.verified = false;
			}
		}
		public void Clear(){
			if(!this.disabled){
				this.parent.GetHook("Unity/Validate").Add(this.Search).Remove();
				this.parent.GetHook("DataBehaviour/ComponentsChanged").Add(this.Search).Remove();
				this.disabled = true;
			}
		}
		public void Setup(string path,UnityObject parent){
			this.disabled = false;
			if(parent.Is<Component>()){
				var component = parent.As<Component>();
				this.path = component.GetPath() + "/" + path;
				this.parent = parent;
				if(!Application.isPlaying){
					this.AddSpecial("[This]",component.gameObject);
					this.AddSpecial("[Self]",component.gameObject);
					this.AddSpecial("[Next]",component.gameObject.GetNextSibling(true));
					this.AddSpecial("[Previous]",component.gameObject.GetPreviousSibling(true));
					this.AddSpecial("[NextEnabled]",component.gameObject.GetNextSibling());
					this.AddSpecial("[PreviousEnabled]",component.gameObject.GetPreviousSibling());
					//this.AddSpecial("[Root]",component.gameObject.GetPrefabRoot());
					Hook.Get(parent,"Unity/Validate").Add(this.Search);
					Hook.Get(parent,"DataBehaviour/ComponentsChanged").Add(this.Search);
					/*if(parent is StateBehaviour){
						var state = (StateBehaviour)parent;
						GameObject stateObject = state.gameObject;
						GameObject parentObject = state.gameObject;
						if(state.controller != null){
							stateObject = state.controller.gameObject;
							parentObject = state.controller.gameObject;
							if(state.controller.controller != null){
								parentObject = state.controller.controller.gameObject;
							}
						}
						this.AddSpecial("[ParentController]",parentObject);
						this.AddSpecial("[Controller]",stateObject);
						this.AddSpecial("[State]",state.gameObject);
					}*/
					this.SetFallback(Target.defaultSearch);
					if(this.searchObject.IsNull()){
						this.Search();
					}
				}
			}
		}
		public void SetFallback(string name){this.fallbackSearch = name;}
		public void AddSpecial(string name,GameObject target){
			if(target.IsNull()){target = this.parent.Is<Component>() ? this.parent.As<Component>().gameObject : null;}
			if(!this.specialNames.Any(x=>x.Contains(name,true))){
				this.specialNames.Add(name);
				this.special.Add(target);
			}
			else{
				var index = this.specialNames.FindIndex(x=>x.Contains(name,true));
				this.special[index] = target;
			}
		}
		public void Search(){
			if(this.mode != TargetMode.Search){return;}
			if(this.search.IsEmpty()){this.search = this.fallbackSearch;}
			if(this.search.IsEmpty()){return;}
			var search = this.search.Replace("\\","/");
			if(!search.IsEmpty()){
				for(var index=0;index<this.special.Count;++index){
					var specialName = this.specialNames[index];
					var special = this.special[index];
					if(!special.IsNull() && search.Contains(specialName,true)){
						var specialPath = special.GetPath();
						search = search.Replace(specialName,specialPath,true);
					}
				}
				if(search.ContainsAny("/",".")){
					var parts = search.Split("/");
					var total = "";
					GameObject current = null;
					for(var index=0;index<parts.Length;++index){
						var part = parts[index];
						current = GameObject.Find(total);
						if(part.IsEmpty()){continue;}
						if(part == ".." || part == "."){
							if(total.IsEmpty()){
								var specialIndex = this.specialNames.FindIndex(x=>x.Contains("[this]",true));
								current = specialIndex != -1 ? this.special[index] : null;
								if(!current.IsNull()){
									if(part == ".."){
										total = current.GetParent().IsNull() ? "" : current.GetParent().GetPath();
									}
									else{total = current.GetPath();}
								}
								continue;
							}
							current = GameObject.Find(total);
							if(!current.IsNull()){
								if(part == ".."){
									total = current.GetParent().IsNull() ? "" : current.GetParent().GetPath();
								}
								continue;
							}
						}
						var next = GameObject.Find(total+part+"/");
						if(next.IsNull() && !current.IsNull() && Attribute.lookup.ContainsKey(current)){
							var match = Attribute.lookup[current].Where(x=>x.Value.info.name.Matches(part)).FirstOrDefault().Value;
							if(match is AttributeGameObject){
								next = match.As<AttributeGameObject>().Get();
								if(!next.IsNull()){
									total = next.GetPath();
								}
								continue;
							}
						}
						total += part + "/";
					}
					search = total;
				}
				this.searchObject = Locate.Find(search);
			}
		}
	}
}