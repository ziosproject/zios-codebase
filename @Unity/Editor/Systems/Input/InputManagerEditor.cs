using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Input{
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.MonoBehaviourEditor;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Inputs;
	//asm Zios.Unity.Editor.Inspectors;
	//asm Zios.Reflection
	[CustomEditor(typeof(InputManager))]
	public class InputManagerEditor : MonoBehaviourEditor{
		public override void OnInspectorGUI(){
			this.title = "Input";
			this.header = this.header ?? UnityFile.GetAsset<Texture2D>("InputIcon.png");
			var target = InputManager.Get();
			target.groups.Draw("Groups");
			//target.DrawFields("");
			if(ProxyEditor.IsPlaying()){
				var current =  Event.current;
				if(current.isKey || current.shift || current.alt || current.control || current.command){
					if(!target.devices.Exists(x=>x.name=="Keyboard")){
						target.devices.Add(new InputDevice("Keyboard"));
					}
				}
			}
		}
	}
}