using UnityEngine;
namespace Zios.Unity.Editor.Console{
	using Zios.Extensions;
	using Zios.SystemAttributes;
	using Zios.Unity.Console;
	using Zios.Unity.Editor.File;
	//asm UnityEditor;
	[AutoInitialize(15)]
	public class ConsoleEditor{
		static ConsoleEditor(){
			var console = Console.Get();
			if(!console.IsNull() && console.skin.IsNull()){
				console.skin = UnityFile.GetAsset<GUISkin>("Console.guiskin");
				console.background = UnityFile.GetAsset<Material>("ConsoleBackground.mat");
				console.inputBackground = UnityFile.GetAsset<Material>("ConsoleInput.mat");
				console.textArrow = UnityFile.GetAsset<Material>("ConsoleArrow.mat");
			}
		}
	}
}