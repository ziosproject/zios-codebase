using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityObject = UnityEngine.Object;
namespace Zios.Unity.Editor.Reflection{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Unity.Proxy;
	//asm UnityEditor;
	public static class ReflectionUnity{
		private static Dictionary<string,Type> types = new Dictionary<string,Type>();
		static ReflectionUnity(){
			Reflection.clearEvents.Add(ReflectionUnity.types.Clear);
		}
		public static Type GetType(string name){
			if(ReflectionUnity.types.ContainsKey(name)){return ReflectionUnity.types[name];}
			var fullCheck = name.ContainsAny(".","+");
			var alternative = name.ReplaceLast(".","+");
			var term = alternative.Split("+").Last();
			foreach(var type in typeof(UnityEditor.Editor).Assembly.GetTypes()){
				var deepMatch = fullCheck && (type.FullName.Contains(name) || type.FullName.Contains(alternative)) && term.Matches(type.Name,true);
				if(type.Name == name || deepMatch){
					ReflectionUnity.types[name] = type;
					return type;
				}
			}
			foreach(var type in typeof(UnityObject).Assembly.GetTypes()){
				var deepMatch = fullCheck && (type.FullName.Contains(name) || type.FullName.Contains(alternative)) && term.Matches(type.Name,true);
				if(type.Name == name || deepMatch){
					ReflectionUnity.types[name] = type;
					return type;
				}
			}
			return null;
		}
	}
	public static class ReflectionUnityExtensions{
		public static bool IsExpanded(this UnityObject current){
			var editorUtility = ReflectionUnity.GetType("InternalEditorUtility");
			return editorUtility.Call<bool>("GetIsInspectorExpanded",current);
		}
		public static void SetExpanded(this UnityObject current,bool state){
			var editorUtility = ReflectionUnity.GetType("InternalEditorUtility");
			editorUtility.Call("SetIsInspectorExpanded",current,state);
		}
		public static void Destroy(this UnityObject target,bool destroyAssets=false){
			if(target.IsNull()){return;}
			if(target is Component){
				var component = target.As<Component>();
				if(component.gameObject.IsNull()){return;}
			}
			if(!Proxy.IsPlaying()){UnityObject.DestroyImmediate(target,destroyAssets);}
			else{UnityObject.Destroy(target);}
		}
	}
}