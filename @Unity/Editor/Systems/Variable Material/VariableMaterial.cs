using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityObject = UnityEngine.Object;
namespace Zios.Unity.Editor.VariableMaterial{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Log;
	using Zios.Reflection;
	using Zios.Unity.Call;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Proxy;
	public class VariableMaterial{
		public static bool debug;
		public static bool dirty;
		public static bool delay;
		public static bool force;
		public static Action writes = ()=>{};
		public static Action updates = ()=>{};
		public static bool IsBroken(Material material){
			if(material.shader.name.Contains("Hidden/InternalErrorShader")){
				foreach(var keyword in material.shaderKeywords){
					if(keyword.Contains("VARIABLE_MATERIAL_")){
						return true;
					}
				}
			}
			return false;
		}
		public static List<Material> GetAll(){
			var materials = UnityFile.GetAssets<Material>("*.mat");
			var variableMaterials = new List<Material>();
			foreach(var material in materials){
				if(material.IsNull()){continue;}
				var editorName = material.shader.GetVariable<string>("customEditor");
				if(editorName.Contains("VariableMaterialEditor") || VariableMaterial.IsBroken(material)){
					variableMaterials.Add(material);
				}
			}
			return variableMaterials;
		}
		public static void Refresh(params UnityObject[] targets){
			VariableMaterial.Refresh(false,targets);
		}
		public static void Refresh(bool delay,params UnityObject[] targets){
			VariableMaterial.delay = delay;
			foreach(var target in targets){
				var material = (Material)target;
				var isFlat = material.shader != null && material.shader.name.Contains("#");
				if(isFlat || VariableMaterial.IsBroken(material)){
					VariableMaterial.Flatten(target);
				}
			}
		}
		public static FileData GetParentShader(UnityObject target){
			var material = (Material)target;
			FileData file;
			if(material.shader.name.Contains("Hidden/InternalErrorShader")){
				foreach(var keyword in material.shaderKeywords){
					if(keyword.Contains("VARIABLE_MATERIAL_")){
						var shaderName = keyword.Split("_").Skip(2).Join("_").ToLower();
						file = File.Find(shaderName+".shader",false);
						if(file.IsNull()){file = File.Find(shaderName+".zshader",false);}
						if(file.IsNull()){Log.Warning("[VariableMaterial] : Parent recovery shader missing : " + shaderName);}
						return file;
					}
				}
				Log.Warning("[VariableMaterial] : Parent shader missing : " + material.name);
			}
			file = UnityFile.Get(material.shader);
			if(!file.IsNull() && file.name.Contains("#")){
				var shaderName = file.name.Split("#")[0];
				file = File.Find(shaderName+".shader",false);
				if(file.IsNull()){file = File.Find(shaderName+".zshader",false);}
				if(file.IsNull()){Log.Warning("[VariableMaterial] : Parent shader/zshader not found : " + shaderName);}
			}
			return file;
		}
		public static void RefreshEditor(){
			if(VariableMaterial.updates.GetInvocationList().Length > 1){
				ProxyEditor.StartAssetEditing();
				VariableMaterial.writes();
				ProxyEditor.StopAssetEditing();
				//ProxyEditor.SaveAssets();
				ProxyEditor.RefreshAssets();
				VariableMaterial.updates();
			}
			VariableMaterial.updates = ()=>{};
			VariableMaterial.writes = ()=>{};
			VariableMaterial.dirty = true;
			VariableMaterial.delay = false;
			ProxyEditor.RebuildInspectors();
		}
		public static void Unflatten(params UnityObject[] targets){
			var shaderName = "";
			foreach(var target in targets){
				var material = (Material)target;
				var shaderFile = VariableMaterial.GetParentShader(target);
				if(shaderFile.IsNull()){continue;}
				shaderName = shaderFile.fullName;
				material.shader = shaderFile.GetAsset<Shader>();
			}
			VariableMaterial.dirty = true;
			if(VariableMaterial.debug){
				Log.Show("[VariableMaterial] " + shaderName + " -- " + targets.Length + " Unflattened.");
			}
			ProxyEditor.RebuildInspectors();
		}
		public static void Flatten(bool force,params UnityObject[] targets){
			VariableMaterial.force = force;
			VariableMaterial.Flatten(targets);
		}
		public static void Flatten(params UnityObject[] targets){
			var originalName = "";
			foreach(var target in targets){
				var material = (Material)target;
				var shaderFile = VariableMaterial.GetParentShader(target);
				if(shaderFile.IsNull()){continue;}
				var timestamp = shaderFile.GetModifiedDate("MdyyHmmff");
				var projectPath = Application.dataPath+"/"+"Shaders";
				var hash = timestamp + "-" + material.shaderKeywords.Join(" ").ToMD5();
				var folderPath = projectPath+"/"+shaderFile.name+"/";
				var outputPath = folderPath+shaderFile.name+"#"+hash+".shader";
				Action update = ()=>{
					ProxyEditor.RecordObject(material,"Variable Material - Shader Set");
					material.EnableKeyword("VARIABLE_MATERIAL_"+shaderFile.name.ToUpper());
					material.shader = UnityFile.GetAsset<Shader>(outputPath);
					ProxyEditor.SetAssetDirty(material);
					if(VariableMaterial.debug){Log.Show("[VariableMaterial] Shader set " + outputPath);}
				};
				if(!VariableMaterial.force && File.Exists(outputPath)){
					VariableMaterial.updates += update;
					continue;
				}
				originalName = shaderFile.fullName;
				var text = shaderFile.ReadText();
				var shaderName = text.Parse("Shader ","{").Trim(' ','"');
				if(shaderName.Contains("#")){continue;}
				var output = "Shader " + '"' + "Hidden/"+shaderName+"#"+hash+'"'+"{\r\n";
				var allowed = new Stack<bool?>();
				var tabs = -1;
				text = text.Replace("\\\r\n","");
				var lineNumber = 0;
				foreach(var current in text.Split("\r\n").Skip(1)){
					lineNumber += 1;
					if(current.IsEmpty()){continue;}
					var line = current;
					var hideBlock = allowed.Count > 0 && allowed.Peek() != true;
					var allowedBranch = line.ContainsAny("#else","#elif") && allowed.Peek() != null;
					var ignoredBranch = line.Contains("@#");
					//if(line.ContainsAny("[KeywordEnum","[Toggle")){continue;}
					if(!ignoredBranch && line.Contains("#endif")){
						allowed.Pop();
						if(allowed.Count == 0){tabs = -1;}
						continue;
					}
					if(hideBlock && !allowedBranch){
						if(!ignoredBranch && line.ContainsAny("#if")){allowed.Push(null);}
						continue;
					}
					if(ignoredBranch){
						var end = line.Contains("#end");
						var include = line.Contains("#include");
						if(tabs < 0){tabs = line.Length - line.TrimStart().Length;}
						if(end){tabs -= 1;}
						line = new string('\t',tabs) + line.TrimStart();
						output += line.Replace("@#","#") + "\r\n";
						if(!end && !include){tabs += 1;}
						continue;
					}
					if(line.Contains("#include")){line = line.Replace("#include \"","#include \"../");}
					if(line.Contains("#pragma shader_feature")){continue;}
					if(line.Contains("#pragma multi_compile ")){continue;}
					if(line.ContainsAny("#if","#elif","#else")){
						var useBlock = false;
						if(line.ContainsAny("#else","#elif")){
							var lastAllowed = allowed.Pop() == true;
							if(lastAllowed){
								allowed.Push(null);
								continue;
							}
							useBlock = line.Contains("#else");
						}
						if(line.ContainsAny("#ifdef","#ifndef")){
							var hasTerm = material.shaderKeywords.Contains(line.Trim().Split(" ").Last());
							useBlock = line.Contains("#ifndef") ? !hasTerm : hasTerm;
						}
						else if(line.Contains("defined")){
							var orBlocks = line.Trim().Trim("#if ").Trim("#elif ").Split("||");
							foreach(var orBlock in orBlocks){
								var andBlocks = orBlock.Split("&&");
								foreach(var andBlock in andBlocks){
									var term = andBlock.Parse("defined(",")");
									var hasTerm = material.shaderKeywords.Contains(term);
									useBlock = andBlock.Contains("!") ? !hasTerm : hasTerm;
									if(!useBlock){break;}
								}
								if(useBlock){break;}
							}
						}
						allowed.Push(useBlock);
						if(useBlock && allowed.Count == 1 && tabs <= 0){
							tabs = line.Length - line.TrimStart().Length;
						}
						continue;
					}
					if(tabs >= 1){
						if(line.Contains("}") && !line.Contains("{")){tabs -= 1;}
						line = new string('\t',tabs) + line.TrimStart();
						if(line.Contains("{") && !line.Contains("}")){tabs += 1;}
					}
					output += line+"\r\n";
				}
				output = output.Replace("{\r\n\t\t\t}","{}");
				var pattern = output.Cut("{\r\n\t\t\t\treturn ",";\r\n\t\t\t");
				while(!pattern.IsEmpty()){
					var replace = pattern.Replace("\r\n","").Replace("\t","");
					output = output.ReplaceFirst(pattern,replace);
					pattern = output.Cut("{\r\n\t\t\t\treturn ",";\r\n\t\t\t");
				}
				if(output != text){
					Action write = ()=>{
						File.Create(outputPath).Write(output);
					};
					VariableMaterial.writes += write;
					VariableMaterial.updates += update;
				}
			}
			if(VariableMaterial.debug){
				Log.Show("[VariableMaterial] " + originalName + " -- " + targets.Length + " flattened.");
			}
			if(VariableMaterial.delay){
				Call.Delay(VariableMaterial.RefreshEditor,0.5f);
				return;
			}
			VariableMaterial.RefreshEditor();
			VariableMaterial.force = false;
		}
	}
}