using UnityEditor;
namespace Zios.Unity.Editor.Inspectors{
	using Zios.Extensions;
	using Zios.Reflection;
	using Zios.Unity.Call;
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.File;
	//asm Zios.File;
	using Editor = UnityEditor.Editor;
	[CustomEditor(typeof(DefaultAsset))]
	public class DefaultAssetEditor : Editor{
		public Editor instance;
		public override void OnInspectorGUI(){
			EditorUI.Reset();
			if(!this.instance.IsNull()){
				try{this.instance.OnInspectorGUI();}
				catch{
					Selection.activeObject = null;
					Call.Delay(()=>{Selection.activeObject = this.target;});
				}
				return;
			}
			var file = UnityFile.Get(this.target);
			if(file != null){
				var prefix = file.isFolder ? "Folder" : "File";
				var format = file.isFolder ? this.target.name : file.extension.ToUpper();
				var editorName = prefix + format + "Editor";
				var type = Reflection.GetType(editorName);
				if(type != null && type.IsSubclassOf(typeof(Editor))){
					this.instance = Editor.CreateEditor(this.target,type);
					this.instance.OnInspectorGUI();
				}
			}
		}
	}
}