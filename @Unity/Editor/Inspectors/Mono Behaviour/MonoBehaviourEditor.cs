using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using MenuFunction = UnityEditor.GenericMenu.MenuFunction;
namespace Zios.Unity.Editor.MonoBehaviourEditor{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Shortcuts;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Call;
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.Extensions;
	using Zios.Unity.Editor.Inspectors;
	using Zios.Unity.Editor.Pref;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Extensions;
	using Zios.Unity.HookExtensions;
	using Zios.Unity.Locate;
	using Zios.Unity.Proxy;
	using Zios.Unity.Time;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	using Editor = UnityEditor.Editor;
	[CustomEditor(typeof(MonoBehaviour),true)][CanEditMultipleObjects]
	public class MonoBehaviourEditor : HeaderEditor{
		private static GameObject[] sorting;
		private static int processIndex;
		public static Dictionary<Type,Dictionary<string,object>> defaults = new Dictionary<Type,Dictionary<string,object>>();
		public static float resumeHierarchyTime = -1;
		public static Dictionary<Editor,bool> offScreen = new Dictionary<Editor,bool>();
		public bool hideDefault;
		public bool setup;
		public bool changed;
		public List<SerializedProperty> properties = new List<SerializedProperty>();
		public List<SerializedProperty> hidden = new List<SerializedProperty>();
		public Dictionary<string,object> dictionaries = new Dictionary<string,object>();
		public Dictionary<SerializedProperty,Rect> propertyArea = new Dictionary<SerializedProperty,Rect>();
		public Dictionary<SerializedProperty,bool> propertyVisible = new Dictionary<SerializedProperty,bool>();
		public Rect area;
		public Rect areaStart;
		public Rect areaEnd;
		public bool areaBegan;
		public bool showAll;
		public bool visible = true;
		public Method dirtyEvent;
		public MonoBehaviour Get(){
			return (MonoBehaviour)this.target;
		}
		public override void OnInspectorGUI(){
			if(EditorPref.Get<bool>("MonoBehaviourEditor-AlwaysDraw")){ProxyEditor.RepaintInspectors();}
			EditorUI.Reset();
			ProxyEditor.GetInspector(this).SetTitle(this.title);
			ProxyEditor.GetInspectors().ForEach(x=>x.wantsMouseMove = true);
			if(!Event.current.IsUseful()){return;}
			if(this.target is MonoBehaviour && this.target.As<MonoBehaviour>().InPrefabFile()){return;}
			this.BeginArea();
			var fastInspector = EditorPref.Get<bool>("MonoBehaviourEditor-FastInspector");
			/*if(fastInspector && MonoBehaviourEditor.offScreen.ContainsKey(this)){
				GUILayout.Space(this.area.height);
				this.CheckChanges();
				return;
			}*/
			if(Event.current.type == EventType.MouseMove){
				Call.Delay(ProxyEditor.RepaintInspectors,0.1f);
			}
			var hideAllDefault = EditorPref.Get<bool>("MonoBehaviourEditor-HideAllDefault",false);
			this.hideDefault = EditorPref.Get<bool>("MonoBehaviourEditor-"+this.target.GetInstanceID()+"HideDefault",false);
			var hideDefault = hideAllDefault || this.hideDefault;
			if(hideDefault){this.SortDefaults();}
			this.serializedObject.Update();
			this.SortProperties();
			this.Setup();
			var type = this.target.GetType();
			this.changed = false;
			var showAdvanced = EditorPref.Get<bool>("MonoBehaviourEditor-Advanced");
			var showInternal = EditorPref.Get<bool>("MonoBehaviourEditor-Internal");
			var showDictionary = EditorPref.Get<bool>("MonoBehaviourEditor-Dictionary");
			EditorGUILayout.BeginVertical();
			foreach(var property in this.properties){
				var target = this.serializedObject.targetObject;
				var isInternal = target.HasAttribute<Internal>(property.name);
				var isAdvanced = target.HasAttribute<Advanced>(property.name);
				var isReadOnly = isInternal || target.HasAttribute<ReadOnly>(property.name);
				var isHidden = !this.showAll && this.hidden.Contains(property);
				if(isAdvanced && !showAdvanced){isHidden = true;}
				if(isInternal && !showInternal){isHidden = true;}
				var currentValue = property.GetObject<object>();
				var hasDefault = MonoBehaviourEditor.defaults.ContainsKey(type) && MonoBehaviourEditor.defaults[type].ContainsKey(property.name);
				if(!this.showAll && hideDefault && hasDefault){
					var defaultValue = MonoBehaviourEditor.defaults[type][property.name];
					if(defaultValue.IsNull()){continue;}
					var isDefault = defaultValue.Equals(currentValue);
					if(isDefault){isHidden = true;}
				}
				if(!isHidden){
					var hasArea = this.propertyArea.ContainsKey(property);
					if(hasArea){
						if(Event.current.shift){
							var canHide = (this.properties.Count - this.hidden.Count) > 1;
							if(this.propertyArea[property].Clicked(0) && canHide){
									var path = "MonoBehaviourEditor-PropertyHide-"+this.target.GetInstanceID()+"-"+property.propertyPath;
									EditorPref.Set<bool>(path,true);
									this.hidden.Add(property);
							}
							if(this.propertyArea[property].Clicked(1)){this.DrawShiftContextMenu();}
						}
						if(fastInspector && this.propertyVisible.ContainsKey(property) && !this.propertyVisible[property]){
							GUILayout.Space(this.propertyArea[property].height);
							continue;
						}
					}
					string propertyName = null;
					if(isReadOnly){GUI.enabled = false;}
					GUI.changed = false;
					EditorGUILayout.BeginVertical();
					if(hasArea){EditorGUI.BeginProperty(this.propertyArea[property],new GUIContent(property.displayName),property);}
					property.Draw(propertyName);
					if(hasArea){EditorGUI.EndProperty();}
					EditorGUILayout.EndVertical();
					this.changed = this.changed || GUI.changed;
					if(isReadOnly){GUI.enabled = true;}
					if(Proxy.IsRepainting()){
						var area = GUILayoutUtility.GetLastRect();
						if(!area.IsEmpty()){this.propertyArea[property] = area.AddHeight(2);}
					}
				}
			}
			if(showDictionary){
				GUI.enabled = false;
				foreach(var item in this.dictionaries){
					item.Value.DrawAuto(item.Key,null,true);
				}
				GUI.enabled = true;
			}
			EditorGUILayout.EndVertical();
			this.EndArea();
			this.DrawContextMenu();
			if(this.changed){
				this.serializedObject.ApplyModifiedProperties();
				//this.serializedObject.targetObject.Call("OnValidate");
				ProxyEditor.SetDirty(this.serializedObject.targetObject,false,true);
			}
			this.CheckChanges();
			if(Proxy.IsRepainting()){
				ProxyEditor.GetInspector(this).SetTitle("Inspector");
			}
		}
		public void CheckChanges(){
			if(Proxy.IsRepainting()){
				var fastInspector = EditorPref.Get<bool>("MonoBehaviourEditor-FastInspector");
				var mousePosition = Event.current.mousePosition;
				this.showAll = Event.current.alt && this.area.Contains(mousePosition);
				if(this.dirtyEvent != null){
					this.dirtyEvent();
					this.dirtyEvent = null;
				}
				var needsRepaint = false;
				if(fastInspector){
					foreach(var property in this.properties){
						if(this.propertyArea.ContainsKey(property)){
							var valid = this.propertyArea[property].InInspectorWindow();
							if(valid != this.propertyVisible.AddNew(property)){
								this.propertyVisible[property] = valid;
								needsRepaint = true;
							}
						}
					}
					/*if(!this.area.IsEmpty() && !this.area.InInspectorWindow()){
						MonoBehaviourEditor.offScreen[this] = true;
					}*/
				}
				if(this.showAll || needsRepaint){this.Repaint();}
			}
		}
		public void BeginArea(){
			//if(this.areaBegan){return;}
			var areaStart = GUILayoutUtility.GetRect(0,0);
			if(!areaStart.IsEmpty() && this.areaStart != areaStart){
				this.AddDirty(()=>{
					this.areaStart = areaStart;
					this.propertyArea.Clear();
					MonoBehaviourEditor.offScreen.Clear();
					this.Repaint();
				});
			}
			this.areaBegan = true;
		}
		public void EndArea(){
			var areaEnd = GUILayoutUtility.GetRect(0,0);
			if(!areaEnd.IsEmpty() && this.areaEnd != areaEnd){
				this.AddDirty(()=>{
					this.area = this.areaStart;
					this.area.height = (areaEnd.y - this.areaStart.y);
				});
			}
			this.areaBegan = false;
		}
		public void Setup(){
			if(this.properties.Count > 0 && !this.setup){
				foreach(var property in this.properties){
					var path = "MonoBehaviourEditor-PropertyHide-"+this.target.GetInstanceID()+"-"+property.propertyPath;
					if(EditorPref.Get<bool>(path,false)){
						this.hidden.Add(property);
					}
				}
				this.setup = true;
			}
		}
		public void SortDefaults(){
			var type = this.target.GetType();
			var defaults = MonoBehaviourEditor.defaults;
			if(!(this.target is MonoBehaviour)){return;}
			if(!defaults.ContainsKey(type)){
				Hook.Get("UnityEditor/HierarchyChanged").Disable();
				defaults.AddNew(type);
				var script = (MonoBehaviour)this.target;
				var component = script.gameObject.AddComponent(type);
				foreach(var item in component.GetVariables()){
					try{
						var name = item.Key;
						var defaultValue = item.Value;
						defaults[type][name] = defaultValue;
					}
					catch{}
				}
				component.Destroy();
				MonoBehaviourEditor.resumeHierarchyTime = Time.Get() + 0.5f;
			}
			else if(MonoBehaviourEditor.resumeHierarchyTime != -1 && Time.Get() > MonoBehaviourEditor.resumeHierarchyTime){
				MonoBehaviourEditor.resumeHierarchyTime = -1;
				Hook.Get("UnityEditor/HierarchyChanged").Enable();
			}
		}
		public void SortProperties(){
			if(this.properties.Count < 1){
				var target = this.serializedObject.targetObject;
				if(target.IsNull()){return;}
				var property = this.serializedObject.GetIterator();
				property.NextVisible(true);
				while(property.NextVisible(false)){
					var realProperty = this.serializedObject.FindProperty(property.propertyPath);
					this.properties.Add(realProperty);
				}
				this.properties = this.properties.OrderBy(x=>target.HasAttribute<Internal>(x.name)).ToList();
				var filter = MemberFilter.FindFlags(Flags.instancePublic).Without<ObsoleteAttribute>();
				foreach(var item in target.GetVariables(filter)){
					if(item.Value.IsNull()){continue;}
					if(item.Value.IsDictionary()){
						this.dictionaries[item.Key] = item.Value;
					}
				}
			}
		}
		public void AddDirty(Method method){this.dirtyEvent += method;}
		public void DrawContextMenu(){
			if(this.area.Clicked(1)){
				var filter = MemberFilter.FindWith<ContextMenu>();
				var menu = new EditorMenu();
				foreach(var pair in this.GetMethods(filter)){
					var method = pair.Value;
					var name = method.GetAttribute<ContextMenu>().menuItem;
					menu.Add(name,false,method.GetAs<Action>(this));
				}
				var alwaysDraw = EditorPref.Get<bool>("MonoBehaviourEditor-AlwaysDraw");
				menu.AddSeparator();
				menu.Add("Always Draw Inspector",alwaysDraw,()=>EditorPref.Toggle("MonoBehaviourEditor-AlwaysDraw"));
				menu.Draw();
			}
		}
		public void DrawShiftContextMenu(){
			var menu = new GenericMenu();
			MenuFunction toggleAdvanced = ()=>EditorPref.Toggle("MonoBehaviourEditor-Advanced");
			MenuFunction toggleInternal = ()=>EditorPref.Toggle("MonoBehaviourEditor-Internal");
			MenuFunction toggleDictionary = ()=>EditorPref.Toggle("MonoBehaviourEditor-Dictionary");
			MenuFunction hideAllDefaults = ()=>EditorPref.Toggle("MonoBehaviourEditor-HideAllDefault");
			MenuFunction hideLocalDefaults = ()=>{
				this.hideDefault = !this.hideDefault;
				EditorPref.Set<bool>("MonoBehaviourEditor-"+this.target.GetInstanceID()+"HideDefault",this.hideDefault);
			};
			menu.AddItem(new GUIContent("Advanced"),EditorPref.Get<bool>("MonoBehaviourEditor-Advanced"),toggleAdvanced);
			menu.AddItem(new GUIContent("Internal"),EditorPref.Get<bool>("MonoBehaviourEditor-Internal"),toggleInternal);
			menu.AddItem(new GUIContent("Dictionary"),EditorPref.Get<bool>("MonoBehaviourEditor-Dictionary"),toggleDictionary);
			menu.AddSeparator("");
			menu.AddItem(new GUIContent("Defaults/Hide All"),EditorPref.Get<bool>("MonoBehaviourEditor-HideAllDefault"),hideAllDefaults);
			menu.AddItem(new GUIContent("Defaults/Hide Local"),this.hideDefault,hideLocalDefaults);
			if(this.hidden.Count > 0){
				MenuFunction unhideAll = ()=>{
					foreach(var property in this.hidden){
						var path = "MonoBehaviourEditor-PropertyHide-"+this.target.GetInstanceID()+"-"+property.propertyPath;
						EditorPref.Set<bool>(path,false);
					}
					this.hidden.Clear();
				};
				menu.AddSeparator("");
				menu.AddItem(new GUIContent("Unhide/All"),false,unhideAll);
				foreach(var property in this.hidden){
					var target = property;
					MenuFunction unhide = ()=>{
						var path = "MonoBehaviourEditor-PropertyHide-"+this.target.GetInstanceID()+"-"+property.propertyPath;
						EditorPref.Set<bool>(path,false);
						this.hidden.Remove(target);
					};
					menu.AddItem(new GUIContent("Unhide/"+property.displayName),false,unhide);
				}
			}
			menu.ShowAsContext();
			Event.current.Use();
		}
		//===============
		// Editor
		//===============
		[MenuItem("Zios/GameObject/Apply Prefab (Selected) &#P")]
		public static void ApplyPrefabSelected(){
			Debug.Log("[DataBehaviour] Applying prefab");
			foreach(var target in Selection.gameObjects){
				MonoBehaviourEditor.ApplyPrefabTarget(target);
			}
		}
		[MenuItem("Zios/GameObject/Sort Components (Selected)")]
		public static void SortSmartSelected(){
			foreach(var target in Selection.gameObjects){
				MonoBehaviourEditor.SortSmartTarget(target);
			}
		}
		[MenuItem("Zios/GameObject/Sort Components (All)")]
		public static void SortSmartAll(){
			MonoBehaviourEditor.sorting = Locate.GetSceneObjects();
			if(MonoBehaviourEditor.sorting.Length > 0){
				MonoBehaviourEditor.processIndex = 0;
				Hook.Get("UnityEditor/Update").Add(MonoBehaviourEditor.SortSmartNext);
			}
		}
		public static void SortSmartNext(){
			Hook.Get("UnityEditor/HierarchyChanged").Disable();
			var index = MonoBehaviourEditor.processIndex;
			var sorting = MonoBehaviourEditor.sorting;
			var canceled = true;
			if(index < MonoBehaviourEditor.sorting.Length-1){
				var current = MonoBehaviourEditor.sorting[index];
				var total = (float)index/sorting.Length;
				var message = index + " / " + sorting.Length + " -- " + current.GetPath();
				canceled = EditorUI.DrawProgressBar("Sorting All Components",message,total);
				current.PauseValidate();
				MonoBehaviourEditor.SortSmartTarget(current);
				current.ResumeValidate();
			}
			MonoBehaviourEditor.processIndex += 1;
			if(canceled || index+1 > sorting.Length-1){
				EditorUI.ClearProgressBar();
				Hook.Get("UnityEditor/Update").Add(MonoBehaviourEditor.SortSmartNext).Remove();
				Hook.Get("UnityEditor/HierarchyChanged").Enable();
			}
		}
		//===============
		// Shared
		//===============
		public static void SortSmartTarget(GameObject target){
			var components = target.GetComponents<Component>().ToList().OrderBy(x=>x.GetAlias()).ToArray();
			MonoBehaviourEditor.Sort(components);
			var controller = components.Find(x=>x.GetType().Name.Contains("State"));
			if(!controller.IsNull()){controller.MoveToTop();}
		}
		public static void Sort(Component[] components){
			foreach(var component in components){
				if(!component.hideFlags.Contains(HideFlags.HideInInspector)){
					component.MoveToBottom();
				}
			}
			foreach(var component in components){
				if(component.hideFlags.Contains(HideFlags.HideInInspector)){
					component.MoveToBottom();
				}
			}
		}
		public static void ApplyPrefabTarget(GameObject target){
			Hook.Get("UnityEditor/HierarchyChanged").Disable();
			target.PauseValidate();
			ProxyEditor.ApplyPrefab(target);
			target.ResumeValidate();
			Hook.Get("UnityEditor/HierarchyChanged").Enable();
		}
		//===============
		// Context
		//===============
		[ContextMenu("Sort (By Type)")]
		public void SortByType(){
			var components = this.Get().GetComponents<Component>().ToList().OrderBy(x=>x.GetType().Name).ToArray();
			MonoBehaviourEditor.Sort(components);
		}
		[ContextMenu("Sort (By Alias)")]
		public void SortByAlias(){
			var components = this.Get().GetComponents<Component>().ToList().OrderBy(x=>x.GetAlias()).ToArray();
			MonoBehaviourEditor.Sort(components);
		}
		[ContextMenu("Sort (Smart)")]
		public void SortSmart(){MonoBehaviourEditor.SortSmartTarget(this.Get().gameObject);}
		[ContextMenu("Move Up")]
		public void MoveItemUp(){this.Get().MoveUp();}
		[ContextMenu("Move Down")]
		public void MoveItemDown(){this.Get().MoveDown();}
		[ContextMenu("Move To Bottom")]
		public void MoveBottom(){this.Get().MoveToBottom();}
		[ContextMenu("Move To Top")]
		public void MoveTop(){this.Get().MoveToTop();}
		[ContextMenu("Apply Prefab")]
		public void ApplyPrefab(){MonoBehaviourEditor.ApplyPrefabTarget(this.Get().gameObject);}
	}
}