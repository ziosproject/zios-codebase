using UnityEditor;
using EditorCustom = UnityEditor.Editor;
namespace Zios.Unity.Editor.Inspectors{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Unity.Components.DisplayEditor;
	[CustomEditor(typeof(DisplayEditor))]
	public class DisplayEditorEditor : EditorCustom{
		public EditorCustom instance;
		public string lastName;
		public override void OnInspectorGUI(){
			this.DrawDefaultInspector();
			var editorName = this.target.As<DisplayEditor>().editorName;
			var type = Reflection.GetType(editorName);
			if(editorName != this.lastName){
				this.lastName = editorName;
				this.instance = null;
			}
			if(!type.IsNull() && this.instance.IsNull()){
				this.instance = EditorCustom.CreateEditor(this.target,type);
			}
			if(!this.instance.IsNull()){
				this.instance.OnInspectorGUI();
			}
		}
	}
}