using UnityEditor;
using UnityAction = UnityEngine.Events.UnityAction;
using UnityCallback = UnityEditor.EditorApplication.CallbackFunction;
using UnityObject = UnityEngine.Object;
using UnityUndo = UnityEditor.Undo;
namespace Zios.Unity.Editor.Hooks{
	using System.Collections;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Shortcuts;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Editor.Supports.Stepper;
	using Zios.Unity.Extensions;
	using Zios.Unity.HookExtensions;
	using Zios.Unity.Proxy;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[AutoInitialize(11)]
	public static class EditorHook{
		public static bool entering;
		public static bool exiting;
		static EditorHook(){
			EditorHook.entering = ProxyEditor.IsSwitching();
			Hook.Create("UnityEditor/GlobalEvent");
			Hook.Create("UnityEditor/Update");
			Hook.Create("UnityEditor/PrefabChanged");
			Hook.Create("UnityEditor/LightmapBaked");
			Hook.Create("UnityEditor/WindowsReordered");
			Hook.Create("UnityEditor/HierarchyChanged");
			Hook.Create("UnityEditor/Asset/Changed");
			Hook.Create("UnityEditor/Asset/Saving");
			Hook.Create("UnityEditor/Asset/Creating");
			Hook.Create("UnityEditor/Asset/Modifying");
			Hook.Create("UnityEditor/Asset/Deleting");
			Hook.Create("UnityEditor/Asset/Moving");
			Hook.Create("UnityEditor/Quit");
			Hook.Create("UnityEditor/ScriptLoaded");
			Hook.Create("UnityEditor/ProjectChanged");
			Hook.Create("UnityEditor/ModeChanged");
			Hook.Create("UnityEditor/EnterPlay");
			Hook.Create("UnityEditor/EnterPlayComplete");
			Hook.Create("UnityEditor/ExitPlay");
			Hook.Create("UnityEditor/ExitPlayComplete");
			Hook.Create("UnityEditor/SceneStarted");
			Hook.Create("UnityEditor/UndoFlushing");
			Hook.Create("UnityEditor/Undo");
			Hook.Create("UnityEditor/Redo");
			Hook.Create("UnityEditor/Camera/PostRender");
			Hook.Create("UnityEditor/Camera/PreRender");
			Hook.Create("UnityEditor/Camera/PreCull");
			Hook.Create("UnityEditor/RepaintInspectors");
			Hook.Create("UnityEditor/RecordObject");
			#if UNITY_5_0_OR_NEWER
			Camera.onPostRender += (Camera camera)=>Hook.Call("UnityEditor/Camera/PostRender",camera);
			Camera.onPreRender += (Camera camera)=>Hook.Call("UnityEditor/Camera/PreRender",camera);
			Camera.onPreCull += (Camera camera)=>Hook.Call("UnityEditor/Camera/PreCull",camera);
			Lightmapping.completed += ()=>Hook.Call("UnityEditor/LightmapBaked");
			#endif
			UnityUndo.willFlushUndoRecord += ()=>Hook.Call("UnityEditor/UndoFlushing");
			UnityUndo.undoRedoPerformed += ()=>Hook.Call("UnityEditor/Undo");
			UnityUndo.undoRedoPerformed += ()=>Hook.Call("UnityEditor/Redo");
			ProxyEditor.PrefabInstanceUpdated((target)=>Hook.Call("UnityEditor/PrefabChanged",target));
			ProxyEditor.ProjectChanged(()=>Hook.Call("UnityEditor/ProjectChanged"));
			Hook.Get("Unity/DeserializeComplete").Add(EditorHook.CheckState);
			EditorApplication.playModeStateChanged += (PlayModeStateChange state)=>{
				Hook.Call("UnityEditor/ModeChanged");
				var changing = ProxyEditor.IsChanging();
				var playing = Proxy.IsPlaying();
				if(changing && !playing){
					Hook.Call("UnityEditor/EnterPlay");
				}
				if(!changing && playing){
					Hook.Call("UnityEditor/ExitPlay");
					EditorHook.exiting = true;
				}
			};
			GameObjectExtensions.GetPathMethods.Append((current)=>{
				var type = ProxyEditor.GetPrefabType(current);
				if(type.MatchesAny("Prefab","ModelPrefab")){
					return "Prefab/"+current.transform.name;
				}
				return current.transform.name;
			});
			GameObjectExtensions.IsPrefabMethods.Append((current)=>{
				return !ProxyEditor.GetPrefabType(current.transform.root.gameObject).IsNull();
			});
			GameObjectExtensions.IsPrefabFileMethods.Append((current)=>{
				var assetPath = ProxyEditor.GetAssetPath(current.transform.root.gameObject);
				return !assetPath.IsEmpty();
			});
			Hook.Get("UnityEditor/RepaintInspectors").Add(ProxyEditor.RepaintInspectors);
			Hook.Get("UnityEditor/RecordObject").Add((object[] arguments)=>{
				var target = (UnityObject)arguments[0];
				var label = (string)arguments[1];
				ProxyEditor.RecordObject(target,label);
			});
			Hook.Get("UnityEditor/SetDirty").Add((object x)=>ProxyEditor.SetDirty((UnityObject)x));
			ProxyEditor.HierarchyChanged(()=>"Global".DelayCallHook("UnityEditor/HierarchyChanged",0.25f));
			EditorApplication.update += ()=>Hook.Call("UnityEditor/Update");
			UnityCallback windowEvent = ()=>Hook.Call("UnityEditor/WindowsReordered");
			UnityCallback globalEvent = ()=>Hook.Call("UnityEditor/GlobalEvent");
			var windowsReordered = typeof(EditorApplication).GetVariable<UnityCallback>("windowsReordered");
			var globalEventHandler = typeof(EditorApplication).GetVariable<UnityCallback>("globalEventHandler");
			var editorQuitHandler = typeof(EditorApplication).GetVariable<UnityAction>("editorApplicationQuit");
			var editorQuitEvent = new UnityAction(()=>Hook.Call("UnityEditor/Quit"));
			typeof(EditorApplication).SetVariable("windowsReordered",windowsReordered+windowEvent);
			typeof(EditorApplication).SetVariable("globalEventHandler",globalEventHandler+globalEvent);
			typeof(EditorApplication).SetVariable("editorApplicationQuit",editorQuitHandler+editorQuitEvent);
		}
		[UnityEditor.Callbacks.DidReloadScripts]
		public static void OnScriptsReloaded(){
			Hook.Call("UnityEditor/ScriptsLoaded");
		}
		public static void CheckState(){
			if(EditorHook.exiting){
				Hook.Call("UnityEditor/ExitPlayComplete");
				EditorHook.exiting = false;
			}
			var switching = ProxyEditor.IsSwitching();
			var playing = Proxy.IsPlaying();
			var isStarting = (!switching && !playing) || (switching && playing);
			if(isStarting){
				if(EditorHook.entering){
					Hook.Call("UnityEditor/EnterPlayComplete");
					EditorHook.entering = false;
				}
				Hook.Call("UnityEditor/SceneStart");
			}
		}
		public static void AddStepper(string eventName,MethodStep method,IList collection,int passes=1,bool inline=false){
			var stepper = new Stepper(method,null,collection,passes,inline);
			stepper.onEnd = ()=>Hook.Get(eventName).Add(stepper.Step).Remove();
			Hook.Get(eventName).Add(stepper.Step);
		}
	}
	public class UtilityListener : AssetPostprocessor{
		public static void OnPostprocessAllAssets(string[] imported,string[] deleted,string[] movedTo, string[] movedFrom){
			Hook.Call("UnityEditor/Asset/Changed");
		}
	}
	public class UtilityModificationListener : UnityEditor.AssetModificationProcessor{
		public static string[] OnWillSaveAssets(string[] paths){
			if(paths.Exists(x=>x.Contains(".unity"))){Hook.Call("UnityEditor/SceneSaving");}
			Hook.Call("UnityEditor/Asset/Saving",paths);
			Hook.Call("UnityEditor/Asset/Modifying",paths);
			return paths;
		}
		public static string OnWillCreateAssets(string path){
			Hook.Call("UnityEditor/Asset/Creating",path);
			Hook.Call("UnityEditor/Asset/Modifying",path);
			return path;
		}
		public static string[] OnWillDeleteAssets(string[] paths,RemoveAssetOptions option){
			Hook.Call("UnityEditor/Asset/Deleting",paths,option.ToName());
			Hook.Call("UnityEditor/Asset/Modifying",paths);
			return paths;
		}
		public static string OnWillMoveAssets(string path,string destination){
			Hook.Call("UnityEditor/Asset/Moving",path,destination);
			Hook.Call("UnityEditor/Asset/Modifying",path);
			return path;
		}
	}
}