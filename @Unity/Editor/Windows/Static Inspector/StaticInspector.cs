using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using MenuFunction = UnityEditor.GenericMenu.MenuFunction;
using UnityObject = UnityEngine.Object;
namespace Zios.Unity.Editor.Windows{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Supports.Accessor;
	using Zios.Supports.Hook;
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.Extensions;
	using Zios.Unity.Extensions;
	using Zios.Unity.Proxy;
	using Zios.Unity.SystemAttributes;
	using Zios.Unity.Time;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	public class StaticInspector : EditorWindow{
		public static string currentAssembly;
		public static string currentNamespace;
		public static string currentClass;
		private Vector2 scrollPosition;
		private Rect viewArea;
		private int selectedAssembly;
		private int selectedNamespace;
		private int selectedClass;
		private object activeClass;
		private bool setup;
		private float nextRepaint;
		private Rect labelArea;
		private Rect valueArea;
		private List<Assembly> assemblies = new List<Assembly>();
		private int setupIndex;
		private Dictionary<int,bool> foldoutState = new Dictionary<int,bool>();
		private Dictionary<string,Accessor> variables = new Dictionary<string,Accessor>();
		private SortedDictionary<string,SortedDictionary<string,List<string>>> classNames = new SortedDictionary<string,SortedDictionary<string,List<string>>>();
		private SortedDictionary<string,SortedDictionary<string,List<Type>>> classes = new SortedDictionary<string,SortedDictionary<string,List<Type>>>();
		[MenuItem("Zios/Window/Static Inspector",false,0)]
		private static void Init(){
			var window = (StaticInspector)EditorWindow.GetWindow(typeof(StaticInspector));
			window.position = new Rect(100,150,200,200);
			window.Start();
		}
		public void OnGUI(){
			Hook.Get("UnityEditor/Update").Add(this.Setup);
			if(this.assemblies.Count < 1){this.setup = false;}
			if(this.setup){
				this.DrawContext();
				this.DrawSelectors();
				this.DrawInspector();
			}
			if(Proxy.IsPlaying()){
				this.Setup();
				this.Repaint();
			}
		}
		public void Start(){}
		public void Setup(){
			if(!this.setup){
				if(this.assemblies.Count < 1){
					this.ResetIndexes(3);
					this.classNames.Clear();
					this.classes.Clear();
					this.setupIndex = 0;
					this.assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
				}
				if(this.setupIndex > this.assemblies.Count-1){
					this.setup = true;
					foreach(var assemblyItem in this.classNames){
						foreach(var namespaceItem in assemblyItem.Value){
							this.classes[assemblyItem.Key][namespaceItem.Key] = this.classes[assemblyItem.Key][namespaceItem.Key].OrderBy(x=>x.Name).ToList();
							this.classNames[assemblyItem.Key][namespaceItem.Key].Sort();
						}
					}
					EditorUI.ClearProgressBar();
					this.Repaint();
					return;
				}
				var assembly = this.assemblies[this.setupIndex];
				var assemblyName = assembly.GetName().Name.TrySplit(".",0);
				var allTypes = assembly.GetTypes();
				foreach(var type in allTypes){
					if(type == null || type.Name.Contains("_AnonStorey")){continue;}
					var space = type.Namespace.IsEmpty() ? "[None]" : type.Namespace;
					this.classes.AddNew(assemblyName).AddNew(space).Add(type);
					this.classNames.AddNew(assemblyName).AddNew(space).Add(type.Name);
				}
				var percent = (float)this.setupIndex/(float)this.assemblies.Count;
				EditorUI.DrawProgressBar("Loading","Sorting Data",percent);
				this.setupIndex += 1;
			}
			if(Time.Get() > this.nextRepaint){
				this.nextRepaint = Time.Get() + 0.25f;
				this.Repaint();
			}
		}
		public void Reset(){
			this.setup = false;
			this.assemblies.Clear();
			this.Setup();
		}
		public void DrawContext(){
			if(this.position.SetX(0).SetY(0).Clicked(1)){
				var menu = new GenericMenu();
				MenuFunction refresh = ()=>{this.Reset();};
				menu.AddItem(new GUIContent("Refresh"),false,refresh);
				menu.ShowAsContext();
			}
		}
		public void DrawSelectors(){
			this.SetTitle("Static");
			var fieldSize = this.position.width/3.0f - 6;
			var assemblyArea = new Rect(5,5,fieldSize,15);
			var namespaceArea = new Rect(5+(fieldSize)+2,5,fieldSize,15);
			var classArea = new Rect(5+(fieldSize*2)+4,5,fieldSize,15);
			GUI.changed = false;
			//=================
			// Assembly
			//=================
			var assemblyNames = this.classNames.Keys.ToArray();
			var assemblyIndex = assemblyNames.IndexOf(StaticInspector.currentAssembly);
			if(assemblyIndex == -1){assemblyIndex = 0;}
			this.selectedAssembly = assemblyNames.Draw(assemblyArea,assemblyIndex);
			if(GUI.changed){this.ResetIndexes(2);}
			StaticInspector.currentAssembly = assemblyNames[this.selectedAssembly];
			//=================
			// Namespace
			//=================
			var namespaces = this.classNames[StaticInspector.currentAssembly].Keys.ToArray();
			var namespaceIndex = namespaces.IndexOf(StaticInspector.currentNamespace);
			if(namespaceIndex == -1){namespaceIndex = 0;}
			this.selectedNamespace = namespaces.Draw(namespaceArea,namespaceIndex);
			if(GUI.changed){this.ResetIndexes(1);}
			StaticInspector.currentNamespace = namespaces[this.selectedNamespace];
			//=================
			// Class
			//=================
			var classes = this.classNames[StaticInspector.currentAssembly][StaticInspector.currentNamespace].ToArray();
			var classIndex = classes.IndexOf(StaticInspector.currentClass);
			if(classIndex == -1){classIndex = 0;}
			this.selectedClass = classes.Draw(classArea,classIndex);
			StaticInspector.currentClass = classes[this.selectedClass];
			this.activeClass = this.classes[StaticInspector.currentAssembly][StaticInspector.currentNamespace][this.selectedClass];
			if(GUI.changed){
				this.variables.Clear();
			}
		}
		public void DrawInspector(){
			this.scrollPosition = GUI.BeginScrollView(new Rect(0,25,Screen.width,Screen.height-45),this.scrollPosition,this.viewArea);
			if(this.activeClass != null && this.variables.Count < 1){
				var names = this.activeClass.GetVariables(Flags.allStatic);
				foreach(var item in names){
					var name = item.Key;
					try{
						var accessor = new Accessor(this.activeClass,name);
						this.variables[name] = accessor;
					}
					catch{}
				}
			}
			if(this.variables.Count > 0){
				this.labelArea = new Rect(13,10,this.position.width*0.415f,15);
				this.valueArea = new Rect(-13+this.position.width*0.415f,10,this.position.width*0.585f,15);
				foreach(var current in this.variables.Copy()){
					var name = current.Key;
					var value = current.Value.Get();
					var accessor = this.variables.ContainsKey(name) ? this.variables[name] : null;
					this.DrawValue(name,value,accessor);
				}
			}
			this.viewArea = this.viewArea.SetHeight(this.valueArea.y+22);
			GUI.EndScrollView();
		}
		public void UpdateValue(Accessor accessor,object value){
			if(accessor != null && GUI.changed){accessor.Set(value);}
		}
		public void DrawValue(string labelText,object value,Accessor accessor=null,int depth=0){
			if(labelText.Contains("$cache")){return;}
			labelText = labelText.ToTitleCase();
			if(value is UnityObject){labelText = labelText + " (" + value.GetType().Name + ")";}
			var labelDrawn = false;
			var label = new GUIContent(labelText);
			GUI.changed = false;
			var common = (value is string || value is bool || value is float || value is int || value is UnityObject || value is Enum || value is Vector2 || value is Vector3);
			if(common){
				label.ToLabel().DrawLabel(this.labelArea);
				labelDrawn = true;
			}
			var hash = 0;
			if(!value.IsNull()){
				hash = value.GetHashCode();
				this.foldoutState.AddNew(hash);
			}
			if(value is GameObject){
				var newValue = value.As<GameObject>().Draw<GameObject>(this.valueArea);
				this.UpdateValue(accessor,newValue);
			}
			else if(value is Component){
				var newValue = value.As<Component>().Draw<Component>(this.valueArea);
				this.UpdateValue(accessor,newValue);
			}
			else if(value.IsNull()){return;}
			else if(value is Enum){
				var name = accessor != null ? accessor.name : "";
				var scope = accessor != null ? accessor.scope : null;
				if(accessor != null && scope.HasAttribute(name,typeof(EnumMaskAttribute))){
					var newValue = ((Enum)value).DrawMask(this.valueArea);
					this.UpdateValue(accessor,newValue);
				}
				else{
					var newValue = ((Enum)value).Draw(this.valueArea);
					this.UpdateValue(accessor,newValue);
				}
			}
			else if(value is string){
				var newValue = ((string)value).Draw(this.valueArea);
				this.UpdateValue(accessor,newValue);
			}
			else if(value is bool){
				var newValue = ((bool)value).Draw(this.valueArea);
				this.UpdateValue(accessor,newValue);
			}
			else if(value is float){
				var newValue =((float)value).Draw(this.valueArea);
				this.UpdateValue(accessor,newValue);
			}
			else if(value is int){
				var newValue = ((int)value).DrawInt(this.valueArea);
				this.UpdateValue(accessor,newValue);
			}
			else if(value is Vector2){
				value.As<Vector2>().DrawAuto(this.valueArea);
			}
			else if(value is Vector3){
				value.As<Vector3>().DrawAuto(this.valueArea);
			}
			else if(value is IList && depth < 9){
				var items = (IList)value;
				/*if(items.Count == 1){
					this.DrawValue(label.text,items[0],null,depth+1);
					return;
				}*/
				label.text = label.text + " (" + items.Count + ")";
				this.foldoutState[hash] = EditorGUI.Foldout(this.labelArea,this.foldoutState[hash],label);
				this.labelArea = this.labelArea.AddY(18);
				this.valueArea = this.valueArea.AddY(18);
				if(this.foldoutState[hash]){
					if(items.Count < 1){return;}
					this.labelArea = this.labelArea.AddX(10);
					var index = 0;
					foreach(var item in items){
						this.DrawValue("Item " + index,item,null,depth+1);
						++index;
					}
					this.labelArea = this.labelArea.AddX(-10);
				}
				return;
			}
			else if(value is IDictionary && depth < 9){
				var items = (IDictionary)value;
				/*if(items.Count <= 1){
					foreach(DictionaryEntry item in items){
						this.DrawValue(label.text,item.Value,null,depth+1);
					}
					return;
				}*/
				label.text = label.text + " (" + items.Count + ")";
				this.foldoutState[hash] = EditorGUI.Foldout(this.labelArea,this.foldoutState[hash],label);
				this.labelArea = this.labelArea.AddY(18);
				this.valueArea = this.valueArea.AddY(18);
				if(this.foldoutState[hash]){
					this.labelArea = this.labelArea.AddX(10);
					var index = 0;
					foreach(DictionaryEntry item in items){
						if(item.Key is string){
							this.DrawValue((string)item.Key,item.Value,null,depth+1);
							--depth;
							continue;
						}
						var itemHash = item.GetHashCode();
						this.foldoutState.AddNew(itemHash);
						this.foldoutState[itemHash] = EditorGUI.Foldout(this.labelArea,this.foldoutState[itemHash],"Item " + index);
						this.labelArea = this.labelArea.AddY(18);
						this.valueArea = this.valueArea.AddY(18);
						if(this.foldoutState[itemHash]){
							this.labelArea = this.labelArea.AddX(10);
							this.DrawValue("Key",item.Key,null,depth+1);
							this.DrawValue("Value",item.Value,null,depth+1);
							this.labelArea = this.labelArea.AddX(-10);
						}
						++index;
					}
					this.labelArea = this.labelArea.AddX(-10);
				}
				return;
			}
			else if(value.GetType().IsSerializable && depth < 9){
				this.foldoutState[hash] = EditorGUI.Foldout(this.labelArea,this.foldoutState[hash],label);
				this.labelArea = this.labelArea.AddY(18);
				this.valueArea = this.valueArea.AddY(18);
				if(this.foldoutState[hash]){
					var fieldNames = value.GetVariables(Flags.instancePublic);
					if(fieldNames.Count() < 1){return;}
					this.labelArea = this.labelArea.AddX(10);
					foreach(var item in fieldNames){
						var fieldName = item.Key;
						try{
							var fieldValue = value.GetVariable(fieldName);
							this.DrawValue(fieldName,fieldValue,null,depth+1);
						}
						catch{}
					}
					this.labelArea = this.labelArea.AddX(-10);
				}
				return;
			}
			else if(!labelDrawn){return;}
			if(GUI.changed && this.activeClass.HasMethod("StaticValidate")){
				this.activeClass.Call("StaticValidate");
			}
			this.labelArea = this.labelArea.AddY(18);
			this.valueArea = this.valueArea.AddY(18);
		}
		public void ResetIndexes(int priority){
			if(priority > 2){this.selectedAssembly = 0;}
			if(priority > 1){this.selectedNamespace = 0;}
			if(priority > 0){this.selectedClass = 0;}
		}
	}
}