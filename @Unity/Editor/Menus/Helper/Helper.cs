#pragma warning disable 0618
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Menus{
	using Zios.Extensions;
	using Zios.Log;
	using Zios.Reflection;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.Pref;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.SpriteManager;
	using PrefabType = Zios.Unity.Editor.ProxyEditor.PrefabType;
	public static class HelperMenu{
		public static bool IsSprite(GameObject instance){
			var filter = instance.GetComponent<MeshFilter>();
			var sprite = SpriteManager.GetSprite(instance.name);
			var inGroup = instance.transform.parent != null && instance.transform.parent.name.Contains("SpriteGroup");
			return (inGroup && sprite != null) || (filter != null && filter.sharedMesh.name == "plane");
		}
		[MenuItem ("Zios/Sprites/Remove Invisible")]
		public static void RemoveInvisibleSprites(){
			var objects = (GameObject[])Resources.FindObjectsOfTypeAll(typeof(GameObject));
			var removed = 0;
			ProxyEditor.RecordObjects(objects,"Remove Invisible Sprites");
			foreach(var gameObject in objects){
				var type = ProxyEditor.GetPrefabType(gameObject);
				if(type == PrefabType.Prefab || type == PrefabType.ModelPrefab || gameObject == null){continue;}
				var position = gameObject.transform.localPosition;
				var isRoot = gameObject.transform.parent == null;
				var pureName = gameObject.name.Contains("@") ? gameObject.name.Split('@')[0] : gameObject.name;
				var parentName = !isRoot ? gameObject.transform.parent.name : "";
				var nested = !isRoot && gameObject.transform.parent.parent == null;
				nested = nested || (parentName.Contains(pureName) && !parentName.Contains("SpriteGroup"));
				if(IsSprite(gameObject) && (isRoot || !nested) && position.x == 0 && position.z == 0){
					Log.Show("[HelperMenu]" + gameObject.name + " has been removed.");
					GameObject.DestroyImmediate(gameObject);
					++removed;
				}
			}
			Log.Show("[HelperMenu]" + removed + " null game objects removed.");
		}
		[MenuItem ("Zios/Animation/Stepped Curves")]
		public static void SteppedCurves(){
			HelperMenu.SplitAnimations(Mathf.Infinity);
		}
		[MenuItem ("Zios/Animation/Separate Animations")]
		public static void SeparateAnimations(){
			HelperMenu.SplitAnimations();
		}
		public static void SplitAnimations(float forceTangent=-1){
			foreach(var selection in Selection.transforms){
				var animation = (Animation)selection.GetComponent("Animation");
				if(animation != null){
					var clips = AnimationUtility.GetAnimationClips(selection.gameObject);
					var newClips = new AnimationClip[clips.Length];
					Log.Show("[HelperMenu] Converting " + clips.Length + " animations...");
					var clipIndex = 0;
					foreach(var clip in clips){
						if(clip == null){
							++clipIndex;
							continue;
						}
						var clipPath = clip.name + ".anim";
						var originalPath = UnityFile.GetAssetPath(clip);
						var savePath = originalPath.GetDirectory() + clipPath;
						var newClip = new AnimationClip();
						if(originalPath.Contains(".anim")){
							Log.Show("[HelperMenu] [" + clipIndex + "] " + clip.name + " skipped.  Already separate .anim file.");
							newClip = clip;
						}
						else{
							newClip.wrapMode = clip.wrapMode;
							var curves = AnimationUtility.GetAllCurves(clip);
							foreach(var data in curves){
								var newKeys = new List<Keyframe>();
								foreach(var key in data.curve.keys){
									var newKey = new Keyframe(key.time,key.value);
									newKey.inTangent = forceTangent != -1 ? forceTangent : key.inTangent;
									newKey.outTangent = forceTangent != -1 ? forceTangent : key.outTangent;
									newKeys.Add(newKey);
								}
								newClip.SetCurve(data.path,data.type,data.propertyName,new AnimationCurve(newKeys.ToArray()));
							}
							Log.Show("[HelperMenu] [" + clipIndex + "] " + clip.name + " processed -- " + savePath);
							ProxyEditor.CreateAsset(newClip,savePath);
						}
						newClips[clipIndex] = newClip;
						++clipIndex;
					}
					AnimationUtility.SetAnimationClips(animation,newClips);
				}
				else{
					Log.Show("[HelperMenu] No animation component found on object -- " + selection.name);
				}
			}
		}
		[MenuItem ("Zios/Sprites/Snap Positions")]
		public static void SnapPositions(){
			var all = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
			for(var index=0;index < all.Length;++index){
				var current = all[index].transform;
				var position = current.localPosition;
				position.x = Mathf.Round(position.x);
				position.y = Mathf.Round(position.y);
				position.z = Mathf.Round(position.z);
				current.localPosition = position;
			}
		}
		//============================
		// Assorted
		//============================
		[MenuItem("Zios/Reload Scripts &#R")]
		public static void ReloadScripts(){
			Log.Show("[Utility] Forced Reload Scripts.");
			typeof(UnityEditorInternal.InternalEditorUtility).Call("RequestScriptReload");
		}
		[MenuItem("Assets/Build AssetBundles")]
		public static void BuildAssetBundles(){
			BuildPipeline.BuildAssetBundles("Assets/",BuildAssetBundleOptions.None,BuildTarget.StandaloneWindows64);
		}
		[MenuItem("Zios/Unhide GameObjects")]
		public static void UnhideAll(){
			foreach(var target in Resources.FindObjectsOfTypeAll<GameObject>()){
				if(!target.name.ContainsAny("SceneCamera","SceneLight","InternalIdentityTransform","Reflection Probes Camera")){
					target.hideFlags = HideFlags.None;
				}
			}
		}
		[MenuItem("Zios/Prefs/Clear Player")]
		public static void ClearAll(){PlayerPrefEditor.ClearAll(true);}
		[MenuItem("Zios/Prefs/Clear Editor")]
		public static void DeleteAll(){EditorPref.ClearAll(true);}
	}
}