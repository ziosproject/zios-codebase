using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Menus{
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.Pref;
	public class Preferences{
		[PreferenceItem("Zios")]
		public static void Draw(){
			var fastInspectorHelp = new GUIContent("Turbo Inspector (Experimental)");
			var alwaysUpdateShadersHelp = new GUIContent("Always Update Shaders");
			var alwaysUpdateParticlesHelp = new GUIContent("Always Update Particles");
			var particleUpdateRangeHelp = new GUIContent("Always Update Particles (Range)");
			fastInspectorHelp.tooltip = "Prevents offscreen attributes/components from being drawn in inspectors. ";
			fastInspectorHelp.tooltip += "Currently has issues with multiple inspectors visible and erratic nudging position offset issues while scrolling.";
			alwaysUpdateShadersHelp.tooltip = "Forces the scene view to repaint every frame.  Huge performance cost, but will allow shaders based on time to update in realtime.";
			alwaysUpdateParticlesHelp.tooltip = "Forces the scene view to repaint every frame.  Huge performance cost, but will continually ensure particles are simulated.";
			particleUpdateRangeHelp.tooltip = "Range at which editor-updated particles will simulate.  Higher values will cost more performance.";
			if("Other".ToLabel().DrawFoldout("Zios.Preferences.Other")){
				EditorGUI.indentLevel += 1;
				var fastInspector = EditorPref.Get<bool>("MonoBehaviourEditor-FastInspector").Draw(fastInspectorHelp);
				var alwaysUpdateShaders = EditorPref.Get<bool>("EditorSettings-AlwaysUpdateShaders").Draw(alwaysUpdateShadersHelp);
				var alwaysUpdateParticles = EditorPref.Get<bool>("EditorSettings-AlwaysUpdateParticles").Draw(alwaysUpdateParticlesHelp);
				var particleUpdateRange = EditorPref.Get<float>("EditorSettings-ParticleUpdateRange",100).Draw(particleUpdateRangeHelp);
				if(GUI.changed){
					EditorPref.Set<bool>("MonoBehaviourEditor-FastInspector",fastInspector);
					EditorPref.Set<bool>("EditorSettings-AlwaysUpdateShaders",alwaysUpdateShaders);
					EditorPref.Set<bool>("EditorSettings-AlwaysUpdateParticles",alwaysUpdateParticles);
					EditorPref.Set<float>("EditorSettings-ParticleUpdateRange",particleUpdateRange);
				}
				EditorGUI.indentLevel -= 1;
			}
		}
	}
}