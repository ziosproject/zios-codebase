using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Menus{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Log;
	using Zios.Unity.Call;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.Hooks;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Editor.Supports.Stepper;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	public static class MaterialCleaner{
		public static bool changes;
		[MenuItem ("Zios/Material/Remove Unused Data (All)")]
		public static void Clean(){MaterialCleaner.Clean(null);}
		public static void Clean(FileData[] materials){
			MaterialCleaner.changes = false;
			var files = materials ?? File.FindAll("*.mat");
			EditorHook.AddStepper("UnityEditor/Update",MaterialCleaner.Step,files,50);
		}
		public static void Step(object collection,int itemIndex){
			var materials = (FileData[])collection;
			var file = materials[itemIndex];
			var last = itemIndex == materials.Length-1;
			Stepper.title = "Updating " + materials.Length + " Materials";
			Stepper.message = "Updating material : " + file.name;
			var text = file.ReadText();
			var copy = text;
			var index = 0;
			var changed = false;
			var removePrevious = false;
			var guid = text.Parse("guid: ",",");
			var shaderPath = ProxyEditor.GetAssetPath(guid);
			if(!shaderPath.IsEmpty()){
				var material = file.GetAsset<Material>();
				var shader = UnityFile.GetAsset<Shader>(shaderPath,false);
				if(shader != null){
					var propertyCount = ProxyEditor.GetPropertyCount(shader);
					var properties = new Dictionary<string,string>();
					for(var propertyIndex=0;propertyIndex<propertyCount;++propertyIndex){
						var name = ProxyEditor.GetPropertyName(shader,propertyIndex);
						properties[name] = ProxyEditor.GetPropertyType(shader,propertyIndex).ToName();
					}
					var keywords = text.Parse("m_ShaderKeywords:","m_").Trim("[]");
					if(!keywords.IsEmpty()){
						var keywordsCleaned = keywords;
						foreach(var keyword in keywords.Replace("\n   ","").Split(" ")){
							if(!properties.ContainsKey(keyword.Split("_")[0],true)){
								keywordsCleaned = keywordsCleaned.Replace(" "+keyword,"");
								changed = true;
							}
						}
						copy = copy.Replace(keywords,keywordsCleaned);
					}
					while(true){
						var nextIndex = text.IndexOf("data:",index+5);
						if(removePrevious){
							var nextGroup = text.IndexOf("\n    m",index);
							var count = nextGroup != -1 && nextGroup < nextIndex ? nextGroup-index : nextIndex-index;
							var section = nextIndex < 0 ? text.Substring(index) : text.Substring(index,count);
							copy = copy.Replace(section,"");
							removePrevious = false;
							changed = true;
						}
						if(nextIndex == -1){break;}
						index = nextIndex;
						var keywordStart = text.IndexOf("name: ",index)+6;
						var keywordEnd = text.IndexOf("\n",keywordStart);
						var name = text.Substring(keywordStart,keywordEnd-keywordStart);
						if(name.IsEmpty()){continue;}
						var emptyTexture = properties.ContainsKey(name) && properties[name] == "Texture" && material.GetTexture(name) == null;
						removePrevious = !properties.ContainsKey(name) || emptyTexture;
						//if(removePrevious){Log.Show("[MaterialCleaner] Removing " + name + " from " + file.fullName);}
					}
					if(changed){
						MaterialCleaner.changes = true;
						Log.Show("[MaterialCleaner] : Cleaned unused serialized data " + file.fullName);
						file.Write(copy);
					}
				}
			}
			if(last){
				if(!MaterialCleaner.changes){Log.Show("[MaterialCleaner] All files already clean.");}
				else{Log.Show("[MaterialCleaner] : Cleaned all materials.");}
				Call.Delay(()=>ProxyEditor.RefreshAssets(),1);
			}
		}
	}
}