﻿//asm UnityEngine;
namespace Zios.Unity.Editor.Configuration.Worker{
	using Zios.Supports.Worker;
	using Zios.SystemAttributes;
	using Zios.Unity.Editor.ProxyEditor;
	using CallbackFunction = UnityEditor.EditorApplication.CallbackFunction;
	[AutoInitialize]
	public class WorkerUnity{
		static WorkerUnity(){
			Worker.monitorHook = (x)=>ProxyEditor.AddUpdate(new CallbackFunction(x));
		}
	}
}