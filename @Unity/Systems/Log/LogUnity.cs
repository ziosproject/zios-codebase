﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
namespace Zios.Unity.Log{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Log;
	using Zios.SystemAttributes;
	using Zios.Unity.Proxy;
	[AutoInitialize(-999)]
	public static class LogUnity{
		static LogUnity(){
			Log.Display = LogUnity.DisplayUnity;
			if(AutoInitializeSettings.debug){Log.Show("[AutoInitialize] Calling static class constructor -- LogUnity");}
		}
		public static void Editor(string text){
			if(!Proxy.IsPlaying()){
				Log.Show(text);
			}
		}
		public static void DisplayUnity(Log log,string message){
			var unityTarget = !log.target.IsNull() ? log.target.As<UnityObject>() : null;
			if(log.type=="Warning"){Debug.LogWarning(message,unityTarget);}
			else if(log.type=="Error"){Debug.LogError(message,unityTarget);}
			else{Debug.Log(message,unityTarget);}
		}
	}
}