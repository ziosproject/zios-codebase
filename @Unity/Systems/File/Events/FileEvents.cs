//includeIf Zios.Unity && Zios.File
namespace Zios.Unity.File.FileEvents{
	using Zios.File;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	[AutoInitialize]
	public static class FileEvents{
		static FileEvents(){
			Hook.Get("UnityEditor/Update").Add(FileMonitor.Update);
		}
	}
}