using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Zios.Unity.Editor.File{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Supports.Hierarchy;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Extensions;
	using Zios.Unity.Proxy;
	using UnityObject = UnityEngine.Object;
	//asm UnityEditor;
	[AutoInitialize(-99)]
	public static class UnityFile{
		public static Dictionary<FileData,object> fileAssets = new Dictionary<FileData,object>();
		public static Dictionary<UnityObject,object> assets = new Dictionary<UnityObject,object>();
		public static Dictionary<UnityObject,string> assetPaths = new Dictionary<UnityObject,string>();
		public static Hierarchy<Type,string,string,UnityObject> namedAssets = new Hierarchy<Type,string,string,UnityObject>();
		static UnityFile(){
			Hook.Get("File/Changed").Add<string>(UnityFile.UpdatePath);
		}
		public static void UpdatePath(string path){
			UnityFile.assets.Clear();
			UnityFile.assetPaths.Clear();
			UnityFile.namedAssets.Clear();
		}
		public static T[] GetAssets<T>(string name="*",bool showWarnings=true) where T : UnityObject{
			var files = File.FindAll(name,showWarnings);
			if(files.Length < 1){return new T[0];}
			return files.Select(x=>x.GetAsset<T>()).Where(x=>!x.IsNull()).ToArray();
		}
		public static Dictionary<string,T> GetNamedAssets<T>(string name="*",bool showWarnings=true) where T : UnityObject{
			if(!UnityFile.namedAssets.AddNew(typeof(T)).ContainsKey(name)){
				var files = UnityFile.GetAssets<T>(name,showWarnings).GroupBy(x=>x.name).Select(x=>x.First());
				UnityFile.namedAssets[typeof(T)][name] = files.ToDictionary(x=>x.name,x=>(UnityObject)x);
			}
			return UnityFile.namedAssets[typeof(T)][name].ToDictionary(x=>x.Key,x=>(T)x.Value);
		}
		public static string GetGUID(string name,bool showWarnings=true){
			var file = File.Find(name,showWarnings);
			if(file != null){return file.GetGUID();}
			return "";
		}
		public static string GetGUID(UnityObject target){
			return ProxyEditor.GetGUID(UnityFile.GetAssetPath(target));
		}
		public static T GetAsset<T>(string name,bool showWarnings=true) where T : UnityObject{
			var file = File.Find(name,showWarnings);
			if(file != null){return file.GetAsset<T>();}
			return default(T);
		}
		public static FileData Get(UnityObject target,bool showWarnings=false){
			var path = FileSettings.dataPath.Replace("Assets","") + UnityFile.GetAssetPath(target);
			return File.Find(path,showWarnings);
		}
		public static string GetAssetPath(UnityObject target){
			if(!UnityFile.assetPaths.ContainsKey(target)){
				UnityFile.assetPaths[target] = ProxyEditor.GetAssetPath(target);
			}
			return UnityFile.assetPaths[target];
		}
		public static T GetAsset<T>(UnityObject target){
			if(!UnityFile.assets.ContainsKey(target)){
				var assetPath = UnityFile.GetAssetPath(target);
				object asset = ProxyEditor.LoadAsset(assetPath,typeof(T));
				if(asset == null){return default(T);}
				UnityFile.assets[target] = Convert.ChangeType(asset,typeof(T));
			}
			return (T)UnityFile.assets[target];
		}
	}
	public static class FileDataExtensions{
		public static AssetType GetAsset<AssetType>(this FileData current) where AssetType : UnityObject{
			var asset = UnityFile.fileAssets.TryGet(current);
			if(asset.IsNull()){
				asset = UnityFile.fileAssets[current] = ProxyEditor.LoadAsset(current.GetAssetPath(),typeof(AssetType));
				current.GetHook("File/DeleteCache").Add(()=>UnityFile.fileAssets.Remove(current));
			}
			return (AssetType)asset;
		}
		public static string GetGUID(this FileData current){
			return ProxyEditor.GetGUID(current.GetAssetPath());
		}
		public static string GetAssetPath(this FileData current){
			return current.path.GetAssetPath();
		}
	}
	public static class MonoBehaviourExtensions{
		public static string GetGUID(this MonoBehaviour current){
			var scriptFile = ProxyEditor.GetMonoScript(current);
			var path = UnityFile.GetAssetPath(scriptFile);
			return ProxyEditor.GetGUID(path);
		}
	}
	public static class UnityObjectExtensions{
		public static string GetAssetPath(this UnityObject current){
			return UnityFile.GetAssetPath(current);
		}
		public static bool IsAsset(this UnityObject current){
			return !UnityFile.GetAssetPath(current).IsEmpty();
		}
	}
}