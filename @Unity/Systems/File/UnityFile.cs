namespace Zios.Unity.File{
	using UnityEngine;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.SystemAttributes;
	using Zios.Unity.Proxy;
	using Application = UnityEngine.Application;
	[AutoInitialize(-99)]
	public static class FileConfig{
		static FileConfig(){
			var needsPersistent = !Proxy.IsEditor() && Application.platform.MatchesAny("IPhonePlayer","MetroPlayerX86","MetroPlayerX64","MetroPlayerARM");
			FileSettings.dataPath = needsPersistent ? Application.persistentDataPath : Application.dataPath;
			FileSettings.dataPath = FileSettings.dataPath.GetRelativePath();
			FileSettings.ignoreFolders = FileSettings.ignoreFolders.Add("./Temp").Add("./Library").Add("./obj");
		}
	}
	public static class TextureExtensions{
		public static Texture2D SaveAs(this Texture current,string path,bool useBlit=false){
			var texture = current is Texture2D ? (Texture2D)current : new Texture2D(1,1);
			if(useBlit){
				RenderTexture.active = new RenderTexture(current.width,current.height,0);
				Graphics.Blit(current,RenderTexture.active);
				texture = new Texture2D(current.width,current.height);
				texture.ReadPixels(new Rect(0,0,current.width,current.height),0,0);
				RenderTexture.active = null;
				RenderTexture.DestroyImmediate(RenderTexture.active);
			}
			File.Write(path,texture.EncodeToPNG());
			return texture;
		}
	}
}