using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.ProgramManager{
	using Zios.Extensions;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Components.ParticleUpdater;
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.MonoBehaviourEditor;
	using Zios.Unity.Editor.ProxyEditor;
	using Zios.Unity.Locate;
	using Zios.Unity.Pref;
	using Zios.Unity.ProgramManager;
	using Zios.Unity.Time;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	//asm Zios.Unity.Editor.Inspectors;
	[AutoInitialize][CustomEditor(typeof(ProgramManager))]
	public class ProgramManagerEditor : MonoBehaviourEditor{
		public override void OnInspectorGUI(){
			this.title = "Program";
			this.header = this.header ?? UnityFile.GetAsset<Texture2D>("ProgramIcon.png");
			base.OnInspectorGUI();
		}
		static ProgramManagerEditor(){
			Hook.Get("UnityEditor/Update").Add(ProgramManagerEditor.UpdateEffects);
			Hook.Get("UnityEditor/EnterPlay").Add(ProgramManagerEditor.UpdateEffects);
		}
		public static void UpdateEffects(){
			var updateShaders = PlayerPref.Get<bool>("EditorSettings-AlwaysUpdateShaders");
			var updateParticles = PlayerPref.Get<bool>("EditorSettings-AlwaysUpdateParticles");
			if(updateShaders){Shader.SetGlobalFloat("timeConstant",Time.Get());}
			foreach(var system in Locate.GetSceneComponents<ParticleSystem>()){
				if(system.IsNull()){continue;}
				var updater = system.gameObject.GetComponent<UpdateParticle>();
				if(updateParticles && updater.IsNull()){
					updater = system.gameObject.AddComponent<UpdateParticle>();
					updater.hideFlags = HideFlags.DontSaveInBuild | HideFlags.NotEditable | HideFlags.HideInInspector;
				}
				if(!updateParticles && !updater.IsNull()){
					GameObject.DestroyImmediate(updater);
				}
			}
			if(updateShaders || updateParticles){ProxyEditor.RepaintSceneView();}
		}
	}
}