using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Zios.Unity.ProgramManager{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Log;
	using Zios.Serializer.Attributes;
	using Zios.Supports.Hook;
	using Zios.Unity.Components.Persistent;
	using Zios.Unity.Console;
	using Zios.Unity.Locate;
	using Zios.Unity.Proxy;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[Singleton]
	public class ProgramManager{
		public bool[] pixelSnap = new bool[3]{false,false,false};
		public int maxFPS = -1;
		public int vsync;
		public int refreshRate = 60;
		public int[] resolution = new int[2]{1920,1080};
		private bool allowResolution = true;
		public void Setup(){
			this.vsync = QualitySettings.vSyncCount;
			Hook.Create("Program/ResolutionChange");
			Application.targetFrameRate = this.maxFPS;
			var screen = Screen.currentResolution;
			this.resolution = new int[2]{Screen.width,Screen.height};
			Locate.GetSceneComponents<Persistent>().Where(x=>x.activateOnLoad).ToList().ForEach(x=>x.gameObject.SetActive(true));
			this.DetectResolution();
		}
		public void Awake(){
			this.Setup();
			Console.AddShortcut("changeResolution","res","resolution");
			Console.AddShortcut("closeProgram","quit");
			Console.AddShortcut("verticalSync","vsync");
			Console.AddKeyword("hide",this.DisableGameObject,1);
			Console.AddKeyword("show",this.EnableGameObject,1);
			Console.AddKeyword("form",this.ToggleGameObject,1);
			Console.AddKeyword("instance",this.InstanceGameObject,1);
			Console.AddKeyword("destroy",this.DestroyGameObject,1);
			Console.AddKeyword("closeProgram",this.CloseProgram);
			//Console.AddCvarMethod("changeResolution",this,"resolution","Change Resolution","",this.ChangeResolution);
			Console.AddCvar("maxfps",typeof(Application),"targetFrameRate","Maximum FPS");
			Console.AddCvar("verticalSync",typeof(QualitySettings),"vSyncCount","Vertical Sync");
		}
		public void Update(){
			this.DetectResolution();
		}
		public void OnValidate(){
			this.Setup();
		}
		//================================
		// Internal
		//================================
		public void InstanceGameObject(string[] values){
			var target = Locate.GetAssets<GameObject>().Where(x=>x.name==values[1]).FirstOrDefault();
			if(!target.IsNull()){
				var instance = GameObject.Instantiate<GameObject>(target);
				instance.SetActive(true);
			}
		}
		public void DestroyGameObject(string[] values){Locate.GetSceneObjects().Where(x=>x.name==values[1]).ToList().ForEach(x=>GameObject.Destroy(x));}
		public void DisableGameObject(string[] values){Locate.GetSceneObjects().Where(x=>x.name==values[1]).ToList().ForEach(x=>x.SetActive(false));}
		public void EnableGameObject(string[] values){Locate.GetSceneObjects().Where(x=>x.name==values[1]).ToList().ForEach(x=>x.SetActive(true));}
		public void ToggleGameObject(string[] values){Locate.GetSceneObjects().Where(x=>x.name==values[1]).ToList().ForEach(x=>x.SetActive(!x.activeInHierarchy));}
		public void DetectResolution(){
			if(!Proxy.IsPlaying()){return;}
			var screen = Screen.currentResolution;
			var size = this.resolution;
			var changedWidth = Screen.width != size[0];
			var changedHeight = Screen.height != size[1];
			var changedRefresh = screen.refreshRate != this.refreshRate;
			if(changedWidth || changedHeight || changedRefresh){
				Hook.Call("Program/ResolutionChange");
				if(!this.allowResolution){
					this.allowResolution = true;
					if(Proxy.IsPlaying()){Log.Show("^7Screen settings auto-adjusted to closest allowed values.");}
					if(changedWidth){this.resolution[0] = Screen.width;}
					if(changedHeight){this.resolution[1] = Screen.height;}
					if(changedRefresh){this.refreshRate = screen.refreshRate;}
				}
				else{
					Screen.SetResolution(size[0],size[1],Screen.fullScreen,this.refreshRate);
					this.allowResolution = false;
				}
			}
			else if(!this.allowResolution){
				this.allowResolution = true;
				var log = "^10Program resolution is : ^8| " + size[0] + "^7x^8|" + size[1];
				if(Proxy.IsPlaying()){Log.Show(log);}
			}
		}
		public void ChangeResolution(string[] values){
			if(values.Length < 3){
				this.allowResolution = false;
				return;
			}
			this.resolution[0] = values[1].ToInt();
			this.resolution[1] = values[1].ToInt();
		}
		public void SnapPixels(string[] values){
			if(values.Length < 2){
				Log.Show("@pixelSnap*");
				return;
			}
			var states = new List<bool>(this.pixelSnap).ToArray();
			if(values.Length == 2){
				values = new string[]{"",values[1],values[1],values[1]};
			}
			for(var index=1;index<values.Length;++index){
				if(index > 4){break;}
				var value = values[index].ToLower();
				states[index-1] = value == "true" || value == "1" ? true : false;
			}
			Log.Show("@pixelSnapX "+states[0]);
			Log.Show("@pixelSnapY "+states[1]);
			Log.Show("@pixelSnapZ "+states[2]);
			this.pixelSnap = states;
		}
		public void CloseProgram(){
			Application.Quit();
		}
	}
}