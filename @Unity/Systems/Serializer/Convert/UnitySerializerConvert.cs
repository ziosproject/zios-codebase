﻿using UnityEngine;
namespace Zios.Unity.Serializer.Convert{
	using Zios.Extensions.Convert;
	using Zios.Serializer.Convert;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Locate;
	using Zios.Unity.Extensions.Convert;
	[AutoInitialize(5)]
	public class UnitySerializerConvert{
		static UnitySerializerConvert(){
			Hook.Get("Serializer/Setup").Add<SerializerConvert>(UnitySerializerConvert.AddHooks);
		}
		public static void AddHooks(SerializerConvert convert){
			var instancer = convert.toNewMethods;
			var serializer = convert.toTextMethods;
			var byteSerializer = convert.toByteMethods;
			var deserializer = convert.toObjectMethods;
			//=============
			// Vector
			//=============
			byteSerializer.Add((current)=>{
				return current is Vector3 ? current.As<Vector3>().ToBytes() : null;
			});
			serializer.Add((current,separator,changesOnly)=>{
				return current is Vector3 ? current.As<Vector3>().ToString() : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(Vector3) ? Vector3.zero.ToVector3(current).Box() : null;
			});
			byteSerializer.Add((current)=>{
				return current is Vector2 ? current.As<Vector2>().ToBytes() : null;
			});
			serializer.Add((current,separator,changesOnly)=>{
				return current is Vector2 ? current.As<Vector2>().ToString() : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(Vector2) ? Vector2.zero.ToVector2(current).Box() : null;
			});
			byteSerializer.Add((current)=>{
				return current is Vector4 ? current.As<Vector4>().ToBytes() : null;
			});
			serializer.Add((current,separator,changesOnly)=>{
				return current is Vector4 ? current.As<Vector4>().ToString() : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(Vector4) ? Vector4.zero.ToVector4(current).Box() : null;
			});
			//=============
			// Texture2D
			//=============
			serializer.Add((current,separator,changesOnly)=>{
				return current is Texture2D ? current.As<Texture2D>().ToText() : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(Texture2D) ? new Texture2D(1,1).ToTexture2D(current).Box() : null;
			});
			//=============
			// Shader
			//=============
			serializer.Add((current,separator,changesOnly)=>{
				return current is Shader ? current.As<Shader>().name : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(Shader) ? Shader.Find(current) : null;
			});
			//=============
			// GUISkin
			//=============
			instancer.Add((type)=>{
				return type == typeof(GUISkin) ? ScriptableObject.CreateInstance<GUISkin>() : null;
			});
			//=============
			// Material
			//=============
			instancer.Add((type)=>{
				return type == typeof(Material) ? new Material(Shader.Find("Diffuse")) : null;
			});
			//=============
			// GUIContent
			//=============
			serializer.Add((current,separator,changesOnly)=>{
				return current is GUIContent ? current.As<GUIContent>().ToText() : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(GUIContent) ? new GUIContent().ToGUIContent(current).Box() : null;
			});
			//=============
			// Color
			//=============
			serializer.Add((current,separator,changesOnly)=>{
				return current is Color ? current.As<Color>().ToText() : null;
			});
			deserializer.Add((current,type,separator)=>{
				return type == typeof(Color) ? current.ToColor("-").Box() : null;
			});
		}
	}
	public static class UnitySerializerExtensions{
		public static string ToText(this Vector4 current){return "("+current.x+","+current.y+","+current.z+","+current.w+")";}
		public static byte[] ToBytes(this Vector2 current){return current.x.ToBytes().Append(current.y);}
		public static byte[] ToBytes(this Vector3 current){return current.x.ToBytes().Append(current.y).Append(current.z);}
		public static byte[] ToBytes(this Vector4 current){return current.x.ToBytes().Append(current.y).Append(current.z).Append(current.w);}
	}
}