﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityObject = UnityEngine.Object;
namespace Zios.Unity.Serializer{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Reflection;
	using Zios.Serializer;
	using Zios.Serializer.Attributes;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Extensions;
	using Zios.Unity.Locate;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[AutoInitialize(10)]
	public static class SerializerUnity{
		public static string path = "[Unity]/";
		public static List<Func<string,string>> pathFilters = new List<Func<string,string>>();
		public static Scene scene;
		static SerializerUnity(){
			Hook.Get("UnityEditor/EnterPlay").Add(SerializerUnity.SaveScene);
			Hook.Get("UnityEditor/SceneStart").Add(SerializerUnity.LoadScene);
			Hook.Get("UnityEditor/ExitPlayComplete").Add(Serializer.main.LoadAuto);
			Func<string,string> filterPath = (path)=>{
				path = path.Replace("[data]",Application.dataPath,true);
				path = path.Replace("[persistentData]",Application.persistentDataPath,true);
				path = path.Replace("[streamingAssets]",Application.streamingAssetsPath,true);
				path = path.Replace("[temporaryCache]",Application.temporaryCachePath,true);
				return path;
			};
			SerializerUnity.pathFilters.Add(filterPath);
		}
		//===================
		// Saving
		//===================
		public static string GetPath(string folder=""){
			return Serializer.main.settings.path + SerializerUnity.path + folder;
		}
		public static void SaveScene(){
			var format = Serializer.main;
			var behaviours = Locate.GetSceneComponents<MonoBehaviour>().Cast<UnityObject>().ToList();
			var scriptable = Locate.GetAssets<ScriptableObject>();
			var targets = behaviours.Extend(scriptable);
			var path = SerializerUnity.GetPath(SerializerUnity.scene.name+"/");
			foreach(var script in targets){
				if(script.IsNull()){continue;}
				var serializable = script.GetVariables(MemberFilter.FindWith<Store>());
				if(serializable.Count < 1){continue;}
				var serialized = new StringBuilder();
				foreach(var target in serializable){
					var name = target.Key;
					var value = target.Value;
					var fieldValue = format.Serialize(value);
					serialized.Append(format.Format(name,fieldValue));
				}
				var contents = serialized.ToString().TrimRight(format.Separator());
				if(!contents.IsEmpty()){
					var instanceID = script.GetInstanceID().ToString();
					var filename = "~"+instanceID+"."+script.GetType().Name;
					serialized.Prepend(format.TypeHeader(instanceID),format.Separator());
					var file = File.AddNew(path+filename);
					Serializer.hashes[script] = filename;
					Serializer.targets[filename] = script;
					file.Write(serialized.ToString().Trim(format.Separator()));
				}
			}
		}
		//===================
		// Loading
		//===================
		public static void LoadScene(){
			SerializerUnity.scene = SceneManager.GetActiveScene();
			var path = SerializerUnity.GetPath(SerializerUnity.scene.name+"/");
			foreach(var file in File.FindAll(path,false)){
				Serializer.main.Load(file);
			}
		}
		public static void LoadScriptInstances(string path){
			var format = Serializer.main;
			var instances = File.FindAll(path,false);
			var lookup = new Dictionary<int,UnityObject>();
			var behaviours = Locate.GetSceneComponents<MonoBehaviour>().Cast<UnityObject>().ToList();
			var scriptable = Locate.GetAssets<ScriptableObject>();
			foreach(var script in behaviours.Extend(scriptable)){
				lookup[script.GetInstanceID()] = script;
			}
			foreach(var instanceFile in instances){
				var lines = instanceFile.ReadText().Split(format.Separator());
				var instanceID = lines.FirstOrDefault().Trim("[","]").ToInt();
				if(lookup.ContainsKey(instanceID)){
					var instance = lookup[instanceID];
					foreach(var line in lines.Skip(1)){
						if(line.Trim().IsEmpty()){continue;}
						var data = format.Parse(line);
						var value = format.Deserialize(data.Value);
						instance.SetVariable(data.Key,value);
					}
				}
			}
		}
	}
}