using System;
using System.Collections.Generic;
using UnityEngine;
namespace Zios.Unity.Pool{
	using Zios.Log;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Locate;
	using Zios.Unity.Proxy;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[Serializable]
	public class PoolPrefab{
		[Internal] public string name;
		public GameObject prefab;
		public Vector3 offset;
		public Vector3 scale = Vector3.one;
		public int maximum = 8;
		public bool uniqueMaterial;
	}
	[AutoInitialize]
	public static class Pool{
		public static Instance empty;
		public static List<PoolPrefab> prefabs = new List<PoolPrefab>();
		public static Dictionary<string,Instance[]> instances = new Dictionary<string,Instance[]>();
		static Pool(){Pool.Setup();}
		public static void Setup(){
			if(Proxy.IsPlaying() && Pool.prefabs.Count > 0){
				var empty = new GameObject("@Null");
				empty.transform.parent = Locate.GetScenePath("@Instances").transform;
				Pool.empty = empty.AddComponent<Instance>();
				Pool.empty.prefab = new PoolPrefab();
			}
		}
		public static Instance FindAvailable(string name){
			if(Pool.instances.ContainsKey(name)){
				foreach(var instance in Pool.instances[name]){
					if(instance.free){
						return instance;
					}
				}
			}
			Log.Warning("[Pool] No instances were available for " + name + "!");
			return Pool.empty;
		}
		public static void Build(PoolPrefab blueprint){
			if(blueprint == null || blueprint.prefab == null){return;}
			var instanceGroup = Locate.GetScenePath("@Instances").transform;
			var slots = Pool.instances[blueprint.name] = new Instance[blueprint.maximum];
			for(var current=0;current<blueprint.maximum;++current){
				var gameObject = (GameObject)GameObject.Instantiate(blueprint.prefab);
				var instance = slots[current] = gameObject.AddComponent<Instance>();
				instance.prefab = blueprint;
				instance.gameObject.SetActive(false);
				instance.gameObject.transform.parent = instanceGroup;
				instance.gameObject.transform.localScale = blueprint.scale;
				if(blueprint.uniqueMaterial){
					var material = instance.gameObject.GetComponent<Renderer>().sharedMaterial;
					instance.gameObject.GetComponent<Renderer>().sharedMaterial = (Material)Material.Instantiate(material);
				}
			}
		}
		public static GameObject AddInstance(string name,Vector3 position,float scale=1.0f,bool mirrorX=false,bool mirrorY=false){
			var instance = Pool.FindAvailable(name);
			if(instance != null){
				var localScale = instance.transform.localScale;
				if(mirrorX){localScale.x *= -1;}
				if(mirrorY){localScale.y *= -1;}
				instance.transform.localScale = localScale * scale;
				instance.transform.position = position + instance.prefab.offset;
				instance.gameObject.SetActive(true);
				instance.free = false;
			}
			instance.gameObject.CallHook("Spawn");
			return instance.gameObject;
		}
	}
}