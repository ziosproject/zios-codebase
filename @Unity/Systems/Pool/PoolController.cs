using UnityEngine;
namespace Zios.Unity.Pool{
	using Zios.Log;
	[AddComponentMenu("Zios/Component/General/Pool Controller")]
	public class PoolController : MonoBehaviour{
		public PoolPrefab[] prefabs = new PoolPrefab[0];
		public void Awake(){
			foreach(var prefab in this.prefabs){
				if(prefab == null || prefab.prefab == null){
					Log.Warning("[PoolController] Prefab for element is missing/corrupt.");
					continue;
				}
				prefab.name = prefab.prefab.name;
			}
			foreach(var prefab in this.prefabs){
				Zios.Unity.Pool.Pool.Build(prefab);
			}
		}
	}
}