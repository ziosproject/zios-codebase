using UnityEngine;
namespace Zios.Unity.Pool{
	using Zios.Extensions;
	using Zios.Supports.Hook;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[AddComponentMenu("")]
	public class Instance : MonoBehaviour{
		public PoolPrefab prefab;
		public bool free = true;
		public void Awake(){
			this.GetHook("Unity/Disable").Add(this.OnDeactivate);
		}
		public void OnDeactivate(){
			if(this.gameObject.IsNull()){return;}
			this.gameObject.SetActive(false);
			this.free = true;
		}
	}
}