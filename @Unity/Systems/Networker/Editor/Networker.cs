using UnityEditor;
using UnityEngine;
namespace Zios.Unity.Editor.Networker{
	using Zios.Unity.Editor.File;
	using Zios.Unity.Editor.MonoBehaviourEditor;
	using Zios.Unity.Networker;
	//asm Zios.Unity.Editor.Inspectors;
	[CustomEditor(typeof(Networker))]
	public class NetworkerEditor : MonoBehaviourEditor{
		public override void OnInspectorGUI(){
			this.title = "Networker";
			this.header = this.header ?? UnityFile.GetAsset<Texture2D>("NetworkerIcon.png");
			base.OnInspectorGUI();
		}
	}
}