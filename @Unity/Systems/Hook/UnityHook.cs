using UnityEngine;
namespace Zios.Unity.HookExtensions{
	using Zios.Supports.Hook;
	using Zios.Unity.Call;
	public static class UnityHookExtensions{
		public static void DelayCallHook(this object current,string name,float delay=0.5f,params object[] terms){
			current.DelayCallHook(current,name,delay,terms);
		}
		public static void DelayCallHook(this object current,object key,string name,float delay=0.5f,params object[] terms){
			Call.Delay(key,()=>current.CallHook(name,terms),delay);
		}
		public static void PauseValidate(this GameObject current){
			var components = current.GetComponentsInChildren<Component>();
			foreach(var component in components){
				component.GetHook("Unity/Validate").Disable();
			}
			current.GetHook("Unity/Validate").Disable();
		}
		public static void ResumeValidate(this GameObject current){
			var components = current.GetComponentsInChildren<Component>();
			foreach(var component in components){
				component.GetHook("Unity/Validate").Enable();
			}
			current.GetHook("Unity/Validate").Enable();
		}
	}
}