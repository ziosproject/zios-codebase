namespace Zios.Unity.Inputs{
	public enum InputRange{Any,Zero,Negative,Positive}
	public static class InputState{
		public static bool disabled;
		public static bool CheckRequirement(InputRange requirement,float intensity){
			var none = requirement == InputRange.Zero && intensity == 0;
			var any = requirement == InputRange.Any && intensity != 0;
			var less = requirement == InputRange.Negative && intensity < 0;
			var more = requirement == InputRange.Positive && intensity > 0;
			return any || less || more || none;
		}
	}
}