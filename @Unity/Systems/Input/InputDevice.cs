using System;
namespace Zios.Unity.Inputs{
	[Serializable]
	public class InputDevice{
		public string name;
		public int id;
		public InputDevice(){}
		public InputDevice(string name,int id=-1){
			this.name = name;
			this.id = id;
		}
	}
}