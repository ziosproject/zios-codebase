using UnityEngine;
namespace Zios.Unity.Hook{
	using Zios.Extensions;
	using Zios.Log;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Locate;
	using Zios.Unity.Proxy;
	using Zios.Unity.Time;
	//asm Zios.Supports.Hierarchy;
	[AddComponentMenu("")][ExecuteInEditMode]
	public class EventDetector : MonoBehaviour,ISerializationCallbackReceiver{
		private static bool showTime = false;
		private float loadStart;
		public static bool wasSerialized;
		public static bool wasDeserialized;
		public static bool loading = true;
		public void Loading(){
			Proxy.busyMethods.Add(()=>EventDetector.loading);
			this.loadStart = Time.Get();
			EventDetector.loading = true;
		}
		public virtual void OnValidate(){
			if(EventDetector.wasDeserialized){
				Hook.Call("Unity/DeserializeComplete");
				EventDetector.wasDeserialized = false;
			}
			Hook.Call("Unity/Validate");
		}
		public virtual void Awake(){
			Hook.Get("Unity/DeserializeComplete").Add(this.Loading);
			Hook.Create("Unity/Awake");
			Hook.Create("Unity/Start");
			Hook.Create("Unity/Update");
			Hook.Create("Unity/FixedUpdate");
			Hook.Create("Unity/LateUpdate");
			Hook.Create("Unity/Enable");
			Hook.Create("Unity/Disable");
			Hook.Create("Unity/GUI");
			Hook.Create("Unity/GUI/Repaint");
			Hook.Create("Unity/GUI/Layout");
			Hook.Create("Unity/GUI/KeyDown");
			Hook.Create("Unity/GUI/KeyUp");
			Hook.Create("Unity/GUI/ScrollWheel");
			Hook.Create("Unity/GUI/ContextClick");
			Hook.Create("Unity/GUI/Mouse/Down");
			Hook.Create("Unity/GUI/Mouse/Up");
			Hook.Create("Unity/GUI/Mouse/Move");
			Hook.Create("Unity/GUI/Mouse/Drag");
			Hook.Create("Unity/GUI/Mouse/EnterWindow");
			Hook.Create("Unity/GUI/Mouse/LeaveWindow");
			Hook.Create("Unity/GUI/Mouse/Drag/Perform");
			Hook.Create("Unity/GUI/Mouse/Drag/End");
			Hook.Create("Unity/GUI/Mouse/Drag/Updated");
			Hook.Create("Unity/GUI/ValidateCommand");
			Hook.Create("Unity/GUI/Ignore");
			Hook.Create("Unity/GUI/Used");
			Hook.Create("Unity/Destroy");
			Hook.Create("Unity/Validate");
			Hook.Create("Unity/Reset");
			Hook.Create("Unity/PlayerConnected");
			Hook.Create("Unity/PlayerDisconnected");
			Hook.Create("Unity/FirstUpdate");
			Hook.Create("Unity/MasterServerEvent");
			Hook.Create("Unity/ApplicationQuit");
			Hook.Create("Unity/ApplicationFocus");
			Hook.Create("Unity/ApplicationPause");
			Hook.Create("Unity/DisconnectedFromServer");
			Hook.Call("Unity/Awake");
		}
		public virtual void Start(){Hook.Call("Unity/Start");}
		public virtual void Update(){
			if(!Proxy.IsLoading() && EventDetector.loading){
				Hook.Call("Unity/FirstUpdate");
				var totalTime = Mathf.Max(Time.Get()-this.loadStart,0);
				if(EventDetector.showTime){
					Log.Show("[Scene] Load complete -- " + (totalTime) + " seconds.");
				}
				this.loadStart = 0;
				EventDetector.loading = false;
			}
			Hook.Call("Unity/Update");
		}
		public virtual void FixedUpdate(){Hook.Call("Unity/FixedUpdate");}
		public virtual void LateUpdate(){Hook.Call("Unity/LateUpdate");}
		public virtual void OnPlayerConnected(){Hook.Call("Unity/PlayerConnected");}
		public virtual void OnPlayerDisconnected(){Hook.Call("Unity/PlayerDisconnected");}
		//public virtual void OnLevelWasLoaded(int level){Hook.Call("Unity/LevelWasLoaded",level);}
		public virtual void OnMasterServerEvent(){Hook.Call("Unity/MasterServerEvent");}
		public virtual void OnApplicationQuit(){Hook.Call("Unity/ApplicationQuit");}
		public virtual void OnApplicationFocus(){Hook.Call("Unity/ApplicationFocus");}
		public virtual void OnApplicationPause(){Hook.Call("Unity/ApplicationPause");}
		public virtual void OnDisconnectedFromServer(){Hook.Call("Unity/DisconnectedFromServer");}
		public virtual void OnGUI(){
			Hook.Call("Unity/GUI");
			var current = Event.current.type;
			if(current == EventType.Repaint){Hook.Call("Unity/GUI/Repaint");}
			else if(current == EventType.Layout){Hook.Call("Unity/GUI/Layout");}
			else if(current == EventType.KeyDown){Hook.Call("Unity/GUI/KeyDown");}
			else if(current == EventType.KeyUp){Hook.Call("Unity/GUI/KeyUp");}
			else if(current == EventType.ScrollWheel){Hook.Call("Unity/GUI/ScrollWheel");}
			else if(current == EventType.ContextClick){Hook.Call("Unity/GUI/ContextClick");}
			else if(current == EventType.MouseDown){Hook.Call("Unity/GUI/Mouse/Down");}
			else if(current == EventType.MouseUp){Hook.Call("Unity/GUI/Mouse/Up");}
			else if(current == EventType.MouseMove){Hook.Call("Unity/GUI/Mouse/Move");}
			else if(current == EventType.MouseDrag){Hook.Call("Unity/GUI/Mouse/Drag");}
			else if(current == EventType.MouseEnterWindow){Hook.Call("Unity/GUI/Mouse/EnterWindow");}
			else if(current == EventType.MouseLeaveWindow){Hook.Call("Unity/GUI/Mouse/LeaveWindow");}
			else if(current == EventType.DragPerform){Hook.Call("Unity/GUI/Mouse/Drag/Perform");}
			else if(current == EventType.DragExited){Hook.Call("Unity/GUI/Mouse/Drag/End");}
			else if(current == EventType.DragUpdated){Hook.Call("Unity/GUI/Mouse/Drag/Updated");}
			else if(current == EventType.ValidateCommand){Hook.Call("Unity/GUI/ValidateCommand");}
			else if(current == EventType.Ignore){Hook.Call("Unity/GUI/Ignore");}
			else if(current == EventType.Used){Hook.Call("Unity/GUI/Used");}
		}
		public virtual void OnEnable(){Hook.Call("Unity/Enable");}
		public virtual void OnDisable(){Hook.Call("Unity/Disable");}
		public virtual void OnDestroy(){
			Hook.Call("Unity/Destroy");
			Hook.all.Remove(this);
		}
		public virtual void Reset(){Hook.Call("Unity/Reset");}
		public virtual void OnBeforeSerialize(){
			EventDetector.wasSerialized = true;
			Proxy.Allow(false);
			Hook.Call("Unity/BeforeSerialize");
			Proxy.Allow(true);
		}
		public virtual void OnAfterDeserialize(){
			//UnityEngine.Debug.Log("Unity/AfterDeserialize");
			/*Proxy.Allow(false);
			Hook.Call("Unity/AfterDeserialize");
			Proxy.Allow(true);*/
			EventDetector.wasDeserialized = true;
		}
	}
	[AutoInitialize(9)]
	public static class EventDetectorHook{
		public static EventDetector detector;
		static EventDetectorHook(){
			EventDetectorHook.Setup();
			Hook.Get("UnityEditor/Update").Add(EventDetectorHook.Update);
		}
		public static void Setup(){
			var main = Locate.GetScenePath("@Main");
			var detector = main.GetComponent<EventDetector>();
			if(detector.IsNull()){
				EventDetectorHook.detector = main.AddComponent<EventDetector>();
				main.hideFlags = HideFlags.HideInHierarchy;
			}
		}
		public static void Update(){
			var detector = EventDetectorHook.detector;
			if(detector.IsNull() || detector.gameObject.IsNull()){
				EventDetectorHook.Setup();
			}
		}
	}
}