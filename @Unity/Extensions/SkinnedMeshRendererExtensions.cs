using UnityEngine;
namespace Zios.Unity.Extensions{
	public static class SkinnedMeshRendererExtension{
		public static void ResetBlendShapes(this SkinnedMeshRenderer renderer){
			var shapeCount = renderer.sharedMesh.blendShapeCount;
			for(var index=0;index<shapeCount;++index){
				renderer.SetBlendShapeWeight(index,0);
			}
		}
	}
}