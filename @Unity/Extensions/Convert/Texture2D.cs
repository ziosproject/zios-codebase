using UnityEngine;
namespace Zios.Unity.Extensions.Convert{
	using Zios.Extensions.Convert;
	public static class ConvertTexture2D{
		//============================
		// From
		//============================
		public static string ToText(this Texture2D current){
			return current.GetPixels32().ToString();
		}
		//============================
		// To
		//============================
		public static Texture2D ToTexture2D(this Texture2D current,string data){
			current.LoadImage(data.ToByteArray());
			return current;
		}
	}
}