using System.Linq;
using UnityEngine;
namespace Zios.Unity.Extensions.Convert{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	public static class ConvertVector2{
		//============================
		// From
		//============================
		public static string ToText(this Vector2 current,bool ignoreDefault=false,Vector2 defaultValue=default(Vector2)){
			return ignoreDefault && current == defaultValue ? "" : current.ToString();
		}
		public static string ToString(this Vector2 current){return "("+current.x+","+current.y+")";}
		public static Vector2 ToRadian(this Vector2 vector){
			var copy = vector;
			copy.x = vector.x / 360.0f;
			copy.y = vector.y / 360.0f;
			return copy;
		}
		public static Quaternion ToRotation(this Vector2 current){
			return Quaternion.Euler(current[1],current[0],current[2]);
		}
		public static float[] ToFloatArray(this Vector2 current){
			return new float[]{current.x,current.y};
		}
		//============================
		// To
		//============================
		public static Vector2 ToVector2(this Vector2 current,string value){return value.ToVector2();}
		public static Vector2 ToVector2(this string current,string separator=","){
			if(!current.Contains(separator)){return Vector2.zero;}
			var values = current.Trim("(",")").Split(separator).ConvertAll<float>().ToArray();
			return new Vector2(values[0],values[1]);
		}
		public static Vector2 ToVector2(this byte[] current){return current.ReadVector2();}
		public static Vector2 ToVector2(this float[] current){
			var x = current.Length >= 1 ? current[0] : 0;
			var y = current.Length >= 2 ? current[1] : 0;
			return new Vector2(x,y);
		}
		public static Vector2 ReadVector2(this byte[] current,int index=0){
			var x = current.ReadFloat(index);
			var y = current.ReadFloat(index+4);
			return new Vector2(x,y);
		}
	}
}