using System.Collections.Generic;
using System.Text;
using UnityEngine;
namespace Zios.Unity.Extensions.Convert{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	public static class AnimationCurveExtension{
		public static string ToText(this AnimationCurve current){
			var output = new StringBuilder();
			foreach(var key in current.keys){
				output.Append(key.time);
				output.Append("-");
				output.Append(key.value);
				output.Append("-");
				output.Append(key.inTangent);
				output.Append("-");
				output.Append(key.outTangent);
				output.Append("|");
			}
			return output.ToString().TrimRight("|");
		}
		public static AnimationCurve ToAnimationCurve(this string value){return new AnimationCurve().SetKeys(value);}
		public static AnimationCurve SetKeys(this AnimationCurve current,string value){
			var keys = new List<Keyframe>();
			foreach(var keyData in value.Split("|")){
				var data = keyData.Split("-").ConvertAll<float>();
				keys.Add(new Keyframe(data[0],data[1],data[2],data[3]));
			}
			return current;
		}
		public static AnimationCurve Copy(this AnimationCurve current){
			return new AnimationCurve(current.keys);
		}
	}
}