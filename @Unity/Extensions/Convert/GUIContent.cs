using UnityEngine;
namespace Zios.Unity.Extensions.Convert{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	public static class ConvertGUIContent{
		//============================
		// From
		//============================
		public static string ToText(this GUIContent current,bool ignoreDefault=false,string defaultValue=""){
			if(ignoreDefault && current.text == defaultValue){return "";}
			var data = current.image.IsNull() ? "" : ConvertTexture2D.ToText(current.image.As<Texture2D>());
			return current.text+"||"+current.tooltip+"||"+data;
		}
		public static string ToString(this GUIContent current){return current.text;}
		//============================
		// To
		//============================
		public static GUIContent ToGUIContent(this GUIContent current,string value){
			var data = value.Split("||");
			current.text = data[0];
			current.tooltip = data[1];
			current.image = data[2].IsEmpty() ? null : new Texture2D(1,1).ToTexture2D(data[2]);
			return current;
		}
		public static GUIContent ToGUIContent(this string current){return new GUIContent(current);}
	}
}