using System.Collections.Generic;
using System.Linq;
using UnityEditor;
namespace Zios.Unity.Editor.Extensions{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	public static class SerializedPropertyExtension{
		public static Dictionary<SerializedProperty,object> cache = new Dictionary<SerializedProperty,object>();
		public static object GetObject(this SerializedProperty current,bool parent=false){
			return current.GetObject<object>(parent);
		}
		public static int GetIndex(this SerializedProperty current){
			var index = -1;
			var path = current.propertyPath;
			if(path.EndsWith("]")){
				var start = path.LastIndexOf('[')+1;
				var end = path.IndexOf(']',start)-1;
				index = path.Cut(start,end).ToInt();
			}
			return index;
		}
		public static T GetObject<T>(this SerializedProperty current,bool parent=false){
			if(cache.ContainsKey(current)){return (T)cache[current];}
			object container = current.serializedObject.targetObject;
			var path = current.propertyPath.Replace(".Array.data[","[");
			var elements = path.Split('.');
			if(parent){elements = elements.Take(elements.Length-1).ToArray();}
			foreach(var element in elements){
				if(element.Contains("[")){
					var elementName = element.Substring(0,element.IndexOf("["));
					var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[","").Replace("]",""));
					container = container.GetVariable(elementName).GetByKey(index);
				}
				else{
					container = container.GetVariable(element);
				}
			}
			cache[current] = container;
			return (T)container;
		}
	}
}