using UnityEngine;
namespace Zios.Unity.Components.Force{
	using Zios.Attributes.Supports;
	using Zios.Supports.Hook;
	using Zios.Unity.Components.ColliderController;
	using Zios.Unity.Components.ManagedBehaviour;
	using Zios.Unity.Extensions;
	//asm Zios.Unity.Components.DataBehaviour;
	[AddComponentMenu("Zios/Component/Motion/Force")]
	public class Force : ManagedBehaviour{
		public AttributeVector3 velocity = Vector3.zero;
		public AttributeVector3 terminalVelocity = new Vector3(20,20,20);
		public AttributeVector3 resistence = new Vector3(8,0,8);
		public AttributeFloat minimumImpactVelocity = 1;
		public AttributeBool disabled = false;
		public override void Awake(){
			base.Awake();
			this.velocity.Setup("Velocity",this);
			this.terminalVelocity.Setup("Terminal Velocity",this);
			this.resistence.Setup("Resistence",this);
			this.minimumImpactVelocity.Setup("Minimum Impact Velocity",this);
			this.disabled.Setup("Disabled",this);
			this.gameObject.CreateHook("Force/Impact");
			this.gameObject.CreateHook("Move");
			this.gameObject.GetHook("Collision").Add<object>(this.OnCollide);
			this.gameObject.GetHook("Add Force").Add<Vector3>(this.AddForce);
			this.gameObject.GetHook("Add Force Raw").Add<Vector3>(this.AddForceRaw);
			this.AddDependent<ColliderController>(this.gameObject);
		}
		public override void Step(){
			if(!this.disabled && this.velocity != Vector3.zero){
				var resistence = Vector3.Scale(this.velocity.Get().Sign(),this.resistence);
				this.velocity.Set(this.velocity - resistence * this.GetTimeOffset());
				this.velocity.Set(this.velocity.Get().Clamp(this.terminalVelocity.Get()*-1,this.terminalVelocity));
				this.gameObject.CallHook("Move",new Vector3(this.velocity.x,0,0));
				this.gameObject.CallHook("Move",new Vector3(0,this.velocity.y,0));
				this.gameObject.CallHook("Move",new Vector3(0,0,this.velocity.z));
			}
		}
		public void AddForce(Vector3 force){
			force *= this.GetTimeOffset();
			this.velocity.Set(this.velocity + force);
		}
		public void AddForceRaw(Vector3 force){
			this.velocity.Set(this.velocity + force);
		}
		public void OnCollide(object collision){
			var data = (CollisionData)collision;
			if(data.isSource){
				var original = this.velocity.Get();
				if(data.sourceController.blocked.forward && this.velocity.z < 0){this.velocity.z.Set(0);}
				if(data.sourceController.blocked.back && this.velocity.z > 0){this.velocity.z.Set(0);}
				if(data.sourceController.blocked.up && this.velocity.y > 0){this.velocity.y.Set(0);}
				if(data.sourceController.blocked.down && this.velocity.y < 0){this.velocity.y.Set(0);}
				if(data.sourceController.blocked.right && this.velocity.x > 0){this.velocity.x.Set(0);}
				if(data.sourceController.blocked.left && this.velocity.x < 0){this.velocity.x.Set(0);}
				if(original != this.velocity.Get()){
					var impact = (this.velocity - original);
					var impactStrength = impact.magnitude;
					if(impactStrength > this.minimumImpactVelocity){
						this.gameObject.CallHook("Force/Impact",impact);
					}
				}
			}
		}
	}
}