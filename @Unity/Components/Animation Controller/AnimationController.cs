using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Zios.Unity.Components.AnimationController{
	using Zios.Attributes.Supports;
	using Zios.Extensions;
	using Zios.Log;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Components.ManagedBehaviour;
	using Zios.Unity.Proxy;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Components.DataBehaviour;
	//asm Zios.Unity.Shortcuts;
	[Serializable]
	public class AnimationData{
		public string name;
		public int priority = -1;
		public float weight = -1;
		public float transitionIn = -1;
		public float transitionOut = -1;
		public bool rewindOnPlay = true;
		public AnimationData currentAnimation;
		public AnimationState state;
		[NonSerialized] public float originalWeight;
		[NonSerialized] public bool active;
	}
	[RequireComponent(typeof(Animation))][AddComponentMenu("Zios/Component/Animation/Animation Controller")]
	public class AnimationController : ManagedBehaviour{
		public AttributeBool highestPriorityOnly = true;
		public AttributeInt defaultPriority = 1;
		public AttributeFloat defaultTransitionIn = 0.15f;
		public AttributeFloat defaultTransitionOut = 0.15f;
		public AttributeString defaultAnimationName = "";
		[Internal][ReadOnly] public AnimationData defaultAnimation;
		[Internal][ReadOnly] public List<AnimationData> currentAnimations;
		public List<AnimationData> animations = new List<AnimationData>();
		public Dictionary<string,AnimationData> current = new Dictionary<string,AnimationData>();
		public Dictionary<string,AnimationData> lookup = new Dictionary<string,AnimationData>();
		//=====================
		// Built-in
		//=====================
		public override void Awake(){
			base.Awake();
			this.Build();
			this.highestPriorityOnly.Setup("Highest Priority Only",this);
			this.defaultPriority.Setup("Default Priority Only",this);
			this.defaultTransitionIn.Setup("Default Transition In",this);
			this.defaultTransitionOut.Setup("Default Transition Out",this);
			this.defaultAnimationName.Setup("Default Animation Name",this);
			this.gameObject.GetHook("Set Animation").Add<string,bool,int>(this.Set);
			this.gameObject.GetHook("Set Animation Default").Add<string>(this.SetDefault);
			this.gameObject.GetHook("Set Animation Speed").Add<string,float>(this.SetSpeed);
			this.gameObject.GetHook("Set Animation Weight").Add<string,float>(this.SetWeight);
			this.gameObject.GetHook("Play Animation").Add<string,int>(this.Play);
			this.gameObject.GetHook("Stop Animation").Add<string>(this.Stop);
			if(this.animations.Count > 0){
				this.defaultAnimationName.SetDefault(this.animations.First().name);
			}
		}
		public override void Start(){
			base.Start();
			if(Proxy.IsPlaying()){
				foreach(var data in this.animations){
					this.lookup[data.name] = data;
					if(data.weight == -1){data.weight = 1.0f;}
					if(data.priority == -1){data.priority = this.defaultPriority;}
					if(data.transitionIn == -1){data.transitionIn = this.defaultTransitionIn;}
					if(data.transitionOut == -1){data.transitionOut = this.defaultTransitionOut;}
					data.originalWeight = data.weight;
				}
				if(this.lookup.ContainsKey(this.defaultAnimationName)){
					this.defaultAnimation = this.lookup[this.defaultAnimationName];
					this.defaultAnimation.active = true;
					this.current[this.name] = this.defaultAnimation;
				}
				else{
					Log.Add("[AnimationController] Default animation (" + this.defaultAnimationName + ") not found.").Type("Warning").Target(this.gameObject).Show();
				}
			}
		}
		private void PlayDefault(){
			var exists = this.defaultAnimation != null && this.lookup.ContainsKey(this.defaultAnimation.name);
			if(!exists){return;}
			float currentWeight = 0;
			var fallback = this.defaultAnimation;
			var name = this.defaultAnimation.name;
			foreach(var item in this.current){
				if(item.Value == fallback){continue;}
				currentWeight += item.Value.state.weight;
			}
			currentWeight = currentWeight < 1 ? 1.0f-currentWeight : 0;
			var transitionTime = this.lookup[name].weight < currentWeight ? fallback.transitionOut : fallback.transitionOut;
			this.lookup[name].weight = currentWeight;
			this.GetComponent<Animation>().Blend(name,currentWeight,transitionTime);
		}
		public override void Step(){
			this.PlayDefault();
			foreach(var data in this.animations){
				if(this.current.ContainsKey(data.name)){
					var name = data.name;
					var zeroWeight = this.lookup[name].weight <= 0.01f && data != this.defaultAnimation;
					if(!data.active || zeroWeight){
						this.GetComponent<Animation>().Blend(name,0,data.transitionOut);
						data.weight = data.originalWeight;
						this.current.Remove(name);
					}
				}
			}
			if(Proxy.IsEditor()){
				this.currentAnimations = this.current.Values.ToList();
			}
		}
		//=====================
		// Internal
		//=====================
		private void Build(){
			var animations = this.GetComponent<Animation>();
			foreach(AnimationState state in animations){
				var data = this.animations.Find(x=>x.name==state.name);
				if(data == null){
					data = new AnimationData();
					this.animations.Add(data);
				}
				data.name = state.name;
				data.state = state;
			}
		}
		[ContextMenu("Restore Defaults")]
		private void RestoreDefaults(){
			foreach(AnimationState state in this.GetComponent<Animation>()){
				var data = this.animations.Find(x=>x.name==state.name);
				if(data != null){
					data.weight = -1;
					data.transitionOut = -1;
					data.transitionIn = -1;
					data.priority = -1;
				}
			}
		}
		public void SetDefault(string name){
			if(this.lookup.ContainsKey(name)){
				this.defaultAnimationName.Set(name);
				this.defaultAnimation = this.lookup[name];
			}
		}
		//=====================
		// Operations
		//=====================
		public void Play(string name,int priority=1){
			var exists = this.lookup.ContainsKey(name);
			var active = this.current.ContainsKey(name);
			if(exists && !active){
				if(this.highestPriorityOnly){
					if(priority == -1){priority = this.lookup[name].priority;}
					foreach(var item in this.current){
						if(item.Value.priority > priority){return;}
					}
					foreach(var item in this.current){
						if(priority > item.Value.priority){this.Stop(item.Value.name);}
					}
				}
				this.current[name] = this.lookup[name];
				this.current[name].active = true;
				if(this.current[name].rewindOnPlay){this.GetComponent<Animation>().Rewind(name);}
				this.GetComponent<Animation>().Blend(name,this.lookup[name].weight,this.lookup[name].transitionIn);
			}
		}
		public void Play(AnimationData data){
			this.Play(data.name,data.priority);
		}
		public void Stop(AnimationData data){
			this.Stop(data.name);
		}
		public void Stop(string name=null){
			if(name.IsEmpty()){
				foreach(var item in this.current){
					item.Value.active = false;
				}
				return;
			}
			if(this.current.ContainsKey(name)){
				this.current[name].active = false;
			}
		}
		public void Set(string name,bool state,int priority=1){
			if(state){
				this.Play(name,priority);
				return;
			}
			this.Stop(name);
		}
		public void SetSpeed(string name,float speed){
			if(this.GetComponent<Animation>()[name]){
				this.GetComponent<Animation>()[name].speed = speed;
			}
		}
		public void SetWeight(string name,float weight){
			if(this.GetComponent<Animation>()[name] && weight != this.lookup[name].weight){
				var data = this.lookup[name];
				data.weight = weight;
				if(this.current.ContainsKey(name)){
					var transitionTime = data.state.weight < weight ? data.transitionOut : data.transitionOut;
					this.GetComponent<Animation>().Blend(name,data.weight,transitionTime);
				}
			}
		}
	}
}