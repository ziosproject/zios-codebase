namespace Zios.Unity.Editor.Components.PC2{
	using Zios.Unity.Components.PC2;
	using Zios.Unity.Editor.EditorUI;
	using Zios.Unity.Editor.File;
	//asm Zios.File;
	//asm Zios.Reflection;
	//asm UnityEngine;
	//asm UnityEditor;
	public class FilePC2Editor : UnityEditor.Editor{
		public PC2Data data;
		public override void OnInspectorGUI(){
			EditorUI.Reset();
			//GUI.enabled = true;
			if(this.data == null){
				var file = UnityFile.Get(this.target);
				this.data = new PC2Data();
				data.Load(file.path);
			}
			this.data.DrawFields("");
		}
	}
}