using System.Linq;
using UnityEngine;
namespace Zios.Unity.Locate.LocateEvents{
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Locate;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	[AutoInitialize(15)]
	public static class LocateEvents{
		private static Component[] allComponents = new Component[0];
		static LocateEvents(){
			Hook.Get("Unity/DeserializeComplete").Add(LocateEvents.SetDirty);
			Hook.Get("UnityEditor/HierarchyChanged").Add(LocateEvents.SetDirty);
			Hook.Get("UnityEditor/Asset/Changed").Add(()=>Locate.assets.Clear());
			Hook.Create("DataBehaviour/ComponentsChanged");
		}
		public static void BuildComponents(){
			var components = Resources.FindObjectsOfTypeAll<Component>();
			if(components.Length != LocateEvents.allComponents.Count() && !LocateEvents.allComponents.SequenceEqual(components)){
				if(Locate.setup){Hook.Call("DataBehaviour/ComponentsChanged");}
				LocateEvents.allComponents = components;
			}
		}
		public static void SetDirty(){
			LocateEvents.BuildComponents();
			Locate.SetDirty();
		}
	}
}