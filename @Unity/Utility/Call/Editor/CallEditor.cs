using System;
using UnityEditor;
namespace Zios.Unity.Editor.Call{
	using Zios.SystemAttributes;
	using Zios.Unity.Call;
	using Zios.Unity.Proxy;
	using CallbackFunction = UnityEditor.EditorApplication.CallbackFunction;
	[AutoInitialize]
	public static class CallEditor{
		static CallEditor(){
			Call.delayHook = (method)=>{
				var callback = new CallbackFunction(method);
				if(EditorApplication.delayCall != callback){
					EditorApplication.delayCall += callback;
				}
			};
		}
		public static void Editor(Action method){
			if(!Proxy.IsPlaying()){
				method();
			}
		}
	}
}