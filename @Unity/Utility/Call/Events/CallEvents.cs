//addon Zios.Unity.Call;
namespace Zios.Unity.Call.Events{
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Unity.Call;
	using Zios.Unity.Proxy;
	using Zios.Unity.Time;
	//asm Zios.Shortcuts;
	//asm Zios.Unity.Shortcuts;
	//asm Zios.Unity.Editor.Hooks;
	[AutoInitialize]
	public static class CallEvents{
		private static float sceneCheck;
		static CallEvents(){
			Hook.Create("UnityEditor/OnSceneLoaded");
			Hook.Create("UnityEditor/OnEditorSceneLoaded");
			Hook.Get("Unity/LateUpdate").Add(Call.CheckDelayed);
			Hook.Get("Unity/LateUpdate").Add(CallEvents.CheckLoaded);
			Hook.Get("UnityEditor/Update").Add(()=>CallEvents.CheckLoaded(true));
			Hook.Get("UnityEditor/Update").Add(()=>Call.CheckDelayed(true));
		}
		public static void CheckLoaded(){CallEvents.CheckLoaded(false);}
		public static void CheckLoaded(bool editor){
			if(editor && Proxy.IsPlaying()){return;}
			if(!editor && !Proxy.IsPlaying()){return;}
			if(Time.Get() < 0.5 && CallEvents.sceneCheck == 0){
				var term = editor ? " Editor" : "";
				Hook.Call("UnityEditor/" + term + "SceneLoaded");
				CallEvents.sceneCheck = 1;
			}
			if(Time.Get() > CallEvents.sceneCheck){
				CallEvents.sceneCheck = 0;
			}
		}
	}
}