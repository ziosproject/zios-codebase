using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using CallbackFunction = UnityEditor.EditorApplication.CallbackFunction;
using UnityObject = UnityEngine.Object;
using UnityUndo = UnityEditor.Undo;
namespace Zios.Unity.Editor.ProxyEditor{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.SystemAttributes;
	using Zios.Unity.Call;
	using Zios.Unity.Editor.Reflection;
	using Zios.Unity.Proxy;
	using Editor = UnityEditor.Editor;
	[AutoInitialize]
	public static class ProxyEditor{
		private static List<UnityObject> delayedDirty = new List<UnityObject>();
		private static EditorWindow inspector;
		private static EditorWindow[] inspectors;
		private static Dictionary<Editor,EditorWindow> editorInspectors = new Dictionary<Editor,EditorWindow>();
		private static Dictionary<UnityObject,SerializedObject> serializedObjects = new Dictionary<UnityObject,SerializedObject>();
		static ProxyEditor(){
			Proxy.busyMethods.Add(ProxyEditor.IsChanging);
			Proxy.busyMethods.Add(ProxyEditor.IsCompiling);
			Proxy.busyMethods.Add(ProxyEditor.IsUpdating);
		}
		//============================
		// AssetDatabase
		//============================
		public static void StartAssetEditing(){
			AssetDatabase.StartAssetEditing();
		}
		public static void StopAssetEditing(){
			AssetDatabase.StopAssetEditing();
		}
		public static void RefreshAssets(){
			AssetDatabase.Refresh();
		}
		public static void SaveAssets(){
			AssetDatabase.SaveAssets();
		}
		public static void ImportAsset(string path){
			AssetDatabase.ImportAsset(path);
		}
		public static void CreateAsset(UnityObject target,string path){
			AssetDatabase.CreateAsset(target,path);
		}
		public static void DeleteAsset(string path){
			AssetDatabase.DeleteAsset(path);
		}
		public static string GetGUID(string path){
			return AssetDatabase.AssetPathToGUID(path);
		}
		public static string GetAssetPath(UnityObject target){
			return AssetDatabase.GetAssetPath(target);
		}
		public static string GetAssetPath(string guid){
			return AssetDatabase.GUIDToAssetPath(guid);
		}
		public static UnityObject LoadAsset(string path,Type type){
			return AssetDatabase.LoadAssetAtPath(path,type);
		}
		public static UnityObject LoadMainAsset(string path){
			return AssetDatabase.LoadMainAssetAtPath(path);
		}
		public static Type LoadAsset<Type>(string path) where Type : UnityObject{
			return AssetDatabase.LoadAssetAtPath<Type>(path);
		}
		public static MonoScript GetMonoScript(MonoBehaviour behaviour){
			return MonoScript.FromMonoBehaviour(behaviour);
		}
		public static bool WriteImportSettings(string path){
			return AssetDatabase.WriteImportSettingsIfDirty(path);
		}
		public static bool CopyAsset(string from,string to){
			return AssetDatabase.CopyAsset(from,to);
		}
		//============================
		// Assets
		//============================
		public static void ClearDirty(){
			ProxyEditor.delayedDirty.Clear();
		}
		public static void MarkSceneDirty(){
			#if UNITY_5_3_OR_NEWER
			UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
			#else
			EditorApplication.MarkSceneDirty();
			#endif
		}
		public static void SetDirty(UnityObject target,bool delayed=false,bool forced=false){
			if(ProxyEditor.IsPlaying()){return;}
			if(target.IsNull()){return;}
			if(delayed){
				if(!ProxyEditor.delayedDirty.Contains(target)){
					//Hook.AddLimited("UnityEditor/EnterPlay",()=>ProxyEditor.SetDirty(target),1);
					//Hook.AddLimited("UnityEditor/EnterPlay",Proxy.ClearDirty,1);
					ProxyEditor.delayedDirty.AddNew(target);
				}
				return;
			}
			EditorUtility.SetDirty(target);
			ProxyEditor.MarkSceneDirty();
			//Utility.UpdatePrefab(target);
		}
		public static void SetAssetDirty(UnityObject target){
			var path = AssetDatabase.GetAssetPath(target);
			var asset = AssetDatabase.LoadMainAssetAtPath(path);
			ProxyEditor.SetDirty(asset,false,true);
		}
		public static bool IsDirty(UnityObject target){
			return typeof(EditorUtility).Call<bool>("IsDirty",target.GetInstanceID());
		}
		//============================
		// PrefabUtility
		//============================
		public static UnityObject GetPrefab(UnityObject target){
			#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.GetPrefabInstanceHandle(target);
			#else
			return PrefabUtility.GetPrefabObject(target);
			#endif
		}
		public static GameObject GetPrefabRoot(GameObject target){
			#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.GetOutermostPrefabInstanceRoot(target);
			#else
			return PrefabUtility.FindPrefabRoot(target);
			#endif
		}
		public static PrefabType GetPrefabType(UnityObject target){
			#if UNITY_2018_3_OR_NEWER
			var data = new PrefabType();
			var type = PrefabUtility.GetPrefabAssetType(target);
			var status = PrefabUtility.GetPrefabInstanceStatus(target);
			var isPrefab = !type.Is("NotAPrefab");
			var isInstance = !status.Is("NotAPrefab");
			if(isPrefab){
				if(isInstance){
					var isDisconnected = status.Is("Disconnected");
					if(isDisconnected){
						if(type.Is("Regular")){data |= PrefabType.DisconnectedPrefabInstance;}
						if(type.Is("Model")){data |= PrefabType.DisconnectedModelPrefabInstance;}
					}
					else{
						if(type.Is("Regular")){data |= PrefabType.PrefabInstance;}
						if(type.Is("Model")){data |= PrefabType.ModelPrefabInstance;}
						if(status.Is("MissingAsset")){data |= PrefabType.MissingPrefabInstance;}
					}
				}
				else{
					if(type.Is("Regular")){data |= PrefabType.Prefab;}
					if(type.Is("Model")){data |= PrefabType.ModelPrefab;}
					if(type.Is("MissingAsset")){data |= PrefabType.MissingPrefabInstance;}
				}
			}
			else{
				data |= PrefabType.None;
			}
			return data;
			#else
			return PrefabUtility.GetPrefabType(target).ToInt().As<PrefabType>();
			#endif
		}
		public static UnityObject CreateEmptyPrefab(GameObject target,string path){
			#if UNITY_2018_3_OR_NEWER
			return PrefabUtility.SaveAsPrefabAsset(target, path);
			#else
			return PrefabUtility.CreateEmptyPrefab(path);
			#endif
		}
		public static object GetCorrespondingObjectFromSource(GameObject target){
			return PrefabUtility.GetCorrespondingObjectFromSource(target);
		}
		public static void ApplyPrefab(GameObject target){
			#if UNITY_2018_3_OR_NEWER
			GameObject root = PrefabUtility.GetOutermostPrefabInstanceRoot(target);
			PrefabUtility.SaveAsPrefabAssetAndConnect(root,PrefabUtility.GetCorrespondingObjectFromSource(root).ToString(), InteractionMode.AutomatedAction);
			#elif UNITY_2018_2
			var root = PrefabUtility.FindPrefabRoot(target);
			ProxyEditor.ReplacePrefab(root,PrefabUtility.GetCorrespondingObjectFromSource(root),ReplacePrefabOptions.ConnectToPrefab);
			#else
			GameObject root = PrefabUtility.FindPrefabRoot(target);
			ProxyEditor.ReplacePrefab(root,PrefabUtility.GetPrefabParent(root),ReplacePrefabOptions.ConnectToPrefab);
			#endif
		}
		/*
		#if UNITY_2018_3_OR_NEWER
		public static int ReplacePrefabOptions(int replacePrefabType,GameObject target,string path){
			if (replacePrefabType == 0)
				PrefabUtility.SavePrefabAsset(target);
			else if (replacePrefabType == 1)
				PrefabUtility.SaveAsPrefabAssetAndConnect(target, path, InteractionMode.AutomatedAction);
			else if (replacePrefabType == 2)
				PrefabUtility.SaveAsPrefabAsset(target, path);
			return (replacePrefabType);
		}
		#else
		public static ReplacePrefabOptions ReplacePrefabOptions(int replacePrefabType,GameObject target){
			ReplacePrefabOptions replacePrefabOptions = new ReplacePrefabOptions();
			switch (replacePrefabType)
			{
				case 0:
					replacePrefabOptions = UnityEditor.ReplacePrefabOptions.Default;
					return replacePrefabOptions;
				case 1:
					replacePrefabOptions = UnityEditor.ReplacePrefabOptions.ConnectToPrefab;
					return replacePrefabOptions;
				case 2:
					replacePrefabOptions = UnityEditor.ReplacePrefabOptions.ReplaceNameBased;
					return replacePrefabOptions;
				default:
					return replacePrefabOptions;
			}
		}
		#endif
		*/
		public static void ApplyPrefab(GameObject target, object source){
			#if UNITY_2018_3_OR_NEWER
			GameObject root = PrefabUtility.GetOutermostPrefabInstanceRoot(target);
			PrefabUtility.SaveAsPrefabAssetAndConnect(root,PrefabUtility.GetCorrespondingObjectFromSource(root).ToString(), InteractionMode.AutomatedAction);
			#elif UNITY_2018_2
			var root = PrefabUtility.FindPrefabRoot(target);
			ProxyEditor.ReplacePrefab(root, PrefabUtility.GetCorrespondingObjectFromSource(root),(ReplacePrefabOptions.ConnectToPrefab));
			#else
			GameObject root = PrefabUtility.FindPrefabRoot(target);
			ProxyEditor.ReplacePrefab(root,PrefabUtility.GetPrefabParent(root),ReplacePrefabOptions.ConnectToPrefab);
			#endif
		}
		public static void ReplacePrefab(GameObject target, object source, ReplacePrefabOptions replacePrefabOptions){
			#if UNITY_2018_3_OR_NEWER
			GameObject root = PrefabUtility.GetOutermostPrefabInstanceRoot(target);
			PrefabUtility.SaveAsPrefabAssetAndConnect(root,PrefabUtility.GetCorrespondingObjectFromSource(root).ToString(), InteractionMode.AutomatedAction);
			#elif UNITY_2018_2
			var root = PrefabUtility.FindPrefabRoot(target);
			PrefabUtility.ReplacePrefab(root, PrefabUtility.GetCorrespondingObjectFromSource(root),(UnityEditor.ReplacePrefabOptions)ReplacePrefabOptions.ConnectToPrefab);
			#else
			GameObject root = PrefabUtility.FindPrefabRoot(target);
			PrefabUtility.ReplacePrefab(root,PrefabUtility.GetPrefabParent(root),ReplacePrefabOptions.ConnectToPrefab);
			#endif
		}
		public static void UpdatePrefab(UnityObject target){
			PrefabUtility.RecordPrefabInstancePropertyModifications(target);
		}
		public static bool ReconnectToLastPrefab(GameObject target){
			#if UNITY_2018_3_OR_NEWER
			PrefabUtility.RevertPrefabInstance(target, InteractionMode.UserAction);
			return PrefabUtility.IsDisconnectedFromPrefabAsset(target);
			#else
			return PrefabUtility.ReconnectToLastPrefab(target);
			#endif
		}
		public static void DisconnectPrefabInstance(UnityObject target){
			#if UNITY_2018_3_OR_NEWER
			PrefabUtility.UnpackPrefabInstance(target.As<GameObject>(), PrefabUnpackMode.Completely, InteractionMode.UserAction);
			#else
			PrefabUtility.DisconnectPrefabInstance(target);
		#endif
		}
		public static UnityEngine.Object InstantiatePrefab(UnityObject target){
			return PrefabUtility.InstantiatePrefab(target);
		}
		//============================
		// ShaderUtil
		//============================
		public static string GetPropertyName(Shader shader,int index){
			return ShaderUtil.GetPropertyName(shader,index);
		}
		public static ShaderUtil.ShaderPropertyType GetPropertyType(Shader shader,int index){
			return ShaderUtil.GetPropertyType(shader,index);
		}
		public static int GetPropertyCount(Shader shader){
			return ShaderUtil.GetPropertyCount(shader);
		}
		//============================
		// EditorApplication
		//============================
		public static bool IsChanging(){
			return Proxy.Allow() && EditorApplication.isPlayingOrWillChangePlaymode;
		}
		public static bool IsUpdating(){
			return Proxy.Allow() && EditorApplication.isUpdating;
		}
		public static bool IsPaused(){
			return Proxy.Allow() && EditorApplication.isPaused;
		}
		public static bool IsPlaying(){
			return Proxy.Allow() && (Application.isPlaying || ProxyEditor.IsChanging());
		}
		public static bool IsCompiling(){
			return Proxy.Allow() && EditorApplication.isCompiling;
		}
		public static bool IsSwitching(){
			return Proxy.Allow() && (ProxyEditor.IsChanging() || ProxyEditor.IsCompiling());
		}
		public static void AddUpdate(CallbackFunction method){
			EditorApplication.update -= method;
			EditorApplication.update += method;
		}
		public static void RemoveUpdate(CallbackFunction method){
			EditorApplication.update -= method;
		}
		public static void AddModeChange(CallbackFunction method){
			#if UNITY_2017_2_OR_NEWER
			EditorApplication.playModeStateChanged += (x)=>method();
			#else
			EditorApplication.playmodeStateChanged += method;
			#endif
		}
		public static void RemoveModeChange(CallbackFunction method){
			#if UNITY_2017_2_OR_NEWER
			#else
			EditorApplication.playmodeStateChanged -= method;
			#endif
		}
		#if UNITY_2018_1_OR_NEWER
		public static void HierarchyChanged(Action method){
			EditorApplication.hierarchyChanged += method;
		}
		#else
		public static void HierarchyChanged(CallbackFunction method){
			EditorApplication.hierarchyWindowChanged += method;
		}
		#endif
		#if UNITY_2018_1_OR_NEWER
		public static void ProjectChanged(Action method){
			EditorApplication.projectChanged += method;
		}
		#else
		public static void ProjectChanged(CallbackFunction method){
			EditorApplication.projectWindowChanged += method;
		}
		#endif
		public static void PrefabInstanceUpdated(PrefabUtility.PrefabInstanceUpdated method){
			PrefabUtility.prefabInstanceUpdated += method;
		}
		//============================
		// EditorUtility
		//============================
		public static string SaveFilePanel(string title,string directory,string fallback,string extension){
			return EditorUtility.SaveFilePanel(title,directory,fallback,extension);
		}
		public static SerializedObject GetSerializedObject(UnityObject target){
			if(!ProxyEditor.serializedObjects.ContainsKey(target)){
				ProxyEditor.serializedObjects[target] = new SerializedObject(target);
			}
			return ProxyEditor.serializedObjects[target];
		}
		public static SerializedObject GetSerialized(UnityObject target){
			var type = typeof(SerializedObject);
			return type.Call<SerializedObject>("LoadFromCache",target.GetInstanceID());
		}
		public static void UpdateSerialized(UnityObject target){
			var serialized = ProxyEditor.GetSerializedObject(target);
			serialized.Update();
			serialized.ApplyModifiedProperties();
			//ProxyEditor.UpdatePrefab(target);
		}
		public static EditorWindow[] GetInspectors(){
			if(ProxyEditor.inspectors == null){
				var inspectorType = ReflectionUnity.GetType("InspectorWindow");
				ProxyEditor.inspectors = inspectorType.Call<EditorWindow[]>("GetAllInspectorWindows");
			}
			return ProxyEditor.inspectors;
		}
		public static EditorWindow GetInspector(Editor editor){
			if(!ProxyEditor.editorInspectors.ContainsKey(editor)){
				var inspectorType = ReflectionUnity.GetType("InspectorWindow");
				var windows = inspectorType.Call<EditorWindow[]>("GetAllInspectorWindows");
				for(var index=0;index<windows.Length;++index){
					var tracker = windows[index].GetVariable<ActiveEditorTracker>("m_Tracker");
					if(tracker == null){continue;}
					for(var editorIndex=0;editorIndex<tracker.activeEditors.Length;++editorIndex){
						var current = tracker.activeEditors[editorIndex];
						ProxyEditor.editorInspectors[current] = windows[index];
					}
				}
			}
			return ProxyEditor.editorInspectors[editor];;
		}
		public static Vector2 GetInspectorScroll(Editor editor){
			return ProxyEditor.GetInspector(editor).GetVariable<Vector2>("m_ScrollPosition");
		}
		public static Vector2 GetInspectorScroll(){
			if(ProxyEditor.inspector.IsNull()){
				var inspectorWindow = ReflectionUnity.GetType("InspectorWindow");
				ProxyEditor.inspector = EditorWindow.GetWindow(inspectorWindow);
			}
			return ProxyEditor.inspector.GetVariable<Vector2>("m_ScrollPosition");
		}
		public static Vector2 GetInspectorScroll(this Rect current){
			var inspectorWindow = ReflectionUnity.GetType("InspectorWindow");
			var window = EditorWindow.GetWindowWithRect(inspectorWindow,current);
			return window.GetVariable<Vector2>("m_ScrollPosition");
		}

		//============================
		// Undo
		//============================
		public static void RecordObject(UnityObject target,string name){
			if(target.IsNull()){return;}
			UnityUndo.RecordObject(target,name);
		}
		public static void RecordObjects(UnityObject[] targets,string name){
			if(targets.IsNull()){return;}
			UnityUndo.RecordObjects(targets,name);
		}
		public static void RegisterCompleteObjectUndo(UnityObject target,string name){
			UnityUndo.RegisterCompleteObjectUndo(target,name);
		}
		public static void RegisterCreatedObjectUndo(UnityObject target,string name){
			UnityUndo.RegisterCreatedObjectUndo(target,name);
		}
		public static void RegisterSceneUndo(UnityObject target,string name){
			#if UNITY_3_0 || UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
			UnityUndo.RegisterSceneUndo(name);
			#else
			UnityUndo.RegisterCompleteObjectUndo(target,name);
			#endif
		}
		public static void RegisterUndo(UnityObject target,string name){
			#if UNITY_3_0 || UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
			UnityUndo.RegisterUndo(target,name);
			#else
			UnityUndo.RecordObject(target,name);
			#endif
		}
		public static void RegisterUndo(UnityObject[] targets,string name){
			#if UNITY_3_0 || UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
			UnityUndo.RegisterUndo(targets,name);
			#else
			UnityUndo.RecordObjects(targets,name);
			#endif
		}
		//============================
		// Other
		//============================
		public static void UpdateSelection(){
			var targets = Selection.objects;
			var focus = GUI.GetNameOfFocusedControl();
			if(targets.Length > 0){
				Selection.activeObject = null;
				Call.Delay(()=>{
					Selection.objects = targets;
					EditorGUI.FocusTextInControl(focus);
					GUI.FocusControl(focus);
				},0.05f);
			}
		}
		public static void RebuildInspectors(){
			//typeof(EditorUtility).Call("ForceRebuildInspectors");
			var inspectorType = ReflectionUnity.GetType("InspectorWindow");
			var windows = inspectorType.Call<EditorWindow[]>("GetAllInspectorWindows");
			for(var index=0;index<windows.Length;++index){
				if(windows[index].IsNull()){continue;}
				var tracker = windows[index].GetVariable<ActiveEditorTracker>("m_Tracker");
				if(tracker == null || System.Object.Equals(tracker,null)){continue;}
				tracker.ForceRebuild();
			}
		}
		public static void ShowInspectors(){
			var inspectorType = ReflectionUnity.GetType("InspectorWindow");
			var windows = inspectorType.Call<EditorWindow[]>("GetAllInspectorWindows");
			for(var index=0;index<windows.Length;++index){
				var tracker = windows[index].GetVariable<ActiveEditorTracker>("m_Tracker");
				if(tracker == null || System.Object.Equals(tracker,null)){continue;}
				for(var editorIndex=0;editorIndex<tracker.activeEditors.Length;++editorIndex){
					tracker.SetVisible(editorIndex,1);
				}
			}
		}
		public static void RepaintInspectors(){
			var inspectorType = ReflectionUnity.GetType("InspectorWindow");
			inspectorType.Call("RepaintAllInspectors");
		}
		public static void RepaintToolbar(){
			ReflectionUnity.GetType("Toolbar").Call("RepaintToolbar");
		}
		public static void RepaintAll(){
			//foreach(var window in Locate.GetAssets<EditorWindow>()){window.Repaint();}
			UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
			//typeof(EditorApplication).Call("RequestRepaintAllViews");
			//ReflectionUnity.GetType("InspectorWindow").Call("RepaintAllInspectors");
			ProxyEditor.RepaintToolbar();
		}
		public static void RepaintGameView(){
			var viewType = ReflectionUnity.GetType("GameView");
			var gameview = EditorWindow.GetWindow(viewType);
			gameview.Repaint();
		}
		public static void RepaintSceneView(){
			if(SceneView.lastActiveSceneView != null){
				SceneView.lastActiveSceneView.Repaint();
			}
		}
		public static int GetLocalID(int instanceID){
			return Unsupported.GetLocalIdentifierInFile(instanceID);
		}
		public static bool MoveComponentUp(Component component){
			return ReflectionUnity.GetType("ComponentUtility").Call<bool>("MoveComponentUp",component);
		}
		public static bool MoveComponentDown(Component component){
			return ReflectionUnity.GetType("ComponentUtility").Call<bool>("MoveComponentDown",component);
		}
	}
	public enum PrefabType{None,Prefab,ModelPrefab,PrefabInstance,ModelPrefabInstance,MissingPrefabInstance,DisconnectedPrefabInstance,DisconnectedModelPrefabInstance}
	public enum ReplacePrefabOptions{Default,ConnectToPrefab,ReplaceNameBased}
}
namespace Zios.Unity.Editor.ProxyEditor{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Unity.Editor.Reflection;
	public static class EditorGameObjectExtensions{
		public static void UpdateSerialized(this Component current){
			ProxyEditor.UpdateSerialized(current);
		}
		public static GameObject GetPrefabRoot(this GameObject current){
			return ProxyEditor.GetPrefabRoot(current);
		}
		public static GameObject GetPrefabRoot(this Component current){
			if(current.IsNull()){return null;}
			return current.gameObject.GetPrefabRoot();
		}
	}
	public static class EditorComponentExtensions{
		public static void Move(this Component current,int amount){
			if(current.IsNull()){return;}
			ProxyEditor.DisconnectPrefabInstance(current);
			while(amount != 0){
				if(amount > 0){
					ProxyEditor.MoveComponentDown(current);
					amount -= 1;
				}
				if(amount < 0){
					ProxyEditor.MoveComponentUp(current);
					amount += 1;
				}
			}
		}
		public static void MoveUp(this Component current){
			if(current.IsNull()){return;}
			var components = current.GetComponents<Component>();
			var position = components.IndexOf(current);
			var amount = 1;
			if(position != 0){
				while(components[position-1].hideFlags.Contains(HideFlags.HideInInspector)){
					position -= 1;
					amount += 1;
				}
			}
			current.Move(-amount);
		}
		public static void MoveDown(this Component current){
			if(current.IsNull()){return;}
			var components = current.GetComponents<Component>();
			var position = components.IndexOf(current);
			var amount = 1;
			if(position < components.Length-1){
				while(components[position+1].hideFlags.Contains(HideFlags.HideInInspector)){
					position += 1;
					amount += 1;
				}
			}
			current.Move(amount);
		}
		public static void MoveToTop(this Component current){
			if(current.IsNull()){return;}
			ProxyEditor.DisconnectPrefabInstance(current);
			var components = current.GetComponents<Component>();
			var position = components.IndexOf(current);
			current.Move(-position);
		}
		public static void MoveToBottom(this Component current){
			if(current.IsNull()){return;}
			ProxyEditor.DisconnectPrefabInstance(current);
			var components = current.GetComponents<Component>();
			var position = components.IndexOf(current);
			current.Move(components.Length-position);
		}
	}
	public static class EditorUnityObjectExtensions{
		public static UnityObject GetPrefab(this UnityObject current){
			return ProxyEditor.GetPrefab(current);
		}
	}
	public static class EditorRectExtensions{
		public static EditorWindow GetInspectorWindow(this Rect current){
			var inspectorWindow = ReflectionUnity.GetType("InspectorWindow");
			return EditorWindow.GetWindowWithRect(inspectorWindow,current);
		}
		public static Rect GetInspectorArea(this Rect current,EditorWindow window=null){
			var windowRect = new Rect(0,0,Screen.width,Screen.height);
			//var window = current.GetInspectorWindow();
			//Log.Show(window.GetVariable<Rect>("position"));
			if(window == null){window = ProxyEditor.GetInspectors().First();}
			var scroll = window.GetVariable<Vector2>("m_ScrollPosition");
			windowRect.x = scroll.x;
			windowRect.y = scroll.y;
			return windowRect;
		}
		public static bool InInspectorWindow(this Rect current,EditorWindow window=null){
			var windowRect = current.GetInspectorArea(window);
			return current.Overlaps(windowRect);
		}
	}
}