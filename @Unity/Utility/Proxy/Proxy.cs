#pragma warning disable 0618
using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
namespace Zios.Unity.Proxy{
	using Source = UnityEngine.Application;
	public static class Application{
		public static bool isLoadingLevel{get{return Source.isLoadingLevel;}}
		public static bool isPlaying{get{return Source.isPlaying;}}
		public static bool isEditor{get{return Source.isEditor;}}
		public static bool isFocused{get{return Source.isFocused;}}
		public static int levelCount{
			get{
				#if UNITY_5_3_OR_NEWER
				return SceneManager.sceneCountInBuildSettings;
				#else
				return Source.levelCount;
				#endif
			}
		}
		public static int loadedLevel{
			get{
				#if UNITY_5_3_OR_NEWER
				return SceneManager.GetSceneByBuildIndex(loadedLevel).buildIndex;
				#else
				return Source.loadedLevel;
				#endif
			}
		}
		public static int targetFrameRate{get{return Source.targetFrameRate;}set{Source.targetFrameRate=value;}}
		public static string loadedLevelName{
			get{
				#if UNITY_5_3_OR_NEWER
				return SceneManager.GetActiveScene().name;
				#else
				return Source.loadedLevelName;
				#endif
			}
		}
		public static string dataPath{get{return Source.dataPath;}}
		public static string persistentDataPath{get{return Source.persistentDataPath;}}
		public static string streamingAssetsPath{get{return Source.streamingAssetsPath;}}
		public static string temporaryCachePath{get{return Source.temporaryCachePath;}}
		public static bool runInBackground{get{return Source.runInBackground;}}
		public static RuntimePlatform platform{get{return Source.platform;}}
		public static string companyName{get{return Source.companyName;}}
		public static string productName{get{return Source.productName;}}
		public static string absoluteURL{get{return Source.absoluteURL;}}
		public static Type LogCallback{get{return typeof(Source.LogCallback);}}
		public static void Quit(){Source.Quit();}
		public static void LoadLevel(int id){
			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadScene(id);
			#else
			Source.LoadLevel(id);
			#endif
		}
		public static void LoadLevelAdditive(int id){
			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadScene(id,LoadSceneMode.Additive);
			#else
			Source.LoadLevelAdditive(id);
			#endif
		}
		public static void LoadLevelAdditiveAsync(int id){
			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadSceneAsync(id,LoadSceneMode.Additive);
			#else
			Source.LoadLevelAdditiveAsync(id);
			#endif
		}
		public static void LoadLevelAsync(int id){
			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadSceneAsync(id);
			#else
			Source.LoadLevelAsync(id);
			#endif
		}
	}
	public static partial class Proxy{
		public static bool allow = true;
		public static List<Func<bool>> busyMethods = new List<Func<bool>>(){()=>Proxy.IsLoading()};
		public static bool Allow(){return Proxy.allow;}
		public static void Allow(bool state){Proxy.allow = state;}
		public static void AddLogCallback(Source.LogCallback method){Source.logMessageReceived += method;}
		public static void RemoveLogCallback(Source.LogCallback method){Source.logMessageReceived -= method;}
		public static bool IsRepainting(){return Event.current.type == EventType.Repaint;}
		public static bool IsLoading(){return Proxy.Allow() && Source.isLoadingLevel;}
		public static bool IsPlaying(){return Proxy.Allow() && Source.isPlaying;}
		public static bool IsEditor(){return Proxy.Allow() && Source.isEditor;}
		public static bool IsBusy(){
			if(!Proxy.Allow()){return true;}
			foreach(var method in Proxy.busyMethods){
				if(method()){return true;}
			}
			return false;
		}
		public static void LoadScene(string name){
			if(!Proxy.Allow()){return;}
			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadScene(name);
			#else
			Application.LoadLevel(name);
			#endif
		}
		public static float GetTime(){
			if(Proxy.Allow()){return UnityEngine.Time.realtimeSinceStartup;}
			return 0;
		}
		public static float GetDeltaTime(){
			if(Proxy.Allow()){return UnityEngine.Time.deltaTime;}
			return 0;
		}
		public static float GetFixedTime(){
			if(Proxy.Allow()){return UnityEngine.Time.fixedTime;}
			return 0;
		}
		public static float GetFixedDeltaTime(){
			if(Proxy.Allow()){return UnityEngine.Time.fixedDeltaTime;}
			return 0;
		}
	}
}
namespace Zios.Unity.Proxy{
	using Zios.Extensions;
	public static class MonoBehaviourExtensions{
		public static bool CanValidate(this MonoBehaviour current){
			var enabled = !current.IsNull() && current.gameObject.activeInHierarchy && current.enabled;
			return !Proxy.IsPlaying() && !Proxy.IsBusy() && enabled;
		}
	}
}