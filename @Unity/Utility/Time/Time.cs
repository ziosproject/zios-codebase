using System.Diagnostics;
namespace Zios.Unity.Time{
	using UnityTime = Zios.Unity.Proxy.Proxy;
	public static class Time{
		public static Stopwatch clock = new Stopwatch();
		public static float Get(){return UnityTime.GetTime();}
		public static float GetDelta(){return UnityTime.GetDeltaTime();}
		public static float GetFixed(){return UnityTime.GetFixedTime();}
		public static float GetFixedDelta(){return UnityTime.GetFixedDeltaTime();}
		public static void Start(){
			Time.clock = Stopwatch.StartNew();
		}
		public static float Check(){return (float)Time.clock.Elapsed.TotalMilliseconds/1000f;}
		public static string Passed(){return Time.Check() + " seconds";}
	}
	public static class FloatExtensions{
		public static bool Elapsed(this float current,bool unity=true){return Time.Get()>=current;}
		public static string Passed(this float current,bool unity=true){return Time.Get()-current+" seconds";}
		public static float AddTime(this float current,bool unity=true){return current+Time.Get();}
	}
	public static class IntExtensions{
		public static bool Elapsed(this int current,bool unity=true){return Time.Get()>=current;}
		public static string Passed(this int current,bool unity=true){return Time.Get()-current+" seconds";}
		public static float AddTime(this int current,bool unity=true){return current+Time.Get();}
	}
}