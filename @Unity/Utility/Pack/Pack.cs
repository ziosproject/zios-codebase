using System;
using System.Collections.Generic;
using UnityEngine;
namespace Zios.Unity.Pack{
	public static class Pack{
		public static int PackBools(params bool[] values){
			var packed = 0;
			var slot = 0;
			for(var index=values.Length-1;index>=0;--index){
				packed |= (values[slot] ? 1 : 0) << index;
				++slot;
			}
			return packed;
		}
		public static bool[] UnpackBools(int amount,int value){
			var unpacked = new List<bool>();
			for(var index=amount-1;index>=0;--index){
				var mask = 1<<index;
				var isActive = (value & mask) == mask;
				unpacked.Add(isActive);
			}
			return unpacked.ToArray();
		}
		public static float PackFloats(params float[] values){
			var packed = 0;
			var amount = values.Length;
			var bitPrecision = 24 / amount;
			var intPrecision = (1<<bitPrecision) - 1;
			var slot = 0;
			for(var index=amount;index>0;--index){
				var shift = bitPrecision * (index - 1);
				packed |= ((int)Math.Floor(values[slot] * (float)intPrecision))<<shift;
				++slot;
			}
			return (float)packed * 0.0000001f;
		}
		public static float[] UnpackFloats(int amount,float value){
			value = value * 10000000;
			var bitPrecision = 24 / amount;
			var intPrecision = 1<<bitPrecision;
			var floatPrecision = (float)intPrecision;
			var unpacked = new List<float>();
			for(var index=amount;index>0;--index){
				var slot = (float)((value / Math.Pow(intPrecision,index - 1)) % floatPrecision) / floatPrecision;
				unpacked.Add(slot);
			}
			return unpacked.ToArray();
		}
		public static float PackFloat4(float a,float b,float c,float d){
			var x = ((int)Math.Floor(a * 63))<<18;
			var y = ((int)Math.Floor(b * 63))<<12;
			var z = ((int)Math.Floor(c * 63))<<6;
			var w = ((int)Math.Floor(d * 63));
			return (x | y | z | w) * 0.0000001f;
		}
	}
}
namespace Zios.Unity.Pack{
	using Zios.Extensions.Convert;
	using Class = Zios.Unity.Pack.Pack;
	public static class PackExtensions{
		public static byte[] Pack(this Vector3 current){
			return Class.PackFloats(current.x,current.y,current.z).ToBytes();
		}
		public static byte[] ToBytes(this Vector4 current,bool pack){
			return Class.PackFloats(current.x,current.y,current.z,current.w).ToBytes();
		}
	}
}