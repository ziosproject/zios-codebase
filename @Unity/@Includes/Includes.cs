using System;
using System.IO;
using UnityEngine;
using UnityEditor;
namespace Zios.Unity.Includes{
	[InitializeOnLoad]
	public static class Includes{
		public static string[] items = new string[]{
			"System.Web"
		};
		static Includes(){
			var path = Application.dataPath;
			var rsp = "";
			foreach(var item in Includes.items){
				rsp += "-r:"+item+".dll\n";
			}
			File.WriteAllText(Application.dataPath+"/csc.rsp",rsp);
			File.WriteAllText(Application.dataPath+"/mcs.rsp",rsp);
		}
	}
}