using UnityEngine;
namespace Zios.Unity.SystemAttributes{
	using Zios.SystemAttributes;
	public class AutoInitializeUnity{
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		public void Begin(){
			if(!Application.isEditor){
				AutoInitialize.Start();
			}
		}
	}
}