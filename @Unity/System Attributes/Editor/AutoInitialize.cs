using UnityEditor;
namespace Zios.Unity.Editor.SystemAttributes{
	using Zios.SystemAttributes;
	[InitializeOnLoad]
	public static class AutoInitializeEditor{
		static AutoInitializeEditor(){
			AutoInitialize.Start();
		}
	}
}