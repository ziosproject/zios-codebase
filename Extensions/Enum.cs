using System;
using System.Linq;
using Zios.Supports.Hierarchy;
namespace Zios.Extensions{
	public static class EnumExtension{
		public static Hierarchy<Type,string,bool> isDefinedCache = new Hierarchy<Type,string,bool>();
		public static Enum Get(this Enum current,string value,int fallback=-1){
			var type = current.GetType();
			var items = Enum.GetNames(type);
			var found = false;
			for(var index=0;index<items.Length;++index){
				var name = items[index];
				if(name.Matches(value,true)){
					value = name;
					found = true;
					break;
				}
			}
			if(!found && fallback != -1){
				value = fallback.ToString();
			}
			return (Enum)Enum.Parse(type,value);
		}
		public static string[] GetNames(this Enum current){
			return Enum.GetNames(current.GetType());
		}
		public static Array GetValues(this Enum current){
			return Enum.GetValues(current.GetType());
		}
		public static Type[] GetValues<Type>(this Enum current){
			return (Type[])Enum.GetValues(typeof(Type));
		}
		public static int GetMaskFull(this Enum current){
			return current.GetValues().Cast<int>().Sum();
		}
		public static Type ParseEnum<Type>(this Type current,string value){
			return (Type)Enum.Parse(current.GetType(),value);
		}
		public static bool IsDefined<Type>(this Type current,string value){
			var cache = EnumExtension.isDefinedCache;
			var type = current.AsType();
			var existingType = cache.TryGet(type);
			var existing = existingType.IsNull() ? false : existingType.TryGet(value);
			if(!existing){
				cache.AddNew(type)[value] = existing = Enum.IsDefined(current.GetType(),value);
			}
			return existing;
		}
	}
}