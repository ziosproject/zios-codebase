using System;
using System.Collections.Generic;
namespace Zios.Extensions.Convert{
	using Zios.Extensions;
	public static class ConvertObject{
		//============================
		// Conversion
		//============================
		public static T As<T>(this object current){
			if(current == null){return default(T);}
			return (T)current;
		}
		public static Type Convert<Type>(this object current){return System.Convert.ChangeType(current,typeof(Type)).As<Type>();}
		public static float ToFloat(this object current){return System.Convert.ChangeType(current,typeof(float)).As<float>();}
		public static int ToInt(this object current){return System.Convert.ChangeType(current,typeof(int)).As<int>();}
		public static double ToDouble(this object current){return System.Convert.ChangeType(current,typeof(double)).As<double>();}
		public static DateTime ToDateTime(this object current){return System.Convert.ChangeType(current,typeof(DateTime)).As<DateTime>();}
		public static string ToText(this object current){return System.Convert.ChangeType(current,typeof(string)).As<string>();}
		public static bool ToBool(this object current){return System.Convert.ChangeType(current,typeof(bool)).As<bool>();}
		//============================
		// Wraps
		//============================
		public static object Box<T>(this T current){
			return current.AsBox();
		}
		public static object AsBox<T>(this T current){
			return (object)current;
		}
		public static T[] AsArray<T>(this T current){
			if(current.IsNull()){return new T[0];}
			return new T[]{current};
		}
		public static T[] AsArray<T>(this T current,int amount){
			return current.AsList(amount).ToArray();
		}
		public static List<T> AsList<T>(this T current){
			if(current.IsNull()){return new List<T>();}
			return new List<T>{current};
		}
		public static List<T> AsList<T>(this T current,int amount){
			if(current.IsNull()){return new List<T>();}
			var collection = new List<T>();
			while(amount > 0){
				collection.Add(current);
				amount -= 1;
			}
			return collection;
		}
	}
}