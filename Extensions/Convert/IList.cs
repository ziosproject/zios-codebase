using System;
using System.Collections.Generic;
using System.Linq;
namespace Zios.Extensions.Convert{
	public static class ConvertIList{
		public static Dictionary<string,Type> ToDictionary<Type>(this IList<Type> current){
			var values = new Dictionary<string,Type>();
			for(var index=0;index<current.Count;++index){
				values["#"+index] = current[index];
			}
			return values;
		}
		public static void ForEach<T>(this IList<T> current,Action<T> method){
			for(var index=0;index<current.Count;++index){
				method(current[index]);
			}
		}
		public static float[] Scale(this IList<float> current,float scalar){
			var result = current.ToArray();
			for(var index=0;index<current.Count;++index){
				result[index] = current[index] * scalar;
			}
			return result;
		}
	}
}