using System;
using System.Collections.Generic;
using System.Linq;
namespace Zios.Extensions.Convert{
	public static class ConvertIEnumerable{
		public static To[] ConvertAll<To>(this IEnumerable<string> current){
			return current.ConvertAll<string,To>();
		}
		public static To[] ConvertAll<From,To>(this IEnumerable<From> current){
			var source = current.ToArray<From>();
			return Array.ConvertAll(source,x=>x.Convert<To>()).ToArray();
		}
		public static Dictionary<TKey,TValue> ToDictionary<TKey,TValue>(this IEnumerable<KeyValuePair<TKey,TValue>> current){
			return current.ToDictionary(x=>x.Key,x=>x.Value);
		}
		public static int ToBitFlags<Type>(this IEnumerable<Type> current,Func<Type,bool> compare){
			var value = 0;
			var increment = 1;
			foreach(var item in current){
				if(compare(item)){value += increment;}
				increment *= 2;
			}
			return value;
		}
		public static int ToBitFlags(this IEnumerable<bool> current){
			return current.ToBitFlags(x=>x);
		}
	}
}