using System;
using System.Text;
namespace Zios.Extensions.Convert{
	using Zios.Extensions;
	using Zios.Supports.Hierarchy;
	public static class ConvertEnum{
		public static Hierarchy<Type,string,int> toIntCache = new Hierarchy<Type,string,int>();
		public static Hierarchy<Type,string,object> toValueCache = new Hierarchy<Type,string,object>();
		public static Hierarchy<Enum,string,string> toNameCache = new Hierarchy<Enum,string,string>();
		public static Hierarchy<Enum,int,bool> containsIntCache = new Hierarchy<Enum,int,bool>();
		public static Hierarchy<Enum,string,bool> containsStringCache = new Hierarchy<Enum,string,bool>();
		public static Hierarchy<Enum,Enum,bool> containsEnumCache = new Hierarchy<Enum,Enum,bool>();
		public static string ToName(this Enum current,string separator=" "){
			var cache = ConvertEnum.toNameCache;
			if(cache.ContainsKey(current) && cache[current].ContainsKey(separator)){
				return cache[current][separator];
			}
			var result = current.ToString().Replace(", ",separator);
			if(result.IsNumber()){
				var allNames = Enum.GetNames(current.GetType());
				var names = new StringBuilder();
				for(var index=0;index<allNames.Length;++index){
					var currentName = allNames[index];
					var value = currentName.ToEnum(current.GetType());
					if(current.Contains(value)){
						names.Append(currentName + separator);
					}
				}
				result = names.ToString().TrimRight(separator);
			}
			cache.AddNew(current)[separator] = result;
			return result;
		}
		public static string ToText(this Enum current,string separator=" ",bool ignoreDefault=false,int defaultValue=0){
			return ignoreDefault && (current.ToInt() == defaultValue) ? null : current.ToName(separator);
		}
		public static int ToInt(this Enum current){
			return System.Convert.ToInt32(current);
		}
		public static bool Contains(this Enum current,int mask){
			bool existing;
			var cache = ConvertEnum.containsIntCache;
			var existingType = cache.AddNew(current);
			if(!existingType.TryGetValue(mask,out existing)){
				cache.AddNew(current)[mask] = existing = (current.ToInt() & mask) != 0;
			}
			return existing;
		}
		public static bool Contains(this Enum current,Enum mask){
			bool existing;
			var cache = ConvertEnum.containsEnumCache;
			var existingType = cache.AddNew(current);
			if(!existingType.TryGetValue(mask,out existing)){
				cache.AddNew(current)[mask] = existing = (current.ToInt() & mask.ToInt()) != 0;
			}
			return existing;
		}
		public static bool Contains(this Enum current,string value){
			bool existing;
			var cache = ConvertEnum.containsStringCache;
			var existingType = cache.AddNew(current);
			if(!existingType.TryGetValue(value,out existing)){
				existing = current.IsDefined(value) ? (current.ToInt() & current.GetInt(value)) != 0 : false;
				cache.AddNew(current)[value] = existing;
			}
			return existing;
		}
		public static Type GetValue<Type>(this Type current,string name){
			object existing;
			var cache = ConvertEnum.toValueCache;
			var type = current.GetType();
			var existingType = cache.AddNew(type);
			if(!existingType.TryGetValue(name,out existing)){
				cache.AddNew(type)[name] = existing = current.ParseEnum(name);
			}
			return (Type)existing;
		}
		public static int GetInt<Type>(this Type current,string name){
			int existing;
			var cache = ConvertEnum.toIntCache;
			var type = current.GetType();
			var existingType = cache.AddNew(type);
			if(!existingType.TryGetValue(name,out existing)){
				cache.AddNew(type)[name] = existing = current.GetValue(name).ToInt();
			}
			return existing;
		}
		public static int GetIndex(this Enum current){
			return current.GetNames().IndexOf(current.ToName());
		}
		public static bool HasAny(this Enum current,params string[] values){
			foreach(var value in values){
				if(current.Contains(value)){return true;}
			}
			return false;
		}
		public static bool HasAll(this Enum current,params string[] values){
			foreach(var value in values){
				if(!current.Contains(value)){return false;}
			}
			return true;
		}
		public static bool Is(this Enum current,params string[] values){
			var exact = 0;
			foreach(var value in values){
				exact |= current.GetInt(value);
			}
			return current.Is(exact);
		}
		public static bool Is(this Enum current,int value){return (current.ToInt() & value) == value;}
		public static bool Is(this Enum current,Enum value){return current.Is(value.ToInt());}
		public static bool Matches(this Enum current,params string[] values){
			var currentInt = current.ToInt();
			foreach(var value in values){
				if(currentInt != current.GetInt(value)){
					return false;
				}
			}
			return true;
		}
		public static bool MatchesAny(this Enum current,params string[] values){
			var currentInt = current.ToInt();
			foreach(var value in values){
				var valueInt = current.GetInt(value);
				if(currentInt == valueInt){return true;}
			}
			return false;
		}
		//===============
		// Shortcuts
		//===============
		public static bool ContainsAny(this Enum current,params string[] values){return current.HasAny(values);}
		public static bool Contains(this Enum current,params string[] values){return current.HasAny(values);}
	}
}