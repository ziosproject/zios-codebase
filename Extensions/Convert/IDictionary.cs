using System.Collections.Generic;
namespace Zios.Extensions.Convert{
	public static class ConvertIDictionary{
		public static string valueSeparator = ":";
		public static SortedList<TKey,TValue> ToSortedList<TKey,TValue>(this IDictionary<TKey,TValue> current){
			return new SortedList<TKey,TValue>(current);
		}
	}
}