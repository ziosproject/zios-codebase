using System;
using System.Collections.Generic;
namespace Zios.Extensions{
	public static class ArrayExtension{
		public static int IndexOf<T>(this Array current,T value){
			return Array.IndexOf(current,value);
		}
		public static int IndexOf<T>(this Array current,Enum enumerable){
			var name = enumerable.ToString();
			return current.IndexOf(name);
		}
		public static T[] Copy<T>(this T[] current){
			var result = new T[current.Length];
			current.CopyTo(result,0);
			return result;
		}
		public static T[] Concat<T>(this T[] current,T[] list){
			var result = new T[current.Length + list.Length];
			current.CopyTo(result,0);
			list.CopyTo(result,current.Length);
			return result;
		}
		public static bool Exists<T>(this T[] current,Predicate<T> predicate){
			return Array.Exists(current,predicate);
		}
		public static T Find<T>(this T[] current,Predicate<T> predicate){
			return Array.Find(current,predicate);
		}
		public static T[] Clear<T>(this T[] current){
			return new T[0]{};
		}
		public static T[] Append<T>(this T[] current,T element){
			return current.Add(element);
		}
		public static T[] Prepend<T>(this T[] current,T element){
			var copy = new List<T>(current);
			copy.Insert(0,element);
			return copy.ToArray();
		}
		public static T[] Add<T>(this T[] current,T element){
			var extra = new T[]{element};
			return current.Concat(extra);
		}
		public static T[] Remove<T>(this T[] current,T value){
			var copy = new List<T>(current);
			copy.Remove(value);
			return copy.ToArray();
		}
		public static T[] RemoveAt<T>(this T[] current,int index){
			var copy = new List<T>(current);
			copy.RemoveAt(index);
			return copy.ToArray();
		}
		public static T[] RemoveAll<T>(this T[] current,T value){
			var copy = new List<T>(current);
			copy.RemoveAll(x=>x.Equals(value));
			return copy.ToArray();
		}
		public static T[] Resize<T>(this T[] current,int newSize){
			Array.Resize(ref current,newSize);
			return current;
		}
	}
}