using System;
namespace Zios.Extensions{
	public static class DelegateExtension{
		public static bool ContainsMethod(this Delegate current,Delegate value){
			if(current.IsNull()){return false;}
			foreach(var item in current.GetInvocationList()){
				if(item == value){return true;}
			}
			return false;
		}
		public static bool Contains(this Delegate current,Delegate value){
			return current.ContainsMethod(value);
		}
	}
}