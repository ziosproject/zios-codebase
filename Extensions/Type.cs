using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Zios.Extensions{
	public static class TypeExtensions{
		public static List<Func<object,bool>> IsKeyTypeMethods = new List<Func<object,bool>>();
		public static Dictionary<Type,object> defaults = new Dictionary<Type,object>();
		public static bool IsType(this Type current,object value){return current.IsType(value.GetType());}
		public static bool IsType(this Type current,Type value){
			if(value.IsInterface){return current.GetInterface(value.Name) != null;}
			return current.IsSubclassOf(value);
		}
		public static bool HasEmptyConstructor(this Type current){
			return typeof(Type).GetConstructor(Type.EmptyTypes) != null;
		}
		public static bool IsKeyType(this Type current){
			foreach(var method in TypeExtensions.IsKeyTypeMethods){
				if(method(current)){
					return true;
				}
			}
			return current.IsCollection();
		}
		public static bool IsGeneric(this Type current){
			return current.ContainsGenericParameters || current.IsGenericType;
		}
		public static bool IsEnumerable(this Type current){
			return current.IsType(typeof(IEnumerable));
		}
		public static bool IsCollection(this Type current){
			return current.IsArray || current.IsList() || current.IsDictionary() || current == typeof(Hashtable);
		}
		public static bool IsDelegate(this Type current){
			return current.IsType(typeof(Delegate));
		}
		public static bool IsString(this Type current){
			return current == typeof(string);
		}
		public static bool IsClass(this Type current){
			return current.IsClass && !current.IsCollection() && !current.IsDelegate() && !current.IsString();
		}
		public static bool IsList(this Type current){
			return current.IsType(typeof(IList)) && current.IsGenericType;
		}
		public static bool IsDictionary(this Type current){
			return current.IsType(typeof(IDictionary)) && current.IsGenericType;
		}
		public static bool IsStatic(this Type current){
			return current.IsAbstract && current.IsSealed;
		}
		public static bool IsSubclass(this Type current,Type value){
			while(value != null && value != typeof(object)){
				var core = value.IsGenericType ? value.GetGenericTypeDefinition() : value;
				if(current == core){return true;}
				value = value.BaseType;
			}
			return false;
		}
		public static string GetName(this Type current,bool full=true){
			var name = full ? current.FullName : current.Name;
			var generics = current.GetGenericArguments();
			if(generics.Length > 0){
				name = name.Split("[")[0];
				name += "["+generics.Select(x=>x.GetName(full)).Join(",")+"]";
			}
			return name;
		}
		public static object GetDefault(this Type current){
			if(!TypeExtensions.defaults.ContainsKey(current)){
				TypeExtensions.defaults[current] = current.IsValueType ? Activator.CreateInstance(current) : null;
			}
			return TypeExtensions.defaults[current];
		}
	}
}