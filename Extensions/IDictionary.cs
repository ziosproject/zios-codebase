using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Zios.Extensions{
	public static class IDictionaryExtension{
		//========================
		// Dictionary
		//========================
		public static Dictionary<TKey,TValue> Copy<TKey,TValue>(this Dictionary<TKey,TValue> current){
			return new Dictionary<TKey,TValue>(current);
		}
		public static Dictionary<TKey,TValue> Remove<TKey,TValue>(this Dictionary<TKey,TValue> current,Func<KeyValuePair<TKey,TValue>,bool> method){
			foreach(var item in current.Copy()){
				if(method(item)){current.Remove(item.Key);}
			}
			return current;
		}
		public static void RemoveValue<TKey,TValue>(this Dictionary<TKey,TValue> current,TValue value){
			foreach(var item in current.Copy()){
				if(item.Value.Equals(value)){
					current.Remove(item.Key);
				}
			}
		}
		public static Key GetKey<Key,Value>(this Dictionary<Key,Value> current,Value value){
			return current.FirstOrDefault(x=>x.Value.Equals(value)).Key;
		}
		public static Dictionary<Key,Value> Merge<Key,Value>(this Dictionary<Key,Value> current,Dictionary<Key,Value> other){
			foreach(var item in other){
				current[item.Key] = item.Value;
			}
			return current;
		}
		public static Dictionary<Key,Value> Difference<Key,Value>(this Dictionary<Key,Value> current,Dictionary<Key,Value> other){
			var output = new Dictionary<Key,Value>();
			foreach(var item in other){
				var key = item.Key;
				Value value;
				if(current.TryGetValue(key,out value)){
					var nullMatch = value.IsNull() && other[key].IsNull();
					var referenceMatch = !nullMatch && !other[key].GetType().IsValueType;
					var valueMatch = !nullMatch && other[key].Equals(current[key]);
					var match = nullMatch || referenceMatch || valueMatch;
					/*if(current[key] is IEnumerable){
						match = current[key].As<IEnumerable>().SequenceEqual(other[key]);
					}*/
					if(match){continue;}
				}
				output[item.Key] = item.Value;
			}
			return output;
		}
		//========================
		// IDictionary
		//========================}
		public static TValue Get<TKey,TValue>(this IDictionary<TKey,TValue> current,TKey key,TValue value=default(TValue)) where TValue : new(){
			TValue output;
			if(!current.TryGetValue(key,out output)){
				return value;
			}
			return output;
		}
		public static void SetValues<TKey,TValue>(this IDictionary<TKey,TValue> current,IList<TValue> values) where TValue : new(){
			var index = 0;
			foreach(var key in current.Keys.ToList()){
				current[key] = values[index];
				++index;
			}
		}
		public static TValue AddDefault<TKey,TValue>(this IDictionary<TKey,TValue> current,TKey key){
			TValue output;
			if(!current.TryGetValue(key,out output)){
				current[key] = output = default(TValue);
			}
			return output;
		}
		public static TValue AddNew<TKey,TValue>(this IDictionary<TKey,TValue> current,TKey key) where TValue : new(){
			TValue output;
			if(!current.TryGetValue(key,out output)){
				current[key] = output = new TValue();
			}
			return output;
		}
		public static TValue AddNewSequence<TKey,TValue>(this IDictionary<IList<TKey>,TValue> current,IList<TKey> key) where TValue : new(){
			if(!current.Keys.ToArray().Exists(x=>x.SequenceEqual(key))){
				current[key] = new TValue();
			}
			return current[key];
		}
		public static bool ContainsKey(this IDictionary current,string value,bool ignoreCase){
			value = value.ToLower();
			foreach(string key in current.Keys){
				if(key.ToLower() == value){
					return true;
				}
			}
			return false;
		}
		public static Value TryGet<Key,Value>(this IDictionary<Key,Value> current,Key key){
			Value output;
			current.TryGetValue(key,out output);
			return output;
		}
		public static Dictionary<TargetKey,TargetValue> Remap<Key,Value,TargetKey,TargetValue>(this Dictionary<Key,Value> current,Func<KeyValuePair<Key,Value>,TargetKey> keyMethod,Func<KeyValuePair<Key,Value>,TargetValue> valueMethod){
			var result = new Dictionary<TargetKey,TargetValue>();
			foreach(var item in current){
				var key = keyMethod(item);
				var value = valueMethod(item);
				result[key] = value;
			}
			return result;
		}
	}
}