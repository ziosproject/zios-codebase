using System;
using System.Collections;
using System.Collections.Generic;
namespace Zios.Extensions{
	public static class ObjectExtensions{
		public static List<Func<object,object,object,object>> SetByKeyMethods = new List<Func<object,object,object,object>>();
		public static List<Func<object,object,object>> GetByKeyMethods = new List<Func<object,object,object>>();
		//============================
		// Checks
		//============================
		public static bool Exists<Type>(this Type current){
			return !current.IsNull();
		}
		public static bool IsDefault<Type>(this Type current){
			return current.Equals(default(Type));
		}
		public static bool IsDefaultValue(this object current){
			return current.Equals(current.GetDefault());
		}
		public static bool IsEmpty(this object current){
			var isEmptyString = (current is string && ((string)current).IsEmpty());
			var isEmptyCollection = (current is IList && ((IList)current).Count == 0);
			return current.IsNull() || isEmptyCollection || isEmptyString;
		}
		public static bool IsNumber(this object current){
			var isByte = current is sbyte || current is byte;
			var isInteger = current is short || current is ushort || current is int || current is uint || current is long || current is ulong;
			var isDecimal = current is float || current is double || current is decimal;
			return isInteger || isDecimal || isByte;
		}
		public static bool IsNull<Type>(this Type current){
			return current == null || current.Equals(null);
		}
		public static bool IsStatic(this object current){return current.AsType().IsStatic();}
		public static bool IsEnum(this object current){return current.AsType().IsEnum;}
		public static bool IsArray(this object current){return current.AsType().IsArray;}
		public static bool IsKeyType(this object current){return current.AsType().IsKeyType();}
		public static bool IsGeneric(this object current){return current.AsType().IsGeneric();}
		public static bool IsList(this object current){return current.AsType().IsList();}
		public static bool IsDictionary(this object current){return current.AsType().IsDictionary();}
		public static bool IsAny<A,B>(this object current){return current.Is<A>() || current.Is<B>();}
		public static bool IsAny<A,B,C>(this object current){return current.Is<A>() || current.Is<B>() || current.Is<C>();}
		public static bool IsAny<A,B,C,D>(this object current){return current.Is<A>() || current.Is<B>() || current.Is<C>() || current.Is<D>();}
		public static bool IsAny<A,B,C,D,E>(this object current){return current.Is<A>() || current.Is<D>() || current.Is<C>() || current.Is<D>() || current.Is<E>();}
		public static bool Is<T>(this object current){
			if(current.IsNull()){return false;}
			var type = current.AsType();
			return type == typeof(T) || type.IsSubclassOf(typeof(T));
		}
		public static bool Is(this Type current,Type value){
			return current == value || current.IsSubclassOf(value);
		}
		public static bool Is<T>(this T current,Type value){
			return typeof(T) == value || typeof(T).IsSubclassOf(value);
		}
		public static bool Is<T>(this T current,string name){
			var type = typeof(T);
			var value = Type.GetType(name);
			if(value.IsNull()){
				System.Console.WriteLine("[ObjectExtension] Type -- " + name + " not found.");
				return false;
			}
			return type == value || type.IsSubclassOf(value);
		}
		public static bool IsNot<T>(this T current,Type value){return !current.Is(value);}
		public static bool IsNot<T>(this T current,string name){return !current.Is(name);}
		public static bool IsNot<T>(this object current){return !current.Is<T>();}
		//============================
		// Other
		//============================
		public static Type AsType(this object current){return current is Type ? (Type)current : current.GetType();}
		public static Type Default<Type>(this Type current){return default(Type);}
		public static object GetDefault(this object current){
			if(current == null){return null;}
			return current.GetType().GetDefault();
		}
		public static object GetByKey<Key>(this object current,Key key){
			if(!key.IsNull()){
				foreach(var method in ObjectExtensions.GetByKeyMethods){
					current = method(current,key);
					if(current != null){return current;}
				}
				if(current is IDictionary){
					var data = (IDictionary)current;
					return data[key];
				}
				else if(current is IList){
					var data = (IList)current;
					var index = System.Convert.ToInt32(key);
					return data[index];
				}
			}
			return null;
		}
		public static object SetByKey<Key,Value>(this object current,Key key,Value value){
			if(!key.IsNull()){
				foreach(var method in ObjectExtensions.SetByKeyMethods){
					current = method(current,key,value);
					if(current != null){return current;}
				}
				if(current is IDictionary){
					var data = (IDictionary)current;
					data[key] = value;
					return data;
				}
				else if(current is IList){
					var data = (IList)current;
					var index = System.Convert.ToInt32(key);
					data[index] = value;
					return data;
				}
			}
			return current;
		}
	}
}