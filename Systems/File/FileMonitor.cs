﻿using System;
using System.Collections.Generic;
using System.IO;
namespace Zios.File{
	using Zios.Extensions;
	using Zios.Log;
	using Zios.Supports.Hook;
	using Zios.Time;
	public class FileMonitor{
		//================
		// Static
		//================
		public static Dictionary<string,FileMonitor> all = new Dictionary<string,FileMonitor>();
		public static bool disabled;
		public static float updateRate = 0.3f;
		public static void Update(){FileMonitor.Update(false);}
		public static void Update(bool force){
			if(FileMonitor.disabled){return;}
			var elapsed = Time.Elapsed("File/Monitor");
			if(force || elapsed > FileMonitor.updateRate){
				var changes = false;
				Time.Start();
				foreach(var item in FileMonitor.all.Copy()){
					var path = item.Key;
					var monitor = item.Value;
					if(monitor.WasChanged()){
						changes = true;
						var deleted = !File.PathExists(path);
						var target = monitor.target;
						if(deleted){
							monitor.target = null;
							FileMonitor.all.Remove(path);
							target.DeleteCache(true);
							continue;
						}
						File.cache.Clear();
						if(target.isFolder){
							foreach(var file in target.contents){
								file.DeleteCache();
							}
							target.contents.Clear();
							File.Scan(path);
						}
						Hook.Call("File/Changed",path);
						if(FileSettings.showUpdates){Log.Show("[File] Updating changed path -- " + path);}
					}
				}
				if(changes){
					Hook.Call("File/Updated");
					if(FileSettings.showUpdates){Log.Show("[File] Updated changes -- " + Time.Passed());}
				}
				Time.Start("File/Monitor");
			}
		}
		public static FileMonitor Add(FileData file){
			var exists = FileMonitor.all.TryGet(file.path);
			if(exists.IsNull()){
				FileMonitor.all[file.path] = exists = new FileMonitor(file.path,file);
			}
			return exists;
		}
		//================
		// Instance
		//================
		public string path;
		public FileData target;
		public DateTime lastModify;
		public FileMonitor(string path,FileData target){
			this.target = target;
			this.path = path;
			this.lastModify = Directory.GetLastWriteTime(this.path);
		}
		public bool WasChanged(){
			var modifyTime = Directory.GetLastWriteTime(this.path);
			if(this.lastModify != modifyTime){
				this.lastModify = modifyTime;
				return true;
			}
			return false;
		}
	}
}
