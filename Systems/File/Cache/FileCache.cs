using SystemFile = System.IO.File;
namespace Zios.File.Cache{
	using System;
	using System.Text;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Log;
	using Zios.Serializer.Attributes;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using Zios.Time;
	[Store(0)]
	public static class FileCacheSettings{
		public static string path = "./File.cache";
		public static bool showUpdates;
		public static bool showTimes;
	}
	[AutoInitialize(-88)]
	public static class FileCache{
		public static bool needsScan;
		static FileCache(){
			Hook.Get("File/Updated").Add(FileCache.Update);
			Hook.Get("DomainUnload").Add(FileCache.Save);
			if(SystemFile.Exists(FileCacheSettings.path)){
				FileCache.Load();
				if(FileCacheSettings.showUpdates){
					Log.Show(FileMonitor.all.Count + " monitors loaded");
					Log.Show(FileData.all.Count + " total loaded");
				}
				FileMonitor.Update(true);
				return;
			}
			FileCache.Rebuild();
		}
		public static void Rebuild(){
			if(!SystemFile.Exists(FileCacheSettings.path)){
				Time.Start();
				File.cache.Clear();
				FileData.all.Clear();
				FileData.byType.Clear();
				File.Scan(FileSettings.rootPath);
				File.Scan(FileSettings.dataPath,true);
				if(FileCacheSettings.showTimes){Log.Show("[FileCache] Scan complete -- " + Time.Passed() + ".");}
				FileCache.Save();
			}
		}
		//===============
		// Storage
		//===============
		public static void Load(){
			Time.Start();
			var path = "";
			var lines = SystemFile.ReadAllLines(FileCacheSettings.path);
			FileData data = null;
			FileData folderData = null;
			for(var index=0;index<lines.Length;++index){
				var line = lines[index];
				if(line.Contains("|")){
					var chunk = line.Split("|");
					path = chunk[0].Replace("$",FileSettings.rootPath);
					var isFolder = !path.EndsWith("!");
					var modify = chunk[1];
					path = path.TrimRight("!");
					data = FileData.Get(path,isFolder);
					if(chunk[0]!="$"){
						var parent = FileData.Get(path.GetDirectory(),true);
						parent.contents.Add(data);
					}
					var monitor = FileMonitor.all.TryGet(path) ?? new FileMonitor(path,data);
					monitor.lastModify = new DateTime(modify.ToLong());
					folderData = data;
				}
				else{
					data = FileData.Get(path+line);
					folderData.contents.Add(data);
				}
			}
			if(FileCacheSettings.showTimes){Log.Show("[FileCache] Load cache complete -- " + Time.Passed() + ".");}
		}
		public static void Save(){
			Time.Start();
			var cachePath = FileCacheSettings.path;
			var output = new StringBuilder();
			FileMonitor.Update(true);
			foreach(var data in FileMonitor.all){
				var target = data.Value.target;
				var monitor = data.Value;
				var path = target.path.Replace(FileSettings.rootPath,"$");
				var suffix = target.isFolder ? "" : "!";
				output.AppendLine(path+suffix+"|"+monitor.lastModify.Ticks);
				foreach(var file in target.contents){
					if(!file.isFolder){
						output.AppendLine(file.fullName);
					}
				}
			}
			SystemFile.WriteAllText(FileCacheSettings.path,output.ToString());
			FileMonitor.all[cachePath.GetDirectory()].WasChanged();
			if(FileCacheSettings.showUpdates){
				Log.Show(FileMonitor.all.Count + " monitors saved");
				Log.Show(FileData.all.Count + " total saved");
			}
			if(FileCacheSettings.showTimes){Log.Show("[FileCache] Save cache complete -- " + Time.Passed() + ".");}
		}
		public static void Update(){
			FileCache.Rebuild();
			FileCache.Save();
		}
	}
}