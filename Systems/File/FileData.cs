﻿using System;
using System.Collections.Generic;
using System.IO;
using SystemFile = System.IO.File;
namespace Zios.File{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Supports.Hook;
	[Serializable]
	public class FileData{
		public static Dictionary<string,FileData> all = new Dictionary<string,FileData>(StringComparer.InvariantCultureIgnoreCase);
		public static Dictionary<string,List<FileData>> byType = new Dictionary<string,List<FileData>>(StringComparer.InvariantCultureIgnoreCase);
		public string path;
		public string directory;
		public string name;
		public string fullName;
		public string extension;
		public bool isFolder;
		public List<FileData> contents = new List<FileData>();
		public FileData(){}
		public static FileData Get(string path,bool isFolder=false){
			var exists = FileData.all.TryGet(path);
			if(exists.IsNull()){exists = new FileData(path,isFolder);}
			return exists;
		}
		public FileData(string path,bool isFolder=false){
			this.path = path;
			this.directory = path.GetDirectory();
			this.name = isFolder ? path.GetPathTerm() : path.GetFilename();
			this.extension = isFolder ? "" : path.GetFileExtension();
			this.fullName = isFolder || this.extension.IsEmpty() ? this.name : this.name + "." + this.extension;
			this.isFolder = isFolder;
			if(isFolder){FileMonitor.Add(this);}
			else{FileData.byType.AddNew(this.extension).AddNew(this);}
			FileData.all[path] = this;
		}
		public override string ToString(){return this.path;}
		public IEnumerable<string> ReadLines(){return SystemFile.ReadAllLines(this.path);}
		public byte[] ReadBytes(){return SystemFile.ReadAllBytes(this.path);}
		public string ReadText(){return SystemFile.ReadAllText(this.path);}
		public FileData Write(byte[] bytes){SystemFile.WriteAllBytes(this.path,bytes);return this;}
		public FileData Write(string contents){SystemFile.WriteAllText(this.path,contents);return this;}
		public FileData Write(string[] lines){SystemFile.WriteAllLines(this.path,lines);return this;}
		public void Delete(){
			this.DeleteCache();
			if(!this.isFolder){
				SystemFile.Delete(this.path);
				return;
			}
			Directory.Delete(this.path);
		}
		public void DeleteCache(bool deep=false){
			if(!this.isFolder){FileData.byType.AddNew(this.extension).Remove(this);}
			else{
				if(deep){foreach(var content in this.contents){content.DeleteCache(true);} }
				FileMonitor.all.Remove(this.path);
			}
			FileData.all.Remove(this.path);
			this.CallHook("File/DeleteCache");
		}
		public void MarkDirty(){SystemFile.SetLastWriteTime(this.path,DateTime.Now);}
		public string GetModifiedDate(string format="M-d-yy"){return SystemFile.GetLastWriteTime(this.path).ToString(format);}
		public string GetAccessedDate(string format="M-d-yy"){return SystemFile.GetLastAccessTime(this.path).ToString(format);}
		public string GetCreatedDate(string format="M-d-yy"){return SystemFile.GetCreationTime(this.path).ToString(format);}
		public string GetChecksum(){return this.ReadText().ToMD5();}
		public long GetSize(){return new FileInfo(this.path).Length;}
		public string GetFolderPath(){
			return this.path.Substring(0,this.path.LastIndexOf("/")) + "/";
		}
	}
}
