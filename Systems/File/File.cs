using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SystemFile = System.IO.File;
namespace Zios.File{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Log;
	using Zios.Serializer.Attributes;
	using Zios.SystemAttributes;
	using Zios.Time;
	[Store(0)]
	public static class FileSettings{
		public static string rootPath = "./";
		public static string dataPath = "./";
		public static string[] ignoreFiles = new string[]{".meta"};
		public static string[] ignoreFolders = new string[]{"~",".vs",".svn",".git"};
		public static bool showUpdates;
		public static bool showTimes;
	}
	[AutoInitialize]
	public static class File{
		public static Dictionary<string,FileData[]> cache = new Dictionary<string,FileData[]>(StringComparer.OrdinalIgnoreCase);
		//===============
		// Setup
		//===============
		public static void Scan(string directory,bool deep=false){
			if(!Directory.Exists(directory)){return;}
			var folder = FileData.Get(directory,true);
			var fileEntries = Directory.GetFiles(directory);
			FileData.Get(directory,true);
			foreach(var filePath in fileEntries){
				if(filePath.ContainsAny(FileSettings.ignoreFiles)){continue;}
				var path = filePath.FixPath();
				folder.contents.AddNew(FileData.Get(path));
			}
			var folderEntries = Directory.GetDirectories(directory);
			foreach(var folderPath in folderEntries){
				if(folderPath.ContainsAny(FileSettings.ignoreFolders)){continue;}
				var path = folderPath.FixPath()+"/";
				folder.contents.AddNew(FileData.Get(path,true));
				if(deep){File.Scan(path,true);}
			}
		}
		//===============
		// Primary
		//===============
		public static List<FileData> FindFolders(string name,bool returnFirstMatch=false){
			var results = new List<FileData>();
			if(!name.EndsWith("/")){name = name+"/";}
			foreach(var item in FileData.all.Where(x=>x.Value.isFolder)){
				var folder = item.Value;
				var folderPath = item.Key;
				if(folderPath.Matches(name,true) || folderPath.EndsWith(name,true)){
					results.Add(folder);
					if(returnFirstMatch){return results;}
				}
			}
			return results;
		}
		public static FileData Get(string path){
			return FileData.all.TryGet(path);
		}
		public static FileData[] FindAll(string search,bool showWarnings=true,bool returnFirstMatch=false){
			search = search.FixPath();
			Time.Start();
			var exists = File.cache.TryGet(search);
			if(!exists.IsNull()){
				return exists;
			}
			var sort = new FileSearch();
			var name = sort.name = search.GetFilename();
			var path = sort.path = search.GetDirectory();
			var type = sort.type = search.GetFileExtension();
			var folderFirst = sort.folderFirst = sort.type.IsEmpty() && !search.ContainsAny("*","+");
			var results = sort.results = new List<FileData>();
			if(sort.folderFirst){results = File.FindFolders(search,returnFirstMatch);}
			if(results.Count == 0){
				var types = new List<string>();
				sort.deep = search.Contains("*");
				sort.target = !path.IsEmpty() && FileData.all.ContainsKey(path) ? FileData.all[path].contents : null;
				if(!path.IsEmpty()){
					sort.target = File.FindFolders(path,true);
					if(sort.target.Count>0){sort.target = sort.target.First().contents;}
				}
				sort.wildcard = name.IsEmpty() || name.MatchesAny("*","+");
				sort.prefixWild = name.StartsWith("*","+");
				sort.suffixWild = name.EndsWith("*","+");
				sort.basicName = name.Remove("*","+");
				sort.firstOnly = returnFirstMatch;
				var basicType = sort.basicType = type.Remove("*","+");
				var allTypes = FileData.byType.Keys;
				if(type.IsEmpty() || type.MatchesAny("*","+")){types = allTypes.ToList();}
				else if(type.EndsWith("*","+")){types.AddRange(allTypes.Where(x=>x.EndsWith(basicType,true)));}
				else if(type.EndsWith("*","+")){types.AddRange(allTypes.Where(x=>x.StartsWith(basicType,true)));}
				else if(FileData.byType.ContainsKey(type)){types.Add(type);}
				foreach(var typeName in types){
					var files = sort.target ?? FileData.byType[typeName];
					File.SearchType(typeName,sort,files);
					if(returnFirstMatch && results.Count > 0){break;}
				}
			}
			if(results.Count == 0 && !folderFirst){results = File.FindFolders(search,returnFirstMatch);}
			if(results.Count == 0 && !search.Contains(".")){results = File.FindAll(search+".*",false,returnFirstMatch).ToList();}
			if(results.Count == 0 && showWarnings){Log.Warning("[File] Path [" + search + "] could not be found.");}
			File.cache[search] = results.ToArray();
			if(FileSettings.showTimes){Log.Show("[File] Find [" + search + "] complete (" + results.Count + ") -- " + Time.Passed() + ".");}
			return results.ToArray();
		}
		public static void SearchType(string type,FileSearch sort,List<FileData> files){
			foreach(var file in files){
				var correctPath = sort.target.Exists() ? true : file.path.Contains(sort.path,true);
				var correctType = sort.target.IsNull() ? true : file.extension.Matches(type,true);
				sort.wildcard = sort.wildcard || (sort.prefixWild && file.name.EndsWith(sort.basicName,true));
				sort.wildcard = sort.wildcard || (sort.suffixWild && file.name.StartsWith(sort.basicName,true));
				if(correctPath && correctType && (sort.wildcard || file.name.Matches(sort.basicName,true))){
					sort.results.Add(file);
					if(sort.firstOnly){return;}
				}
				if(sort.deep){File.SearchType(type,sort,file.contents);}
			}
		}
		public static FileData AddNew(string path){
			return File.Find(path,false) ?? File.Create(path);
		}
		public static FileData Create(string path){
			path = path.FixPath();
			var isFolder = path.GetFilename().IsEmpty();
			var data = FileData.Get(path,isFolder);
			if(!isFolder){
				var folder = path.GetDirectory();
				if(!folder.IsEmpty() && !File.PathExists(folder)){File.Create(folder);}
				SystemFile.Create(path).Dispose();
			}
			else{
				Directory.CreateDirectory(path);
			}
			return data;
		}
		public static void Copy(string path,string destination){
			SystemFile.Copy(path,destination,true);
		}
		public static void Delete(string path){
			var file = File.Find(path);
			if(!file.IsNull()){
				file.Delete();
			}
		}
		public static FileData Write(string path,byte[] bytes){return File.AddNew(path).Write(bytes);}
		public static FileData Write(string path,string content){return File.AddNew(path).Write(content);}
		public static FileData Write(string path,string[] lines){return File.AddNew(path).Write(lines);}
		public static byte[] ReadBytes(string path){return File.AddNew(path).ReadBytes();}
		public static string ReadText(string path){return File.AddNew(path).ReadText();}
		public static IEnumerable<string> ReadLines(string path){return File.AddNew(path).ReadLines();}
		//===============
		// Shorthand
		//===============
		public static bool Exists(string name){
			return File.FindAll(name,false,true).Length>0;
		}
		public static bool PathExists(string path){
			path = Path.GetFullPath(path);
			return !path.IsEmpty() && (SystemFile.Exists(path) || Directory.Exists(path));
		}
		public static FileData Find(string name,bool showWarnings=true){
			return File.FindAll(name,showWarnings,true).FirstOrDefault();
		}
	}
	public class FileSearch{
		public bool deep;
		public bool folderFirst;
		public bool wildcard;
		public bool prefixWild;
		public bool suffixWild;
		public bool firstOnly;
		public string name;
		public string basicName;
		public string basicType;
		public string path;
		public string type;
		public List<FileData> target;
		public List<FileData> results;
	}
}