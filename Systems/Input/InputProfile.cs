using System.Collections.Generic;
namespace Zios.Input{
	using Zios.File;
	using Zios.Log;
	using Zios.Serializer;
	using Zios.Serializer.Attributes;
	using Zios.Supports.Hook;
	//asm Zios.Reflection;
	public class InputProfile{
		[Skip]
		public static List<InputProfile> all = new List<InputProfile>();
		public string name;
		public bool disabled;
		public List<string> requiredDevices = new List<string>();
		public Dictionary<string,string> mappings = new Dictionary<string,string>();
		static InputProfile(){
			InputProfile.LoadAll();
		}
		public InputProfile(){
			InputProfile.all.Add(this);
			Hook.Get("Input/UpdateDevices").Add<bool>(this.Validate);
		}
		public bool Validate(){
			foreach(var device in this.requiredDevices){
				if(!Input.devices.ContainsKey(device)){
					Log.Show("[InputProfile] Validate(). Could not find device '"+device+"'.");
					this.disabled = true;
					return false;
				}
			}
			return true;
		}
		public void Save(){
			Serializer.main.Save(this,this.name+".profile",true);
		}
		public static void Load(FileData profile){
			var instance = Serializer.main.Load<InputProfile>(profile);
			instance.Validate();
		}
		public static void LoadAll(){
			var files = File.FindAll("*.profile");
			if(files.Length != 0){
				foreach(var file in files){
					InputProfile.Load(file);
				}
			}
		}
	}
}