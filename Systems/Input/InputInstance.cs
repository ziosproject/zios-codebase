using System.Collections.Generic;
namespace Zios.Input{
	using Zios.Extensions;
    public class InputInstance{
        public string name;
        public InputProfile profile;
        public Dictionary<string,float> values;
        public void Update(){
            if(this.profile.IsNull()){
                foreach(var data in this.profile.mappings){
                    var actionPath = data.Key;
                    var deviceButton = data.Value;
                    var value = Input.GetValue(deviceButton);
                    this.values[actionPath] = value;
                }
            }
        }
		public float GetValue(string name){
			return this.values.TryGet(name);
		}
    }
}