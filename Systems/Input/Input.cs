using System.Collections.Generic;
using System.Linq;
namespace Zios.Input{
	using Zios.Supports.Hook;
    public static class Input{
        public static List<string> actions = new List<string>();
        public static Dictionary<string,string> devices = new Dictionary<string,string>();
        static Input(){
            Input.CheckDevices();
        }
        public static void CheckDevices(){
            Hook.Call("Input/CheckDevices");
        }
        public static float GetValue(string deviceButton){
            return Hook.Call<float>("Input/GetValue",deviceButton);
        }
        public static List<string> Get(){
            return Hook.Call<List<string>>("Input/Get");
        }
        public static string GetFirst(){
            return Input.Get().First();
        }
    }
}