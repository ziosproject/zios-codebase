using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
namespace Zios.Input.Console{
	using Zios.Input;
    using Zios.Log;
    public static class Console{
        public static void Setup(){
            var profile = new InputProfile();
			var actions = new List<string>{
				"Player1/MoveForward",
                "Player1/MoveBackward",
                "Player1/MoveLeft",
                "Player1/MoveRight",
                "Player1/Shoot"
			};
            Input.actions.AddRange(actions);
            profile.name = "Test";
            foreach(var action in Input.actions){
				var device = String.Empty;
                var button = String.Empty;
                Log.Show("Press a key to map to action '"+action+"'.");
                while((button = Input.Get().First()) == String.Empty);
				Log.Show("Key '"+button+"' mapped.");
				device = button.Split('/').First();
				if(!profile.requiredDevices.Contains(device)){
					profile.requiredDevices.Add(device);
				}
                profile.mappings.Add(action,button);
				Thread.Sleep(500);
            }
			profile.Save();
        }
    }
}