using System.Collections.Generic;
using System.Linq;
using SharpDX.XInput;
using XInputModule = SharpDX.XInput;
namespace Zios.Input.SharpDX.XInput{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Input;
	using Zios.Reflection;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	using State = XInputModule.State;
	[AutoInitialize]
	public static class XInputLink{
		public static List<Controller> controllers = new List<Controller>{
			new Controller(UserIndex.One),
			new Controller(UserIndex.Two),
			new Controller(UserIndex.Three),
			new Controller(UserIndex.Four)
		};
		public static State[] previousStates = new State[4];
		static XInputLink(){
			Hook.Get("Input/CheckDevices").Add(XInputLink.CheckDevices);
			Hook.Get("Input/GetValue").Add<string,float>(XInputLink.GetValue);
			Hook.Get("Input/Get").Add<List<string>>(XInputLink.Get);
		}
		public static void CheckDevices(){
			var prefix = "XInput Controller #";
			var connected = new bool[4]{false,false,false,false};
			for(var index=0;index<XInputLink.controllers.Count;index++){
				if(XInputLink.controllers[index].IsConnected){connected[index] = true;}
				if(connected[index] && !Input.devices.ContainsKey(prefix+index)){Input.devices.Add(prefix+index,prefix+index);}
				if(!connected[index] && Input.devices.ContainsKey(prefix+index)){Input.devices.Remove(prefix+index);}
			}
		}
		public static float GetValue(string deviceInput){
			var deviceNumber = int.Parse(deviceInput.Split('/').First().Split('#').Last());
			var inputName = deviceInput.Split('/').Last();
			var state = XInputLink.controllers[deviceNumber].GetState();
			return state.Gamepad.GetVariable(inputName).ToFloat().InverseLerp(0,1);
		}
		public static List<string> Get(){
			var inputList = new List<string>();
			var prefix = "XInput Controller #";
			foreach(var XInputDevice in Input.devices.Keys.Where(x=>x.Contains(prefix))){
				var index = XInputDevice.Split('#').Last().ToInt();
				var state = XInputLink.controllers[index].GetState();
				if(XInputLink.previousStates[index].PacketNumber != state.PacketNumber){
					var gamepad = state.Gamepad;
					var buttons = state.Gamepad.Buttons.ToName(",").Split(',');
					if(buttons.First() != "None"){
						foreach(var button in buttons){
							inputList.Add(prefix+index.ToString()+"/"+button);
						}
					}
					if(gamepad.LeftThumbX.ToFloat().Scale(-32768f,32767,0f,1f) > 0f){inputList.Add(prefix+index.ToString()+"/LeftThumbX");}
					if(gamepad.LeftThumbY.ToFloat().Scale(-32768f,32767,0f,1f) > 0f){inputList.Add(prefix+index.ToString()+"/LeftThumbY");}
					if(gamepad.LeftTrigger.ToFloat().Scale(0f,255f,0f,1f) > 0f){inputList.Add(prefix+index.ToString()+"/LeftTrigger");}
					if(gamepad.RightThumbX.ToFloat().Scale(-32768f,32767,0f,1f) > 0f){inputList.Add(prefix+index.ToString()+"/RightThumbX");}
					if(gamepad.RightThumbY.ToFloat().Scale(-32768f,32767,0f,1f) > 0f){inputList.Add(prefix+index.ToString()+"/RightThumbY");}
					if(gamepad.RightTrigger.ToFloat().Scale(0f,255f,0f,1f) > 0f){inputList.Add(prefix+index.ToString()+"/RightTrigger");}
				}
				XInputLink.previousStates[index] = state;
			}
			return inputList.Count > 0 ? inputList : new List<string>{""};
		}
		//InverseLerp did nothing but clamping and nothing at all without Saturate.
		//This method takes a value and its old range, and scales that value to a new range.
		public static float Scale(this float current,float oldMinimum,float oldMaximum,float newMinimum,float newMaximum){
			return (current-(oldMinimum))/(oldMaximum-(oldMinimum))*(newMaximum-newMinimum)+newMinimum;
		}
	}
}