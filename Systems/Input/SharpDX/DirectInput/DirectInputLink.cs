using System;
using System.Collections.Generic;
using System.Linq;
//asm SharpDX;
using SharpDX.DirectInput;
using SharpDXInput = SharpDX.DirectInput.DirectInput;
namespace Zios.Input.SharpDX.DirectInput{
	using Zios.Input;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	[AutoInitialize]
	public static class DirectInputLink{
		public static SharpDXInput directInput = new SharpDXInput();
		public static List<Keyboard> keyboards = new List<Keyboard>();
		public static List<Mouse> mice = new List<Mouse>();
		public static List<Joystick> joysticks = new List<Joystick>();
		static DirectInputLink(){
			Hook.Get("Input/CheckDevices").Add(DirectInputLink.CheckDevices);
			Hook.Get("Input/GetValue").Add<string,float>(DirectInputLink.GetValue);
			Hook.Get("Input/Get").Add<List<string>>(DirectInputLink.Get);
		}
		public static void CheckDevices(){
			if(Input.devices.Count != 0){
				foreach(var device in Input.devices){
					var GUID = Guid.Parse(device.Value);
					if(!directInput.IsDeviceAttached(GUID)){
						Input.devices.Remove(device.Key);
					}
				}
			}
			foreach(var device in DirectInputLink.directInput.GetDevices()){
				if(device.Type == DeviceType.Keyboard){
					var keyboard = new Keyboard(DirectInputLink.directInput);
					//Acquire is supposed to return a state (enum?), but is declared as void in SharpDX.
					//We can't know if acquiring the device fails or not. Could have helped detecting errors before use.
					keyboard.Acquire();
					DirectInputLink.keyboards.Add(keyboard);
				}
				else if(device.Type == DeviceType.Mouse){
					var mouse = new Mouse(DirectInputLink.directInput);
					mouse.Properties.BufferSize = 256;
					mouse.Acquire();
					DirectInputLink.mice.Add(mouse);
				}
				else if(device.Type == DeviceType.Gamepad || device.Type == DeviceType.Joystick){
					var joystick = new Joystick(DirectInputLink.directInput,device.InstanceGuid);
					joystick.Properties.BufferSize = 256;
					joystick.Acquire();
					DirectInputLink.joysticks.Add(joystick);
				}
				if(!Input.devices.ContainsValue(device.InstanceGuid.ToString())){
					var deviceNumber = 1;
					var occurrences = Input.devices.Keys.Where(x=>x.Contains(device.InstanceName));
					foreach(var occurrence in occurrences){deviceNumber++;}
					Input.devices.Add(device.InstanceName + "#" + deviceNumber.ToString(),device.InstanceGuid.ToString());
				}
			}
		}
		public static float GetValue(string deviceButton){
			var deviceName = deviceButton.Split('/').First();
			var inputName = deviceButton.Split('/').Last();
			var GUID = String.Empty;
			Input.devices.TryGetValue(deviceName,out GUID);
			foreach(var mouse in DirectInputLink.mice){
				if(mouse.Information.InstanceGuid == Guid.Parse(GUID)){
					var data = mouse.GetBufferedData();
					foreach(var state in data){
						if(state.Offset.ToString() == inputName){return state.Value;}
					}
				}
			}
			foreach(var joystick in DirectInputLink.joysticks){
				if(joystick.Information.InstanceGuid == Guid.Parse(GUID)){
					var data = joystick.GetBufferedData();
					foreach(var state in data){
						if(state.Offset.ToString() == inputName){return state.Value;}
					}
				}
			}
			return float.NaN;
		}
		public static List<string> Get(){
			var inputList = new List<string>();
			foreach(var keyboard in DirectInputLink.keyboards){
				var GUID = keyboard.Information.InstanceGuid.ToString();
				var deviceName = Input.devices.First(x=>x.Value==GUID.ToString()).Key;
				var keys = keyboard.GetCurrentState().PressedKeys.Select(x=>x.ToString());
				foreach(var key in keys){inputList.Add(deviceName + "/" + key);}
			}
			foreach(var mouse in DirectInputLink.mice){
				var GUID = mouse.Information.InstanceGuid.ToString();
				var deviceName = Input.devices.First(x=>x.Value==GUID.ToString()).Key;
				var data = mouse.GetBufferedData();
				foreach(var state in data){inputList.Add(deviceName + "/" + state.Offset);}
			}
			foreach(var joystick in DirectInputLink.joysticks){
				var GUID = joystick.Information.InstanceGuid.ToString();
				var deviceName = Input.devices.First(x=>x.Value==GUID.ToString()).Key;
				var data = joystick.GetBufferedData();
				foreach(var state in data){inputList.Add(deviceName + "/" + state.Offset);}
			}
			inputList.Remove("None");
			return inputList.Count > 0 ? inputList : new List<string>{""};
		}
	}
}