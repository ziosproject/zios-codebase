﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using SystemFile = System.IO.File;
namespace Zios.Serializer{
	using System.Collections;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Log;
	using Zios.Reflection;
	using Zios.Serializer.Attributes;
	using Zios.Serializer.Convert;
	using Zios.Supports.Hierarchy;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	[AutoInitialize(8)]
	public class Serializer{
		//================
		// Static
		//================
		public static Serializer main = new Serializer();
		public static Dictionary<string,Type> hashBuffer = new Dictionary<string,Type>();
		public static Hierarchy<object,object,string> links = new Hierarchy<object,object,string>();
		public static Dictionary<string,object> targets = new Dictionary<string,object>();
		public static Dictionary<object,string> hashes = new Dictionary<object,string>();
		public static Dictionary<object,string> paths = new Dictionary<object,string>();
		static Serializer(){
			Serializer.Reset();
			if(SystemFile.Exists("Serializer.settings")){
				var mainSettings = SystemFile.ReadAllText("Serializer.settings");
				Serializer.main.settings = Serializer.main.Deserialize<SerializerSettings>(mainSettings);
			}
			if(Serializer.main.settings.autoload){Serializer.main.LoadAuto();}
			Hook.Get("DomainUnload").Add(Serializer.main.SaveAuto);
		}
		public static void Reset(){
			Serializer.links.Clear();
			Serializer.targets.Clear();
			Serializer.hashes.Clear();
			Serializer.paths.Clear();
		}
		//================
		// Instance
		//================
		private int depth = -1;
		public SerializerSettings settings = new SerializerSettings();
		public SerializerConvert convert = new SerializerConvert();
		public Stack<KeyValuePair<object,object>> currentTarget = new Stack<KeyValuePair<object,object>>();
		public Stack<List<string>> currentBlocks = new Stack<List<string>>();
		public Dictionary<string,string> instanceBuffer = new Dictionary<string,string>();
		//===================
		// Core
		//===================
		public Serializer(){
			Hook.Call("Serializer/Setup",this.convert);
			this.convert.valueSeparator = this.SeparatorKey();
			this.convert.splitIgnoreStart = this.SeparatorEscape()+this.BlockStart();
			this.convert.splitIgnoreEnd = this.SeparatorEscape()+this.BlockEnd();
			this.convert.toTextMethods.Add(this.Escape);
			this.convert.toTextMethods.Add(this.SerializeCollection);
			this.convert.toTextMethods.Add(this.SerializeInstance);
			this.convert.toCollectionMethods.Add(this.TargetCollection);
			this.convert.toObjectMethods.Add(this.LoadBlock);
			this.convert.toObjectMethods.Add(this.LoadReference);
			this.convert.toObjectMethods.Add(this.Unescape);
			this.convert.toObjectMethods.Add(this.DeserializeInstance);
			this.SortAttributes();
		}
		public virtual bool IgnoreDefault(){return false;}
		public virtual string FileHeader(){return "";}
		public virtual string TypeStart(){return "[";}
		public virtual string TypeEnd(){return "]";}
		public virtual string TypeHeader(string value){return this.Separator()+this.TypeStart()+value.Replace("`","-")+this.TypeEnd()+this.Separator();}
		public virtual string CollectionStart(){return "[[";}
		public virtual string CollectionEnd(){return "]]";}
		public virtual string CollectionHeader(string value){return this.Separator()+this.CollectionStart()+value.Replace("`","-")+this.CollectionEnd()+this.Separator();}
		public virtual string Separator(){return "\n"+this.Depth();}
		public virtual string SeparatorItem(){return ",";}
		public virtual string SeparatorField(){return " = ";}
		public virtual string SeparatorKey(){return ":";}
		public virtual string SeparatorEscape(){return "`";}
		public virtual string Hash(){return "@";}
		public virtual string BlockStart(){return "{";}
		public virtual string BlockEnd(){return "}";}
		public virtual string Block(string value){return this.BlockStart()+value+this.BlockEnd();}
		public virtual void AddDepth(){this.depth++;}
		public virtual void TakeDepth(){this.depth--;}
		public virtual string Depth(){
			if(!this.settings.byValue || this.depth < 1){return "";}
			return new string('\t',this.depth);
		}
		public virtual string[] Special(){
			return new string[]{
				this.TypeStart(),
				this.TypeEnd(),
				this.Separator(),
				this.SeparatorItem(),
				this.SeparatorField(),
				this.SeparatorKey(),
				this.SeparatorEscape(),
				this.Hash(),
				this.BlockStart(),
				this.BlockEnd()
			};
		}
		public virtual string Escape(object value,string separator,bool ignoreDefault=false){
			if(!(value is string)){return null;}
			var text = value.As<string>().ToText();
			if(text.ContainsAny(this.Special()) || text.EndsWith(" ","\t") || text.StartsWith(" ","\t")){
				text = text.Replace(this.SeparatorEscape(),"#%1%");
				return this.SeparatorEscape()+text+this.SeparatorEscape();
			}
			return text;
		}
		public virtual object Unescape(string serialized,Type type,string separator=","){
			if(type!=typeof(string)){return null;}
			if(serialized.StartsWith(this.SeparatorEscape())){
				serialized = serialized.Trim(this.SeparatorEscape());
				serialized = serialized.Replace("#%1%",this.SeparatorEscape());
			}

			return serialized;
		}
		public virtual string Format(string name,string value){
			if(value.IsEmpty()){return "";}
			return name+this.SeparatorField()+value+this.Separator();
		}
		public virtual KeyValuePair<string,string> Parse(string line){
			var data = line.SplitIgnore(this.SeparatorField(),this.SeparatorEscape(),this.SeparatorEscape());
			var name = data[0].Trim();
			var value = data[1].Trim();
			return new KeyValuePair<string,string>(name,value);
		}
		public virtual void SortAttributes(){
			var filter = MemberFilter.FindWith<Store>();
			foreach(var type in Reflection.GetTypes(filter)){
				var store = type.GetAttribute<Store>();
				if(store.byValue.IsNull()){
					if(type.IsStatic()){store.byValue = this.settings.staticsByValue;}
					if(type.HasAttribute<Singleton>()){
						store.byValue = this.settings.singletonsByValue;
					}
				}
			}
		}
		//===================
		// Hooks
		//===================
		public virtual void TargetCollection(object collection,object key){
			var target = new KeyValuePair<object,object>(collection,key);
			this.currentTarget.Push(target);
		}
		public virtual object LoadBlock(string value,Type type,string separator=","){
			if(value.StartsWith("#@#")){
				var blocks = this.currentBlocks.Peek();
				var text = blocks[value.TrimLeft("#@#").ToInt()];
				return this.convert.ToType(text,type,separator);
			}
			return null;
		}
		public virtual object LoadReference(string value,Type type,string separator=","){
			if(value.StartsWith(this.Hash())){
				var targetPair = this.currentTarget.Peek();
				var target = targetPair.Key;
				var targetKey = targetPair.Value;
				Serializer.links.AddNew(target)[targetKey] = value;
				Serializer.hashBuffer[value] = type;
				return "@Skip";
			}
			return null;
		}
		//===================
		// Saving
		//===================
		public virtual FileData Save(object value,bool? saveByValue=null){return this.Save(value,"",null,saveByValue);}
		public virtual FileData Save(object value,string filename,bool? saveByValue=null){return this.Save(value,filename,null,saveByValue);}
		public virtual FileData Save(object value,string filename,MemberFilter filter,bool? saveByValue=null){
			var manualSave = this.settings.folder.IsDefault();
			var hash =  filename.IsEmpty() ? this.GetHash(value) : filename;
			Serializer.targets[hash] = value;
			Serializer.hashes[value] = hash;
			var serialized = this.FileHeader()+this.Serialize(value,filter,saveByValue).Trim(this.BlockStart(),this.BlockEnd());
			var type = this.GetExtension(serialized);
			var path = filename.IsEmpty() ? hash+type : filename;
			var savePath = Serializer.paths.TryGet(value) ?? this.GetPath(path);
			//Log.Show("[Serializer] Saving -- " + hash);
			var output = File.Write(savePath,serialized);
			Serializer.paths[value] = savePath;
			Hook.Call("Serializer/Save",savePath,output);
			if(!this.FallbackByValue(value,saveByValue)){
				this.instanceBuffer.Remove(hash);
				foreach(var buffer in this.instanceBuffer){
					hash = buffer.Key;
					serialized = buffer.Value;
					value = Serializer.targets[hash];	
					type = this.GetExtension(serialized);
					serialized = this.FileHeader()+serialized;
					savePath = Serializer.paths.TryGet(value) ?? this.GetPath(hash+type);
					var bufferFile = File.Write(savePath,serialized);
					Serializer.paths[value] = savePath;
					Hook.Call("Serializer/Save",savePath,bufferFile);
				}
				this.instanceBuffer.Clear();
			}
			return output;
		}
		public virtual void SaveAuto(){
			if(!this.settings.autosave){return;}
			foreach(var current in Serializer.targets.Copy()){
				var instance = current.Value;
				this.Save(instance);
			}
		}
		//===================
		// Loading
		//===================
		public virtual object Load(string path){return this.Load<object>(path);}
		public virtual Type Load<Type>(string path){return this.Load<Type>(File.Find(path));}
		public virtual object Load(FileData file){return this.Load<object>(file);}
		public virtual Type Load<Type>(FileData file){return (Type)this.Load(file,typeof(Type));}
		public virtual object Load(FileData file,Type type){
			if(file.IsNull()){return null;}
			var serialized = file.ReadText();
			var result = this.Deserialize(serialized,type);
			Serializer.hashes[result] = file.name;
			Serializer.targets[file.name] = result;
			Serializer.paths[result] = file.path;
			foreach(var buffer in Serializer.hashBuffer.Copy()){
				var hash = buffer.Key;
				var fieldType = buffer.Value;
				this.Load(File.Find(hash),fieldType);
			}
			this.LinkInstances(result);
			Serializer.hashBuffer.Clear();
			Hook.Call("Serializer/Load",file.path,file);
			return result;
		}
		public virtual void LoadAuto(){
			this.LoadStatics(this.settings.path+this.settings.staticsFolder);
			this.LoadSingletons(this.settings.path+this.settings.singletonFolder);
		}
		public virtual void LoadStatics(string path){
			this.settings.folder.Set(this.settings.staticsReferencesFolder);
			this.settings.byValue.Set(this.settings.staticsByValue);
			foreach(var type in this.GetStatics()){
				if(!type.IsStatic()){continue;}
				var filename = this.GetFilename(type,".static");
				var file = File.Find(filename,false);
				if(file.IsNull()){
					this.Save(type,path+filename);
					continue;
				}
				this.Load(file);
			}
			this.settings.byValue.Revert();
			this.settings.folder.Revert();
		}
		public virtual void LoadSingletons(string path){
			this.settings.folder.Set(this.settings.singletonReferencesFolder);
			foreach(var type in this.GetSingletons()){
				var filename = this.GetFilename(type,".singleton");
				var file = File.Find(filename,false);
				var instance = file.IsNull() ? Activator.CreateInstance(type) : this.Load(file);
				if(file.IsNull()){
					this.Save(instance,path+filename);
				}
			}
			this.settings.folder.Revert();
		}
		public virtual void LoadDirectory(string path){
			var instances = File.FindAll(path,false);
			foreach(var instanceFile in instances){
				this.Load(instanceFile);
			}
		}
		public virtual void LinkInstances(){
			foreach(var link in Serializer.links){
				var target = link.Key;
				this.LinkInstances(target);
			}
		}
		public virtual void LinkInstances(object target){
			if(target.IsNull() || !Serializer.links.ContainsKey(target)){return;}
			foreach(var data in Serializer.links[target]){
				var key = data.Key;
				var hash = data.Value;
				var match = Serializer.targets.TryGet(hash);
				if(match.IsNull()){continue;}
				if(target.AsType().IsCollection()){
					if(key is string && key.As<string>().StartsWith(this.Hash())){
						var keyHash = key.As<string>();
						key = Serializer.targets.TryGet(keyHash);
						if(key.IsNull()){
							Log.Warning("[Serializer] : Could not link collection key reference -- " + keyHash + " : " + match);
							continue;
						}
					}
					target.SetByKey(key,match);
				}
				else{
					target.SetVariable(key.As<string>(),match);
				}
			}
		}
		//===================
		// Serialization
		//===================
		public virtual bool FallbackByValue(object value,bool? serializeByValue){
			if(serializeByValue.IsNull()){
				serializeByValue = this.settings.byValue;
				var store = value.GetAttribute<Store>();
				if(!store.IsNull() && !store.byValue.IsNull()){
					return (bool)store.byValue;
				}
			}
			return (bool)serializeByValue;
		}
		public virtual MemberFilter FallbackFilter(object value,MemberFilter filter){
			if(filter.IsNull()){
				filter = this.settings.serializeFilter;
				var store = value.GetAttribute<Store>();	
				if(!store.IsNull()){
					if(value.IsStatic()){store.scope |= BindingFlags.Static;}
					return filter.Flags(store.scope);
				}
			}
			return filter;
		}
		public virtual string Serialize(object value,MemberFilter filter,bool? serializeByValue=null){
			var byValue = this.FallbackByValue(value,serializeByValue);
			filter = this.FallbackFilter(value,filter);
			this.settings.byValue.Set(byValue);
			this.settings.serializeFilter.Set(filter);
			var hashOrSerialized  = this.convert.ToTextAuto(value,this.SeparatorItem(),this.IgnoreDefault());
			this.settings.serializeFilter.Revert();
			this.settings.byValue.Revert();
			if(byValue){this.instanceBuffer.Clear();}
			return this.instanceBuffer.TryGet(hashOrSerialized) ?? hashOrSerialized;
		}
		public virtual string Serialize(object value,bool? serializeByValue=null){return this.Serialize(value,null,serializeByValue);}
		public virtual string SerializeCollection(object value,string separator,bool ignoreDefault=false){
			if(!value.AsType().IsCollection()){return null;}
			if(this.MaxDepth()){return "@TooDeep";}
			var hash = this.GetHash(value);
			var existing = this.settings.byValue ? null : this.instanceBuffer.TryGet(hash);
			if(existing.IsNull()){
				this.AddDepth();
				var type = value.AsType();
				if(!this.settings.byValue){
					this.instanceBuffer[hash] = existing = "";
					existing += this.CollectionHeader(value.AsType().GetName());
				}
				if(type.IsArray){existing += this.convert.ToText(value.As<IEnumerable>(),this.SeparatorItem());}
				else if(type.IsList()){existing += this.convert.ToText(value.As<IList>(),this.SeparatorItem());}
				else if(type.IsDictionary()){existing += this.convert.ToText(value.As<IDictionary>(),this.SeparatorItem());}
				if(!this.settings.byValue){
					existing = existing.Trim(this.Separator());
					this.instanceBuffer[hash] = existing;
					Serializer.targets[hash] = value;
					Serializer.hashes[value] = hash;
				}
				this.TakeDepth();
			}
			if(this.settings.byValue){
				return (this.BlockStart()+existing+this.BlockEnd()).ReplaceLast("\t"+this.BlockEnd(),this.BlockEnd());
			}
			return hash;
		}
		public virtual string SerializeInstance(object instance,string separator,bool ignoreDefault=false){
			if(instance.AsType().IsDelegate()){return "@Skip";}
			if(!instance.AsType().IsClass()){return null;}
			if(this.MaxDepth()){return "@TooDeep";}
			var type = instance.AsType();
			var hash = this.GetHash(instance);
			var existing = this.settings.byValue ? null : this.instanceBuffer.TryGet(hash);
			if(existing.IsNull()){
				this.AddDepth();
				var serialized = new StringBuilder();
				var filter = this.settings.serializeFilter.Get();
				serialized.Append(this.TypeHeader(type.GetName()));
				if(!this.settings.byValue){
					this.instanceBuffer[hash] = "";
				}
				foreach(var variable in instance.GetVariableMembers(filter)){
					var name = variable.Key;
					var member = variable.Value;
					var value = member.GetValue(instance);
					var result = this.convert.ToTextAuto(value,separator,ignoreDefault);
					if(result=="@Skip"){continue;}
					if(this.settings.byValue){
						if(!value.IsNull()){
							var valueType = value.AsType();
							var typeMismatch = member.GetValueType() != valueType;
							var isClass = valueType.IsClass();
							var isCollection = valueType.IsCollection();
							if(typeMismatch && isCollection){
								result = this.CollectionHeader(valueType.GetName()) + result;
							}
						}
					}
					if(!this.IgnoreDefault() && result.IsNull()){
						result = "null";
					}
					result = this.Format(name,result);
					serialized.Append(result);
				}
				existing = serialized.ToString();
				this.instanceBuffer.Remove(hash);
				if(!this.settings.byValue || this.depth < 1){existing = existing.Trim(this.Separator());}
				if(!this.settings.byValue){
					this.instanceBuffer[hash] = existing;
					Serializer.targets[hash] = instance;
					Serializer.hashes[instance] = hash;
				}
				this.TakeDepth();
			}
			if(this.settings.byValue){
				return (this.BlockStart()+existing+this.BlockEnd()).ReplaceLast("\t"+this.BlockEnd(),this.BlockEnd());
			}
			return hash;
		}
		//===================
		// Deserialization
		//===================
		public virtual object Deserialize(string serialized){return this.Deserialize<object>(serialized);}
		public virtual Type Deserialize<Type>(string serialized){return this.convert.ToType<Type>(serialized,this.SeparatorItem());}
		public virtual object Deserialize(string serialized,Type type){return this.convert.ToType(serialized,type,this.SeparatorItem());}
		public virtual object DeserializeInstance(string serialized,Type type,string separator=","){
			var original = serialized;
			var blocks = this.GetBlocks(ref serialized);
			var lines = serialized.Trim().SplitIgnore(this.Separator(),this.SeparatorEscape(),this.SeparatorEscape());
			var parsedType = this.GetParsedType(lines,type);
			if(type==typeof(object) && parsedType==null){
				Log.Warning("[Serializer] Could not determine type for deserialization -- " + serialized);
				return null;
			}
			object instance = null;
			type = parsedType ?? type;
			if(type.IsCollection()){
				serialized = parsedType.IsNull() ? original : lines.Skip(1).Join(this.Separator());
				if(serialized.StartsWith(this.BlockStart())){
					serialized = serialized.TrimLeft(this.BlockStart()).TrimRight(this.BlockEnd());
				}
				if(type.IsArray){instance = this.convert.ToArray(serialized,type,separator);}
				else if(type.IsList()){instance = this.convert.ToList(serialized,type,separator);}
				else if(type.IsDictionary()){instance = this.convert.ToDictionary(serialized,type,separator);}
			}
			else if(type.IsClass()){
				this.currentBlocks.Push(blocks);
				instance = this.convert.ToInstanceAuto(type);
				if(instance.IsNull()){return null;}
				foreach(var line in lines.Skip(1)){
					var data = new KeyValuePair<string,string>("","");
					try{data = this.Parse(line);}
					catch{
						Log.Warning("[Serializer] Unable to parse line -- " + line);
						continue;
					}
					var name = data.Key;
					var valueText = data.Value;
					if(!instance.HasVariable(name)){
						Log.Warning("[Serializer] : " + type.FullName + " does not contain field -- " + name + ". Data possibly out of date.");
						continue;
					}
					var fieldType = instance.GetVariableType(name);
					this.currentTarget.Push(new KeyValuePair<object,object>(instance,name));
					var value = this.convert.ToType(valueText,fieldType,this.SeparatorItem());
					this.currentTarget.Pop();
					instance.SetVariable(name,value);
				}
				this.currentBlocks.Pop();
			}
			return instance;
		}
		public virtual Type GetParsedType(string[] lines,Type type){
			var first = lines.First();
			var isCollection = type.IsCollection();
			var symbolStart = isCollection ? this.CollectionStart() : this.TypeStart();
			var symbolEnd = isCollection ? this.CollectionEnd() : this.TypeEnd();
			Type parsedType = null;
			if(first.StartsWith(symbolStart) && first.EndsWith(symbolEnd)){
				var typeName = first.RemoveFirst(symbolStart).RemoveLast(symbolEnd).Replace("-","`");
				parsedType = Reflection.GetType(typeName);
			}
			return parsedType;
		}
		public virtual List<string> GetBlocks(ref string serialized){
			var blocks = new List<string>();
			if(serialized.ContainsAll(this.BlockStart(),this.BlockEnd())){
				var index = 0;
				foreach(var block in serialized.SplitNodes(this.BlockStart(),this.BlockEnd()).children){
					var existing = this.BlockStart()+block.whole+this.BlockEnd();
					serialized = serialized.ReplaceFirst(existing,"#@#"+index);
					blocks.Add(block.whole.Trim());
					index += 1;
				}
			}
			return blocks;
		}
		//===================
		// Utility
		//===================
		public virtual Type[] GetSingletons(){
			var filter = MemberFilter.FindWith<Singleton>();
			return Reflection.GetTypes(filter).OrderBy(x=>x.GetAttribute<Singleton>().priority).ToArray();
		}
		public virtual Type[] GetStatics(){
			var filter = MemberFilter.FindWith<Store>().Flags(Flags.allStatic);
			return Reflection.GetTypes(filter).OrderBy(x=>x.GetAttribute<Store>().priority).ToArray();
		}
		public string GetExtension(string serialized){
			if(!this.settings.useTypeAsExtension){return ".instance";}
			var first = serialized.Split(this.Separator())[0];
			if(first.StartsWith(this.CollectionStart())){
				first = first.RemoveFirst(this.TypeStart()).RemoveLast(this.TypeEnd());
			}
			var nodes = first.SplitNodes(this.TypeStart(),this.TypeEnd());
			return "."+this.GetType(nodes.children);
		}
		public string GetType(List<Node> nodes){
			var output = "";
			var terms = "";
			foreach(var node in nodes){
				foreach(var generic in node.value.Split(",")){
					terms += generic.Split(".").Last().Split("-")[0]+",";
				}
				terms = terms.TrimEnd(',');
				output += terms;
				output += "["+this.GetType(node.children)+"]";
			}
			return output.Trim("[]");
		}
		public virtual string GetPath(string path=""){
			if(path.Contains(":") || path.StartsWith(".")){return path;}
			var directory = path.GetDirectory().IsEmpty() ? this.settings.folder.Get() : path.GetDirectory();
			return this.settings.path+directory+path.GetPathTerm();
		}
		public virtual string GetFilename(Type type,string extension){
			return type.Name.Replace(type.Name+"."+type.Name,type.Name)+extension;
		}
		public virtual string GetHash(object target){
			if(target.IsNull()){return null;}
			var hash = Serializer.hashes.TryGet(target);
			if(hash.IsNull()){
				var type = target.AsType();
				if(target.HasAttribute<Singleton>()){return this.GetFilename(type,".singleton");}
				if(type.HasAttribute<Store>()){return this.GetFilename(type,".static");}
				this.settings.nextHash += 1;
				hash = this.Hash()+this.settings.nextHash;
				while(File.Exists(hash)){
					this.settings.nextHash += 1;
					hash = this.Hash()+this.settings.nextHash;
				}
				Serializer.hashes[target] = hash;
				return hash;
			}
			return hash;
		}
		public virtual bool MaxDepth(){
			if(this.depth > 9){
				Log.Error("[Serializer] Maximum Depth (9) reached. Check data for cyclic references or serialize by reference instead.");
				return true;
			}
			return false;
		}
	}
}