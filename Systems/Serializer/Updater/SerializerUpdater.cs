using SystemFile = System.IO.File;
namespace Zios.Serializer.Updater{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.File;
	using Zios.Serializer;
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	[AutoInitialize(5)]
	public class SerializerUpdater{
		static SerializerUpdater(){
			Hook.Get("Serializer/Save").Add<string,FileData>(SerializerUpdater.Monitor);
			Hook.Get("Serializer/Load").Add<string,FileData>(SerializerUpdater.Monitor);
			Hook.Get("Serializer/LoadStatic").Add<string,FileData>(SerializerUpdater.Monitor);
			Hook.Get("File/Changed").Add<string>(SerializerUpdater.Update);
		}
		public static void Update(string path){
			var serializer = Serializer.main;
			var missing = !SystemFile.Exists(path);
			if(path.Contains(serializer.settings.path) || missing){
				Hook.Get("File/Updated").Add(serializer.LoadAuto).Limit(1);
			}
		}
		public static void Monitor(string path,FileData file){
			if(path.ContainsAny(".singleton",".static")){
				FileMonitor.Add(file);
			}
		}
	}
}