﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Zios.Serializer{
	using Zios.Extensions;
	public class XMLSerializer : Serializer{
		public override string FileHeader(){return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";}
		public override string Format(string name,string value){return "<"+name+">"+value+"</"+name+">";}
		public override string GetFilename(Type type,string extension){
			return (type.Name+extension.Replace(".","-")+".xml").Replace(type.Name+"."+type.Name,type.Name);
		}
		public override KeyValuePair<string,string> Parse(string line){
			var name = line.Split(">").First().Trim("<");
			var value = line.Split(">").Skip(1).ToString().Split("<").First();
			return new KeyValuePair<string,string>(name,value);
		}
	}
}
