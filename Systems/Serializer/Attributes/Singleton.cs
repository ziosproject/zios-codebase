using System.Reflection;
namespace Zios.Serializer.Attributes{
	public class Singleton : Store{
		public Singleton():base(){}
		public Singleton(params string[] keywords):base(keywords){}
		public Singleton(int priority,params string[] keywords):base(priority,keywords){}
		public Singleton(int priority):base(priority){}
		public override void Start(){
			this.scope = BindingFlags.Public|BindingFlags.Instance;
			this.serializeRules = SerializeRules.Always;
		}
	}
}