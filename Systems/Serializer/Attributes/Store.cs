using System;
using System.Reflection;
namespace Zios.Serializer.Attributes{
	using Zios.Extensions;
	public enum SerializeRules{Once,Always};
	public enum DeserializeRules{AtStartup,Manual};
	public class Store : Attribute{
		public static BindingFlags defaultScope = BindingFlags.Public|BindingFlags.Instance;
		public BindingFlags scope = Store.defaultScope;
		public int priority = 1000;
		public bool? byValue = null;
		public SerializeRules serializeRules = SerializeRules.Once;
		public DeserializeRules deserializeRules = DeserializeRules.AtStartup;
		public Store(){this.Start();}
		public Store(int priority):this(){this.priority = priority;}
		public Store(int priority,params string[] keywords):this(keywords){this.priority = priority;}
		public Store(params string[] keywords){this.Setup(keywords);}
		public virtual void Start(){}
		public virtual void Setup(params string[] keywords){
			this.Start();
			var flags = new BindingFlags();
			if(keywords.ContainsAny("SerializeOnce","Once")){this.serializeRules = SerializeRules.Once;}
			if(keywords.ContainsAny("SerializeAlways","Always")){this.serializeRules = SerializeRules.Always;}
			if(keywords.ContainsAny("DeserializeAtStartup","AtStartup")){this.deserializeRules = DeserializeRules.AtStartup;}
			if(keywords.ContainsAny("DeserializeManual","Manual")){this.deserializeRules = DeserializeRules.Manual;}
			if(keywords.Contains("ByValue",true)){this.byValue = true;}
			if(keywords.Contains("ByReference",true)){this.byValue = false;}
			if(keywords.Contains("Static",true)){flags |= BindingFlags.Static;}
			if(keywords.Contains("Public",true)){flags |= BindingFlags.Public;}
			if(keywords.Contains("Private",true)){flags |= BindingFlags.NonPublic;}
			if(keywords.Contains("Instance",true)){flags |= BindingFlags.Instance;}
			if(keywords.Contains("AllStatic",true)){flags |= BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Static;}
			if(keywords.Contains("AllPublic",true)){flags |= BindingFlags.Public|BindingFlags.Static|BindingFlags.Instance;}
			if(keywords.Contains("AllPrivate",true)){flags |= BindingFlags.NonPublic|BindingFlags.Static|BindingFlags.Instance;}
			if(keywords.Contains("AllInstance",true)){flags |= BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance;}
			this.scope = flags;
		}
	}
}