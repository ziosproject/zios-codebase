﻿using System;
namespace Zios.Serializer{
	using Zios.File;
	using Zios.Reflection;
	using Zios.Serializer.Attributes;
	using Zios.Supports.Mutant;
	public class SerializerSettings{
		public bool autoload = true;
		public bool autosave = true;
		public bool useTypeAsExtension = true;
		public bool singletonsByValue = true;
		public bool staticsByValue = true;
		public Mutant<bool> byValue = false;
		public Mutant<MemberFilter> serializeFilter = new MemberFilter(Store.defaultScope).Without(new Type[]{typeof(NonSerializedAttribute),typeof(Skip)});
		public int nextHash = 0;
		public string path = FileSettings.dataPath+".Serialized/";
		public Mutant<string> folder = "[Manual]/";
		public string singletonFolder = "";
		public string staticsFolder = "";
		public string singletonReferencesFolder = "[Instances]/";
		public string staticsReferencesFolder = "[Instances]/";
		public string format = "Zios.Serializer";
	}
}
