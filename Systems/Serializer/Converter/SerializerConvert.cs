﻿using System;
using System.Collections.Generic;
namespace Zios.Serializer.Convert{
	using System.Collections;
	using System.Text;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Log;
	public class SerializerConvert{
		public string splitIgnoreStart = "\"";
		public string splitIgnoreEnd = "\"";
		public string valueSeparator = ":";
		public List<Func<Type,object>> toNewMethods = new List<Func<Type,object>>();
		public List<Func<object,byte[]>> toByteMethods = new List<Func<object,byte[]>>();
		public List<Func<object,string,bool,string>> toTextMethods = new List<Func<object,string,bool,string>>();
		public List<Func<string,Type,string,object>> toObjectMethods = new List<Func<string,Type,string,object>>();
		public List<Action<object,object>> toCollectionMethods = new List<Action<object,object>>();
		public SerializerConvert(){SerializerExtensions.active = this;}
		public virtual object ToInstanceAuto(Type type){
			if(type.IsStatic()){return type;}
			object instance = null;
			foreach(var build in this.toNewMethods){
				instance = build(type);
				if(!instance.IsNull()){return instance;}
			}
			if(type.GetConstructor(Type.EmptyTypes).IsNull()){
				Log.Warning("[Serializer] "+type.Name+" Instance has no handler or empty constructor. Cannot deserialize.");
				return null;
			}
			return Activator.CreateInstance(type);
		}
		public virtual string ToTextAuto(object current,string separator="-",bool ignoreDefault=false){
			if(current.IsNull()){return null;}
			var type = current.GetType();
			foreach(var custom in this.toTextMethods){
				var result = custom(current,separator,ignoreDefault);
				if(result=="@Skip"){return result;}
				if(!result.IsNull()){return result;}
			}
			if(current is float){return current.As<float>().ToText(ignoreDefault);}
			else if(current is int){return current.As<int>().ToText(ignoreDefault);}
			else if(current is bool){return current.As<bool>().ToText(ignoreDefault);}
			else if(current is string){return current.As<string>().ToText(ignoreDefault);}
			else if(current is byte){return current.As<byte>().ToText(ignoreDefault);}
			else if(current is short){return current.As<short>().ToText(ignoreDefault);}
			else if(current is double){return current.As<double>().ToText(ignoreDefault);}
			else if(type.IsEnum){return current.As<Enum>().ToText(separator,ignoreDefault);}
			else if(type.IsArray){return this.ToText(current.As<IEnumerable>(),separator);}
			else if(type.IsList()){return this.ToText(current.As<IList>(),separator);}
			else if(type.IsDictionary()){return this.ToText(current.As<IDictionary>(),separator);}
			Log.Show("[SerializerConvert] has no method to convert " + current + " (" + current.GetType() +") to a string.");
			return null;
		}
		public virtual object ToType(string current,Type type,string separator=","){
			if(current=="null"){return type.GetDefault();}
			foreach(var custom in this.toObjectMethods){
				var result = custom(current,type,separator);
				if(result is string && result.As<string>()=="@Skip"){return type.GetDefault();}
				if(!result.IsNull()){return result;}
			}
			if(type == typeof(string)){return current;}
			else if(type == typeof(float)){return ConvertString.ToSingle(current);}
			else if(type == typeof(double)){return ConvertString.ToDouble(current);}
			else if(type == typeof(short)){return ConvertString.ToShort(current);}
			else if(type == typeof(int)){return ConvertString.ToInt(current);}
			else if(type == typeof(bool)){return ConvertString.ToBool(current);}
			else if(type == typeof(byte)){return ConvertString.ToByte(current);}
			else if(type == typeof(byte[])){return ConvertString.ToByteArray(current);}
			else if(type.IsEnum){return ConvertString.ToEnum(current,type,separator);}
			else if(type.IsArray){return this.ToArray(current,type,separator);}
			else if(type.IsList()){return this.ToList(current,type,separator);}
			else if(type.IsDictionary()){return this.ToDictionary(current,type,separator);}
			Log.Show("[SerializerConvert] has no method to parse " + current + " to a " + type.Name);
			return type.GetDefault();
		}
		public virtual Type ToType<Type>(string current,string separator=","){
			return (Type)this.ToType(current,typeof(Type),separator);
		}
		//==================
		// Array / List
		//==================
		public virtual IList ToArray(string value,Type type,string separator=","){
			if(!value.IsEmpty()){
				var index = 0;
				var values = value.SplitIgnore(separator,this.splitIgnoreStart,this.splitIgnoreEnd);
				var instance = (IList)Activator.CreateInstance(type,values.Length);
				var elementType = type.GetElementType();
				foreach(var item in values){
					foreach(var method in this.toCollectionMethods){method(instance,index);}
					instance[index] = this.ToType(item,elementType,separator);
					index += 1;
				}
				return instance;
			}
			return (IList)Activator.CreateInstance(type,0);
		}
		public virtual IList ToList(string value,Type type,string separator=","){
			var index = 0;
			var instance = (IList)Activator.CreateInstance(type);
			if(!value.IsEmpty()){
				var genericType = type.GetGenericArguments()[0];
				var values = value.SplitIgnore(separator,this.splitIgnoreStart,this.splitIgnoreEnd);
				foreach(var item in values){
					foreach(var method in this.toCollectionMethods){method(instance,index);}
					instance.Add(this.ToType(item,genericType,separator));
					index += 1;
				}
			}
			return instance;
		}
		public virtual string ToText(IEnumerable current,string separator=",",bool ignoreDefault=false){
			var output = new StringBuilder();
			var index = 0;
			var prefix = "";
			foreach(var value in current){
				index += 1;
				var result = this.ToTextAuto(value,separator,ignoreDefault);
				if(ignoreDefault){
					if(result.IsEmpty()){continue;}
					prefix = "["+(index-1)+"]";
				}
				output.Append(prefix+result+separator);
			}
			return output.ToString().TrimRight(separator);
		}
		//==================
		// Dictionary
		//==================
		public virtual IDictionary ToDictionary(string value,Type type,string itemSeparator=","){
			var instance = (IDictionary)Activator.CreateInstance(type);
			if(!value.IsEmpty()){
				var keyType = type.GetGenericArguments()[0];
				var valueType = type.GetGenericArguments()[1];
				var valueSeparator = this.valueSeparator;
				var values = value.SplitIgnore(itemSeparator,this.splitIgnoreStart,this.splitIgnoreEnd);
				foreach(var item in values){
					var parts = item.SplitIgnore(valueSeparator,this.splitIgnoreStart,this.splitIgnoreEnd);
					var keyData = this.ToType(parts[0],keyType);
					foreach(var method in this.toCollectionMethods){method(instance,keyData);}
					var valueData = this.ToType(parts[1],valueType,itemSeparator);
					instance[keyData] = valueData;
				}
			}
			return instance;
		}
		public string ToText(IDictionary current,string itemSeparator=",",bool changesOnly=false){
			var output = new StringBuilder();
			var index = 0;
			var prefix = "";
			foreach(DictionaryEntry item in current){
				index += 1;
				var key = this.ToTextAuto(item.Key,itemSeparator,false);
				var value = this.ToTextAuto(item.Value,itemSeparator,changesOnly);
				if(changesOnly && value.IsEmpty()){continue;}
				output.Append(prefix+key+ConvertIDictionary.valueSeparator+value+itemSeparator);
			}
			return output.ToString().TrimRight(itemSeparator);
		}
		public byte[] ToBytesAuto(object current){
			foreach(var custom in this.toByteMethods){
				var result = custom(current);
				if(!result.IsNull()){return result;}
			}
			if(current is float){return current.As<float>().ToBytes();}
			else if(current is int){return current.As<int>().ToBytes();}
			else if(current is bool){return current.As<bool>().ToBytes();}
			else if(current is string){return current.As<string>().ToStringBytes();}
			else if(current is byte){return current.As<byte>().ToBytes();}
			else if(current is short){return current.As<short>().ToBytes();}
			else if(current is double){return current.As<double>().ToBytes();}
			Log.Show("[ConvertObject] has no method to convert " + current + " (" + current.GetType() +") to bytes.");
			return new byte[0];
		}
	}
	public static class SerializerExtensions{
		public static SerializerConvert active;
		public static string ToTextAuto(this object current,string separator="-",bool ignoreDefault=false){
			return SerializerExtensions.active.ToTextAuto(current,separator,ignoreDefault);
		}
		public static string ToText(this IEnumerable current,string separator=",",bool ignoreDefault=false){
			return SerializerExtensions.active.ToText(current,separator,ignoreDefault);
		}
		public static string ToText(this IDictionary current,string itemSeparator=",",bool changesOnly=false){
			return SerializerExtensions.active.ToText(current,itemSeparator,changesOnly);
		}
		public static Type ToType<Type>(this string current,string separator=","){
			return SerializerExtensions.active.ToType<Type>(current,separator);
		}
		public static object ToType(this string current,Type type,string separator=","){
			return SerializerExtensions.active.ToText(current,separator);
		}
		public static IList ToArray(this string current,Type type,string separator=","){
			return SerializerExtensions.active.ToArray(current,type,separator);
		}
		public static IList ToList(this string current,Type type,string separator=","){
			return SerializerExtensions.active.ToList(current,type,separator);
		}
		public static IDictionary ToDictionary(this string current,Type type,string itemSeparator=","){
			return SerializerExtensions.active.ToDictionary(current,type,itemSeparator);
		}
		public static byte[] ToBytesAuto(this object current){
			return SerializerExtensions.active.ToBytesAuto(current);
		}
		public static byte[] Append(this byte[] current,object value){
			current = current.Concat(value.ToBytesAuto());
			return current;
		}
		public static byte[] Prepend(this byte[] current,object value){
			current = value.ToBytesAuto().Concat(current);
			return current;
		}
	}
}