using System.Collections.Generic;
namespace Zios.Log{
	using Zios.Extensions;
	public delegate void LogMethod(Log log,string message);
	public class Log{
		public static int depth = 0;
		public static string mark = "�";
		public static LogMethod Display = Log.DisplayDefault;
		public static Dictionary<object,Log> limits = new Dictionary<object,Log>();
		public static Dictionary<object,List<Log>> groups = new Dictionary<object,List<Log>>();
		public static void DisplayDefault(Log log,string message){
			System.Console.WriteLine(message);
		}
		private static void Prepare(Log log,string label=null){
			var message = log.message;
			if(log.limit == -1 || log.limit > 0){
				if(Log.depth>0){message = new string(' ',Log.depth*4)+Log.mark+" "+log.message;}
				Log.Display(log,message);
				log.limit -= 1;
			}
		}
		//=====================
		// SHORTHAND
		//=====================
		public static void TakeDepth(){Log.depth -= 1;}
		public static void GiveDepth(){Log.depth += 1;}
		public static Log Add(object message){return new Log(message);}
		public static Log AddLimit(object key,object message,int limit=1){
			var exists = Log.limits.TryGet(key);
			if(exists.IsNull()){
				exists = Log.limits[key] = new Log(message).Limit(limit);
			}
			return exists;
		}
		public static Log Error(object message){return Log.Add(message).Type("Error").Show();}
		public static Log Warning(object message){return Log.Add(message).Type("Warning").Show();}
		public static Log Show(object message){return Log.Add(message).Show();}
		public static Log ShowTarget(object message,object target){return Log.Add(message).Target(target).Show();}
		//=====================
		// INSTANCE
		//=====================
		public string message;
		public string type;
		public object target;
		public int limit = -1;
		public Log(object message){this.message = message.ToString();}
		public Log Limit(int limit){this.limit = limit;return this;}
		public Log Type(string type){this.type = type;return this;}
		public Log Target(object target){this.target = target;return this;}
		public Log Show(bool allow=true){
			if(allow){Log.Prepare(this);}
			return this;
		}
		public Log Depth(){Log.GiveDepth();return this;}
		public Log DepthTake(){Log.TakeDepth();return this;}
	}
	public static class LogExtensions{
		public static void Show<Key,Value>(this IDictionary<Key,Value> current,string label="[Dictionary]"){
			Log.Show(label).Depth();
			foreach(var item in current){
				var message = item.Key + " : " + item.Value;
				Log.Show(message);
			}
			Log.TakeDepth();
		}
		public static void Show<Value>(this IEnumerable<Value> current,string label="[List]"){
			Log.Show(label).Depth();
			foreach(var item in current){
				Log.Show(item);
			}
			Log.TakeDepth();
		}
	}
}