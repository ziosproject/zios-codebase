using System;
namespace Zios.Program{
	using Zios.Supports.Hook;
	using Zios.SystemAttributes;
	[AutoInitialize]
	public static class Program{
		public static bool exiting;
		static Program(){
			AppDomain.CurrentDomain.ProcessExit += Program.Exit;
			AppDomain.CurrentDomain.DomainUnload += Program.Exit;
		}
		public static void Exit(object x,EventArgs y){
			if(!Program.exiting){
				Program.exiting = true;
				Hook.Call("DomainUnload");
			}
		}
	}
}