﻿using System.Collections.Generic;
using System.Web;
namespace Zios.Server.Files{
	using System.IO;
	using Zios.Extensions;
	using Zios.Reflection;
	using Zios.Server;
	using Zios.SystemAttributes;
	[AutoInitialize(5)]
	public class Files{
		public static string root = "./client";
		public static Dictionary<string,string> map = new Dictionary<string,string>();
		static Files(){
			Files.Parse();
			Response.handleMethods.Add(Files.Handle);
		}
		public static void Parse(){
			var config = Reflection.FindType("Server.Config");
			if(!config.IsNull()){
				var data = config.GetVariable<Dictionary<string,List<string>>>("content");
				foreach(var line in data.AddNew("MimeMapping")){
					var name = line.Parse(""," ");
					var value = line.Remove(name).Trim();
					Log.Add("[Files] Loaded MimeMapping : [" + name + "] to [" + value + "]"); 
					Files.map["."+name.TrimLeft(".")] = value;
				}
			}
		}
		public static void Handle(){
			var data = Response.Get();
			var path = data.GetPath();
			var fileTarget = Files.root + path;
			var folderTarget = fileTarget.ReplaceFirst("@","");
			if(path.StartsWith("/@") && Directory.Exists(folderTarget)){
				foreach(var file in Directory.GetFiles(folderTarget)){
					data.outputString.Append(file.TrimLeft(Files.root)+"|");
				}
				data.outputString.Length -= 1;
			}
			if(File.Exists(fileTarget)){
				var extension = Path.GetExtension(fileTarget);
				var mimeType = Files.map.TryGet(extension) ?? MimeMapping.GetMimeMapping(fileTarget);
				data.context.Response.ContentType = mimeType;
				data.outputBytes = File.ReadAllBytes(fileTarget);
			}
		}
	}
}