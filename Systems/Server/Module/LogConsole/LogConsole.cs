﻿namespace Zios.Server.LogConsole{
	using System;
	using Zios.Server;
	using Zios.SystemAttributes;
	[AutoInitialize(1)]
	public class LogConsole{
		static LogConsole(){
			Log.AddMethod(LogConsole.Write);
		}
		public static void Write(string message){
			Console.WriteLine(message);
		}
	}
}
