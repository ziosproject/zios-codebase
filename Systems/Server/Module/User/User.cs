﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Zios.Server.User{
	using Konscious.Security.Cryptography;
	using Zios.Extensions;
	using Zios.Serializer.Attributes;
	using Zios.Server;
	using Zios.SystemAttributes;
	//asm Konscious.Security.Cryptography.Argon2;
	//asm Konscious.Security.Cryptography.Blake2;
	//asm System.Numerics.Vectors;
	[AutoInitialize]
	public class User{
		[Store] public static Dictionary<string,User> all;
		public string name;
		public string email;
		public string password;
		static User(){
			Task.Add("/create/user",User.Create);
			Task.Add("/view/user",User.View);
			Task.Add("/edit/user",User.Edit);
			Task.Add("/remove/user",User.Remove);
		}
		public static User Get(string name){return User.all[name];}
		public static object Create(){
			var response = Response.Get();
			var name = response.GetInput("username");
			var password = response.GetInput("password");
			var email = response.GetInput("email");
			if(User.all.ContainsKey(name)){
				return response.AddOutput("UserNameAlreadyExists");
			}
			if(!User.all.Where(x=>x.Value.email==email).FirstOrDefault().IsNull()){
				return response.AddOutput("UserEmailAlreadyExists");
			}
			var user = User.all[name] = new User();
			user.name = name;
			user.email = email;
			user.password = new Argon2id(Encoding.UTF8.GetBytes(password)).ToString();
			response.AddOutput("UserCreated");
			return user;
		}
		public static object View(){return null;}
		public static object Edit(){return null;}
		public static object Remove(){return null;}
		public void Authenticate(){}
	}
}