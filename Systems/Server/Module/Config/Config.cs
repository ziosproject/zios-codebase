﻿using System;
using System.Collections.Generic;
namespace Zios.Server.Config{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Server;
	using Zios.SystemAttributes;
	using File = System.IO.File;
	[AutoInitialize(0)]
	public class Config{
		public static Dictionary<string,List<string>> content = new Dictionary<string,List<string>>();
		static Config(){
			Config.Read();
			Config.Parse();
		}
		public static void Read(){
			if(!File.Exists("./HttpServer.cfg")){return;}
			var scope = "None";
			foreach(var line in File.ReadAllLines("./HttpServer.cfg")){
				if(String.IsNullOrEmpty(line.Trim())){continue;}
				if(line.StartsWith("[") && line.EndsWith("]")){
					scope = line.Trim('[',']');
					continue;
				}
				Config.content.AddNew(scope).Add(line);
			}
		}
		public static void Parse(){
			var content = Config.content;
			foreach(var line in content.AddNew("Addresses")){
				Http.addresses.Add(line);
			}
			foreach(var line in content.AddNew("Logging")){
				var name = line.Parse(""," ");
				var value = line.Remove(name).Trim();
				name = name.ToLower();
				if(name=="messageprefix"){Log.prefix = value;}
			}
			foreach(var line in content.AddNew("Headers")){
				var name = line.Parse(""," ");
				var value = line.Remove(name).Trim();
				Response.headers[name] = value;
			}
			foreach(var line in content.AddNew("System")){
				var name = line.Parse(""," ");
				var value = line.Remove(name).Trim();
				name = name.ToLower();
				if(name=="logresponsetime"){Response.showTimer = value.ToBool();}
				if(name=="logresponserequest"){Response.showRequest = value.ToBool();}
			}
		}
	}
}