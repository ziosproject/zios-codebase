﻿using System.Collections.Generic;
using System.IO;
namespace Zios.Server.LogFile{
	using System.Text;
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Reflection;
	using Zios.Server;
	using Zios.SystemAttributes;
	using File = System.IO.File;
	[AutoInitialize(1)]
	public class LogFile{
		public static int frequency = 1;
		public static int current = 0;
		public static StringBuilder buffer = new StringBuilder();
		public static Dictionary<string,LogDate> files = new Dictionary<string,LogDate>();
		static LogFile(){
			Log.AddMethod(LogFile.Write);
			Http.AddHook("pass",LogFile.Save);
			LogFile.Parse();
		}
		public static void Write(string message){
			lock(LogFile.buffer){
				LogFile.buffer.AppendLine(message);
				LogFile.current += 1;
			}
		}
		public static void Save(){
			if(LogFile.files.Count > 0){
				lock(LogFile.buffer){
					if(LogFile.current >= LogFile.frequency){
						foreach(var item in LogFile.files){
							item.Value.Save();
						}
						LogFile.buffer.Clear();
						LogFile.current = 0;
					}
				}
			}
		}
		public static void Parse(){
			var config = Reflection.FindType("Server.Config");
			if(!config.IsNull()){
				var data = config.GetVariable<Dictionary<string,List<string>>>("content");
				foreach(var line in data.AddNew("Logging")){
					var name = line.Parse(""," ");
					var value = line.Remove(name).Trim();
					name = name.ToLower();
					if(value.ToLower() == "none"){continue;}
					if(name == "storefrequency"){
						LogFile.frequency = value.ToLower().Remove("every","message","messages").Trim().ToInt();
					}
					if(name == "storecurrent"){
						LogFile.files["current"] = new LogDate(value);
						if(File.Exists(value)){
							File.Delete(value);
						}
					}
					if(name == "storedaily"){LogFile.files["daily"] = new LogDate(value);}
					if(name == "storemonthly"){LogFile.files["monthly"] = new LogDate(value);}
					if(name == "storeyearly"){LogFile.files["yearly"] = new LogDate(value);}
				}
			}
		}
	}
	public class LogDate{
		public string path;
		public LogDate(string path){this.path = path;}
		public void Save(){
			var path = this.path.ParseDates();
			var folder = path.Split("/").SkipLast().Join("/");
			Directory.CreateDirectory(folder);
			File.AppendAllText(path,LogFile.buffer.ToString());
		}
	}
}