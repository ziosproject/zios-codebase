﻿using System.Collections.Generic;
namespace Zios.Server.Versions{
	using Zios.SystemAttributes;
	[AutoInitialize]
	public class Versions<Target>{
		public static Dictionary<Target,Versions<Target>> all;
		public Target active;
		public List<Target> revisions;
	}
}