﻿using System.Collections.Generic;
namespace Zios.Server.Element{
	using Zios.Server;
	using Zios.SystemAttributes;
	[AutoInitialize]
	public class Element{
		public static List<Element> all;
		public int id;
		public string name;	
		static Element(){
			Task.Add("/get/element",Element.Get);
			Task.Add("/count/elements",Element.Count);
		}
		public static Element Get(){
			//var response = ResponseData.Get();
			//var id = response.url.replace("/get/element/","");
			//var element = Element.all.Find(x=>x.id==id);
			return null;
		}
		public static object Count(){
			return Element.all.Count;
		}
		public static void Edit(){}
		public static void Remove(){}
		public static void Create(){}
	}
	public class Element<Type> : Element{public Type value;}
}