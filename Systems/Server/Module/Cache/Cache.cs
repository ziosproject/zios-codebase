﻿using System.Collections.Generic;
namespace Zios.Server.Cache{
	using Zios.SystemAttributes;
	[AutoInitialize(2)]
	public class Cache{
		public static Dictionary<string,Cache> all;
		public string command;
		public string output;
		public List<string> dirtyTriggers;
	}
}