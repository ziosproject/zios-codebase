﻿using System.Collections.Generic;
namespace Zios.Server.Permission{
	using Zios.Server.Element;
	using Zios.Server.Link;
	using Zios.Server.User;
	using Zios.SystemAttributes;
	[AutoInitialize(1)]
	public class Permission{
		public static string ownerPassword = "okay";
		public static Dictionary<Element,User> allUsers;
		public static Dictionary<Element,Permission> allTags;
		public static Dictionary<Link,Permission> allLinks;
		public Dictionary<string,bool> allow;
	}
}