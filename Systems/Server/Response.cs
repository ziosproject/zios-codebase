﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
namespace Zios.Server{
	using System.IO;
	using Zios.Extensions;
	public class Response{
		//============
		// STATIC
		//============
		public static Dictionary<Thread,Response> all = new Dictionary<Thread,Response>();
		public static List<Action> handleMethods = new List<Action>();
		public static Dictionary<string,string> headers = new Dictionary<string,string>();
		public static bool showTimer = true;
		public static bool showRequest = true; 
		public static Response Get(){
			return Response.all[Thread.CurrentThread];
		}
		//============
		// INSTANCE
		//============
		public HttpListenerContext context;
		public Dictionary<string,string> input;
		public Dictionary<string,string> cookies;
		public byte[] outputBytes;
		public StringBuilder outputString = new StringBuilder();
		public Stopwatch timer;
		public Response(HttpListenerContext context){
			if(Response.showTimer){this.timer = Stopwatch.StartNew();}
			this.context = context;
			this.context.Response.ContentType = "text/html";
			Response.all[Thread.CurrentThread] = this;
		}
		public string GetPath(){
			return this.context.Request.Url.LocalPath;
		}
		public string Display(){
			var output = this.outputString.ToString();
			this.context = null;
			Response.all.Remove(Thread.CurrentThread);
			return output;
		}
		public void FindInput(){
			if(this.input == null){
				this.input = new Dictionary<string,string>();
				var getData = this.context.Request.QueryString;
				foreach(KeyValuePair<string,string> item in getData){
					this.input[item.Key] = item.Value;
				}
				var postData = new StreamReader(this.context.Request.InputStream).ReadToEnd();
				Log.Add(postData);
			}
		}
		public void FindCookies(){
			if(this.cookies == null){
				this.cookies = new Dictionary<string,string>();
				foreach(Cookie cookie in this.context.Request.Cookies){
					this.cookies[cookie.Name] = cookie.Value;
				}
			}
		}
		public string GetInput(string name){
			this.FindInput();
			return this.input.TryGet(name);
		}
		public string GetCookie(string name){
			this.FindCookies();
			return this.cookies.TryGet(name);
		}
		public void Handle(){
			foreach(var method in Response.handleMethods){
				method();
			}
		}
		public object AddOutput(string name,object data=null){
			//Serializer.main.Save(data);
			return null;
			//this.outputString.Append("#"+name+"|"data
		}
		public void Write(){
			var response = this.context.Response;
			var request = this.context.Request;
			var buffer = this.outputBytes ?? Encoding.UTF8.GetBytes(this.outputString.ToString());
			foreach(var data in Response.headers){
				response.AppendHeader(data.Key,data.Value);
			}
			response.ContentLength64 = buffer.Length;
			response.OutputStream.Write(buffer,0,buffer.Length);
			if(Response.showTimer || Response.showRequest){
				var message = "[Response] ";
				if(Response.showRequest){
					var path = request.Url.LocalPath;
					var ip = request.Headers.Get("X-Forwarded-For");
					if(ip.IsEmpty()){ip = request.RemoteEndPoint.Address.ToString();}
					var contentType = this.context.Response.ContentType;
					message += ip + " : " + contentType + " : " + path + " : ";
				}
				if(Response.showTimer){
					var elapsed = (float)this.timer.Elapsed.TotalMilliseconds/1000f;
					message += elapsed + " seconds";
				}
				message = message.TrimRight(" : ");
				Log.Add(message);
			}
		}
	}
}