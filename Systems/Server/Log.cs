﻿using System;
using System.Collections.Generic;
using Zios.Extensions;
namespace Zios.Server{
	public class Log{
		public static List<Action<string>> methods = new List<Action<string>>();
		public static string prefix = "";
		public static void AddMethod(Action<string> method){
			if(!Log.methods.Contains(method)){
				Log.methods.Add(method);
			}
		}
		public static void Add(object text){
			Log.Add(text.ToString());
		}
		public static void Add(string text,bool usePrefix=true){
			var prefix = usePrefix ? Log.prefix.ParseDates() : "";
			foreach(var method in Log.methods){
				method(prefix+text);
			}
		}
		public static void Add(Exception exception){
			Log.Add("Exception -- " + exception.Message);
			Log.Add(exception.ToString());
		}
	}
}
