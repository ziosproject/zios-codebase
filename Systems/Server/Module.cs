﻿using System.Collections.Generic;
using System.Reflection;
namespace Zios.Server{
	using System;
	using Zios.File;
	using Zios.SystemAttributes;
	[AutoInitialize(-1)]
	public class Module{
		public static Dictionary<string,Assembly> all = new Dictionary<string,Assembly>();
		static Module(){
			var modules = File.FindAll("/modules/*.dll",false);
			foreach(var file in modules){
				Module.all[file.name] = Assembly.LoadFrom(file.path);
				Console.WriteLine("[Module] Loaded : " + file.path);
			}
			if(modules.Length > 0){
				AutoInitialize.Start();
			}
		}
	}
}