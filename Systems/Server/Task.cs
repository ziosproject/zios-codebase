﻿using System;
using System.Collections.Generic;
namespace Zios.Server{
	using Zios.SystemAttributes;
	[AutoInitialize(3)]
	public class Task{
		public static Dictionary<string,List<KeywordExpression>> groups = new Dictionary<string,List<KeywordExpression>>();
		public static Dictionary<string,Func<object>> keywords = new Dictionary<string,Func<object>>();
		static Task(){
			Server.Response.handleMethods.Add(Task.Response);
		}
		public static void Response(){
			var data = Server.Response.Get();
			var path = data.GetPath();
			Task.CheckGroup(path);
			Task.Check(path);
		}
		public static void Add(string command,Func<object> method){
			Task.keywords[command] = method;
		}
		public static void AddGroup(string command,params string[] subCommands){
			//Actions.groups[command] = subCommands.ToList();
		}
		public static object Call(string command){
			if(command == null || command == ""){return null;}
			if(Task.keywords.ContainsKey(command)){
				return Task.keywords[command]();
			}
			Log.Add("Action does not exist -- " + command);
			return null;
		}
		public static void Check(string command){
			foreach(var action in Task.keywords){
				if(command.Contains(action.Key)){
					action.Value();
				}
			}
		}
		public static void CheckGroup(string command){
			foreach(var group in Task.groups){
				if(command.Contains(group.Key)){
					foreach(var expression in group.Value){
						var keyword = expression.keyword;
						var result = Task.Call(keyword);
						var usesBranching = expression.ifTrue != null || expression.ifFalse != null;
						if(usesBranching && result is bool){
							var state = (bool)result;
							var action = state ? expression.ifTrue : expression.ifFalse;
							Task.Call(action);
						}
					}
				}
			}
		}
	}
	public class KeywordExpression{
		public string keyword;
		public string ifTrue;
		public string ifFalse;
	}
}