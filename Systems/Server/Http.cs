﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
namespace Zios.Server{
	using Zios.Extensions;
	public class Http{
		public static HttpListener listener = new HttpListener();
		public static List<string> addresses = new List<string>();
		public static Dictionary<string,List<Action>> hooks = new Dictionary<string,List<Action>>();
		public static Dictionary<string,List<Action<Exception>>> errorHooks = new Dictionary<string,List<Action<Exception>>>();
		static Http(){
			Http.hooks.AddNew("pass");
			Http.hooks.AddNew("requestStart");
			Http.hooks.AddNew("requestEnd");
			Http.errorHooks.AddNew("requestError");
		}
		public static void AddHook(string name,Action method){
			Http.hooks.AddNew(name).AddNew(method);
		}
		public static void Start(params string[] userAddresses){
			Log.Add(new string('-',80),false);
			Log.Add(new string(' ',33)+"SESSION START"+new string(' ',33),false);
			Log.Add(new string('-',80),false);
			var addresses = userAddresses.ToList();
			if(Http.addresses.Count < 1){
				Http.addresses.Add("http://127.0.0.1:80/");
			}
			addresses.AddRange(Http.addresses);
			foreach(var address in addresses){
				try{
					Log.Add("[Http] Listening at : " + address);
					Http.listener.Prefixes.Add(address);
					Http.listener.Start();
				}
				catch(Exception exception){
					Log.Add(exception);
				}
			}
			Http.listener.Start();
			while(Http.listener.IsListening){
				var waitRequest = Http.listener.GetContext();
				ThreadPool.QueueUserWorkItem(Http.Process,waitRequest);
				foreach(var method in Http.hooks["pass"]){method();}
			}
		}
		public static void Process(object data){
			var context = (HttpListenerContext)data;
			var response = new Response(context);
			foreach(var method in Http.hooks["requestStart"]){method();}
			try{
				response.Handle();
				response.Write();
			}
			catch(Exception exception){
				Log.Add(exception);
				foreach(var method in Http.errorHooks["requestError"]){method(exception);}
			}
			context.Response.OutputStream.Close();
			foreach(var method in Http.hooks["requestEnd"]){method();}
		}
	}
}