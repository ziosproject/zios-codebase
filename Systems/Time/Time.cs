using System.Collections.Generic;
using System.Diagnostics;
namespace Zios.Time{
	using Zios.Extensions;
	using Zios.SystemAttributes;
	[AutoInitialize(-1)]
	public static class Time{
		public static Dictionary<string,Stopwatch> timers = new Dictionary<string,Stopwatch>();
		static Time(){Time.Start("Time/Global");}
		public static float Get(){return (float)Time.Elapsed("Time/Global");}
		public static void Start(string name="Time/Measure"){Time.GetWatch(name).Restart();}
		public static void Stop(string name="Time/Measure"){Time.GetWatch(name).Stop();}
		public static void Reset(string name="Time/Measure"){Time.GetWatch(name).Reset();}
		public static double Elapsed(string name="Time/Measure"){return Time.GetWatch(name).Elapsed.TotalMilliseconds/1000f;}
		public static string Passed(string name="Time/Measure"){return Time.Elapsed(name) + " seconds";}
		public static Stopwatch GetWatch(string name="Time/Measure"){
			var exists = Time.timers.TryGet(name);
			if(exists.IsNull()){
				exists = Time.timers.AddNew(name);
				exists.Start();
			}
			return exists;
		}
	}
	public static class FloatExtensions{
		public static bool Elapsed(this float current){return Time.Elapsed()>=current;}
		public static string Passed(this float current){return Time.Elapsed()-current+" seconds";}
		public static double AddTime(this float current){return current+Time.Elapsed();}
	}
	public static class IntExtensions{
		public static bool Elapsed(this int current){return Time.Elapsed()>=current;}
		public static string Passed(this int current){return Time.Elapsed()-current+" seconds";}
		public static double AddTime(this int current){return current+Time.Elapsed();}
	}
}