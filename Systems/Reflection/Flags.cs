using System.Reflection;
namespace Zios.Reflection{
	public static class Flags{
		public const BindingFlags none = BindingFlags.Default;
		public const BindingFlags all = BindingFlags.Static|BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public;
		public const BindingFlags allFlat = BindingFlags.Static|BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public|BindingFlags.FlattenHierarchy;
		public const BindingFlags allDeclared = BindingFlags.Static|BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public|BindingFlags.DeclaredOnly;
		public const BindingFlags allStatic = BindingFlags.Static|BindingFlags.Public|BindingFlags.NonPublic;
		public const BindingFlags staticPublic = BindingFlags.Static|BindingFlags.Public;
		public const BindingFlags staticPrivate = BindingFlags.Static|BindingFlags.NonPublic;
		public const BindingFlags instance = BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public;
		public const BindingFlags instanceDeclared = BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public|BindingFlags.DeclaredOnly;
		public const BindingFlags instancePrivate = BindingFlags.Instance|BindingFlags.NonPublic;
		public const BindingFlags instancePublic = BindingFlags.Instance|BindingFlags.Public;
	}
}