using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace Zios.Reflection{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Log;
	public static partial class Reflection{
		public static Dictionary<Type,InfoSet<MemberInfo>> variables = new Dictionary<Type,InfoSet<MemberInfo>>();
		//=========================
		// Get
		//=========================
		public static MemberInfo GetVariableMember(this object current,string name,MemberFilter filter=null,params object[] parameters){
			Func<string,BindingFlags,MemberInfo> method = (target,flags)=>{
				MemberInfo existing = current.GetField(target,filter);
				existing = existing ?? current.GetProperty(target,filter);
				return existing;
			};
			return current.GetMember(name,filter,Reflection.variables,method);
		}
		public static Dictionary<string,MemberInfo> GetVariableMembers(this object current,MemberFilter filter=null){
			Func<BindingFlags,MemberInfo[]> method = (flags)=>{
				var members = new Dictionary<string,MemberInfo>();
				foreach(var item in current.GetMembers(filter)){
					if(item.Value is FieldInfo || item.Value is PropertyInfo){
						members[item.Key] = item.Value;
					}
				}
				return members.Values.ToArray();
			};
			return current.GetMembers(filter,Reflection.variables,method);
		}
		public static Type GetVariableType(this object current,string name,MemberFilter filter=null){
			return current.GetVariableMember(name,filter).GetValueType();
		}
		public static object GetVariable(this object current,string name,MemberFilter filter=null){
			return current.GetVariableMember(name,filter).GetValue(current);
		}
		public static ReturnType GetVariable<ReturnType>(this object current,string name,MemberFilter filter=null){
			return (ReturnType)current.GetVariableMember(name,filter).GetValue(current);
		}
		//=========================
		// Get All
		//=========================
		public static Dictionary<string,object> GetVariables(this object current,MemberFilter filter=null){
			var values = new Dictionary<string,object>();
			foreach(var item in current.GetVariableMembers(filter)){
				values[item.Key] = item.Value.GetValue(current);
			}
			return values;
		}
		public static Dictionary<string,Type> GetVariables<Type>(this object current,MemberFilter filter=null){
			filter = filter == null ? MemberFilter.empty.IsType<Type>() : filter.IsType<Type>();
			var values = new Dictionary<string,Type>();
			foreach(var item in current.GetVariableMembers(filter)){
				values[item.Key] = (Type)item.Value.GetValue(current);
			}
			return values;
		}
		//=========================
		// Set
		//=========================
		public static void SetValue<Type>(this object current,MemberInfo info,Type value){
			info.SetValue(current,value);
		}
		public static void SetValue<Type>(this MemberInfo info,object instance,Type value){
			if(info.IsNull()){return;}
			var target = instance.AsType().IsStatic() ? null : instance;
			if(info.MemberType == MemberTypes.Field){
				var field = info.As<FieldInfo>();
				field.SetValue(target,value);
			}
			if(info.MemberType == MemberTypes.Property){
				var property = info.As<PropertyInfo>();
				if(property.CanWrite){
					property.SetValue(target,value,null);
				}
			}
		}
		public static void SetVariables<Type>(this object current,IDictionary<string,Type> values,MemberFilter filter=null){
			foreach(var item in values){
				current.SetVariable<Type>(item.Key,item.Value,filter);
			}
		}
		public static void SetVariable<Type>(this object current,string name,Type value,MemberFilter filter=null){
			var member = current.GetVariableMember(name,filter);
			member.SetValue(current,value);
		}
		public static Target UseVariables<Target>(this Target current,Target other,MemberFilter filter=null){
			foreach(var item in current.GetVariableMembers(filter)){
				var name = item.Key;
				var member = item.Value;
				current.SetValue(member,other.GetVariable(name,filter));
			}
			return current;
		}
		public static void ClearVariable(this object current,string name,MemberFilter filter=null){
			var existing = current.GetVariableMember(name,filter);
			if(!existing.IsNull()){
				existing.SetValue<object>(current,null);
			}
		}
		public static object InstanceVariable(this object current,string name,MemberFilter filter=null,bool force=false){
			var member = current.GetVariableMember(name,filter);
			var instance = member.GetValue(current);
			if(force || instance.IsNull()){
				var instanceType = member.GetValueType();
				if(!instanceType.GetConstructor(Type.EmptyTypes).IsNull()){
					try{
						instance = Activator.CreateInstance(instanceType);
						member.SetValue(current,instance);
					}
					catch{}
				}
			}
			return instance;
		}
		//=========================
		// Set Value
		//=========================
		public static void SetValuesByType<Target>(this object current,IList<Target> values,MemberFilter filter=null){
			var existing = current.GetVariables(filter);
			var index = 0;
			foreach(var item in existing){
				if(index >= values.Count){break;}
				current.SetVariable(item.Key,values[index]);
				++index;
			}
		}
		public static void SetValuesByName<Target>(this object current,Dictionary<string,Target> values,MemberFilter filter=null){
			var existing = current.GetVariables(filter);
			foreach(var item in existing){
				if(!values.ContainsKey(item.Key)){
					if(Reflection.debug){Log.Show("[ReflectionVariable] No key found when attempting to assign values by name -- " + item.Key);}
					continue;
				}
				current.SetVariable(item.Key,values[item.Key]);
			}
		}
		//=========================
		// Other
		//=========================
		public static bool HasVariable(this object current,string name,MemberFilter filter=null){
			return current.HasField(name,filter) || current.HasProperty(name,filter);
		}
		public static bool IsVariable(this MemberInfo info){
			if(info.MemberType == MemberTypes.Field){return true;}
			if(info.MemberType == MemberTypes.Method){return true;}
			return false;
		}
		public static bool CanWrite(this MemberInfo info){
			if(info is FieldInfo){
				var field = info.As<FieldInfo>();
				var isConstant = field.IsLiteral && !field.IsInitOnly;
				return !isConstant;
			}
			if(info is PropertyInfo){return info.As<PropertyInfo>().CanWrite;}
			return true;
		}

		public static string GetAlias(this object current){
			if(current.HasVariable("alias")){return current.GetVariable<string>("alias");}
			//if(current.HasVariable("name")){return current.GetVariable<string>("name");}
			return current.GetType().Name;
		}
		public static Target Copy<Target>(this Target current) where Target : class,new(){
			return new Target().UseVariables(current);
		}
		public static Target Clone<Target>(this Target current) where Target : class{
			if(current.IsNull()){return null;}
			var method = current.GetType().GetMethod("MemberwiseClone",Flags.instancePrivate);
			if(method != null){
				return (Target)method.Invoke(current,null);
			}
			return null;
		}
	}
}