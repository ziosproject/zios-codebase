using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
namespace Zios.Reflection{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Supports.Hierarchy;
	public static partial class Reflection{
		public static bool debug;
		public static StringBuilder signature = new StringBuilder();
		public static List<Type> allTypes = new List<Type>();
		public static List<Assembly> assemblies = new List<Assembly>();
		public static Hierarchy<Type,string,FieldInfo> searched = new Hierarchy<Type,string,FieldInfo>();
		public static Hierarchy<Type,string,FieldInfo> all = new Hierarchy<Type,string,FieldInfo>();
		public static Hierarchy<string,MemberFilter,Type[]> types = new Hierarchy<string,MemberFilter,Type[]>();
		public static Dictionary<Type,InfoSet<MemberInfo>> members = new Dictionary<Type,InfoSet<MemberInfo>>();
		public static Dictionary<Type,InfoSet<PropertyInfo>> properties = new Dictionary<Type,InfoSet<PropertyInfo>>();
		public static Dictionary<Type,InfoSet<Type>> classes = new Dictionary<Type,InfoSet<Type>>();
		public static Dictionary<Type,InfoSet<FieldInfo>> fields = new Dictionary<Type,InfoSet<FieldInfo>>();
		public static List<Action> clearEvents = new List<Action>();
		//=========================
		// Internal
		//=========================
		public static void Clear(){
			Reflection.allTypes.Clear();
			Reflection.assemblies.Clear();
			Reflection.searched.Clear();
			Reflection.all.Clear();
			Reflection.types.Clear();
			Reflection.members.Clear();
			Reflection.properties.Clear();
			Reflection.classes.Clear();
			Reflection.fields.Clear();
			Reflection.attributes.Clear();
			Reflection.memberAttributes.Clear();
			Reflection.parameters.Clear();
			Reflection.variables.Clear();
			Reflection.methods.Clear();
			foreach(var method in Reflection.clearEvents){
				method();
			}
		}
		//===============
		// Single
		//===============
		public static Type GetType(string name){
			var types = Reflection.GetTypes();
			return types.Where(x=>x.FullName==name).FirstOrDefault() ?? types.Where(x=>x.Name==name).FirstOrDefault();
		}
		public static Type FindType(string name){
			var types = Reflection.GetTypes();
			return types.Where(x=>x.FullName.Contains(name)).FirstOrDefault();
		}
		public static Target GetMember<Target>(this object current,string name,MemberFilter filter,Dictionary<Type,InfoSet<Target>> scope,Func<string,BindingFlags,Target> GetMethod) where Target : MemberInfo{
			var hasFilter = filter != null;
			var type = current.AsType();
			try{
				if(!hasFilter){return scope[type].searched[name];}
				return scope[type].filtered[filter][name];
			}
			catch{
				filter = filter ?? MemberFilter.empty;
				var existing = GetMethod(name,filter.flags);
				var target = hasFilter ? scope.AddNew(type).filtered.AddNew(filter) : scope.AddNew(type).searched;
				if(hasFilter && !filter.Keep(existing)){existing = null;}
				if(!existing.IsNull()){
					target[existing.GetMemberSignature()] = existing;
				}
				target[name] = existing;
				return existing;
			}
		}
		public static MemberInfo GetMember(this object current,string name,MemberFilter filter=null){
			Func<string,BindingFlags,MemberInfo> method = (x,flags)=>current.AsType().GetMember(x,flags).FirstOrDefault();
			return current.GetMember(name,filter,Reflection.members,method);
		}
		public static Type GetClass(this object current,string name,MemberFilter filter=null){
			return current.GetMember(name,filter,Reflection.classes,current.AsType().GetNestedType);
		}
		public static FieldInfo GetField(this object current,string name,MemberFilter filter=null){
			return current.GetMember(name,filter,Reflection.fields,current.AsType().GetField);
		}
		public static PropertyInfo GetProperty(this object current,string name,MemberFilter filter=null){
			return current.GetMember(name,filter,Reflection.properties,current.AsType().GetProperty);
		}
		public static Type GetValueType(this MemberInfo info){
			if(info.MemberType == MemberTypes.Field){return info.As<FieldInfo>().FieldType;}
			if(info.MemberType == MemberTypes.Property){return info.As<PropertyInfo>().PropertyType;}
			if(info.MemberType == MemberTypes.Event){return info.As<EventInfo>().EventHandlerType;}
			if(info.MemberType == MemberTypes.Method){return info.As<MethodInfo>().ReturnType;}
			if(info.MemberType == MemberTypes.Constructor){return info.As<ConstructorInfo>().DeclaringType;}
			return null;
		}
		public static object GetValue(this MemberInfo info,object instance=null){
			var target = instance == null || instance.AsType().IsStatic() ? null : instance;
			if(info is FieldInfo){
				var field = info.As<FieldInfo>();
				return field.GetValue(target);
			}
			else if(info is PropertyInfo){
				var property = info.As<PropertyInfo>();
				if(property.CanRead){
					if(property.GetIndexParameters().Length>0){return null;}
					return property.GetValue(target,null);
				}
			}
			return null;
		}
		//===============
		// Multiple
		//===============
		public static List<Assembly> GetAssemblies(){
			if(Reflection.assemblies.Count < 1){Reflection.assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();}
			return Reflection.assemblies;
		}
		public static List<Type> GetTypes(){
			if(Reflection.allTypes.Count < 1){
				var assemblies = Reflection.GetAssemblies();
				foreach(var assembly in assemblies){
					var types = assembly.GetTypes();
					foreach(var type in types){
						Reflection.allTypes.Add(type);
					}
				}
			}
			return Reflection.allTypes;
		}
		public static Type[] GetTypes(MemberFilter filter){
			filter = filter ?? MemberFilter.empty;
			try{
				return Reflection.types["*"][filter];
			}
			catch{
				return Reflection.types.AddNew("*")[filter] = Reflection.GetTypes().KeepWhere(filter).ToArray();
			}
		}
		public static Type[] GetTypes<Target>(){
			var filter = MemberFilter.empty.IsType<Target>();
			return Reflection.GetTypes(filter);
		}
		public static Dictionary<string,Target> GetMembers<Target>(this object current,MemberFilter filter,Dictionary<Type,InfoSet<Target>> scope,Func<BindingFlags,Target[]> GetMethod) where Target : MemberInfo{
			var hasFilter = filter != null;
			var type = current.AsType();
			try{
				if(!hasFilter){
					var exists = scope[type].all;
					if(exists.Count!=-1){return exists;}
				}
				return scope[type].allFiltered[filter];
			}
			catch{
				filter = filter ?? MemberFilter.empty;
				Dictionary<string,Target> entries;
				if(!hasFilter){entries = scope.AddNew(type).all = new Dictionary<string,Target>();}
				else{entries = scope.AddNew(type).allFiltered.AddNew(filter);}
				IEnumerable<Target> members = GetMethod(filter.flags);
				if(hasFilter){members = members.KeepWhere(filter);}
				foreach(var member in members){
					entries[member.GetMemberSignature()] = member;
				}
				return entries;
			}
		}
		public static Dictionary<string,MemberInfo> GetMembers(this object current,MemberFilter filter=null){
			return current.GetMembers(filter,Reflection.members,current.AsType().GetMembers);
		}
		public static Dictionary<string,Type> GetClasses(this object current,MemberFilter filter=null){
			return current.GetMembers(filter,Reflection.classes,current.AsType().GetNestedTypes);
		}
		public static Dictionary<string,FieldInfo> GetFields(this object current,MemberFilter filter=null){
			return current.GetMembers(filter,Reflection.fields,current.AsType().GetFields);
		}
		public static Dictionary<string,PropertyInfo> GetProperties(this object current,MemberFilter filter=null){
			return current.GetMembers(filter,Reflection.properties,current.AsType().GetProperties);
		}
		public static bool HasMember(this object current,string name,MemberFilter filter=null){
			return !current.GetMember(name,filter).IsNull();
		}
		public static bool HasField(this object current,string name,MemberFilter filter=null){
			return !current.GetField(name,filter).IsNull();
		}
		public static bool HasProperty(this object current,string name,MemberFilter filter=null){
			return !current.GetProperty(name,filter).IsNull();
		}
		public static bool HasClass(this object current,string name,MemberFilter filter=null){
			return !current.GetClass(name,filter).IsNull();
		}
		public static string GetMemberSignature(this MemberInfo current){
			var name = current.Name;
			if(current.MemberType == MemberTypes.Method){
				var method = current.As<MethodInfo>();
				if(!method.ReturnType.IsNull()){name = method.ReturnType.Name + " " + name;}
				name += method.GetMethodParameters().Select(x=>x.ParameterType.As<object>()).ToArray().GetSignature();
			}
			else if(current.MemberType == MemberTypes.Constructor){
				var method = current.As<ConstructorInfo>();
				name += method.GetMethodParameters().Select(x=>x.ParameterType.As<object>()).ToArray().GetSignature();
			}
			return name;
		}
		public static string GetSignature(this object[] current,string name=""){
			if(current == null || current.Length < 1){return name;}
			var values = Reflection.signature;
			values.Clear();
			values.Append(name).Append("(");
			foreach(var item in current){
				values.Append(item.AsType().Name).Append(",");
			}
			values.Length -= 1;
			return values.Append(")").ToString();
		}
	}
	public class InfoSet<Info>{
		public Dictionary<string,Info> searched = new Dictionary<string,Info>();
		public Dictionary<string,Info> all;
		public Hierarchy<MemberFilter,string,Info> filtered = new Hierarchy<MemberFilter,string,Info>();
		public Hierarchy<MemberFilter,string,Info> allFiltered = new Hierarchy<MemberFilter,string,Info>();
	}
}