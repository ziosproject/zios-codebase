using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace Zios.Reflection{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Serializer.Attributes;
	using Flag = Zios.Reflection.Flags;
	public static class MemberFilterExtensions{
		public static bool Keep<Type>(this Type target,MemberFilter filter){return filter.Keep(target);}
		public static IEnumerable<Type> KeepWhere<Type>(this IEnumerable<Type> targets,MemberFilter filter){return filter.KeepWhere(targets);}
	}
	public class MemberFilter{
		public static List<MemberFilter> existing = new List<MemberFilter>();
		public static MemberFilter empty = new MemberFilter();
		public Type[] withAttributes = new Type[0];
		public Type[] withoutAttributes = new Type[0];
		public BindingFlags flags = Flag.all;
		public bool flagsOnly;
		public bool ignoreGenerics;
		public Type isType;
		public Type ofType;
		public static MemberFilter FindFlags(BindingFlags flags){return MemberFilter.Find(flags);}
		public static MemberFilter FindType<Type>(){return MemberFilter.Find(Flag.all,null,null,typeof(Type));}
		public static MemberFilter FindType(Type type){return MemberFilter.Find(Flag.all,null,null,type);}
		public static MemberFilter FindWith(Type[] with){return MemberFilter.Find(Flag.all,with);}
		public static MemberFilter FindWithout(Type[] without){return MemberFilter.Find(Flag.all,null,without);}
		public static MemberFilter FindWith<Attribute>(){return MemberFilter.Find(Flag.all,typeof(Attribute).AsArray());}
		public static MemberFilter FindWithout<Attribute>(){return MemberFilter.Find(Flag.all,null,typeof(Attribute).AsArray());}
		public static MemberFilter Find(BindingFlags flags,Type[] with=null,Type[] without=null,Type isType=null,Type ofType=null,bool ignoreGenerics=false){
			var match = MemberFilter.existing.Where(x=>x.Matches(flags,with,without,isType,ofType,ignoreGenerics)).FirstOrDefault();
			if(match.IsNull()){
				match = new MemberFilter(flags,with,without,isType,ofType,ignoreGenerics);
				MemberFilter.existing.Add(match);
			}
			return match;
		}
		public MemberFilter(BindingFlags flags=Flag.all,Type[] with=null,Type[] without=null,Type isType=null,Type ofType=null,bool ignoreGenerics=false){
			if(with != null){this.withAttributes = with;}
			if(without != null){this.withoutAttributes = without;}
			this.flagsOnly = (with == null || with.Length < 1) && (without == null || without.Length < 1) && (isType == null) && (ofType == null);
			this.ignoreGenerics = ignoreGenerics;
			this.flags = flags;
			this.isType = isType;
			this.ofType = ofType;
		}
		public bool Matches(BindingFlags flags,Type[] with,Type[] without,Type isType,Type ofType,bool ignoreGenerics=false){
			if(this.flags != flags){return false;}
			if(this.isType != isType){return false;}
			if(this.ofType != ofType){return false;}
			if(this.ignoreGenerics != ignoreGenerics){return false;}
			if(with != null && !(this.withAttributes.Length == 0 && with.Length == 0)){
				var sizeMismatch = this.withAttributes.Length != with.Length;
				if(sizeMismatch || !this.withAttributes.SequenceEqual(with)){return false;}
			}
			if(without != null && !(this.withoutAttributes.Length == 0 && without.Length == 0)){
				var sizeMismatch = this.withoutAttributes.Length != without.Length;
				if(sizeMismatch || !this.withoutAttributes.SequenceEqual(without)){return false;}
			}
			return true;
		}
		public bool Matches(MemberFilter other){
			return this.Matches(other.flags,other.withAttributes,other.withoutAttributes,other.isType,other.ofType,other.ignoreGenerics);
		}
		public bool Keep<Target>(Target target){
			if(this.flagsOnly){return true;}
			if(target.IsNull()){return false;}
			var isInfo = typeof(Target) != typeof(System.Type);
			var type = isInfo ? target.As<MemberInfo>() : target.AsType();
			var isVariable = isInfo && type.IsVariable();
			var valueType = isInfo ? type.GetValueType() : type;
			var with = this.withAttributes;
			var without = this.withoutAttributes;
			if(this.ignoreGenerics && valueType.IsGeneric()){return false;}
			if(with.Length > 0){
				var targets = isVariable ? type.GetAttributes().Extend(valueType.GetAttributes()) : type.GetAttributes();
				if(!targets.ContainsAll(with,(x,y)=>x.Is(y))){return false;}
			}
			if(without.Length > 0){
				var targets = isVariable ? type.GetAttributes().Extend(valueType.GetAttributes()) : type.GetAttributes();
				if(targets.ContainsAny(without,(x,y)=>x.Is(y))){return false;}
			}
			if(!this.isType.IsNull() && valueType!=this.isType){return false;}
			if(!this.ofType.IsNull() && valueType.Is(this.ofType)){return false;}
			return true;
		}
		public IEnumerable<Type> KeepWhere<Type>(IEnumerable<Type> targets){
			if(this.flagsOnly){return targets;}
			return targets.Where(x=>this.Keep(x));
		}
		//===============
		// Conversion
		//===============
		public static implicit operator MemberFilter(BindingFlags flags){return MemberFilter.FindFlags(flags);}
		public static implicit operator MemberFilter(Type isType){return MemberFilter.FindType(isType);}
		//public static implicit operator MemberFilter(Type[] with){return MemberFilter.FindWith(with);}
		public MemberFilter Copy(){return new MemberFilter(this.flags,this.withAttributes,this.withoutAttributes,this.isType,this.ofType);}
		public MemberFilter Flags(BindingFlags flags){return this.Build(flags:flags);}
		public MemberFilter IsType(Type type){return this.Build(isType:type);}
		public MemberFilter IsType<Type>(){return this.Build(isType:typeof(Type));}
		public MemberFilter OfType(Type type){return this.Build(ofType:type);}
		public MemberFilter OfType<Type>(){return this.Build(ofType:typeof(Type));}
		public MemberFilter With(Type[] attributes){return this.Build(with:attributes);}
		public MemberFilter With<Type>(){return this.Build(with:typeof(Type).AsArray());}
		public MemberFilter Without(Type[] attributes){return this.Build(without:attributes);}
		public MemberFilter Without<Type>(){return this.Build(without:typeof(Type).AsArray());}
		public MemberFilter IgnoreGenerics(){return this.Build(ignoreGenerics:true);}
		public MemberFilter SetFlags(BindingFlags flags){
			this.flags = flags;
			return this;
		}
		public MemberFilter SetType(Type isType){
			this.isType = isType;
			return this;
		}
		public MemberFilter SetType<Type>(){
			this.isType = typeof(Type);
			return this;
		}
		public MemberFilter SetOfType(Type ofType){
			this.ofType = ofType;
			return this;
		}
		public MemberFilter SetOfType<Type>(){
			this.ofType = typeof(Type);
			return this;
		}
		public MemberFilter SetWith(Type[] withAttributes){
			this.withAttributes = withAttributes;
			return this;
		}
		public MemberFilter SetWith<Type>(){
			this.withAttributes = typeof(Type).AsArray();
			return this;
		}
		public MemberFilter SetWithout(Type[] withoutAttributes){
			this.withoutAttributes = withoutAttributes;
			return this;
		}
		public MemberFilter SetWithout<Type>(){
			this.withoutAttributes = typeof(Type).AsArray();
			return this;
		}
		public MemberFilter Build(BindingFlags flags=Flag.none,Type[] with=null,Type[] without=null,Type isType=null,Type ofType=null,bool? ignoreGenerics=null){
			flags = flags == Flag.none ? this.flags : flags;
			with = with ?? this.withAttributes;
			without = without ?? this.withoutAttributes;
			isType = isType ?? this.isType;
			ofType = ofType ?? this.ofType;
			var skipGenerics = ignoreGenerics.IsNull() ? this.ignoreGenerics : (bool)ignoreGenerics;
			return MemberFilter.Find(flags,with,without,isType,ofType,skipGenerics);
		}
	}
}