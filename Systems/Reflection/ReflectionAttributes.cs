using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace Zios.Reflection{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Supports.Hierarchy;
	public static partial class Reflection{
		public static Hierarchy<MemberInfo,string,string,List<Attribute>> attributes = new Hierarchy<MemberInfo,string,string,List<Attribute>>();
		public static Dictionary<MemberInfo,List<Type>> memberAttributes = new Dictionary<MemberInfo,List<Type>>();
		public static List<Type> GetAttributes(this MemberInfo current){
			if(!Reflection.memberAttributes.ContainsKey(current)){
				Reflection.memberAttributes[current] = current.GetCustomAttributes(true).Select(x=>x.GetType()).ToList();
			}
			return Reflection.memberAttributes[current];
		}
		public static Dictionary<string,List<Attribute>> GetAttributes(this object current,string memberName=null){
			var type = current is MemberInfo ? current.As<MemberInfo>() : current.AsType();
			memberName = memberName ?? "@Self";
			if(!Reflection.attributes.ContainsKey(type) || !Reflection.attributes[type].ContainsKey(memberName)){
				MemberInfo member = type;
				var target = Reflection.attributes.AddNew(type).AddNew(memberName);
				if(memberName != "@Self"){member = Reflection.GetMember(type,memberName);}
				foreach(var attribute in member.GetCustomAttributes(true)){
					target.AddNew(attribute.GetType().Name).Add(attribute.As<Attribute>());
				}
			}
			return Reflection.attributes[type][memberName];
		}
		public static IEnumerable<Target> GetAttributes<Target>(this object current,string memberName=null) where Target : Attribute{
			return Reflection.GetAttributes(current,memberName).SelectMany(x=>x.Value).Where(x=>x is Target).Cast<Target>();
		}
		public static IEnumerable<Attribute> GetAttributes(this object current,Type attribute,string memberName=null){
			return Reflection.GetAttributes(current,memberName).SelectMany(x=>x.Value).Where(x=>x.Is(attribute)).Cast<Attribute>();
		}
		public static IEnumerable<Attribute> GetAttributes(this object current,string attributeName,string memberName=null){
			var attribute = Reflection.GetType(attributeName);
			if(attribute.IsNull()){return null;}
			return Reflection.GetAttributes(current,attribute,memberName);
		}
		public static Target GetAttribute<Target>(this object current,string memberName=null) where Target : Attribute{
			return Reflection.GetAttributes<Target>(current,memberName).FirstOrDefault();
		}
		public static Attribute GetAttribute(this object current,Type attribute,string memberName=null){
			return Reflection.GetAttributes(current,attribute,memberName).FirstOrDefault();
		}
		public static Attribute GetAttribute(this object current,string attributeName,string memberName=null){
			return Reflection.GetAttributes(current,attributeName,memberName).FirstOrDefault();
		}
		public static bool HasAttribute(this object current,Type attribute){
			return Reflection.GetAttributes(current).ContainsKey(attribute.Name);
		}
		public static bool HasAttribute<Target>(this object current,string memberName=null){
			return Reflection.GetAttributes(current,memberName).ContainsKey(typeof(Target).Name);
		}
		public static bool HasAttribute(this object current,string attributeName,string memberName=null){
			return Reflection.GetAttributes(current).ContainsKey(attributeName);
		}
		public static bool HasAttribute(this object current,string memberName,Type attribute){
			return Reflection.GetAttributes(current).ContainsKey(attribute.GetType().Name);
		}
		public static bool HasAttributes(this object current,string memberName,params Type[] attributes){
			var existing = Reflection.GetAttributes(current);
			foreach(var attribute in attributes){
				if(!existing.ContainsKey(attribute.GetType().Name)){
					return false;
				}
			}
			return true;
		}
	}
}