using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace Zios.Reflection{
	using Zios.Extensions;
	using Zios.Extensions.Convert;
	using Zios.Supports.Hierarchy;
	using Zios.Supports.Invoker;
	public static partial class Reflection{
		public static Hierarchy<Type,string,Invoker> invokers = new Hierarchy<Type,string,Invoker>();
		public static Dictionary<Type,InfoSet<MethodInfo>> methods = new Dictionary<Type,InfoSet<MethodInfo>>();
		public static Dictionary<string,MethodInfo> methodPaths = new Dictionary<string,MethodInfo>();
		public static Dictionary<MemberInfo,ParameterInfo[]> parameters = new Dictionary<MemberInfo,ParameterInfo[]>();
		//=========================
		// Get
		//=========================
		public static MethodInfo GetMethod(this object current,string name,MemberFilter filter=null,params object[] parameters){
			var signature = Reflection.GetSignature(parameters,name);
			Func<string,BindingFlags,MethodInfo> method = (memberName,flags)=>{
				var types = parameters.Select(x=>x.AsType()).ToArray();
				var type = current.AsType();
				return type.GetMethod(name,flags,null,types,null) ?? type.GetMethod(name,flags);
			};
			return current.GetMember(signature,filter,Reflection.methods,method);
		}
		public static Dictionary<string,MethodInfo> GetMethods(this object current,MemberFilter filter=null){
			return current.GetMembers(filter,Reflection.methods,current.AsType().GetMethods);
		}
		//=========================
		// Call
		//=========================
		public static MethodType GetAs<MethodType>(this MethodInfo current,object target=null){
			if(target is Type || target.IsNull()){return Delegate.CreateDelegate(typeof(MethodType),current).As<MethodType>();}
			return Delegate.CreateDelegate(typeof(MethodType),target,current).As<MethodType>();
		}
		public static Invoker GetInvoker(this object current,string name,params object[] parameters){
			var type = current.AsType();
			var signature = name;
			if(parameters != null){signature = Reflection.GetSignature(parameters,name);}
			try{return Reflection.invokers[type][signature];}catch{}
			var invoker = Reflection.invokers.AddNew(type).TryGet(signature);
			if(invoker == null){
				var isStatic = current is Type;
				var methods = current.GetMethods().Values.Where(x=>x.Name==name).Distinct().ToArray();
				if(methods.Length > 1){
					foreach(var method in methods){
						var id = Reflection.GetSignature(method.GetParameters().Select(x=>x.ParameterType).ToArray(),name);
						Reflection.invokers[type][id] = isStatic ? method.GetExpressionInvoker() : method.GetInvoker();
					}
					signature = Reflection.GetSignature(parameters,name);
					invoker = Reflection.invokers[type].TryGet(signature);
				}
				if(methods.Length == 1){
					invoker = Reflection.invokers.AddNew(type)[signature] = isStatic ? methods[0].GetExpressionInvoker() : methods[0].GetInvoker();
				}
			}
			return invoker;
		}
		public static void Call(this object current,string name,params object[] parameters){
			var target = current is Type ? null : current;
			current.GetInvoker(name,parameters)(target,parameters);
		}
		public static void Call(this object current,string name){
			var target = current is Type ? null : current;
			current.GetInvoker(name,null)(target,null);
		}
		public static ReturnType Call<ReturnType>(this object current,string name,params object[] parameters){
			var target = current is Type ? null : current;
			return (ReturnType)current.GetInvoker(name,parameters)(target,parameters);
		}
		public static ReturnType Call<ReturnType>(this object current,string name){
			var target = current is Type ? null : current;
			return (ReturnType)current.GetInvoker(name,null)(target,null);
		}
		public static object CallPath(this string current,params object[] parameters){
			return current.CallPath<object>(parameters);
		}
		public static ReturnType CallPath<ReturnType>(this string current,params object[] parameters){
			var assemblies = Reflection.GetAssemblies();
			foreach(var assembly in assemblies){
				var allMethods = assembly.GetTypes().SelectMany(x=>x.GetMethods());
				var method = allMethods.FirstOrDefault(x=>(x.DeclaringType.FullName+"."+x.Name)==current);
				if(!method.IsNull()){
					var result = method.Invoke(null,parameters);
					if(!result.IsNull()){
						return result.As<ReturnType>();
					}
				}
			}
			return default(ReturnType);
		}
		//=========================
		// Other
		//=========================
		public static ParameterInfo[] GetMethodParameters(this MemberInfo current){
			var result = Reflection.parameters.TryGet(current);
			if(result.IsNull()){
				var parameters = current is ConstructorInfo ? current.As<ConstructorInfo>().GetParameters() : current.As<MethodInfo>().GetParameters();
				result = Reflection.parameters[current] = parameters;
			}
			return result;
		}
		public static bool HasMethod(this object current,string name,MemberFilter filter=null,params object[] parameters){
			return !current.GetMethod(name,filter,parameters).IsNull();
		}
	}
}