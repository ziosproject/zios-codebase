class Extensions{
	constructor(){
		var types = [Array,String,NodeList,HTMLCollection];
		for(var x in types){
			var type = types[x];
			type.prototype.first = function(){return this[0];}
			type.prototype.last  = function(){return this[this.length-1];}
			type.prototype.contains = function(){
				for(var index=0;index<arguments.length;++index){
					var term = arguments[index];
					if(!this.includes(term)){return false;}
				}
				return true;
			}
			type.prototype.containsAny = function(){
				for(var index=0;index<arguments.length;++index){
					var term = arguments[index];
					if(this.includes(term)){return true;}
				}
				return false;
			}
			type.prototype.matchesAny = function(){
				for(var index=0;index<arguments.length;++index){
					var term = arguments[index];
					if(this == term){return true;}
				}
				return false;
			}
		}
		String.prototype.remove = function(){
			for(var index=0;index<arguments.length;++index){
				var term = arguments[index];
				this.replace(term,"");
			}
		}
		String.prototype.replaceAll = function(search,replacement){
			return this.split(search).join(replacement);
		};
	}
}
class ZiosClient{
	constructor(){
		Data.Setup(DataZson);
		View.Setup();
		new Extensions();
	}
	Setup(){
		Form.Setup();
	}
}
class View{
	constructor(name){
		this.htmlTerms = [];
		this.queryTerms = [];
		View.all[name] = this;
	}
	static Setup(){
		View.all = [];
		View.total = 0;
		View.loaded = 0;
		View.ready = false;
		new Request("https://manager.ziosproject.com/@views/",View.Sort);
	}
	static Sort(request){
		var views = request.responseText.split("|");
		View.total = views.length;
		for(var path of views){
			new Request(path,View.Parse);
		}
	}
	static Parse(request){
		var viewText = request.responseText;
		var view = null;
		for(var line in viewText.split("\n")){
			line = line.toLowerCase();
			if(line.contains("[","]")){
				var name = line.remove("[","]");
				view = new View(name);
			}
			if(line.includes("addhtml")){
				var data = line.split(" ");
				var template = data[1];
				var insertPoint = data.length > 2 ? data[2] : "";
				view.htmlTerms[template] = insertPoint;
			}
			if(line.includes("query")){
				var data = line.split(" ");
				view.queryTerms.push(data[1]);
			}
		}
		View.loaded += 1;
		View.ready = View.loaded == View.total;
	}
	static Handle(request){
		while(!View.ready){console.log("waiting!");}
		Data.Parse(request.responseText);
	}
}
class DataZson{
	static Setup(){
		DataZson.open = "[";
		DataZson.close = "]";
		DataZson.assignment = "=";
		DataZson.separator = "|";
		DataZson.escapeBlock = "`";
		DataZson.listSeparator = ",";
	}
}
class DataJson{
	static Setup(){
		DataJson.open = "{";
		DataJson.close = "}";
		DataJson.assignment = ":";
		DataJson.separator = ",";
		DataJson.escapeBlock = '"';
		DataJson.listSeparator = ",";
	}
}
class Data{
	static Setup(symbols){
		Data.groupNodes = false;
		Data.groupFields = false;
		Data.parseTypes = false;
		symbols.Setup();
		for(var name in symbols){
			Data[name] = symbols[name];
		}
	}
	static Parse(data){
		var buffer = "";
		var current = new Scope();
		var base = null;
		var fieldName = null
		data = data.trim();
		data = data.replaceAll(Data.close+Data.separator,Data.close);
		data = data.replaceAll(Data.assignment+Data.open,Data.open);
		if(data.startsWith(Data.open)){data = data.substring(1);}
		if(data.endsWith(Data.close)){data = data.substring(0,data.length-1);}
		var end = false;
		var escapeOpen = false;
		for(var index=0;index<data.length;++index){
			var letter = data[index];
			var end = (index+1)==data.length;
			if(letter == Data.escapeBlock){
				escapeOpen = !escapeOpen;
				continue;
			}
			if(escapeOpen){
				buffer += letter;
				continue;
			}
			if(fieldName != null && (end || (letter.matchesAny(Data.separator,Data.close)))){
				if(end){buffer += letter;}
				var value = buffer;
				if(Data.parseTypes){
					if(value != "" && !isNaN(value)){
						value = value.includes(".") && parseFloat(value) || parseInt(value);
					}
					else{
						var lower = value.toLowerCase();
						if(lower.matchesAny("true","false")){
							value = value == "true" && true || false;
						}
					}
				}
				current[fieldName] = value;
				if(Data.groupFields){current.fields[fieldName] = value;}
				fieldName = null;
			}
			if(letter == Data.open){
				var scope = new Scope(buffer,null,current)
				current[buffer] = scope;
				if(Data.groupNodes){current.nodes.push(scope);}
				current = scope;
				if(base == null){base = scope;}
			}
			else if(letter == Data.assignment){fieldName = buffer;}
			else if(letter == Data.close){current = current.parent;}
			if(letter.matchesAny(Data.open,Data.close,Data.separator,Data.assignment)){
				buffer = "";
				continue;
			}
			buffer += letter;
		}
		return base;
	}
	static ParseAlt(data){
		var buffer = "";
		var current = new Scope();
		var base = null;
		var fieldName = null
		data = data.trim();
		data = data.replaceAll(Data.close+Data.separator,Data.close);
		data = data.replaceAll(Data.assignment+Data.open,Data.open);
		if(data.startsWith(Data.open)){data = data.substring(1);}
		if(data.endsWith(Data.close)){data = data.substring(0,data.length-1);}
		var currentIndex = 0;
		var next = {
			"open" : 0,
			"close" : 0,
			"assignment" : 0,
			"separator" : 0,
			"escapeBlock" : 0
		};
		for(var term in next){
			next[term] = data.indexOf(Data[term],currentIndex);
			if(next[term] == -1){delete next[term];}
		}
		var end = false;
		var escapeOpen = false;
		var operations = 0;
		while(!end){
			operations += 1;
			var endIndex = Infinity;
			for(var term in next){
				if(escapeOpen && term != "escapeBlock"){continue;}
				if(next[term] < endIndex){
					endIndex = next[term];
					name = term;
				}
			}
			if(endIndex == Infinity){
				end = true;
				endIndex = data.length-1;
			}
			else{
				next[name] = data.indexOf(Data[name],endIndex+1);
				if(next[name] == -1){delete next[name];}
			}
			var buffer = data.substring(currentIndex,endIndex);
			currentIndex = endIndex+1;
			if(name == "escape"){
				escapeOpen = !escapeOpen;
			}
			else if(fieldName != null && (end || name.matchesAny("separator","close"))){
				var value = buffer;
				if(Data.parseTypes){
					if(value != "" && !isNaN(value)){
						value = value.includes(".") && parseFloat(value) || parseInt(value);
					}
					else{
						var lower = value.toLowerCase();
						if(lower.matchesAny("true","false")){
							value = value == "true" && true || false;
						}
					}
				}
				current[fieldName] = value;
				if(Data.groupFields){current.fields[fieldName] = value;}
				fieldName = null;
			}
			else if(name == "open"){
				var scope = new Scope(buffer,null,current)
				current[buffer] = scope;
				if(Data.groupNodes){current.nodes.push(scope);}
				current = scope;
				if(base == null){base = scope;}
			}
			else if(name == "assignment"){fieldName = buffer;}
			if(name == "close"){current = current.parent;}
		}
		return base;
	}
	static Bench(data){
		var Run = function(label,field,symbols,method,text,count=1){
			Data.Setup(symbols);
			var startTime = window.performance.now();
			for(var current=0;current<count;++current){
				Data[field] = method(text);
			}
			var endTime = window.performance.now()-startTime;
			console.log(label + " : " + endTime + " ms for " + (text.length*1*count/1024/1024) + " MB of data");
			return endTime;
		};
		var jsonText = '{"services":{"redis":{"image":"alpine","deploy":{"replicas":"6"},"config":{"parallelism":"2","delay":"10s"}},"restart_policy":{"condition":"on-failure"}}}';
		var zsonText = "services[redis[image=alpine|deploy[replicas=6]config[parallelism=2|delay=10s]]restart_policy[condition=true";
		var zsonEscaped = "`services`[`redis`[`image`=`alpine`|`deploy`[`replicas`=`6`]`config`[`parallelism`=`2`|`delay`=`10s`]]`restart_policy`[`condition`=true";
		console.log("-------------------");
		var timeA = Run("JSON       ","A",DataJson,JSON.parse,jsonText,100000);
		Run("Zios (ZSON)","B",DataZson,Data.Parse,zsonText,100000);
		Run("Zios (ZSON-Alt)","C",DataZson,Data.ParseAlt,zsonText,100000);
		Run("Zios (ZSON-Escaped)","D",DataZson,Data.Parse,zsonEscaped,100000);
		var timeB = Run("Zios (JSON)","E",DataJson,Data.Parse,jsonText,100000);
		console.log("-------------------");
		console.log("JSON is " + timeB/timeA + "x faster!");
		console.log("-------------------");
	}
}
class Scope{
	constructor(name="",value=null,parent=null){
		this.name = name;
		this.parent = parent;
		if(Data.groupNodes){this.nodes = [];}
		if(Data.groupFields){this.fields = {};}
	}
}
class Request{
	constructor(url,completeMethod=null,data=null,httpMethod="get"){
		console.log("[Request] " + url);
		this.request = new XMLHttpRequest();
		this.request.overrideMimeType("text/plain");
		this.request.open(this.method,url,true);
		if(httpMethod == "get"){this.request.send();}
		if(httpMethod == "post"){this.request.send(data);}
		this.request.onreadystatechange = function(){
			if(this.readyState == 4 && this.status == 200){
				if(completeMethod){
					completeMethod(this);
				}
			}
		};
	}
}
class Form{
	static Setup(){
		var forms = document.getElementsByTagName("form");
		for(var index=0;index<forms.length;++index){
			var form = forms[index];
			form.onsubmit = Form.Process;
		}
	}
	static Process(event){
		event.preventDefault();
		event.stopPropagation();
		var data = "";
		var httpMethod = this.method;
		if(httpMethod == "post"){
			var inputs = this.getElementsByTagName("input")
			data = ""
			for(var index=0;index<inputs.length;++index){
				var input = inputs[index];
				if(input.type == "submit"){continue;}
				data += input.name + "=" + input.value + "&";
			}
			data = data.substring(0,data.length-1);
		}
		new Request(this.action,View.Handle,data,httpMethod);
	}
}
var client = new ZiosClient();
window.onload = client.Setup;