class Request{
	constructor(url,completeMethod=null,data=null,httpMethod="get"){
		console.log("[Request] " + url);
		this.request = new XMLHttpRequest();
		this.request.overrideMimeType("text/plain");
		this.request.open(httpMethod,url,true);
		if(httpMethod == "get"){this.request.send();}
		if(httpMethod == "post"){this.request.send(data);}
		this.request.onreadystatechange = function(){
			if(this.readyState == 4 && this.status == 200){
				if(completeMethod){
					completeMethod(this);
				}
			}
		};
	}
}