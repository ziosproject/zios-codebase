class Extensions{
	constructor(){
		var types = [Array,String,NodeList,HTMLCollection];
		for(var x in types){
			var type = types[x];
			type.prototype.first = function(){return this[0];}
			type.prototype.last  = function(){return this[this.length-1];}
			type.prototype.contains = function(){
				for(var index=0;index<arguments.length;++index){
					var term = arguments[index];
					if(!this.includes(term)){return false;}
				}
				return true;
			}
			type.prototype.containsAny = function(){
				for(var index=0;index<arguments.length;++index){
					var term = arguments[index];
					if(this.includes(term)){return true;}
				}
				return false;
			}
			type.prototype.matchesAny = function(){
				for(var index=0;index<arguments.length;++index){
					var term = arguments[index];
					if(this == term){return true;}
				}
				return false;
			}
		}
		String.prototype.remove = function(){
			for(var index=0;index<arguments.length;++index){
				var term = arguments[index];
				this.replace(term,"");
			}
		}
		String.prototype.replaceAll = function(search,replacement){
			return this.split(search).join(replacement);
		};
	}
}