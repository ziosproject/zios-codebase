class DataJson{
	static Setup(){
		if(!DataJson.hasOwnProperty("open")){
			DataJson.open = "{";
			DataJson.close = "}";
			DataJson.assignment = ":";
			DataJson.separator = ",";
			DataJson.escapeBlock = '"';
			//DataJson.listSeparator = ",";
			DataJson.requireSeparator = true;
			DataJson.requireRoot = true;
		}
	}
}