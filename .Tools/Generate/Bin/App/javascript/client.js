class Client{
	constructor(){
		Data.Setup(DataZson);
		View.Setup();
		new Extensions();
	}
	Setup(){
		Form.Setup();
	}
}
var client = new Client();
window.onload = client.Setup;