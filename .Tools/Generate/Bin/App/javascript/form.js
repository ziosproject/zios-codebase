class Form{
	static Setup(){
		var forms = document.getElementsByTagName("form");
		for(var index=0;index<forms.length;++index){
			var form = forms[index];
			form.onsubmit = Form.Process;
		}
	}
	static Process(event){
		event.preventDefault();
		event.stopPropagation();
		var data = "";
		var httpMethod = this.method;
		if(httpMethod == "post"){
			var inputs = this.getElementsByTagName("input")
			data = ""
			for(var index=0;index<inputs.length;++index){
				var input = inputs[index];
				if(input.type == "submit"){continue;}
				data += input.name + "=" + input.value + "&";
			}
			data = data.substring(0,data.length-1);
		}
		new Request(this.action,View.Handle,data,httpMethod);
	}
}