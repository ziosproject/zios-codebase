class View{
	constructor(name){
		this.htmlTerms = [];
		this.queryTerms = [];
		View.all[name] = this;
	}
	static Setup(){
		View.all = [];
		View.total = 0;
		View.loaded = 0;
		View.ready = false;
		new Request("/@views/",View.Sort);
	}
	static Sort(request){
		var views = request.responseText.split("|");
		View.total = views.length;
		for(var path of views){
			new Request(path,View.Parse);
		}
	}
	static Parse(request){
		var viewText = request.responseText;
		var view = null;
		for(var line in viewText.split("\n")){
			line = line.toLowerCase();
			if(line.contains("[","]")){
				var name = line.remove("[","]");
				view = new View(name);
			}
			if(line.includes("addhtml")){
				var data = line.split(" ");
				var template = data[1];
				var insertPoint = data.length > 2 ? data[2] : "";
				view.htmlTerms[template] = insertPoint;
			}
			if(line.includes("query")){
				var data = line.split(" ");
				view.queryTerms.push(data[1]);
			}
		}
		View.loaded += 1;
		View.ready = View.loaded == View.total;
	}
	static Handle(request){
		while(!View.ready){console.log("waiting!");}
		Data.Parse(request.responseText);
	}
}