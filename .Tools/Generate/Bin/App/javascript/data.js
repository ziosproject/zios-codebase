class Data{
	static Setup(symbols){
		Data.groupNodes = false;
		Data.groupFields = false;
		Data.parseTypes = false;
		Data.autoEscape = false;
		Data.requireSeparator = false;
		Data.requireRoot = false;
		symbols.Setup();
		for(var name in symbols){
			Data[name] = symbols[name];
		}
	}
	static Parse(data){
		var buffer = "";
		var current = new Scope();
		var base = null;
		var fieldName = null
		if(Data.requireSeparator){
			data = data.trim();
			data = data.replaceAll(Data.close+Data.separator,Data.close);
			data = data.replaceAll(Data.assignment+Data.open,Data.open);
		}
		if(Data.requireRoot){
			if(data.startsWith(Data.open)){data = data.substring(1);}
			if(data.endsWith(Data.close)){data = data.substring(0,data.length-1);}
		}
		var end = false;
		var escapeOpen = false;
		var symbols = [Data.separator,Data.close,Data.open,Data.assignment];
		for(var index=0;index<data.length;++index){
			var letter = data[index];
			if(letter == Data.escapeBlock){
				escapeOpen = !escapeOpen;
				continue;
			}
			if(escapeOpen || !symbols.includes(letter)){
				buffer += letter;
				continue;
			}
			var end = (index+1)==data.length;
			if(fieldName != null && (end || (letter == Data.separator || letter == Data.close))){
				if(end){buffer += letter;}
				var value = buffer;
				if(Data.parseTypes){
					if(value != "" && !isNaN(value)){
						value = value.includes(".") && parseFloat(value) || parseInt(value);
					}
					else{
						var lower = value.toLowerCase();
						if(lower == true || lower == false){
							value = value == "true" && true || false;
						}
					}
				}
				current[fieldName] = value;
				if(Data.groupFields){current.fields[fieldName] = value;}
				fieldName = null;
			}
			if(letter == Data.open){
				var scope = new Scope(buffer,null,current)
				current[buffer] = scope;
				if(Data.groupNodes){current.nodes.push(scope);}
				current = scope;
				if(base == null){base = scope;}
			}
			else if(letter == Data.assignment){fieldName = buffer;}
			else if(letter == Data.close){current = current.parent;}
			buffer = "";
		}
		return base;
	}
}
class Scope{
	constructor(name="",value=null,parent=null){
		this.name = name;
		this.parent = parent;
		if(Data.groupNodes){this.nodes = [];}
		if(Data.groupFields){this.fields = {};}
	}
}