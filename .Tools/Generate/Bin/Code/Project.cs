using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using Zios.Extensions;
using Zios.Reflection;
namespace Zios.Generate{
	public class Project : Data{
		public static List<Project> all = new List<Project>();
		public string uuid = Guid.NewGuid().ToString();
		public string folder;
		public string defines;
		public string dllPath;
		public string namespacePath;
		public string namespaceName;
		public List<Source> allSourceReferences = new List<Source>();
		public List<Project> allProjectReferences = new List<Project>();
		public List<Library> allLibraryReferences = new List<Library>();
		public List<Source> sourceReferences = new List<Source>();
		public List<Library> libraryReferences = new List<Library>();
		public List<Project> projectReferences = new List<Project>();
		public List<string> referencePaths = new List<string>();
		public List<string> userReferencePaths = new List<string>();
		public bool standalone;
		public bool alreadySaved;
		public bool alreadyCompiled;
		public bool alreadyZipped;
		public bool alreadySorted;
		public static Project Find(string namespaceName,string namespacePath="",bool standalone=false){
			foreach(var project in Project.all){
				if(project.standalone != standalone){continue;}
				if(project.namespaceName == namespaceName){return project;}
				//if(project.namespacePath == namespacePath){return project;}
			}
			return null;
		}
		public static Project Create(string namespaceName,string namespacePath){
			return Project.Find(namespaceName,namespacePath) ?? new Project(namespaceName,namespacePath);
		}
		//=========================
		// SUPPORT
		//=========================
		public Project(string namespaceName="",string namespacePath="",bool standalone=false){
			this.namespacePath = namespacePath;
			this.namespaceName = namespaceName;
			this.standalone = standalone;
			Project.all.Add(this);
		}
		public bool IsTarget(){
			var target = Settings.targetNamespace.ToLower();
			var namespaceName = this.namespaceName.ToLower();
			if(target.Contains("*")){
				if(target.EndsWith("*") && namespaceName.StartsWith(target.Trim('*'))){return true;}
				if(target.StartsWith("*") && namespaceName.EndsWith(target.Trim('*'))){return true;}
			}
			return target == namespaceName;
		}
		public void BuildReferences<Type>(string targetName,string defaultName) where Type : Data{
			var target = this.GetVariable<List<Type>>(targetName);
			if(target.Count < 1){
				var defaultValues = this.GetVariable<List<Type>>(defaultName);
				target.AddRange(defaultValues);
				foreach(var project in this.projectReferences){
					project.BuildReferences<Type>(targetName,defaultName);
					foreach(var reference in project.GetVariable<List<Type>>(targetName)){
						if(!target.Contains(reference)){
							target.Add(reference);
						}
					}
				}
				var organized = target.OrderBy(x=>x.path).ToList();
				this.SetVariable(targetName,organized);
			}
		}
		public void BuildIncludes(){
			var references = this.projectReferences.Select(x=>x.namespaceName).ToList();
			var extras = Source.CheckAll(references);
			foreach(var include in extras){
				var match = Project.Find(include.namespaceName);
				if(match != null && match != this){
					this.projectReferences.Add(match);
				}
			}
		}
		public void Build(){
			this.libraryReferences.Add(Library.Find("mscorlib"));
			this.libraryReferences.Add(Library.Find("System.Core"));
			foreach(var reference in this.referencePaths){
				var isInternal = reference.StartsWith("UnityE") || reference.StartsWith("Microsoft") || reference.StartsWith("System");
				var project = Project.Find(reference);
				if(!isInternal){
					this.userReferencePaths.Add(reference);
				}
				if(project != null && !this.projectReferences.Contains(project) && project != this){
					this.projectReferences.Add(project);
				}
				else if(project == null){
					var library = Library.Find(reference) ?? Library.Find(reference,true);
					if(library != null && !this.libraryReferences.Contains(library)){
						this.libraryReferences.Add(library);
					}
					else if(library == null && !isInternal){
						//Warn("["+this.namespaceName+"] Ignoring missing reference for -- " + reference);
					}
				}
			}
			this.projectReferences = this.projectReferences.OrderBy(x => x.namespaceName).ToList();
			this.libraryReferences = this.libraryReferences.OrderBy(x => x.path).ToList();
			this.userReferencePaths = this.userReferencePaths.OrderBy(_p_3 => _p_3).ToList();
		}
		//=========================
		// SAVING
		//=========================
		public bool CheckUnity(List<Project> references){
			foreach(var reference in references){
				if(reference.namespaceName.Contains(".Unity.")){
					//Warn("["+Project.namespaceName+"] is using Unity references, but isn't in a Unity namespace.");
					return true;
				}
				if(this.CheckUnity(reference.projectReferences)){
					return true;
				}
			}
			return false;
		}
		public void Save(){
			var reference = "";
			//var standaloneLabel = this.standalone && "standalone" || "";
			//Console.WriteLine(Utility.ShowIndex() + "[" + Color.Wrap("gray",this.namespaceName) + "] : Saving " + standaloneLabel + ".csproj");
			var folderName = this.standalone ? "@" + this.namespaceName : this.namespaceName;
			var folderPath = this.folder = Settings.releasePath + folderName + "/";
			var sourcePath = folderPath + "Source/";
			var binaryPath = folderPath + "Binary/";
			this.path = folderPath + this.namespaceName + ".csproj";
			Utility.CreateFolder(folderPath);
			Utility.CreateFolder(sourcePath);
			var libraryPath = "";
			if(this.libraryReferences.Count > 0){
				libraryPath = folderPath + "Library/";
				Utility.CreateFolder(libraryPath);
			}
			var isUnity = this.namespaceName.Contains(".Unity.") || this.CheckUnity(this.projectReferences);
			var isEditor = this.namespaceName.Contains(".Editor.");
			var constants = Settings.userFlags;
			var output = CsProj.template;
			output = output.Replace("$uuid",this.uuid);
			output = output.Replace("$assemblyName",this.namespaceName);
			output = output.Replace("$frameworkVersion",Settings.frameworkVersion);
			output = output.Replace("$outputPath",binaryPath.Replace(folderPath,"./"));
			var csProjSources = "";
			var csProjProjects = "";
			var csProjLibraries = "";
			var csProjItemGroups = "";
			var csProjPropertyGroups = "";
			if(isUnity){
				constants += CsProj.unityVersionFlags + CsProj.unityFlags;
				var unityProjectType = isEditor ? "Editor:5" : "Game:1";
				var unityPropertyGroup = CsProj.unityPropertyGroup.Replace("$unityVersion",Settings.unityVersion);
				unityPropertyGroup = unityPropertyGroup.Replace("$unityProjectType",unityProjectType);
				csProjPropertyGroups += unityPropertyGroup;
				constants += isEditor ? CsProj.unityEditorFlags : CsProj.unityBuildFlags;
			}
			this.defines = constants;
			output = output.Replace("$constants",constants);
			if(!this.standalone){
				this.alreadySaved = true;
				foreach(var project in this.projectReferences){
					project.Save();
				}
			}
			if(this.standalone){
				this.BuildReferences<Source>("allSourceReferences","sourceReferences");
				csProjSources += this.CopySource(this.allSourceReferences,sourcePath);
			}
			else{
				this.BuildReferences<Project>("allProjectReferences","projectReferences");
				csProjSources += this.CopySource(this.sourceReferences,sourcePath);
				foreach(var project in this.projectReferences){
					var projectPath = "../" + project.namespaceName + "/" + project.namespaceName + ".csproj";
					reference = CsProj.projectReference.Replace("$projectPath",projectPath);
					reference = reference.Replace("$projectUUID",project.uuid);
					reference = reference.Replace("$projectName",project.namespaceName);
					csProjProjects += reference;
				}
			}
			this.BuildReferences<Library>("allLibraryReferences","libraryReferences");
			foreach(var library in this.allLibraryReferences){
				var fileName = library.path.Split("/").SkipLast();
				Utility.Copy(library.path,libraryPath + fileName,Settings.symLinkLibraries);
				reference = CsProj.libraryReference.Replace("$libraryPath","./Library/" + fileName);
				reference = reference.Replace("$libraryName",library.name);
				csProjLibraries += reference;
			}
			if(csProjLibraries.IsEmpty()){csProjItemGroups += "\n\t<ItemGroup>\n" + csProjLibraries + "\t</ItemGroup>";}
			if(csProjProjects.IsEmpty()){csProjItemGroups += "\n\t<ItemGroup>\n" + csProjProjects + "\t</ItemGroup>";}
			if(csProjSources.IsEmpty()){csProjItemGroups += "\n\t<ItemGroup>\n" + csProjSources + "\t</ItemGroup>";}
			output = output.Replace("$additionalPropertyGroups",csProjPropertyGroups);
			output = output.Replace("$additionalItemGroups",csProjItemGroups);
			File.WriteAllText(this.path,output);
		}
		public string CopySource(List<Source> targets,string path){
			var csProjSources = "";
			foreach (var source in targets){
				var sourcePath = source.path;
				var fileName = sourcePath.Split("/").SkipLast();
				var folder = Utility.GetFolder(path);
				var destination = path + fileName;
				if(Settings.preserveStructure){
					destination = Utility.CopyPath(sourcePath,path,Settings.symLinkSource);
				}
				else{
					Utility.Copy(sourcePath,destination,Settings.symLinkSource);
				}
				destination = destination.Replace(folder,"./");
				csProjSources += CsProj.sourceReference.Replace("$sourcePath",destination);
			}
			return csProjSources;
		}
		//=========================
		// COMPILE
		//=========================
		public void CompileProcess(){
			var binaryPath = this.folder + "Binary/";
			Utility.CreateFolder(binaryPath);
			this.dllPath = binaryPath + this.namespaceName + ".dll";
			this.alreadyCompiled = true;
			//Console.WriteLine(Utility.ShowIndex() + "[" + Color.Wrap("gray",this.namespaceName) + "] : Compiling " + standaloneTerm + ".dll");
			var defines = !this.defines.IsEmpty() ? Compile.defines.Replace("$defines",this.defines) : "";
			var command = Compile.template.Replace("$outputName",this.dllPath.Remove(".dll"));
			command = command.Replace("$defines",defines);
			var sources = "";
			var sourceReferences = this.standalone ? this.allSourceReferences : this.sourceReferences;
			var libraryReferences = this.standalone ? this.allLibraryReferences : this.libraryReferences;
			foreach(var source in sourceReferences){
				var sourcePath = source.path;
				sources += Compile.sourceReference.Replace("$path",sourcePath.Replace("/","\\"));
			}
			command = command.Replace("$sourceFiles",sources.Trim());
			var references = libraryReferences.Select(x=>x.path).ToList();
			foreach(var project in this.projectReferences){
				project.CompileProcess();
			}
			//Utility.ThreadWait<Project>(this.projectReferences,"compileThread");
			if(!this.standalone){
				foreach(var project in this.projectReferences){
					references.Add(project.dllPath);
				}
			}
			var dllReferences = "";
			foreach(var reference in references){
				while(!File.Open(reference,FileMode.Open).CanRead){
					Thread.Sleep(50);
				}
				dllReferences += Compile.dllReference.Replace("$path",reference.Replace("/","\\"));
			}
			command = command.Replace("$dllReferences",dllReferences.Trim()).Trim();
			Utility.System(command);
		}
		//=========================
		// ZIPPING
		//=========================
		public void Zip(){
			var file = this.GetZipFile(this.namespaceName,this.standalone);
			var zip = new ZipArchive(file,ZipArchiveMode.Create);
			var zipPath = Settings.releasePath + file.Name;	
			//Console.WriteLine(Utility.ShowIndex() + "[" + Color.Wrap("gray",this.namespaceName) + "]" + " : Creating " + zip.filename);
			Settings.keep.Add(zipPath);
			var references = new List<Project>{this};
			if(this.standalone){references.AddRange(this.allProjectReferences);}
			foreach(var project in references){
				foreach(var filePath in Utility.GetFiles(project.folder,new List<string>())){
					if(Settings.zipIgnoreLibrary && filePath.Contains("/Library/")){
						continue;
					}
					var archivePath = Utility.Repath(filePath.Remove("../"));
					zip.CreateEntryFromFile(filePath,archivePath);
				}
			}
			if(!this.standalone){
				var solutionPath = Utility.Repath(Settings.releasePath + this.namespaceName + ".sln");
				zip.CreateEntryFromFile(solutionPath,solutionPath);
			}
			Settings.openPath = "/select," + Utility.GetShellPath(zipPath);
		}
		public FileStream GetZipFile(string name,bool standalone){
			name = "["+name+"]";
			name += Settings.generatedInfo + ".";
			name += standalone ? "Standalone" : "Shared";
			name += ".zip";
			return new FileStream(name,FileMode.Create);
		}
		//=========================
		// ASMDEF
		//=========================
		public void SaveAsmdef(){
			var unityPath = this.folder + "Unity/";
			Utility.CreateFolder(unityPath);
			var references = "";
			if(this.userReferencePaths.Count > 0){
				references = this.userReferencePaths.Join("\n\t\t\"" + "\",\n\t\t\"") + "\n\t";
			}
			var asm = Asmdef.template.Replace("$asmdef",this.namespaceName);
			asm = asm.Replace("$references",references);
			if(this.namespaceName.Contains(".Editor.")){
				asm = asm.Replace("$editorOnly","\"" + "Editor" + "\"");
			}
			asm = asm.Remove("$editorOnly");
			var asmPath = this.folder + "Unity/" + this.namespaceName + ".asmdef";
			//Console.WriteLine(Utility.ShowIndex() + "[" + Color.Wrap("gray",this.namespaceName) + "]" + " : Saving asmdef.");
			File.WriteAllText(asmPath,asm);
		}
		//=========================
		// PACKAGE
		//=========================
		public void SavePackage(){
			var unityPath = this.folder + "Unity/";
			Utility.CreateFolder(unityPath);
			var dependencies = "";
			var version = Settings.unityVersionClean + ".r" + Settings.svnRevision;
			if(this.userReferencePaths.Count > 0){
				dependencies = "\n";
				foreach(var path in this.userReferencePaths){
					var dependency = Package.dependency.Replace("$packageName",path);
					dependency = dependency.Replace("$packageVersion",version);
					dependencies += dependency;
				}
				dependencies += "\t";
			}
			var package = Package.template.Replace("$description","This is a module for " + this.namespaceName + ".");
			package = package.Replace("$displayName",this.namespaceName.Replace("."," ").ToTitleCase());
			package = package.Replace("$name",this.namespaceName);
			package = package.Replace("$unityVersion",Settings.unityVersionClean);
			package = package.Replace("$category",Settings.packageCategory);
			package = package.Replace("$version",version);
			package = package.Replace("$dependencies",dependencies);
			var packagePath = this.folder + "Unity/" + this.namespaceName + ".json";
			//Console.WriteLine(Utility.ShowIndex() + "[" + Color.Wrap("gray",this.namespaceName) + "]" + " : Saving package.");
			File.WriteAllText(packagePath,package);
		}
	}
}