using System.IO;
namespace Zios.Generate{
	public static class Template{
		public static string path = "./Templates/";
	}
	public static class Meta{
		public static string template = File.ReadAllText(Template.path+"Meta.template");
	}
	public static class Asmdef{
		public static string template = File.ReadAllText(Template.path+"Asmdef.template");
		public static string dependency = "\t\t\"$packageName\": \"$packageVersion\",\n";
	}
	public static class Package{
		public static string template = File.ReadAllText(Template.path+"Package.template");
		public static string dependency = "\t\t\"$packageName\": \"$packageVersion\",\n";
	}
	public static class Sln{
		public static string template = File.ReadAllText(Template.path+"Sln.template");
		public static string projectReference = "Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = \"$projectName\", \"$projectPath\", \"{$projectGUID}\" EndProject\n";
		public static string buildOption = "\t\t{$projectGUID}.Release|Any CPU.Build.0 = Release|Any CPU\n";
	}
	public static class Compile{
		public static string compilerPath = Utility.GetPath() + "Compiler";
		public static string template = File.ReadAllText(Template.path + "Build.template").Replace("$compilerPath", compilerPath);
		public static string defines = "/define:\"$defines\"";
		public static string sourceReference = "\"$path\" ";
		public static string dllReference = "/reference:\"$path\" ";
	}
	public static class CsProj{
		public static string template;
		public static string projectReference;
		public static string sourceReference;
		public static string libraryReference;
		public static string unityPropertyGroup;
		public static string unityVersionFlags;
		public static string unityEditorFlags;
		public static string unityBuildFlags;
		public static string unityFlags;
		static CsProj(){
			CsProj.template            = File.ReadAllText(Template.path+"Csproj.template");
			CsProj.projectReference    = "\t\t<ProjectReference Include=\"$projectPath\">\n";
			CsProj.projectReference   += "\t\t\t<Project>{$projectUUID}</Project>\n";
			CsProj.projectReference   += "\t\t\t<Name>$projectName</Name>\n";
			CsProj.projectReference   += "\t\t</ProjectReference>\n";
			CsProj.sourceReference     = "\t\t<Compile Include=\"$sourcePath\" />\n";
			CsProj.libraryReference    = "\t\t<Reference Include=\"$libraryName\">\n";
			CsProj.libraryReference   += "\t\t\t<HintPath>$libraryPath</HintPath>\n";
			CsProj.libraryReference   += "\t\t\t<Private>False</Private>\n";
			CsProj.libraryReference   += "\t\t</Reference>\n";
			CsProj.unityPropertyGroup  = "\t\t<ProjectTypeGuids>{E097FAD1-6243-4DAD-9C02-E9B9EFC3FFC1};{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}</ProjectTypeGuids>\n";
			CsProj.unityPropertyGroup += "\t\t<UnityProjectGenerator>Unity/VSTU</UnityProjectGenerator>\n";
			CsProj.unityPropertyGroup += "\t\t<UnityProjectType>$unityProjectType</UnityProjectType>\n";
			CsProj.unityPropertyGroup += "\t\t<UnityBuildTarget>StandaloneWindows64:19</UnityBuildTarget>\n";
			CsProj.unityPropertyGroup += "\t\t<UnityVersion>$unityVersion</UnityVersion>\n";
			CsProj.unityPropertyGroup += "\t</PropertyGroup>";
			CsProj.unityPropertyGroup += "\n\t<PropertyGroup>\n";
			CsProj.unityEditorFlags    = "UNITY_EDITOR;UNITY_EDITOR_64;UNITY_EDITOR_XXX;";
			CsProj.unityBuildFlags     = "UNITY_STANDALONE;PLATFORM_STANDALONE;PLATFORM_STANDALONE_XXX;UNITY_STANDALONE_XXX;";
			CsProj.unityFlags          = "DEBUG;TRACE;PLATFORM_ARCH_64;UNITY_64;UNITY_ANALYTICS;ENABLE_AUDIO;ENABLE_CACHING;ENABLE_CLOTH;ENABLE_DUCK_TYPING;ENABLE_MICROPHONE;ENABLE_MULTIPLE_DISPLAYS;ENABLE_PHYSICS;ENABLE_SPRITES;ENABLE_GRID;ENABLE_TILEMAP;ENABLE_TERRAIN;ENABLE_TEXTURE_STREAMING;ENABLE_DIRECTOR;ENABLE_UNET;ENABLE_LZMA;ENABLE_UNITYEVENTS;ENABLE_WEBCAM;ENABLE_WWW;ENABLE_CLOUD_SERVICES_COLLAB;ENABLE_CLOUD_SERVICES_COLLAB_SOFTLOCKS;ENABLE_CLOUD_SERVICES_ADS;ENABLE_CLOUD_HUB;ENABLE_CLOUD_PROJECT_ID;ENABLE_CLOUD_SERVICES_USE_WEBREQUEST;ENABLE_CLOUD_SERVICES_UNET;ENABLE_CLOUD_SERVICES_BUILD;ENABLE_CLOUD_LICENSE;ENABLE_EDITOR_HUB;ENABLE_EDITOR_HUB_LICENSE;ENABLE_WEBSOCKET_CLIENT;ENABLE_DIRECTOR_AUDIO;ENABLE_DIRECTOR_TEXTURE;ENABLE_TIMELINE;ENABLE_EDITOR_METRICS;ENABLE_EDITOR_METRICS_CACHING;ENABLE_MANAGED_JOBS;ENABLE_MANAGED_TRANSFORM_JOBS;ENABLE_MANAGED_ANIMATION_JOBS;INCLUDE_DYNAMIC_GI;INCLUDE_GI;ENABLE_MONO_BDWGC;PLATFORM_SUPPORTS_MONO;RENDER_SOFTWARE_CURSOR;INCLUDE_PUBNUB;ENABLE_VIDEO;ENABLE_PACKMAN;ENABLE_CUSTOM_RENDER_TEXTURE;ENABLE_LOCALIZATION;ENABLE_SUBSTANCE;ENABLE_RUNTIME_GI;ENABLE_MOVIES;ENABLE_NETWORK;ENABLE_CRUNCH_TEXTURE_COMPRESSION;ENABLE_UNITYWEBREQUEST;ENABLE_CLOUD_SERVICES;ENABLE_CLOUD_SERVICES_ANALYTICS;ENABLE_CLOUD_SERVICES_PURCHASING;ENABLE_CLOUD_SERVICES_CRASH_REPORTING;ENABLE_OUT_OF_PROCESS_CRASH_HANDLER;ENABLE_EVENT_QUEUE;ENABLE_CLUSTER_SYNC;ENABLE_CLUSTERINPUT;ENABLE_VR;ENABLE_AR;ENABLE_WEBSOCKET_HOST;ENABLE_MONO;NET_2_0_SUBSET;ENABLE_PROFILER;UNITY_ASSERTIONS;ENABLE_UNITY_COLLECTIONS_CHECKS;ENABLE_BURST_AOT;UNITY_TEAM_LICENSE;ENABLE_VSTU;";
		}
	}
}