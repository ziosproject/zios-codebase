using System;
using System.Collections.Generic;
namespace Zios.Generate{
	public class Source : Data{
		public static List<Source> all = new List<Source>();
		public string namespaceName = "";
		public List<List<string>> includeIf = new List<List<string>>();
		public Source(string path=""){
			this.path = path;
			Source.all.Add(this);
		}
		public Source CheckInclude(List<string> references){
			foreach(var group in this.includeIf){
				var hasAll = true;
				foreach(var include in group){
					var hasItem = false;
					foreach(var reference in references){
						if(reference.Contains(include)){
							hasItem = true;
							break;
						}
					}
					if(!hasItem){
						hasAll = false;
						break;
					}
				}
				if(hasAll){
					return this;
				}
			}
			return null;
		}
		public static List<Source> CheckAll(List<string> references){
			var includes = new List<Source>();
			foreach(var source in Source.all){
				if(source.CheckInclude(references) != null){
					includes.Add(source);
				}
			}
			return includes;
		}
	}
}