using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
namespace Zios.Generate{
	public class Solution : Data{
		public static List<Solution> all = new List<Solution>{};
		public List<Project> projectReferences = new List<Project>();
		public Thread saveThread;
		public bool standalone;
		public string folder;
		public string name;
		public string uuid = Guid.NewGuid().ToString();
		public Solution(string folder="",string name="",List<Project> references=null,bool standalone=false){
			this.folder = folder;
			this.name = name;
			this.path = this.folder + this.name + ".sln";
			this.projectReferences = references;
			this.standalone = standalone;
			Solution.all.Add(this);
		}
		public void Save(){
			if(Settings.multiThreading){
				lock(this.saveThread){
					if(this.standalone || this.saveThread == null){
						this.saveThread = new Thread(this.SaveProcess);
						this.saveThread.Start();
					}
				}
				return;
			}
			this.SaveProcess();
		}
		public void SaveProcess(){
			var output = Sln.template.Replace("$solutionGUID",this.uuid);
			var projectReferences = "";
			var buildOptions = "";
			foreach(var project in this.projectReferences){
				var projectPath = "./" + project.namespaceName;
				if(!this.standalone){
					projectPath += "/" + project.namespaceName;
				}
				var reference = Sln.projectReference.Replace("$projectName",project.namespaceName);
				reference = reference.Replace("$projectGUID",project.uuid);
				reference = reference.Replace("$projectPath",projectPath+".csproj");
				buildOptions += Sln.buildOption.Replace("$projectGUID",project.uuid);
				projectReferences += reference;
			}
			output = output.Replace("$solutionProjectReferences",projectReferences.Trim());
			output = output.Replace("$solutionBuildOptions",buildOptions.Trim());
			Settings.openPath = "/select,"+Utility.GetShellPath(this.path);
			File.WriteAllText(this.path,output);
		}
		public void Compile(){
			foreach(var project in this.projectReferences){
				project.CompileProcess();
			}
		}
	}
}