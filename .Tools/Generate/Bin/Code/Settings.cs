using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Zios.Extensions;
using Zios.Extensions.Convert;
using Zios.Reflection;
namespace Zios.Generate{
	public class Settings{
		public static List<string> keep = new List<string>();
		public static bool hasStartPath = false;
		public static string startPath = Utility.GetPath() + "../../";
		public static string releasePath = Utility.GetPath() + "Release/";
		public static string libraryPath = Utility.GetPath() + "Library/";
		public static string unityPath = "";
		public static string unityVersionClean = "";
		public static string generatedInfo = "";
		public static string svnRevision = "";
		public static string targetNamespace = "";
		public static bool verboseLogging = true;
		public static bool multiThreading = false;
		public static bool preserveStructure = true;
		public static bool saveProjects = true;
		public static bool saveStandalone = true;
		public static bool saveLinked = true;
		public static bool compileProjects = true;
		public static bool savePackage = true;
		public static string packageCategory = "Zios";
		public static bool saveAsmdef = true;
		public static bool zipResults = true;
		public static bool zipIgnoreLibrary = true;
		public static string appendInfo = ".{unityPlatform}.{unityVersion}.r{svnRevision}";
		public static string frameworkVersion = "4.5";
		public static string unityVersion = "2018.2.2f1";
		public static string unityPlatform = "Windows";
		public static string userFlags = "";
		public static bool symLinkFiles = true;
		public static bool symLinkBinaries = true;
		public static bool symLinkLibraries = true;
		public static bool symLinkSource = true;
		public static bool openResults = true;
		public static string openPath = "";
		public static bool cleanupResults = false;
		static Settings(){
			Utility.PreviousFolder(true);
			Utility.CreateFolder("Release");
		}
	}
}