﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Zios.Extensions;
using Zios.Extensions.Convert;
using Zios.Reflection;
using Zios.Supports.Worker;
namespace Zios.Generate{
	public static class Generate{
		public static Stopwatch time = new Stopwatch();
		public static string initial = Utility.GetPath();
		public static List<Library> libraries = new List<Library>();
		public static List<Source> saved = new List<Source>();
		static Generate(){
			Generate.time.Start();
			//Generate.Cleanup();
			Generate.CheckSupport();
			Generate.SetInfo();
			Generate.SetUnityPath();
			Generate.SetUnityFlags();
			Generate.Scan(Settings.libraryPath);
			Generate.Scan(Settings.unityPath);
			Generate.Scan(Settings.startPath);
			Generate.Build();
			Generate.Save();
			Generate.SaveAsmdef();
			Generate.SavePackage();
			Generate.Compile();
			Generate.Zip();
			Generate.Cleanup();
			if(Settings.openResults){
				Utility.System(@"explorer "+Settings.openPath);
			}
			//Generate.time.ElapsedMilliseconds/1000f + " seconds elapsed.","darkgray");
		}
		public static void CheckSupport(){
			if(Settings.symLinkSource || Settings.symLinkLibraries){
				try{
					var path = Utility.GetPath();
					Utility.SymLink(path + "Generate.py", path + "Generate.test");
					Utility.Remove(path + "Generate.test");
					//Color.Success("Symlinking is enabled/allowed.");
				}
				catch{
					//Color.Fail("No permission to SymLink. Reverting to copying.");
					Settings.symLinkSource = false;
					Settings.symLinkLibraries = false;
				}
			}
		}
		public static void SetInfo(){
			Settings.generatedInfo = Settings.appendInfo.ToLower();
			foreach(var item in typeof(Settings).GetVariables()){
				var name = item.Key;
				var value = item.Value;
				Settings.generatedInfo = Settings.generatedInfo.Replace("{"+name.ToLower()+"}",value.ToString());
			}
		}
		public static string GetPlatformSuffix(){
			var platform = Settings.unityPlatform.ToLower();
			if(platform.Contains("win")){return "WIN";}
			if(platform.Contains("osx") || platform.Contains("mac")){return "OSX";}
			if(platform.Contains("linux")){return "LINUX";}
			return "WHAT";
		}
		public static void SetUnityFlags(){
			var namedVersion = Settings.unityVersionClean;
			var version = namedVersion.Count(x=>x=='.') > 1 ? namedVersion.RemoveLast(".").ToFloat() : namedVersion.ToFloat();
			var upperVersion = namedVersion.Replace(".", "_");
			var directives = "UNITY_" + upperVersion + ";";
			while(upperVersion.Count(x=>x=='_') > 0){
				upperVersion = upperVersion.Split("_").SkipLast().Join("_");
				directives += "UNITY_" + upperVersion + ";";
			}
			if(version >= 5.3){directives += "UNITY_5_3_OR_NEWER;";}
			if(version >= 5.34){directives += "UNITY_5_3_4_OR_NEWER;";}
			if(version >= 5.4){directives += "UNITY_5_4_OR_NEWER;";}
			if(version >= 5.5){directives += "UNITY_5_5_OR_NEWER;";}
			if(version >= 5.6){directives += "UNITY_5_6_OR_NEWER;";}
			if(version >= 2017){directives += "UNITY_2017_OR_NEWER;";}
			if(version >= 2017.1){directives += "UNITY_2017_1_OR_NEWER;";}
			if(version >= 2017.2){directives += "UNITY_2017_2_OR_NEWER;";}
			if(version >= 2017.3){directives += "UNITY_2017_3_OR_NEWER;";}
			if(version >= 2017.4){directives += "UNITY_2017_4_OR_NEWER;";}
			if(version >= 2018){directives += "UNITY_2018_OR_NEWER;";}
			if(version >= 2018.1){directives += "UNITY_2018_1_OR_NEWER;";}
			if(version >= 2018.2){directives += "UNITY_2018_2_OR_NEWER;";}
			if(version >= 2018.3){directives += "UNITY_2018_3_OR_NEWER;";}
			var platform = Generate.GetPlatformSuffix();
			CsProj.unityVersionFlags = directives;
			CsProj.unityEditorFlags = CsProj.unityEditorFlags.Replace("XXX",platform);
			CsProj.unityBuildFlags = CsProj.unityBuildFlags.Replace("XXX",platform);
		}
		public static void SetUnityPath(){
			var version = Settings.unityVersion;
			foreach(var suffix in new List<string>{"b","a","r","f","rc"}){
				var index = 0;
				while(index < 9){
					version = version.Remove(suffix +index);
					index += 1;
				}
			}
			Settings.unityVersionClean = version;
			Settings.unityPath = Utility.GetPath() + "Unity/" + version + "/";
		}
		//=======================
		// MAIN
		//=======================
		public static void Scan(string currentPath){
			foreach(var item in Directory.GetFiles(currentPath)){
				var path = currentPath + item;
				if(Directory.Exists(path) && !item.StartsWith(".")){
					Directory.SetCurrentDirectory(path);
					Generate.Scan(path + "/");
					Directory.SetCurrentDirectory(currentPath);
					continue;
				}
				var isCS = path.Contains(".cs") && !path.Contains(".cs.meta") && !path.Contains(".csproj");
				var isSO = path.Contains(".so") && !path.Contains(".so.meta");
				var isDLL = path.Contains(".dll") && !path.Contains(".dll.meta");
				if(isDLL || isSO){new Library(path);}
				if(isCS){
					var includes = new List<string>();
					Project active = null;
					var source = new Source(path);
					foreach(var current in File.ReadAllLines(item)){
						var line = current.Trim();
						if(line.StartsWith("//includeIf")){
							var @base = line.Split("//includeIf").Last().Trim();
							var sets = @base.Remove("(").Remove(")").Split("||");
							foreach(var element in sets){
								source.includeIf.Add(element.Split("&&").Select(x=>x.Trim()).ToList());
							}
						}
						if(line.StartsWith("namespace")){
							var namespaceName = line.Split("namespace").Last().Remove("{").Trim();
							source.namespaceName = namespaceName;
							active = Project.Create(namespaceName,currentPath);
							if(!active.sourceReferences.Select(x=>x.path).ToList().Contains(path)){
								active.sourceReferences.Add(source);
							}
						}
						var entry = "";
						if(line.StartsWith("//asm")){
							entry = line.Split("//asm").Last().Trim().Trim(";");
						}
						if(line.StartsWith("using") && !line.Contains("(")){
							entry = line.Split("using").Last().Trim().Trim(";");
							if(line.Contains("=")){
								entry = entry.Split("= ").Last().Split(".").SkipLast().Join(".");
							}
						}
						if(entry.IsEmpty()){
							includes.Add(entry);
						}
					}
					if(active != null){
						foreach(var include in includes){
							if(!active.referencePaths.Contains(include) && include != active.namespaceName){
								active.referencePaths.Add(include);
							}
						}
					}
				}
			}
		}
		public static void Build(){
			foreach(var project in Project.all.Copy()){
				if(Settings.saveStandalone && project.IsTarget()){
					var standalone = new Project(project.namespaceName,project.namespacePath,true);
					standalone.sourceReferences = project.sourceReferences.Copy();
					standalone.referencePaths = project.referencePaths.Copy();
					standalone.Build();
				}
				project.Build();
			}
			foreach(var project in Project.all.Copy()){
				project.BuildIncludes();
			}
		}
		public static void Save(){
			Solution solution;
			var targets = new List<Project>();
			Directory.SetCurrentDirectory(Settings.releasePath);
			var linkedTargets = Project.all.Where(x=>!x.standalone && x.IsTarget()).ToList();
			var standaloneTargets = Project.all.Where(x=>x.standalone).ToList();
			if(Settings.saveLinked){
				targets = linkedTargets;
				foreach(var project in targets){
					project.Save();
				}
			}
			if(Settings.saveStandalone){
				targets = standaloneTargets;
				foreach(var project in targets){
					project.Save();
				}
			}
			if(Settings.saveLinked){
				targets = linkedTargets;
				foreach(var project in targets){
					solution = new Solution(Settings.releasePath, project.namespaceName,project.allProjectReferences);
					solution.Save();
				}
			}
			if(Settings.saveStandalone){
				targets = standaloneTargets;
				var standalone = new List<Solution>();
				foreach(var project in targets){
					solution = new Solution(Settings.releasePath+"@"+project.namespaceName+"/",project.namespaceName,new List<Project>{project},true);
					solution.Save();
					standalone.Add(solution);
				}
			}
		}
		public static void SaveAsmdef(){
			if(Settings.saveAsmdef){
				var targets = Project.all.Where(x=>!x.folder.IsEmpty()).ToList();
				MethodStep<Project> step = x=>{
					x.SaveAsmdef();
					return true;
				};
				Worker.Create(targets).OnStep(step).Build();
			}
		}
		public static void SavePackage(){
			if(Settings.savePackage){
				var targets = Project.all.Where(x=>!x.folder.IsEmpty()).ToList();
				MethodStep<Project> step = x=>{
					x.SavePackage();
					return true;
				};
				Worker.Create(targets).OnStep(step).Build();
			}
		}
		public static void Compile(){
			if(Settings.compileProjects){
				var targets = Project.all.Where(x=>!x.folder.IsEmpty()).ToList();
				MethodStep<Project> step = x=>{
					x.CompileProcess();
					return true;
				};
				Worker.Create(targets).OnStep(step).Build();
			}
		}
		public static void Zip(){
			if(Settings.zipResults){
				var targets = Project.all.Where(x=>x.IsTarget()).ToList();
				MethodStep<Project> step = x=>{
					x.Zip();
					return true;
				};
				Worker.Create(targets).OnStep(step).Build();
			}
		}
		public static void Cleanup(bool force=false){
			if(force || Settings.cleanupResults){
				foreach(var item in Directory.GetFiles(Settings.releasePath)){
					var path = Settings.releasePath + item;
					var skip = false;
					foreach(var keep in Settings.keep){
						if(path.Contains(keep)){
							skip = true;
							break;
						}
					}
					if(skip){continue;}
					if(Directory.Exists(path)){
						Directory.Delete(path,true);
						continue;
					}
					File.Delete(path);
				}
			}
		}
	}
}