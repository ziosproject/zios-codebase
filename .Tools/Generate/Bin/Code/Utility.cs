using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Zios.Extensions;
namespace Zios.Generate{
	public class Utility{
		public static Dictionary<object,object> folderMap = new Dictionary<object,object>{};
		public static string System(string command,string arguments=""){
			var info = new ProcessStartInfo(command,arguments);
			info.UseShellExecute = false;
			var process = Process.Start(info);
			process.WaitForExit();
			return process.StandardOutput.ReadToEnd();
		}
		public static string GetPath(){
			return Directory.GetCurrentDirectory().FixPath() + "/";
		}
		public static object GetShellPath(string path=null){
			path = path ?? Utility.GetPath();
			path = path.Replace("/","\\");
			return "\"" + path + "\"";
		}
		public static List<string> GetFiles(string path,List<string> items){
			items = items ?? new List<string>();
			foreach(var current in Directory.GetFiles(path)){
				if(Directory.Exists(current)){
					items = Utility.GetFiles(current,items);
					continue;
				}
				items.Add(path);
			}
			return items;
		}
		public static void SymLink(string source,string destination){
			var platform = Environment.OSVersion.Platform;
			if(platform == PlatformID.Win32NT){
				Utility.System("mklink " + destination + " " + source);
			}
			else if(platform == PlatformID.Unix || platform == PlatformID.MacOSX){
				Utility.System("ln -s " + source + " " + destination);
			}
		}
		public static void CopyFolder(string source,string destination,bool asSymLink=false){
			Utility.CreateFolder(destination);
			foreach(var item in Directory.GetFiles(source)){
				var path = source + item;
				var goalPath = destination + item;
				if(Directory.Exists(path)){
					Utility.CopyFolder(path + "/",goalPath + "/",asSymLink);
					continue;
				}
				Utility.Copy(path,goalPath,asSymLink);
			}
		}
		public static void Copy(string source,string destination,bool asSymLink=false,bool overwrite=true){
			if(overwrite){Utility.Remove(destination);}
			if(asSymLink){
				Utility.SymLink(source,destination);
				return;
			}
			try{File.Copy(source,destination);}
			catch{}
		}
		public static string CopyPath(string source,string destination,bool asSymLink=false){
			var fileName = source.Split("/").Last();
			var targets = source.Remove(Settings.startPath).Split("/").SkipLast();
			var basePath = destination;
			destination = destination + targets.Join("/") + "/" + fileName;
			var path = basePath;
			foreach(var target in targets){
				path += target + "/";
				Utility.CreateFolder(path);
			}
			Utility.Copy(source,destination,asSymLink);
			return destination;
		}
		public static void Remove(string path){
			try{File.Delete(path);}
			catch{}
		}
		public static void CreateFolder(string path,bool change=false){
			try{Directory.CreateDirectory(path);}
			catch{}
			if(change){Directory.SetCurrentDirectory(path);}
		}
		public static string GetFolder(string path){
			return path.GetDirectory();
		}
		public static string GetFolderName(string path){
			return path.GetDirectory().Split("/").Last();
		}
		public static string PreviousFolder(bool change=false){
			var path = Utility.GetFolder(Utility.GetPath());
			if(change){Directory.SetCurrentDirectory(path);}
			return path;
		}
		public static string Repath(string target,string path="",bool flatten=false){
			if(flatten){return target.Replace(path,"");}
			return target.Replace(Settings.releasePath,"");
		}
	}
}