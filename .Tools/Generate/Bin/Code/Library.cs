using System;
using System.Collections.Generic;
using System.Linq;
using Zios.Extensions;
namespace Zios.Generate{
	public class Library : Data{
		public string name;
		public string uuid = Guid.NewGuid().ToString();
		public static List<Library> all = new List<Library>();
		public static Library Find(string name,bool findPartial=false){
			foreach(var library in Library.all){
				if(library.name == name){
					return library;
				}
			}
			if(findPartial && name.Contains(".")){
				var reduced = name.Split(".").SkipLast().Join(".");
				var result = Library.Find(reduced,true);
				if(!result.IsNull()){
					return result;
				}
			}
			return null;
		}
		public Library(string path=""){
			this.path = path;
			this.name = path.Split('/').Last().Replace(".dll","").Replace(".so","");
			Library.all.Add(this);
		}
	}
}