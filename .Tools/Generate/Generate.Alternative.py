#=======================
# SYSTEM
#=======================
import os,sys
sys.dont_write_bytecode = True
sys.path[0] = sys.path[0]+"\\BinPython"
os.chdir(sys.path[0])
#=======================
# IMPORTS
#=======================
from Settings import Settings as x
from Support.Utility import Utility
from Generate import Generate
#================
# Strings
#================
x.frameworkVersion  = "3.5"
x.targetNamespace   = "Zios"
x.appendInfo        = " .r{svnRevision}.{unityPlatform}.{unityVersion}"
x.unityVersion      = "2018.2.2f1"
x.unityPlatform     = "Windows"
x.packageCategory   = "Zios"
x.userFlags         = ""
#================
# Bools
#================
x.verboseLogging    = True
x.multiThreading    = False
x.preserveStructure = True
x.symLinkLibraries  = True
x.symLinkSource     = True
x.saveStandalone    = True
x.saveLinked        = False
x.savePackage       = True
x.saveAsmdef        = True
x.compileProjects   = True
x.zipBinaries       = False
x.zipSource         = False
x.zipLibraries      = False
x.zipFlatten        = False
x.openResults       = True
x.cleanupResults    = True
#================
Generate()