import os,sys
import threading
import time
import Settings as Program
from Support.Color import Color,Info
try    : raw_input
except : raw_input = input
class Utility:
	allowPrompt = True
	folderMap = {}
	itemIndex = 0
	folderIndex = 0
	clearLine = "\033[K"
	previousLine = "\033[1A"
	@staticmethod
	def ShowIndex():
		Utility.itemIndex += 1
		return "{:<4}".format(str(Utility.itemIndex)+".")
	@staticmethod
	def Command(term):
		os.system(term + "> nul 2>&1")
	@staticmethod
	def GetPath():
		return os.getcwd().replace("\\","/")+"/"
	@staticmethod
	def GetShellPath(path=None):
		path = path or Utility.GetPath()
		path = path.replace("/","\\")
		return '"'+path+'"'
	@staticmethod
	def Prompt(name,default,existing=None):
		display = lambda color,text : "{:<30}".format(name) + Color.Wrap("gray"," [Current : ") + Color.Wrap(color,str(text)) + Color.Wrap("gray","]")
		if existing is not None :
			print(display("silver",existing))
			return existing
		result = default
		prompt = display("silver",default)
		if not Utility.allowPrompt:
			print(prompt)
			return default
		field = raw_input(prompt)
		if field:
			if field.endswith("!"):
				Utility.allowPrompt = False
			result = field.strip("!")
			if type(default) is bool:
				result = not (field.lower().startswith("f") or field == "0")
			print(Utility.previousLine + display("lightblue",result) + Utility.clearLine)
		return result
	@staticmethod
	def GetFiles(path,items=[]):
		for item in os.listdir(path):
			current = path + item
			if os.path.isdir(current):
				items = Utility.GetFiles(current+"/",items)
			else:
				items.append(current)
		return items
	@staticmethod
	def SaveFile(path,contents):
		file = open(path,"w")
		file.write(contents)
		file.close()
	@staticmethod
	def SymLink(source,destination):
		os.symlink(source,destination)
	@staticmethod
	def CopyFolder(source,destination,asSymLink=False):
		Utility.CreateFolder(destination)
		for item in os.listdir(source):
			path = source + item
			goalPath = destination + item
			if os.path.isdir(path):
				Utility.CopyFolder(path+"/",goalPath+"/",asSymLink)
				continue
			Utility.Copy(path,goalPath,asSymLink)
	@staticmethod
	def Copy(source,destination,asSymLink=False,overwrite=True):
		if overwrite : Utility.Remove(destination)
		if asSymLink:
			Utility.SymLink(source,destination)
			return
		try : shutil.copyfile(source,destination)
		except : pass
	@staticmethod
	def CopyPath(source,destination,asSymLink=False):
		fileName = source.split("/")[-1]
		targets = source.replace(Program.Settings.startPath,"").split("/")[:-1]
		basePath = destination
		destination = destination + "/".join(targets)+"/"+fileName
		path = basePath
		for target in targets:
			path += target + "/"
			Utility.CreateFolder(path)
		Utility.Copy(source,destination,asSymLink)
		return destination
	@staticmethod
	def Remove(path):
		try    : os.remove(path)
		except : pass
	@staticmethod
	def CreateFolder(path,change=False):
		try : os.mkdir(path)
		except : pass
		if change : os.chdir(path)
	@staticmethod
	def GetFolder(path):
		return "/".join(path.strip().rstrip("/").split("/")[0:-1]) + "/"
	@staticmethod
	def GetFolderName(path):
		return path.split("/")[-2:-1][0]
	@staticmethod
	def PreviousFolder(change=False):
		path = Utility.GetFolder(Utility.GetPath())
		if change : os.chdir(path)
		return path
	@staticmethod
	def Repath(target,path="",flatten=False):
		if flatten:
			return target.replace(path,"")
		return target.replace(Program.Settings.releasePath,"")
	@staticmethod
	def ShowProgress(percent,prefix="",suffix=""):
		if Program.Settings.verboseLogging : return
		if type(suffix) is list:
			suffix = str(min(Utility.itemIndex,len(suffix))) + "/" + str(len(suffix))
		percent = min(percent,1)
		endChar = percent >= 1 and "\n" or "\r"
		progress = int(percent*50) * "▄"
		bar = Color.Wrap("blue","{:<50}".format(progress))
		progress = "["+ bar + "] "
		print(prefix + progress + suffix + Utility.clearLine,end=endChar)
		Utility.itemIndex += 1
	@staticmethod
	def ThreadWait(targets,term,label=""):
		if Program.Settings.multiThreading and len(targets) > 0:
			mainThread = threading.current_thread().name == 'MainThread'
			time.sleep(0.05)
			while True:
				liveThreads = [x.__dict__[term].isAlive() for x in targets if x.__dict__[term]]
				remaining = len(targets) - liveThreads.count(True)
				percent = 1-(liveThreads.count(True)/len(targets))
				suffix = str(remaining) + "/" + str(len(targets))
				if mainThread:
					Utility.ShowProgress(percent,label,suffix)
				if liveThreads.count(True) < 1:
					break
				time.sleep(0.05)