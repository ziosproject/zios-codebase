from Support.Debug import log
Info = lambda text : log.info(text+"\033[K")
class Color:
	first             = False
	foreground        = "38;5;"
	background        = "48;5;"
	defaultForeground = '249'
	defaultBackground = '0'
	black             = '232'
	gray              = '242'
	silver            = '249'
	white             = '255'
	green             = '119'
	red               = '203'
	orange            = '214'
	yellow            = '228'
	lightblue         = '39'
	darkred           = '52'
	darkblue          = '25'
	darkgray          = '234'
	gravel            = '66'
	cyan              = '81'
	blue              = '33'
	bold              = '1'
	italic            = '3'
	underline         = '4'
	@staticmethod
	def Set(name,foreground=True):
		term = name
		if name in Color.__dict__ :
			type = foreground and Color.foreground or Color.background
			term = type + Color.__dict__[name]
		return '\033[' + term + 'm'
	@staticmethod
	def Back(name):
		return Color.Set(name,False)
	@staticmethod
	def Text(name):
		return Color.Set(name)
	@staticmethod
	def Start(name):
		return Color.Set(name)
	@staticmethod
	def End():
		return Color.Set("defaultForeground") + Color.Set("defaultBackground",False)
	@staticmethod
	def Wrap(name,text):
		return Color.Start(name) + text + Color.End()
	@staticmethod
	def Format(text,background,foreground,size):
		return Color.Back(background) + Color.Text(foreground) + Color.Text("1") + ("{:<"+str(size)+"}").format(text) + Color.End()
	@staticmethod
	def Info(name,text):
		return Info(Color.Wrap(name,text))
def Marked(x,color):
	if not Color.first : Info("")
	Info(MarkedStyle(x,color))
	Color.first = False
def MarkedStyle(x,color):
	return Color.Back(color) + Color.Text("white") + Color.Text("1") + "{:<81}".format(x) + Color.End()
def Header(x) : Marked(x,"darkgray")
def Success(x): Color.Info("lightblue",x)
def Error(x)  : Color.Info("red",x)
def Fail(x)   : Color.Info("orange",x)
def Warn(x)   : Color.Info("yellow",x)
def Note(x)   : Color.Info("gray",x)
Color.Set("defaultForeground")
Color.Set("defaultBackground",False)