import threading
import Settings as Program
from uuid import uuid4 as guid
from Support.Color import Color,Info
from Support.Utility import Utility
from Template import Sln
class Solution:
	all = []
	def __init__(self,folder="",name="",references=[],standalone=False):
		self.uuid = str(guid())
		self.folder = folder
		self.name = name
		self.path = self.folder + self.name + ".sln"
		self.projectReferences = references
		self.saveThread = None
		self.standalone = standalone
		Solution.all.append(self)
	def Save(self):
		if Program.Settings.multiThreading:
			Program.Settings.lock.acquire()
			if self.standalone or not self.saveThread:
				self.saveThread = threading.Thread(target=self.SaveProcess)
				self.saveThread.start()
			Program.Settings.lock.release()
		else:
			self.SaveProcess()
	def SaveProcess(self):
		if Program.Settings.verboseLogging : Info(Utility.ShowIndex() + Color.Wrap("gray","["+self.name+"]") + " : " + Color.Wrap("silver","Saving .sln"))
		output = Sln.template.replace("$solutionGUID",self.uuid)
		projectReferences = ""
		buildOptions = ""
		for project in self.projectReferences:
			projectPath = "./"+project.namespaceName
			if not self.standalone : projectPath += "/"+project.namespaceName
			reference = Sln.projectReference.replace("$projectName",project.namespaceName)
			reference = reference.replace("$projectGUID",project.uuid)
			reference = reference.replace("$projectPath",projectPath+".csproj")
			buildOptions += Sln.buildOption.replace("$projectGUID",project.uuid)
			projectReferences += reference
		output = output.replace("$solutionProjectReferences",projectReferences.strip())
		output = output.replace("$solutionBuildOptions",buildOptions.strip())
		Program.Settings.openPath = "/select,"+Utility.GetShellPath(self.path)
		Utility.SaveFile(self.path,output)
	def Compile(self):
		if Program.Settings.verboseLogging : Info(Color.Wrap("gray","["+self.name+"]") + ":" +Color.Wrap("silver","Compiling .sln"))
		for project in self.projectReferences:
			project.Compile()