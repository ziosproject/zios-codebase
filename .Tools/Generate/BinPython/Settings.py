import subprocess
import threading
import sys
from Support.Color import Color,Header,Success,Fail,MarkedStyle
from Support.Utility import Utility
#=================
# SETUP
#=================
Utility.PreviousFolder(True)
Utility.CreateFolder("Release")
#=================
# CONFIGURATION
#=================
class Settings:
	@staticmethod
	def Has(term):
		values = Settings.__dict__
		if term not in values : return None
		return values[term]
	@staticmethod
	def Setup():
		Has = Settings.Has
		Header("SETTINGS")
		x = Settings
		skip = False
		x.lock                  = threading.Lock()
		x.keep                  = []
		x.hasStartPath          = Has("startPath")
		x.startPath             = x.hasStartPath or Utility.GetPath()+"../../"
		x.unityPath             = ""
		x.unityVersionClean     = ""
		x.generatedInfo         = ""
		x.svnRevision           = str(subprocess.check_output("./Compiler/svn.exe info ../")).lower().split("revision:")[-1].split("\\")[0].strip()
		x.releasePath           = Utility.GetPath() + "Release/"
		x.libraryPath           = Utility.GetPath() + "Library/"
		x.targetNamespace       = Utility.Prompt("Target Namespace(s)","Zios.Unity.Editor.Themes",Has("targetNamespace"))
		Utility.allowPrompt     = Utility.Prompt(Color.Format("Customize Common","darkgray","silver",30),False)
		x.verboseLogging        = Utility.Prompt("Verbose Logging",True,Has("verboseLogging"))
		x.multiThreading        = Utility.Prompt("Use Multithreading",False,Has("multiThreading"))
		#x.flattenDistribution   = Utility.Prompt("Flatten Distribution",True,Has("flattenDistribution"))
		#x.flattenIgnoreSource   = Utility.Prompt(" | Ignore Source",True,Has("flattenIgnoreSource"))
		x.preserveStructure     = Utility.Prompt("Preserve Source Structure",True,Has("preserveStructure"))
		x.saveProjects          = Utility.Prompt("Save CsProj/Sln",True,Has("saveProjects"))
		skip                    = Utility.allowPrompt and not x.saveProjects
		Utility.allowPrompt	    = not skip and Utility.allowPrompt or False
		x.saveStandalone        = Utility.Prompt(" | Save Standalone",False,Has("saveStandalone"))
		#if x.saveStandalone and "*" in x.targetNamespace:
		#	x.singleStandalone  = Utility.Prompt("   | As Single",True,Has("singleStandalone"))
		x.saveLinked            = Utility.Prompt(" | Save Linked",x.saveProjects,Has("saveLinked"))
		x.compileProjects       = Utility.Prompt(" | Compile",x.saveProjects,Has("compileProjects"))
		#if x.compileProjects:
		#	x.includeDependents = Utility.Prompt("   | Include Dependents",x.saveProjects,Has("includeDependents"))
		Utility.allowPrompt     = skip or Utility.allowPrompt
		skip                    = False
		x.savePackage           = Utility.Prompt("Save Package",True,Has("savePackage"))
		if x.savePackage:
			x.packageCategory   = Utility.Prompt(" | Category","Zios",Has("packageCategory"))
		x.saveAsmdef            = Utility.Prompt("Save Asmdef",True,Has("saveAsmdef"))
		x.zipResults            = Utility.Prompt("Zip Results",False,Has("zipResults"))
		if x.zipResults:
			x.zipIgnoreLibrary  = Utility.Prompt(" | Ignore Library",True,Has("zipIgnoreLibrary"))
		Utility.allowPrompt     = True
		Utility.allowPrompt     = Utility.Prompt(Color.Format("Customize Advanced","darkgray","silver",30),False)
		x.startPath             = Utility.Prompt("Source Path",x.startPath,x.hasStartPath)
		x.appendInfo            = Utility.Prompt("Append Info",".{unityPlatform}.{unityVersion}.r{svnRevision}",Has("appendInfo"))
		x.frameworkVersion      = Utility.Prompt(".Net Version","4.6.2",Has("frameworkVersion"))
		x.unityVersion          = Utility.Prompt("Unity Version","2018.2.2f1",Has("unityVersion"))
		x.unityPlatform         = Utility.Prompt("Unity Platform","Windows",Has("unityPlatform"))
		x.userFlags             = Utility.Prompt("User Flags","",Has("userFlags"))
		x.symLinkFiles          = Utility.Prompt("Symlink Files",True,Has("symLinkFiles"))
		skip                    = Utility.allowPrompt and not x.symLinkFiles
		Utility.allowPrompt	    = not skip and Utility.allowPrompt or False
		x.symLinkBinaries       = Utility.Prompt(" | Binaries",True,Has("symLinkBinaries"))
		x.symLinkLibraries      = Utility.Prompt(" | Libraries",True,Has("symLinkLibraries"))
		x.symLinkSource         = Utility.Prompt(" | Source",True,Has("symLinkSource"))
		Utility.allowPrompt     = skip or Utility.allowPrompt
		skip                    = False
		x.openResults           = Utility.Prompt("Open Results",True,Has("openResults"))
		x.openPath              = x.releasePath.replace("/","\\")
		x.cleanupResults        = Utility.Prompt("Cleanup Results",False,Has("cleanupResults"))
		#globals().update(locals())