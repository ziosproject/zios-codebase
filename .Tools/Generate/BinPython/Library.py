from uuid import uuid4 as guid
class Library:
	all = []
	@staticmethod
	def Find(name,findPartial=False):
		for library in Library.all:
			if library.name == name : return library
		if findPartial and "." in name:
			reduced = ".".join(name.split(".")[0:-1])
			result = Library.Find(reduced,True)
			if result :	return result
		return None
	def __init__(self,path=""):
		self.uuid = str(guid())
		self.path = path
		self.name = path.split("/")[-1].replace(".dll","").replace(".so","")
		Library.all.append(self)