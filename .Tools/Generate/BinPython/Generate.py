#=======================
# SYSTEM
#=======================
import os
import sys
import time
import shutil
import subprocess
sys.dont_write_bytecode = True
#=======================
# IMPORTS
#=======================
import Settings as Program
from Support.Color import Color,Header,Marked,Info,Success,Fail
from Support.Utility import Utility
from Solution import Solution
from Library import Library
from Source import Source
from Project import Project
from Template import CsProj
class Generate:
	#=======================
	# SETUP
	#=======================
	def __init__(self):
		Program.Settings.Setup()
		self.Cleanup()
		self.time = time.time()
		self.CheckSupport()
		self.SetInfo()
		self.SetUnityPath()
		self.SetUnityFlags()
		self.initial = Utility.GetPath()
		self.libraries = []
		self.saved = []
		if not Program.Settings.verboseLogging:
			Header("PROCESS")
		self.Scan(Program.Settings.libraryPath)
		self.Scan(Program.Settings.unityPath)
		self.Scan(Program.Settings.startPath)
		self.Build()
		self.Save()
		self.SaveAsmdef()
		self.SavePackage()
		self.Compile()
		self.Zip()
		self.Cleanup()
		if Program.Settings.openResults:
			subprocess.Popen(r'explorer '+Program.Settings.openPath)
		Marked(str(time.time()-self.time) + " seconds elapsed.","darkgray")
	def CheckSupport(self):
		if Program.Settings.symLinkSource or Program.Settings.symLinkLibraries:
			try :
				path = Utility.GetPath()
				Utility.SymLink(path+"Generate.py",path+"Generate.test")
				Utility.Remove(path+"Generate.test")
				Success("Symlinking is enabled/allowed.")
			except :
				Fail("No permission to SymLink. Reverting to copying.")
				os.system("pause")
				Program.Settings.symLinkSource = False
				Program.Settings.symLinkLibraries = False
	def SetInfo(self):
		Program.Settings.generatedInfo = Program.Settings.appendInfo.lower()
		for item in Program.Settings.__dict__:
			Program.Settings.generatedInfo = Program.Settings.generatedInfo.replace("{"+item.lower()+"}",str(Program.Settings.__dict__[item])).strip()
	def GetPlatformSuffix(self):
		platform = Program.Settings.unityPlatform.lower()
		if "win" in platform : return "WIN"
		if "osx" in platform or "mac" in platform : return "OSX"
		if "linux" in platform : return "LINUX"
	def SetUnityFlags(self):
		namedVersion = Program.Settings.unityVersionClean
		version = namedVersion.count(".") > 1 and float("".join(namedVersion.rsplit(".",1))) or float(namedVersion)
		upperVersion = namedVersion.replace(".","_")
		directives = "UNITY_"+upperVersion+";"
		while upperVersion.count("_")>0:
			upperVersion = "_".join(upperVersion.split("_")[0:-1])
			directives += "UNITY_"+upperVersion+";"
		if version >= 5.3     : directives += "UNITY_5_3_OR_NEWER;"
		if version >= 5.34    : directives += "UNITY_5_3_4_OR_NEWER;"
		if version >= 5.4     : directives += "UNITY_5_4_OR_NEWER;"
		if version >= 5.5     : directives += "UNITY_5_5_OR_NEWER;"
		if version >= 5.6     : directives += "UNITY_5_6_OR_NEWER;"
		if version >= 2017    : directives += "UNITY_2017_OR_NEWER;"
		if version >= 2017.1  : directives += "UNITY_2017_1_OR_NEWER;"
		if version >= 2017.2  : directives += "UNITY_2017_2_OR_NEWER;"
		if version >= 2017.3  : directives += "UNITY_2017_3_OR_NEWER;"
		if version >= 2017.4  : directives += "UNITY_2017_4_OR_NEWER;"
		if version >= 2018    : directives += "UNITY_2018_OR_NEWER;"
		if version >= 2018.1  : directives += "UNITY_2018_1_OR_NEWER;"
		if version >= 2018.2  : directives += "UNITY_2018_2_OR_NEWER;"
		if version >= 2018.3  : directives += "UNITY_2018_3_OR_NEWER;"
		platform = self.GetPlatformSuffix()
		CsProj.unityVersionFlags = directives
		CsProj.unityEditorFlags = CsProj.unityEditorFlags.replace("XXX",platform)
		CsProj.unityBuildFlags = CsProj.unityBuildFlags.replace("XXX",platform)
	def SetUnityPath(self):
		version = Program.Settings.unityVersion
		for suffix in ["b","a","r","f","rc"]:
			index = 0
			while index < 9:
				version = version.replace(suffix+str(index),"")
				index += 1
		Program.Settings.unityVersionClean = version
		Program.Settings.unityPath = Utility.GetPath()+"Unity/"+version+"/"
	#=======================
	# MAIN
	#=======================
	def Scan(self,currentPath):
		for item in os.listdir(currentPath):
			path = currentPath + item
			if os.path.isdir(path) and not item.startswith("."):
				os.chdir(path)
				self.Scan(path+"/")
				os.chdir(currentPath)
				continue
			isCS  = ".cs"  in path and ".cs.meta"  not in path and ".csproj" not in path
			isSO  = ".so"  in path and ".so.meta"  not in path
			isDLL = ".dll" in path and ".dll.meta" not in path
			if isDLL or isSO:
				Library(path)
			if isCS:
				using = []
				active = None
				source = Source(path)
				lines = None
				try : lines = open(item,"r",encoding="utf-8-sig").readlines()
				except Exception as e: lines = open(item,"r").readlines()
				for line in lines:
					line = line.strip()
					if line.startswith("//includeIf"):
						base = line.split("//includeIf")[-1].strip()
						sets = base.replace("(","").replace(")","").split("||")
						for item in sets:
							source.includeIf.append([item.strip() for item in item.split("&&")])
					if line.startswith("namespace"):
						namespace = line.split("namespace")[-1].replace("{","").strip()
						source.namespace = namespace
						active = Project.Create(namespace,currentPath)
						if path not in [source.path for source in active.sourceReferences]:
							active.sourceReferences.append(source)
					entry = None
					if line.startswith("//asm"):
						entry = line.split("//asm")[-1].strip().strip(";")
					if line.startswith("using") and "(" not in line:
						entry = line.split("using")[-1].strip().strip(";")
						if "=" in line:
							entry =  ".".join(entry.split("= ")[-1].split(".")[:-1])
					if entry:
						using.append(entry)
				if active:
					for item in using:
						if item not in active.referencePaths and item != active.namespaceName:
							active.referencePaths.append(item)
	def Build(self):
		for project in Project.all[:]:
			if Program.Settings.saveStandalone and project.IsTarget():
				standalone = Project(project.namespaceName,project.namespacePath,True)
				standalone.sourceReferences = project.sourceReferences[:]
				standalone.referencePaths = project.referencePaths[:]
				standalone.Build()
			project.Build()
		for project in Project.all[:]:
			project.BuildIncludes()
	def Save(self):
		os.chdir(Program.Settings.releasePath)
		linkedTargets = [x for x in Project.all if not x.standalone and x.IsTarget()]
		standaloneTargets = [x for x in Project.all if x.standalone]
		if Program.Settings.saveLinked:
			targets = linkedTargets
			Utility.itemIndex = 0
			label = "{:<30}".format("Saving .csproj (Linked)")
			if Program.Settings.verboseLogging:
				Header("SAVING CSPROJ (LINKED)")
			for project in targets:
				project.Save()
				if not Program.Settings.multiThreading:
					Utility.ShowProgress(Utility.itemIndex/len(targets),label,targets)
			Utility.ThreadWait(targets,"saveThread",label)
		if Program.Settings.saveStandalone:
			targets = standaloneTargets
			Utility.itemIndex = 0
			label = "{:<30}".format("Saving .csproj (Standalone)")
			if Program.Settings.verboseLogging:
				Header("SAVING CSPROJ (STANDALONE)")
			for project in targets:
				project.Save()
				if not Program.Settings.multiThreading:
					Utility.ShowProgress(Utility.itemIndex/len(targets),label,targets)
			Utility.ThreadWait(targets,"saveThread",label)
		if Program.Settings.saveLinked:
			targets = linkedTargets
			Utility.itemIndex = 0
			label = "{:<30}".format("Saving .sln (Linked)")
			if Program.Settings.verboseLogging:
				Header("SAVING SLN (LINKED)")
			for project in targets:
				solution = Solution(Program.Settings.releasePath,project.namespaceName,project.allProjectReferences)
				solution.Save()
				if not Program.Settings.multiThreading:
					Utility.ShowProgress(Utility.itemIndex/len(targets),label,targets)
			Utility.ThreadWait(Solution.all,"saveThread",label)
		if Program.Settings.saveStandalone:
			targets = standaloneTargets
			Utility.itemIndex = 0
			label = "{:<30}".format("Saving .sln (Standalone)")
			if Program.Settings.verboseLogging:
				Header("SAVING SLN (STANDALONE)")
			standalone = []
			for project in targets:
				solution = Solution(Program.Settings.releasePath+"@"+project.namespaceName+"/",project.namespaceName,[project],True)
				solution.Save()
				standalone.append(solution)
				if not Program.Settings.multiThreading:
					Utility.ShowProgress(Utility.itemIndex/len(targets),label,targets)
			Utility.ThreadWait(standalone,"saveThread",label)
	def SaveAsmdef(self):
		if Program.Settings.saveAsmdef:
			Utility.itemIndex = 0
			if Program.Settings.verboseLogging:
				Header("SAVING ASMDEF")
			targets = [x for x in Project.all if x.folder]
			for project in targets:
				project.SaveAsmdef()
	def SavePackage(self):
		if Program.Settings.savePackage:
			Utility.itemIndex = 0
			if Program.Settings.verboseLogging:
				Header("SAVING PACKAGE")
			targets = [x for x in Project.all if x.folder]
			for project in targets:
				project.SavePackage()
	def Compile(self):
		if Program.Settings.compileProjects:
			Utility.itemIndex = 0
			targets = [x for x in Project.all if x.folder]
			label = "{:<30}".format("Compiling .csproj")
			if Program.Settings.verboseLogging:
				Header("COMPILING ")
			for project in targets:
				alreadyCompiled = project.alreadyCompiled
				project.Compile()
				if not Program.Settings.multiThreading and not alreadyCompiled:
					Utility.ShowProgress(Utility.itemIndex/len(targets),label,targets)
			Utility.ThreadWait(targets,"compileThread",label)
	def Zip(self):
		if Program.Settings.zipResults:
			Utility.itemIndex = 0
			targets = [x for x in Project.all if x.IsTarget()]
			label = "{:<30}".format("Zipping Results")
			if Program.Settings.verboseLogging:
				Header("ZIPPING ")
			for project in targets:
				project.Zip()
			Utility.ThreadWait(targets,"zipThread",label)
	def Cleanup(self,force=False):
		if force or Program.Settings.cleanupResults:
			if not force and Program.Settings.verboseLogging:
				Header("CLEANUP ")
			for item in os.listdir(Program.Settings.releasePath):
				path = Program.Settings.releasePath + item
				skip = False
				for keep in Program.Settings.keep:
					if keep in path:
						skip = True
						break
				if not skip:
					if os.path.isdir(path):
						while os.path.isdir(path):
							if Program.Settings.verboseLogging : Info("Removing path " + Color.Wrap("gray","["+path+"]"))
							shutil.rmtree(path,ignore_errors=True)
					else:
						if Program.Settings.verboseLogging : Info("Removing file " + Color.Wrap("gray","["+path+"]"))
						os.remove(path)
#=======================
# START
#=======================
directMode = __name__ == "__main__"
Utility.allowPrompt = directMode
if directMode:
	Generate()
	os.system("pause")