from Support.Utility import Utility
import os
path = "./Templates/"
class Meta:
	template           = open(path+"Meta.template","r").read()
class Asmdef:
	template           = open(path+"Asmdef.template","r").read()
	dependency         = '\t\t"$packageName": "$packageVersion",\n'
class Package:
	template           = open(path+"Package.template","r").read()
	dependency         = '\t\t"$packageName": "$packageVersion",\n'
class Sln:
	template           = open(path+"Sln.template","r").read()
	projectReference   = 'Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "$projectName", "$projectPath", "{$projectGUID}" EndProject\n'
	buildOption        = '\t\t{$projectGUID}.Release|Any CPU.Build.0 = Release|Any CPU\n'
class Compile:
	compilerPath       = Utility.GetPath() + "Compiler"
	#compilerPath      = Utility.GetPath() + "Compiler/Rosyln"
	template           = open(path+"Build.template","r").read().replace("$compilerPath",compilerPath)
	defines            = '/define:"$defines"'
	sourceReference    = '"$path" '
	dllReference       = '/reference:"$path" '
class CsProj:
	template            = open(path+"Csproj.template","r").read()
	projectReference    = '\t\t<ProjectReference Include="$projectPath">\n'
	projectReference   += '\t\t\t<Project>{$projectUUID}</Project>\n'
	projectReference   += '\t\t\t<Name>$projectName</Name>\n'
	projectReference   += '\t\t</ProjectReference>\n'
	sourceReference     = '\t\t<Compile Include="$sourcePath" />\n'
	libraryReference    = '\t\t<Reference Include="$libraryName">\n'
	libraryReference   += '\t\t\t<HintPath>$libraryPath</HintPath>\n'
	libraryReference   += '\t\t\t<Private>False</Private>\n'
	libraryReference   += '\t\t</Reference>\n'
	unityPropertyGroup  = '\n\t<PropertyGroup>\n'
	unityPropertyGroup += '\t\t<ProjectTypeGuids>{E097FAD1-6243-4DAD-9C02-E9B9EFC3FFC1};{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}</ProjectTypeGuids>\n'
	unityPropertyGroup += '\t\t<UnityProjectGenerator>Unity/VSTU</UnityProjectGenerator>\n'
	unityPropertyGroup += '\t\t<UnityProjectType>$unityProjectType</UnityProjectType>\n'
	unityPropertyGroup += '\t\t<UnityBuildTarget>StandaloneWindows64:19</UnityBuildTarget>\n'
	unityPropertyGroup += '\t\t<UnityVersion>$unityVersion</UnityVersion>\n'
	unityPropertyGroup += '\t</PropertyGroup>'
	unityVersionFlags  = ""
	unityEditorFlags   = "UNITY_EDITOR;UNITY_EDITOR_64;UNITY_EDITOR_XXX;"
	unityBuildFlags    = "UNITY_STANDALONE;PLATFORM_STANDALONE;PLATFORM_STANDALONE_XXX;UNITY_STANDALONE_XXX;"
	unityFlags         = "DEBUG;TRACE;PLATFORM_ARCH_64;UNITY_64;UNITY_ANALYTICS;ENABLE_AUDIO;ENABLE_CACHING;ENABLE_CLOTH;ENABLE_DUCK_TYPING;ENABLE_MICROPHONE;ENABLE_MULTIPLE_DISPLAYS;ENABLE_PHYSICS;ENABLE_SPRITES;ENABLE_GRID;ENABLE_TILEMAP;ENABLE_TERRAIN;ENABLE_TEXTURE_STREAMING;ENABLE_DIRECTOR;ENABLE_UNET;ENABLE_LZMA;ENABLE_UNITYEVENTS;ENABLE_WEBCAM;ENABLE_WWW;ENABLE_CLOUD_SERVICES_COLLAB;ENABLE_CLOUD_SERVICES_COLLAB_SOFTLOCKS;ENABLE_CLOUD_SERVICES_ADS;ENABLE_CLOUD_HUB;ENABLE_CLOUD_PROJECT_ID;ENABLE_CLOUD_SERVICES_USE_WEBREQUEST;ENABLE_CLOUD_SERVICES_UNET;ENABLE_CLOUD_SERVICES_BUILD;ENABLE_CLOUD_LICENSE;ENABLE_EDITOR_HUB;ENABLE_EDITOR_HUB_LICENSE;ENABLE_WEBSOCKET_CLIENT;ENABLE_DIRECTOR_AUDIO;ENABLE_DIRECTOR_TEXTURE;ENABLE_TIMELINE;ENABLE_EDITOR_METRICS;ENABLE_EDITOR_METRICS_CACHING;ENABLE_MANAGED_JOBS;ENABLE_MANAGED_TRANSFORM_JOBS;ENABLE_MANAGED_ANIMATION_JOBS;INCLUDE_DYNAMIC_GI;INCLUDE_GI;ENABLE_MONO_BDWGC;PLATFORM_SUPPORTS_MONO;RENDER_SOFTWARE_CURSOR;INCLUDE_PUBNUB;ENABLE_VIDEO;ENABLE_PACKMAN;ENABLE_CUSTOM_RENDER_TEXTURE;ENABLE_LOCALIZATION;ENABLE_SUBSTANCE;ENABLE_RUNTIME_GI;ENABLE_MOVIES;ENABLE_NETWORK;ENABLE_CRUNCH_TEXTURE_COMPRESSION;ENABLE_UNITYWEBREQUEST;ENABLE_CLOUD_SERVICES;ENABLE_CLOUD_SERVICES_ANALYTICS;ENABLE_CLOUD_SERVICES_PURCHASING;ENABLE_CLOUD_SERVICES_CRASH_REPORTING;ENABLE_OUT_OF_PROCESS_CRASH_HANDLER;ENABLE_EVENT_QUEUE;ENABLE_CLUSTER_SYNC;ENABLE_CLUSTERINPUT;ENABLE_VR;ENABLE_AR;ENABLE_WEBSOCKET_HOST;ENABLE_MONO;NET_2_0_SUBSET;ENABLE_PROFILER;UNITY_ASSERTIONS;ENABLE_UNITY_COLLECTIONS_CHECKS;ENABLE_BURST_AOT;UNITY_TEAM_LICENSE;ENABLE_VSTU;"
