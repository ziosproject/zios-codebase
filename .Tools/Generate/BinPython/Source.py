class Source:
	all = []
	def __init__(self,path=""):
		self.path = path
		self.namespace = ""
		self.includeIf = []
		Source.all.append(self)
	def CheckInclude(self,references=[]):
		for set in self.includeIf:
			hasAll = True
			for include in set:
				hasItem = False
				for reference in references:
					if include in reference:
						hasItem = True
						break
				if not hasItem:
					hasAll = False
					break
			if hasAll:
				return self
		return None
	@staticmethod
	def CheckAll(references=[]):
		includes = []
		for source in Source.all:
			if source.CheckInclude(references):
				includes.append(source)
		return includes