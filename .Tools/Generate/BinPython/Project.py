import os
import time
import threading
import zipfile
import subprocess
import Settings as Program
from uuid import uuid4 as guid
from Support.Color import Color,Info,Warn,Note
from Support.Utility import Utility
from Template import CsProj,Asmdef,Package,Compile
from Library import Library
from Source import Source
import sys
class Project:
	all = []
	@staticmethod
	def Find(namespaceName,namespacePath="",standalone=False):
		for project in Project.all:
			if project.standalone != standalone : continue
			if project.namespaceName == namespaceName : return project
			#if project.namespacePath == namespacePath : return project
		return None
	@staticmethod
	def Create(namespaceName,namespacePath):
		return Project.Find(namespaceName,namespacePath) or Project(namespaceName,namespacePath)
	def __init__(self,namespaceName="",namespacePath="",standalone=False):
		self.uuid = str(guid())
		self.path = ""
		self.folder = ""
		self.defines = ""
		self.dllPath = ""
		self.namespacePath = namespacePath
		self.namespaceName = namespaceName
		self.standalone = standalone
		self.allSourceReferences = []
		self.allProjectReferences = []
		self.allLibraryReferences = []
		self.sourceReferences = []
		self.libraryReferences = []
		self.projectReferences = []
		self.referencePaths = []
		self.userReferencePaths = []
		self.saveThread = None
		self.compileThread = None
		self.zipThread = None
		self.alreadySaved = False
		self.alreadyCompiled = False
		self.alreadyZipped = False
		self.alreadySorted = False
		Project.all.append(self)
	def Get(self,name) :
		return self.__dict__[name]
	def Set(self,name,value) :
		self.__dict__[name] = value
		return self.Get(name)
	#=========================
	# SUPPORT
	#=========================
	def IsTarget(self):
		target = Program.Settings.targetNamespace.lower()
		namespace = self.namespaceName.lower()
		if "*" in target:
			if target.endswith("*") and namespace.startswith(target.strip("*")):
				return True
			if target.startswith("*") and namespace.endswith(target.strip("*")):
				return True
		return target == namespace
	def BuildReferences(self,targetName,defaultName):
		target = self.Get(targetName)
		if len(target) < 1:
			target = self.Set(targetName,self.Get(defaultName)[:])
			for project in self.projectReferences:
				project.BuildReferences(targetName,defaultName)
				for reference in project.Get(targetName):
					if reference not in target:
						target.append(reference)
			organized = sorted(target,key=lambda x:x.path)
			self.Set(targetName,organized)
	def BuildIncludes(self):
		references = [x.namespaceName for x in self.projectReferences]
		extras = Source.CheckAll(references)
		for include in extras:
			match = Project.Find(include.namespace)
			if match and match != self:
				if Program.Settings.verboseLogging : Note("[" + Color.Wrap("silver",self.namespaceName) + Color.Set("gray") + "]" + " Autoincluded " + Color.Wrap("white",match.namespaceName))
				self.projectReferences.append(match)
	def Build(self):
		self.libraryReferences.append(Library.Find("mscorlib"))
		self.libraryReferences.append(Library.Find("System.Core"))
		for reference in self.referencePaths:
			internal = reference.startswith("UnityE") or reference.startswith("Microsoft") or reference.startswith("System")
			project = Project.Find(reference)
			if not internal:
				self.userReferencePaths.append(reference)
			if project and project not in self.projectReferences and project != self:
				self.projectReferences.append(project)
			elif not project:
				library = Library.Find(reference) or Library.Find(reference,True)
				if library and library not in self.libraryReferences:
					self.libraryReferences.append(library)
				elif not library and not internal:
					if Program.Settings.verboseLogging : Warn("[" + self.namespaceName + "] Ignoring missing reference for -- " + reference)
		self.projectReferences = sorted(self.projectReferences,key=lambda x:x.namespaceName)
		self.libraryReferences = sorted(self.libraryReferences,key=lambda x:x.path)
		self.userReferencePaths = sorted(self.userReferencePaths)
	#=========================
	# SAVING
	#=========================
	def Save(self):
		if Program.Settings.multiThreading:
			Program.Settings.lock.acquire()
			if self.standalone or not self.saveThread:
				self.saveThread = threading.Thread(target=self.SaveProcess)
				self.saveThread.start()
			Program.Settings.lock.release()
		elif self.standalone or not self.alreadySaved:
			self.SaveProcess()
	def CheckUnity(self,references):
		for reference in references:
			if ".Unity." in reference.namespaceName:
				Warn("["+self.namespaceName+"] is using Unity references, but isn't in a Unity namespace.")
				return True
			if self.CheckUnity(reference.projectReferences):
				return True
	def SaveProcess(self):
		if Program.Settings.verboseLogging :
			standaloneLabel = self.standalone and "standalone" or ""
			Info(Utility.ShowIndex() + "[" + Color.Wrap("gray",self.namespaceName) + "] : Saving " + standaloneLabel + ".csproj")
		folderName = self.standalone and "@"+self.namespaceName or self.namespaceName
		self.folder = folderPath = Program.Settings.releasePath+folderName+"/"
		sourcePath = folderPath+"Source/"
		binaryPath = folderPath+"Binary/"
		self.path = folderPath + self.namespaceName + ".csproj"
		Utility.CreateFolder(folderPath)
		Utility.CreateFolder(sourcePath)
		if len(self.libraryReferences) > 0:
			libraryPath = folderPath+"Library/"
			Utility.CreateFolder(libraryPath)
		isUnity = ".Unity." in self.namespaceName or self.CheckUnity(self.projectReferences)
		isEditor = ".Editor." in self.namespaceName
		constants = Program.Settings.userFlags
		output = CsProj.template
		output = output.replace("$uuid",self.uuid)
		output = output.replace("$assemblyName",self.namespaceName)
		output = output.replace("$frameworkVersion",Program.Settings.frameworkVersion)
		output = output.replace("$outputPath",binaryPath.replace(folderPath,"./"))
		csProjSources = ""
		csProjProjects = ""
		csProjLibraries = ""
		csProjItemGroups = ""
		csProjPropertyGroups = ""
		if isUnity:
			constants += CsProj.unityVersionFlags + CsProj.unityFlags
			unityProjectType = isEditor and "Editor:5" or "Game:1"
			unityPropertyGroup = CsProj.unityPropertyGroup.replace("$unityVersion",Program.Settings.unityVersion)
			unityPropertyGroup = unityPropertyGroup.replace("$unityProjectType",unityProjectType)
			csProjPropertyGroups += unityPropertyGroup
			constants += isEditor and CsProj.unityEditorFlags or CsProj.unityBuildFlags
		self.defines = constants
		output = output.replace("$constants",constants)
		if not self.standalone:
			self.alreadySaved = True
			for project in self.projectReferences:
				project.Save()
		if self.standalone:
			self.BuildReferences("allSourceReferences","sourceReferences")
			csProjSources += self.CopySource(self.allSourceReferences,sourcePath)
		else:
			self.BuildReferences("allProjectReferences","projectReferences")
			csProjSources += self.CopySource(self.sourceReferences,sourcePath)
			for project in self.projectReferences:
				projectPath = "../" + project.namespaceName + "/" + project.namespaceName + ".csproj"
				reference = CsProj.projectReference.replace("$projectPath",projectPath)
				reference = reference.replace("$projectUUID",project.uuid)
				reference = reference.replace("$projectName",project.namespaceName)
				csProjProjects += reference
		self.BuildReferences("allLibraryReferences","libraryReferences")
		for library in self.allLibraryReferences:
			fileName = library.path.split("/")[-1]
			Utility.Copy(library.path,libraryPath+fileName,Program.Settings.symLinkLibraries)
			reference = CsProj.libraryReference.replace("$libraryPath","./Library/"+fileName)
			reference = reference.replace("$libraryName",library.name)
			csProjLibraries += reference
		if csProjLibraries : csProjItemGroups += "\n\t<ItemGroup>\n"+csProjLibraries+"\t</ItemGroup>"
		if csProjProjects : csProjItemGroups += "\n\t<ItemGroup>\n"+csProjProjects+"\t</ItemGroup>"
		if csProjSources  : csProjItemGroups += "\n\t<ItemGroup>\n"+csProjSources+"\t</ItemGroup>"
		output = output.replace("$additionalPropertyGroups",csProjPropertyGroups)
		output = output.replace("$additionalItemGroups",csProjItemGroups)
		Utility.SaveFile(self.path,output)
	def CopySource(self,targets,path):
		csProjSources = ""
		for source in targets:
			sourcePath = source.path
			fileName = sourcePath.split("/")[-1]
			folder = Utility.GetFolder(path)
			destination = path+fileName
			if Program.Settings.preserveStructure:
				destination = Utility.CopyPath(sourcePath,path,Program.Settings.symLinkSource)
			else:
				Utility.Copy(sourcePath,destination,Program.Settings.symLinkSource)
			destination = destination.replace(folder,"./")
			csProjSources += CsProj.sourceReference.replace("$sourcePath",destination)
		return csProjSources
	#=========================
	# COMPILE
	#=========================
	def Compile(self):
		if Program.Settings.multiThreading:
			Program.Settings.lock.acquire()
			if not self.compileThread:
				self.compileThread = threading.Thread(target=self.CompileProcess)
				self.compileThread.start()
			Program.Settings.lock.release()
		elif not self.alreadyCompiled:
			self.CompileProcess()
	def CompileProcess(self):
		binaryPath = self.folder+"Binary/"
		Utility.CreateFolder(binaryPath)
		self.dllPath = binaryPath + self.namespaceName + ".dll"
		self.alreadyCompiled = True
		if Program.Settings.verboseLogging:
			standaloneTerm = self.standalone and "Standalone " or ""
			Info(Utility.ShowIndex() + "["+Color.Wrap("gray",self.namespaceName)+"] : Compiling " + standaloneTerm + ".dll")
		defines = self.defines and Compile.defines.replace("$defines",self.defines) or ""
		command = Compile.template.replace("$outputName",self.dllPath.replace(".dll",""))
		command = command.replace("$defines",defines)
		sources = ""
		sourceReferences = self.standalone and self.allSourceReferences or self.sourceReferences
		libraryReferences = self.standalone and self.allLibraryReferences or self.libraryReferences
		for source in sourceReferences:
			sourcePath = source.path
			sources += Compile.sourceReference.replace("$path",sourcePath.replace("/","\\"))
		command = command.replace("$sourceFiles",sources.strip())
		references = [x.path for x in libraryReferences]
		for project in self.projectReferences:
			project.Compile()
		Utility.ThreadWait(self.projectReferences,"compileThread")
		if not self.standalone:
			for project in self.projectReferences:
				references.append(project.dllPath)
		dllReferences = ""
		for reference in references:
			while not os.access(reference,os.R_OK):
				time.sleep(0.05)
			dllReferences += Compile.dllReference.replace("$path",reference.replace("/","\\"))
		command = command.replace("$dllReferences",dllReferences.strip()).strip()
		subprocess.call(command)
	#=========================
	# ZIPPING
	#=========================
	def Zip(self):
		if Program.Settings.multiThreading:
			Program.Settings.lock.acquire()
			if not self.zipThread:
				self.zipThread = threading.Thread(target=self.ZipProcess)
				self.zipThread.start()
			Program.Settings.lock.release()
		elif not self.alreadyZipped:
			self.ZipProcess()
	def ZipProcess(self):
		zip = self.GetZip(self.namespaceName,self.standalone)
		zipPath = Program.Settings.releasePath+zip.filename
		if Program.Settings.verboseLogging : Info(Utility.ShowIndex() + "["+Color.Wrap("gray",self.namespaceName)+"]" + " : Creating " + zip.filename)
		Program.Settings.keep.append(zipPath)
		references = self.standalone and [self] or [self]+self.allProjectReferences
		for project in references:
			for filePath in Utility.GetFiles(project.folder,[]):
				if Program.Settings.zipIgnoreLibrary and "/Library/" in filePath : continue
				archivePath = Utility.Repath(filePath.replace("../",""),False)
				zip.write(filePath,arcname=archivePath)
		if not self.standalone:
			solutionPath = Utility.Repath(Program.Settings.releasePath+self.namespaceName+".sln")
			zip.write(solutionPath)
		zip.close()
		Program.Settings.openPath = "/select,"+Utility.GetShellPath(zipPath)
	def GetZip(self,name,standalone):
		name = "["+name+"]"
		name += Program.Settings.generatedInfo + "."
		name += standalone and "Standalone" or "Shared"
		name += ".zip"
		return zipfile.ZipFile(name,"w",zipfile.ZIP_DEFLATED)
	#=========================
	# ASMDEF
	#=========================
	def SaveAsmdef(self):
		unityPath = self.folder+"Unity/"
		Utility.CreateFolder(unityPath)
		references = ""
		if len(self.userReferencePaths) > 0:
			references = '\n\t\t"'+'",\n\t\t"'.join(self.userReferencePaths)+'\n\t'
		asm = Asmdef.template.replace("$asmdef",self.namespaceName)
		asm = asm.replace("$references",references)
		if ".Editor." in self.namespaceName:
			asm = asm.replace("$editorOnly",'"'+"Editor"+'"')
		asm = asm.replace("$editorOnly","")
		asmPath = self.folder+"Unity/"+self.namespaceName+".asmdef"
		if Program.Settings.verboseLogging : Info(Utility.ShowIndex() + "["+Color.Wrap("gray",self.namespaceName)+"]" + " : Saving asmdef.")
		Utility.SaveFile(asmPath,asm)
	#=========================
	# PACKAGE
	#=========================
	def SavePackage(self):
		unityPath = self.folder+"Unity/"
		Utility.CreateFolder(unityPath)
		dependencies = ""
		version = Program.Settings.unityVersionClean+".r"+Program.Settings.svnRevision
		if len(self.userReferencePaths) > 0:
			dependencies = "\n"
			for path in self.userReferencePaths:
				dependency = Package.dependency.replace("$packageName",path)
				dependency = dependency.replace("$packageVersion",version)
				dependencies += dependency
			dependencies += "\t"
		package = Package.template.replace("$description","This is a module for "+self.namespaceName+".")
		package = package.replace("$displayName",self.namespaceName.replace("."," ").title())
		package = package.replace("$name",self.namespaceName)
		package = package.replace("$unityVersion",Program.Settings.unityVersionClean)
		package = package.replace("$category",Program.Settings.packageCategory)
		package = package.replace("$version",version)
		package = package.replace("$dependencies",dependencies)
		packagePath = self.folder+"Unity/"+self.namespaceName+".json"
		if Program.Settings.verboseLogging : Info(Utility.ShowIndex() + "["+Color.Wrap("gray",self.namespaceName)+"]" + " : Saving package.")
		Utility.SaveFile(packagePath,package)