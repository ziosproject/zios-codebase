import os,sys
initial = os.getcwd().replace("\\","/") + "/../"
os.system("cls")
class Parser:
	def __init__(self):
		self.types = [".cs",".sln",".csproj"]
		self.skip = [".meta",".cache"]
		self.newline = "\n"
	def Match(self,name,terms):
		for term in terms:
			if term in name : return term
		return None
	def Scan(self,path):
		for item in os.listdir(path):
			currentPath = path.strip("/")+"/"+item
			if os.path.isdir(currentPath) and not item.startswith("."):
				self.Scan(currentPath)
			if self.Match(currentPath,self.types) and not self.Match(currentPath,self.skip):
				print(" " + '\033[104m\033[97m'+"Processing :"+'\033[0m' + " " + currentPath.replace(initial,""))
				output = ""
				try:
					file = open(currentPath,'r')
					lines = file.readlines()
					file.close()
					for line in lines :
						if line.strip() == "" : continue
						output += line.rstrip() + self.newline
					file = open(currentPath,'w')
					file.write(output.rstrip())
					file.close()
				except:
					print(" " + '\033[101m\033[97m'+"     Error :"+'\033[0m' + " " + currentPath)
					os.system("pause")
Parser().Scan(initial)
os.system("pause")
