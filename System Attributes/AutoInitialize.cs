using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
namespace Zios.SystemAttributes{
	using Zios.Log;
	using Zios.Reflection;
	using Zios.Serializer.Attributes;
	public class AutoInitialize : Attribute{
		public static List<Type> ready = new List<Type>();
		public int priority = 1000;
		public AutoInitialize(){}
		public AutoInitialize(int priority){this.priority = priority;}
		public static void Start(){
			Reflection.Clear();
			var filter = MemberFilter.FindWith<AutoInitialize>();
			var types = Reflection.GetTypes(filter).OrderBy(x=>x.GetAttribute<AutoInitialize>().priority);
			foreach(var type in types){
				if(AutoInitializeSettings.debug && !AutoInitialize.ready.Contains(type)){
					AutoInitialize.ready.Add(type);
					var priority = type.GetAttribute<AutoInitialize>().priority;
					Log.Show("[AutoInitialize] Calling static class constructor -- " + type.FullName + " -- " + priority);
				}
				RuntimeHelpers.RunClassConstructor(type.TypeHandle);
			}
		}
	}
	[Store]
	public static class AutoInitializeSettings{
		public static bool debug;
	}
}